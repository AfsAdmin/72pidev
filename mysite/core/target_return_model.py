import pyodbc
import pandas as pd
import math
from scipy.optimize import LinearConstraint
from scipy.optimize import minimize
import numpy as np
from .models import UniversePricesMain, UsUniversePricesMain


def Optimal_Portfolio_Volatility(Portfolio,Target_Return,current_country,returns):
    try:        
        Portfolio["Average_Return"]=""
        Portfolio["Annualized_Return"]=""
        Portfolio["Standard_Deviation"]=""
        Portfolio["Count"]=""        
        for i in range(0,len(Portfolio)):
            Sub_returns=returns[returns["Factset_Ticker"]==Portfolio.loc[i,"Factset_Ticker"]]
            Sub_returns.reset_index(drop=True,inplace=True)
            Portfolio.loc[i,"Count"]=len(Sub_returns['Return'])
            Portfolio.loc[i,"Average_Return"]=Sub_returns['Return'].mean()
            Portfolio.loc[i,"Annualized_Return"]=Portfolio.loc[i,"Average_Return"]*252
            Portfolio.loc[i,"Standard_Deviation"]=Sub_returns['Return'].std()*math.sqrt(252)
            
        returns_trans=returns.pivot(index='Date',columns='Factset_Ticker')["Return"]
        covar_matrix=returns_trans.cov()*252
        covar_matrix=np.asarray(covar_matrix)
        
        ##Minimization Function
        def calculate_stdev(x,covar_matrix):
            Weight_Matrix=np.asarray(x)/100                 
            res=np.matmul(Weight_Matrix.T,covar_matrix)
            res2=Weight_Matrix*res
            stdev=math.sqrt(sum(res2)) 
            return stdev    
        
        ##Constraints
        def constraint(x):    
            expected_return=sum((np.asarray(x)/100)*list(Portfolio["Annualized_Return"]))
            obj=expected_return-Target_Return
            return obj
                
        cons1 = ({'type': 'eq', 'fun': lambda x:  np.sum(x[0:len(x)])-100})
        cons2=({'type': 'ineq', 'fun': constraint})
        
        cons= [cons1,cons2]
        
        ##Bounds 
        bnds=[]
        for i in range(0,len(Portfolio)):
            p=(1,20)
            bnds.append(tuple(p))
        bnds=tuple(bnds)
        
        #Weights   
        p=100/len(Portfolio)
        weights=[]
        for i in range(0,len(Portfolio)):
            weights.append(p)        
        opt= minimize(calculate_stdev, weights,args=covar_matrix, method='SLSQP',constraints=cons,bounds=bnds)
        new_weights=np.asarray(opt.x)/100
        expected_return=sum(new_weights*list(Portfolio["Annualized_Return"]))
        actual_weights=list(Portfolio["Weight"])
        actual_return=sum(np.asarray(actual_weights)*list(Portfolio["Annualized_Return"]))
        output_1=pd.DataFrame({"Actual_Return":[actual_return],"Expected Return":[expected_return],
                'Standard Deviation':[opt.fun]})            
        output_2=pd.DataFrame({"Weight":list(new_weights)})
        output_2["Company"]=Portfolio["Company"]
        output_2["Factset_Ticker"]=Portfolio["Factset_Ticker"]    
        output_2=output_2[["Company","Factset_Ticker","Weight"]]
        return output_1,output_2

    except Exception as e:
        return pd.DataFrame(),pd.DataFrame()











