# -*- coding: utf-8 -*-
"""
Created on Thu Feb 20 16:02:39 2020

@author: Harshitha.b
"""
import pandas as pd
import pyodbc
from .models import CustomerPortfolioDetails, UniversePricesMain, UsUniversePricesMain, SecurityMaster, UsSecurityMaster
from django.db.models import Max
def exposureDashboard(user,current_country,portfolio_data,Prices_data,Gics_Data):
    # try:
    prices_data = Prices_data.copy()  
    #stock wise exposure
    prices_data = pd.merge(portfolio_data,prices_data,on=['Company','Factset_Ticker'],how='inner')
    prices_data['Exposure']=prices_data['Quantity']*prices_data['Price']
    prices_data['Exposure%']=prices_data['Exposure']/(prices_data['Exposure'].sum())
    stock_wise_exposure = prices_data[['Company','Factset_Ticker','Exposure','Exposure%']]
    stock_wise_exposure.sort_values(by=['Exposure%'],ascending=False,inplace=True)
    stock_wise_exposure.reset_index(drop=True,inplace=True)
    stock_wise_exposure['Exposure%'] = stock_wise_exposure['Exposure%']*100    
    Gics_Data = Gics_Data.copy()
    Gics_Data.columns=['Company','Factset_Ticker','Security_Code','GICS','Market_Cap_Category']
    gics_data = Gics_Data.copy()
    market_cap = gics_data[['Company','Factset_Ticker','Market_Cap_Category']]
    gics_data = gics_data[['Company','Factset_Ticker','GICS']]
    gics_data =pd.merge(prices_data,gics_data,on=['Company','Factset_Ticker'],how='inner')
    gics_wise_exposure=gics_data.groupby(['GICS'])["Exposure"].agg('sum').reset_index()   
    gics_wise_exposure.columns=['GICS','Exposure']
    gics_wise_exposure['Exposure%']=gics_wise_exposure['Exposure']/(gics_wise_exposure['Exposure'].sum())
    gics_wise_exposure.sort_values(by=['Exposure%'],ascending=False,inplace=True)
    gics_wise_exposure.reset_index(drop=True,inplace=True)
    gics_wise_exposure['Exposure%'] = gics_wise_exposure['Exposure%']*100
    market_cap_wise = pd.merge(prices_data,market_cap,on=['Company','Factset_Ticker'],how='inner')
    market_cap_wise['Exposure'] = market_cap_wise['Quantity']*market_cap_wise['Price']
    market_cap_wise_exposure = market_cap_wise.groupby('Market_Cap_Category')['Exposure'].agg('sum').reset_index()
    market_cap_wise_exposure.columns =['Market Cap Category','Exposure']
    market_cap_wise_exposure['Exposure%'] = market_cap_wise_exposure['Exposure']/(market_cap_wise_exposure['Exposure'].sum())
    market_cap_wise_exposure.sort_values(by='Exposure%',ascending =False,inplace=True)
    market_cap_wise_exposure.reset_index(drop=True,inplace=True)
    market_cap_wise_exposure['Exposure%'] = market_cap_wise_exposure['Exposure%']*100    
    sec_code_data = Gics_Data[['Company','Factset_Ticker','Security_Code']]
    stock_wise_exposure = pd.merge(stock_wise_exposure,sec_code_data,on=['Company','Factset_Ticker'],how='inner')
    stock_wise_exposure['Company'] =  stock_wise_exposure['Company'] + ' (' +stock_wise_exposure['Security_Code'] + ")"
    top3_stocks_by_exposure = stock_wise_exposure.head(3)    
    del top3_stocks_by_exposure['Security_Code']
    del top3_stocks_by_exposure['Factset_Ticker']
    top_sector_by_exposure = gics_wise_exposure.loc[0:0]  
    return top3_stocks_by_exposure,stock_wise_exposure,gics_wise_exposure,top_sector_by_exposure,market_cap_wise_exposure
    # except Exception as e:
    #     top3_stocks_by_exposure,stock_wise_exposure=pd.DataFrame(),pd.DataFrame(),pd.DataFrame()
    #     gics_wise_exposure,top_sector_by_exposure,market_cap_wise_exposure=pd.DataFrame(),pd.DataFrame(),pd.DataFrame()
    #     return top3_stocks_by_exposure,stock_wise_exposure,gics_wise_exposure,top_sector_by_exposure,market_cap_wise_exposure

