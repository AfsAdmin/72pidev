from mysite.core.models import *
from mysite.core.Appconfigproperties import *
import pandas as pd
import pyodbc
from django.utils import timezone
from django.db.models import Max, Min, Sum, Aggregate
from django.db import connection

# start code here
def portfolioCheck(username, country):
    try:
        ccount = CustomerPortfolioDetails.objects.filter(customer_name=username,model_portfolio='yes').count()
        if ccount == 0:
            modelportfolios = ModelPortfolioDetails.objects.filter(add_on='yes').values('company_name','quantity','sector_name','model_portfolio_name','country').order_by('company_name')
            tradecompanies =', '.join('"{0}"'.format(w) for w in [portfolio['company_name'].replace("'","''") for portfolio in modelportfolios] ).replace('"',"'")
            modelPortfolioDF = pd.DataFrame(list(modelportfolios))
            modelPortfolioDF.columns = ['Company','Quantity','Sector','Portfolio','Country']
            #Default query for india portfolio
            query = '''SELECT B.COMPANY, A.DATE, A.PRICE FROM UNIVERSE_PRICES_MAIN A JOIN (SELECT COMPANY, MIN(DATE) DATE FROM UNIVERSE_PRICES_MAIN WHERE DATE >= '2019-01-01' AND
                           COMPANY IN ('''+tradecompanies+''') GROUP BY FACTSET_TICKER, COMPANY) B ON A.COMPANY = B.COMPANY AND A.DATE = B.DATE '''
            if country == 'US':  #Change the query based on country choosen
                query = '''SELECT B.COMPANY, A.DATE, A.PRICE FROM US_UNIVERSE_PRICES_MAIN A JOIN (SELECT COMPANY, MIN(DATE) DATE FROM US_UNIVERSE_PRICES_MAIN WHERE DATE >= '2019-01-01' AND
                           COMPANY IN ('''+tradecompanies+''') GROUP BY FACTSET_TICKER, COMPANY) B ON A.COMPANY = B.COMPANY AND A.DATE = B.DATE '''
            cursor = connection.cursor() #Create cursor from the DB connection
            try:
                resultSet = cursor.execute(query).fetchall() #Excecute the query and fetch all the records from the DB
            except Exception as e:
                cursor.close() #Close the connection, if any exception occurs
            results = [list(item) for item in resultSet] #Convert pyodbc row to list
            pricesDF = pd.DataFrame(results, columns=['Company','Date','Price']) #Convert resultset to dataframe
            resultDF = pd.merge(modelPortfolioDF,pricesDF,on="Company",how="left")
            # for i in range(0,len(resultDF)):
            #     if not CustomerPortfolioDetails.objects.filter(customer_name=username,portfolio_name=resultDF['Portfolio'][i],company_name=resultDF['Company'][i]).exists():
            #         i=i+1
            #         CustomerPortfolioDetails(customer_name=username,portfolio_name=resultDF['Portfolio'][i],
            #         company_name=resultDF['Company'][i],sector_name=resultDF['Sector'][i],price=resultDF['Price'][i],
            #         quantity=resultDF['Quantity'][i],model_portfolio='yes', portfolio_type='General',country=resultDF['Country'][i]).save()
            #     else:
            #         pass
            #     if not CustomerPortfolioLog.objects.filter(customer_name=username,portfolio_name=resultDF['Portfolio'][i]).exists():
            #         CustomerPortfolioLog(customer_name=username,portfolio_name=resultDF['Portfolio'][i],
            #         last_action='portfolio_created',created_at=timezone.now(),status='active').save()
            #     else:
            #         pass
            # print('Model portfolios created for you!!')            
            
            stocks = CustomerPortfolioDetails.objects.filter(customer_name=username).values().order_by('company_name')
        else:
            print('Model portfolios already created for you!!')
    except Exception as e:
        print(e)
    portfolios = CustomerPortfolioDetails.objects.filter(customer_name=username).order_by('-created_at').first()
    return portfolios.portfolio_name

def missingPortfolioCheck(username, country):
    try:
        currentPortfolioName = 'Our India Portfolio'  #Do not consider the 72pi portfolio as model portfolio for now
        if country == 'US':
            currentPortfolioName = 'Our US Portfolio'
        modelPortfolioCheck = ModelPortfolioDetails.objects.filter(add_on='yes', country=country).values('model_portfolio_name').distinct()
        modelPortfolioCheck_C = CustomerPortfolioDetails.objects.filter(customer_name=username,model_portfolio='yes',country=country).values('portfolio_name').distinct()
        modelPortfolioCheckList = [item['model_portfolio_name'] for item in modelPortfolioCheck]
        modelPortfolioCheck_CList = [item['portfolio_name'] for item in modelPortfolioCheck_C if item['portfolio_name'] != currentPortfolioName]
        if len(modelPortfolioCheckList) != len(modelPortfolioCheck_CList):
            try:
                missingPortfolioList = set(list(set(modelPortfolioCheckList)-set(modelPortfolioCheck_CList)) + list(set(modelPortfolioCheck_CList)-set(modelPortfolioCheckList)))
                modelportfolios = ModelPortfolioDetails.objects.filter(model_portfolio_name__in=missingPortfolioList).values('company_name','quantity','sector_name','model_portfolio_name','country').order_by('company_name')
                tradecompanies =', '.join('"{0}"'.format(w) for w in [portfolio['company_name'].replace("'","''") for portfolio in modelportfolios] ).replace('"',"'")
                modelPortfolioDF = pd.DataFrame(list(modelportfolios))
                modelPortfolioDF.columns = ['Company','Quantity','Sector','Portfolio','Country']
                #Default query for india portfolio
                query = '''SELECT B.COMPANY, A.DATE, A.PRICE FROM UNIVERSE_PRICES_MAIN A JOIN (SELECT COMPANY, MIN(DATE) DATE FROM UNIVERSE_PRICES_MAIN WHERE DATE >= '2019-01-01' AND
                            COMPANY IN ('''+tradecompanies+''') GROUP BY FACTSET_TICKER, COMPANY) B ON A.COMPANY = B.COMPANY AND A.DATE = B.DATE '''
                if country == 'US':  #Change the query based on country choosen
                    query = '''SELECT B.COMPANY, A.DATE, A.PRICE FROM US_UNIVERSE_PRICES_MAIN A JOIN (SELECT COMPANY, MIN(DATE) DATE FROM US_UNIVERSE_PRICES_MAIN WHERE DATE >= '2019-01-01' AND
                            COMPANY IN ('''+tradecompanies+''') GROUP BY FACTSET_TICKER, COMPANY) B ON A.COMPANY = B.COMPANY AND A.DATE = B.DATE '''
                cursor = connection.cursor() #Create cursor from the DB connection
                try:
                    resultSet = cursor.execute(query).fetchall() #Excecute the query and fetch all the records from the DB
                except Exception as e:
                    cursor.close() #Close the connection, if any exception occurs
                results = [list(item) for item in resultSet] #Convert pyodbc row to list
                pricesDF = pd.DataFrame(results, columns=['Company','Date','Price']) #Convert resultset to dataframe
                resultDF = pd.merge(modelPortfolioDF,pricesDF,on="Company",how="left")
                # for i in range(0,len(resultDF)):
                #     if not CustomerPortfolioDetails.objects.filter(customer_name=username,portfolio_name=resultDF['Portfolio'][i],company_name=resultDF['Company'][i]).exists():
                #         CustomerPortfolioDetails(customer_name=username,portfolio_name=resultDF['Portfolio'][i],
                #         company_name=resultDF['Company'][i],sector_name=resultDF['Sector'][i],price=resultDF['Price'][i],
                #         quantity=int(resultDF['Quantity'][i]),model_portfolio='yes', portfolio_type='General',country=resultDF['Country'][i]).save()
                #     if not CustomerPortfolioLog.objects.filter(customer_name=username,portfolio_name=resultDF['Portfolio'][i]).exists():
                #         CustomerPortfolioLog(customer_name=username,portfolio_name=resultDF['Portfolio'][i],
                #         last_action='portfolio_created',created_at=timezone.now(),status='active').save()
                #     else:
                #         pass
                # print('Missed Model portfolios created for you!!')
                stocks = CustomerPortfolioDetails.objects.filter(customer_name=username).values().order_by('company_name')
            except Exception as e:
                print(e)
        else:
            print('Your Missing Model Portfolios updated for you!!')
    except Exception as e:
        print(e)
    portfolios = CustomerPortfolioDetails.objects.filter(customer_name=username).order_by('-created_at').first()
    # return portfolios.portfolio_name


def ourPortfolioAdding(username, country):
    try:
        currentPortfolioName = 'Our India Portfolio'  #Default name for 72pi India portfolio
        transactionModel = PositionsInput #Default model for transactions
        securityMasterModel = SecurityMaster
        if country == 'US': #country check
            currentPortfolioName = 'Our US Portfolio' #Portfolio name change for 72pi USA
            transactionModel = UsTransactions #Model change for US
            securityMasterModel = UsSecurityMaster
        #Count of current 72pi active portfolio
        currentPortfolio_C = transactionModel.objects.filter(customer='suresh', action__in=['BUY', 'Buy','buy']).exclude(secdescription__in=
                             transactionModel.objects.filter(customer='suresh', action__in=['SELL','Sell','sell']).values_list('secdescription')).distinct().count()
        #Count of 72pi portfolio in customer table
        customerPortfolio_C = CustomerPortfolioDetails.objects.filter(customer_name=username, model_portfolio='yes', portfolio_name=currentPortfolioName, country=country).count()
        if currentPortfolio_C != customerPortfolio_C:  #Checking both are same are not, if not same
            #Read current 72pi portfolio from transactions
            currentPortfolios = transactionModel.objects.filter(customer='suresh', action__in=['BUY', 'Buy','buy']).exclude(secdescription__in=
                                transactionModel.objects.filter(customer='suresh', action__in=['SELL','Sell','sell']).values_list('secdescription')).values('secdescription','action','quantity', 'tradedate').distinct()
            portfolioDF = pd.DataFrame(currentPortfolios) #Convert this to dataframe
            tradeStocks = list(portfolioDF['secdescription'].unique()) #Fetch list of stocks from the transactions
            tradecompanies = ', '.join('"{0}"'.format(w) for w in [tradeStocks[i].replace("'","''") for i in range(0,len(tradeStocks))]).replace('"',"'")  #Convert this to query string
            #Default query for india portfolio
            query = '''SELECT B.COMPANY, A.DATE, A.PRICE FROM UNIVERSE_PRICES_MAIN A JOIN (SELECT COMPANY, MIN(DATE) DATE FROM UNIVERSE_PRICES_MAIN WHERE DATE >= '2019-01-01' AND
                           COMPANY IN ('''+tradecompanies+''') GROUP BY FACTSET_TICKER, COMPANY) B ON A.COMPANY = B.COMPANY AND A.DATE = B.DATE '''
            if country == 'US':  #Change the query based on country choosen
                query = '''SELECT B.COMPANY, A.DATE, A.PRICE FROM US_UNIVERSE_PRICES_MAIN A JOIN (SELECT COMPANY, MIN(DATE) DATE FROM US_UNIVERSE_PRICES_MAIN WHERE DATE >= '2019-01-01' AND
                           COMPANY IN ('''+tradecompanies+''') GROUP BY FACTSET_TICKER, COMPANY) B ON A.COMPANY = B.COMPANY AND A.DATE = B.DATE '''
            cursor = connection.cursor() #Create cursor from the DB connection
            try:
                resultSet = cursor.execute(query).fetchall() #Excecute the query and fetch all the records from the DB
            except Exception as e:
                cursor.close() #Close the connection, if any exception occurs
            results = [list(item) for item in resultSet] #Convert pyodbc row to list
            pricesDF = pd.DataFrame(results, columns=['Company','Date','Price']) #Convert resultset to dataframe
            gics = securityMasterModel.objects.filter(fs_name__in=tradeStocks).values('fs_name','gics') #Fetch the GICS from the DB
            gicsDF = pd.DataFrame(list(gics)) #Connverting gics to dataframe
            resultDF = pd.merge(pricesDF, gicsDF, how='inner', left_on='Company',right_on='fs_name') #Dataset join
            activeDF = pd.merge(portfolioDF, resultDF, how='inner', left_on='secdescription',right_on='Company')
            # if CustomerPortfolioDetails.objects.filter(customer_name=username, portfolio_name=currentPortfolioName).exists(): #Checking fod the existance
            #     stocksDF = pd.DataFrame(list(CustomerPortfolioDetails.objects.filter(customer_name=username, portfolio_name=currentPortfolioName,country=country).values()))
            #     exitCompanies = list(stocksDF['company_name'].unique())
            #     activeDF = activeDF[~activeDF['secdescription'].isin(exitCompanies)]  #To Remove/Add of missing/excess stocks from customer portfolio details
            #     if currentPortfolio_C < customerPortfolio_C: #Deleting the extra stock, Which means sold stocks
            #         for ind in activeDF.index:
            #             CustomerPortfolioDetails.objects.filter(customer_name=username, portfolio_name=currentPortfolioName, company_name=activeDF['fs_name'][ind]).delete()
            #         CustomerPortfolioLog.objects.filter(customer_name=username,portfolio_name=currentPortfolioName).update(last_action='stock_deleted',last_modified_at=timezone.now(),status='active').save()
            #     else:
            #         for ind in activeDF.index: #Saving the missing stocks, Which means buy stocks
            #             CustomerPortfolioDetails(customer_name=username, portfolio_name=currentPortfolioName,company_name=activeDF['fs_name'][ind],sector_name=activeDF['gics'][ind],
            #             price=activeDF['Price'][ind],quantity=int(activeDF['quantity'][ind]),portfolio_type='General',model_portfolio='yes',country=country).save()
            #         CustomerPortfolioLog.objects.filter(customer_name=username,portfolio_name=currentPortfolioName).update(last_action='stock_added',last_modified_at=timezone.now(),status='active').save()
            #     print("Done checking the 72pi Portfolio")
            # else:  # Saving the portfolio first time
            #     for ind in activeDF.index:
            #         if not CustomerPortfolioDetails.objects.filter(customer_name=username, portfolio_name=currentPortfolioName, company_name=activeDF['fs_name'][ind]).exists():
            #             CustomerPortfolioDetails(customer_name=username, portfolio_name=currentPortfolioName,company_name=activeDF['fs_name'][ind],sector_name=activeDF['gics'][ind],
            #             price=activeDF['Price'][ind],quantity=int(activeDF['quantity'][ind]),portfolio_type='General',model_portfolio='yes',country=country).save()
            #         else:
            #             print("something wents wrong")
            #     CustomerPortfolioLog(customer_name=username,portfolio_name=currentPortfolioName,last_action='portfolio_created',created_at=timezone.now(),status='active').save()
            #     print("Hey, 72pi Portfolio added for you!")
        else:
            print("No changes in 72pi portfolio")
    except Exception as e:
        print(e)
