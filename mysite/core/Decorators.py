
from django.http import HttpResponseRedirect
from django.core.exceptions import PermissionDenied
from django.urls import reverse
from django.shortcuts import render
from django.shortcuts import redirect
from mysite.core.models import CustomerSubscription


def SubscriptionValidation(view):
    """Validate the current user subscription."""
    def wrap(request, *args, **kwargs):
        currentLoginUser = request.user
        if CustomerSubscription.objects.filter(customer_name=currentLoginUser).exists():
            subscription = CustomerSubscription.objects.filter(customer_name=currentLoginUser, subscription_status='ACTIVE').order_by('subscription_starts').first()
            if subscription:
                typeOfSubscription = subscription.subscription_type
                if typeOfSubscription == 'PREMIUM' or typeOfSubscription == 'FREE':
                    expiryOfSubscription = subscription.subscription_ends
                    from datetime import date
                    currentDay = date.today()
                    if currentDay <= expiryOfSubscription:
                        return view(request, *args, **kwargs)
                    else:
                        subscription.subscription_status='EXPIRED'
                        subscription.save()
                        upcomingsubscription = CustomerSubscription.objects.filter(customer_name=currentLoginUser, subscription_status='UPCOMING').order_by('subscription_starts').first()
                        if upcomingsubscription:
                            typeOfSubscription = upcomingsubscription.subscription_type
                            upcomingsubscription.subscription_status='ACTIVE'
                            upcomingsubscription.save()
                            if typeOfSubscription == 'PREMIUM' or typeOfSubscription == 'FREE':
                                expiryOfSubscription = upcomingsubscription.subscription_ends
                                from datetime import date
                                currentDay = date.today()
                                if currentDay <= expiryOfSubscription:
                                    return view(request, *args, **kwargs)
                                else:
                                    subscription.subscription_status='EXPIRED'
                                    subscription.save()
                                    return render(request,'SubscriptionExpired.html',{'expirytime':expiryOfSubscription})
                        else:
                            return render(request,'SubscriptionExpired.html')
                else:
                    return render(request,'SubscriptionAccessDenied.html')
            else:
                return render(request,'SubscriptionExpired.html')
        else:
            return render(request,'NoSubscription.html')
    return wrap
            
