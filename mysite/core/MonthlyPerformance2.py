import pandas as pd
from scipy import stats
import statistics
import math

#portfolio verus benchmark metrics
def additional_calcs_monthly_returns(monthly_data,market_index):
    try:
        index_value=monthly_data.columns.tolist().index(str(market_index))
        data=monthly_data[['Year','Month','PNL']]
        data.columns=['Year','Month','Fund']
        data['Market_index']=monthly_data.iloc[:,index_value:index_value+1]
        data['Excess']=data['Fund']-data['Market_index']
        positive_month_portfolio_number=len(list(filter(lambda x:x>=0,data['Fund'].tolist())))
        negative_month_portfolio_number=len(list(filter(lambda x:x<0,data['Fund'].tolist())))
        out_performance=len(list(filter(lambda x:x>=0,data['Excess'].tolist())))/len(data)
        under_performance=len(list(filter(lambda x:x<0,data['Excess'].tolist())))/len(data)
        best_month=data.sort_values('Fund',ascending=True).tail(1)
        worst_month=data.sort_values('Fund',ascending=True).head(1)
        best_month_value=str(best_month.iat[0,1])+"-"+str(best_month.iat[0,0])+" ("+str(round(best_month.iat[0,2]*100,2))+"%)"
        worst_month_value=str(worst_month.iat[0,1])+"-"+str(worst_month.iat[0,0])+" ("+str(round(worst_month.iat[0,2]*100,2))+"%)"
        avg_excess_vs_benchmark=data['Excess'].mean()
        avg_monthly_return=data['Fund'].mean()
        beta= stats.linregress(data["Market_index"].values,data["Fund"].values)[0:1][0]#beta calculation
        fund_returns=data[["Fund"]]
        fund_returns.columns=['Returns']
        index_returns=data[["Market_index"]]
        index_returns.columns=['Returns']
        correlation=float(fund_returns.corrwith(index_returns, axis =0))#correlation calculation
        tracking_error=statistics.stdev(data['Excess'].tolist())*(math.sqrt(12))
        dict1={'Metrics':['# of Positive Months of Portfolio','# of Negative Months of Portfolio','Outperformance vs Benchmark','Underperformance vs Benchmark',
                        'Best Month','Worst Month', 'Avg Excess Return vs Benchmark','Avg Monthly Return','Beta (Monthly Return)','Correlation','Tracking Error'],
            'values':[positive_month_portfolio_number,negative_month_portfolio_number,out_performance,under_performance,best_month_value,worst_month_value,avg_excess_vs_benchmark,
            avg_monthly_return,beta,correlation,tracking_error]}
        output_data=pd.DataFrame(dict1,columns=['Metrics','values'])
        return output_data
    except Exception as e:
        return pd.DataFrame() 






    

