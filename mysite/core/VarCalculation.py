
import pandas as pd
import numpy as np
import math
import pyodbc
import datetime
from mysite.core.models import *
from mysite.core.Appconfigproperties import *


def VarCalculationMain(Trade_Dt,whereCondition,user_name,portfolio_name,country,portfolio_type):
    try:
        VarDates=[Trade_Dt]
        if portfolio_type=='General':
            conn=pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server}; SERVER='+SERVER+'; PORT='+PORT+'; DATABASE='+DATABASE+';UID='+UID+';PWD='+PWD+';') 
            Exposure_Data=pd.read_sql("select [Company Name] as Company,Price,Quantity from Customer_Portfolio_Details where [Customer Name]='" + user_name + "' and [Portfolio Name]='" + portfolio_name + "'",conn)
            conn.close()
            Exposure_Data=Exposure_Data[['Company','Quantity']]
        else:
            var_objects=PortfolioV5.objects.filter(customer_name=user_name,portfolio_name=portfolio_name).values("date",'company','value_mkt_price')
            Exposure_Data=pd.DataFrame(var_objects)
            Exposure_Data.columns=['Date','Company','Net']

        stock1=list(Exposure_Data["Company"])
        stock=', '.join('"{0}"'.format(w) for w in [stock1[i].replace("'","''") for i in range (0,len(stock1))] ).replace('"',"'")
        lookback=252;horizon=1
        
        Component_VaR=pd.DataFrame()
        Individual_VaR=pd.DataFrame()

        Portfolio_VaR_Component=pd.DataFrame()
        Portfolio_VaR_Component["Date"]=""
        Portfolio_VaR_Component["VaR$"]=""
        Portfolio_VaR_Component["Net$"]=""
        Portfolio_VaR_Component["VaR%"]=""
        
        Portfolio_VaR_Individual=pd.DataFrame()
        Portfolio_VaR_Individual["Date"]=""
        Portfolio_VaR_Individual["VaR$"]=""
        Portfolio_VaR_Individual["Net$"]=""
        Portfolio_VaR_Individual["VaR%"]=""
        
        for i in range (0,len(VarDates)):
            prices_query=""
            if country=='IN':
                if whereCondition!="":
                    prices_query="select Company,Date,Price,[Return] from Master_ConversationalAI where [date] between dateadd(d,-365,'" + str(VarDates[i]) + "') and '" +  str(VarDates[i]) +"' and Company in (" + stock +") and " + whereCondition +" and country='IN' order by company,date"
                else:
                    prices_query="select Company,Date,Price,[Return] from Master_ConversationalAI where [date] between dateadd(d,-365,'" + str(VarDates[i]) + "') and '" +  str(VarDates[i]) +"' and Company in (" + stock +") and country='IN' order by company,date"
            else:
                if whereCondition!="":
                    prices_query="select Company,Date,Price,[Return] from Master_ConversationalAI where [date] between dateadd(d,-365,'" + str(VarDates[i]) + "') and '" +  str(VarDates[i]) +"' and Company in (" + stock +") and " + whereCondition +" and country='US' order by company,date"
                else:
                    prices_query="select Company,Date,Price,[Return] from Master_ConversationalAI where [date] between dateadd(d,-365,'" + str(VarDates[i]) + "') and '" +  str(VarDates[i]) +"' and Company in (" + stock +") and country='US' order by company,date"
            conn=pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server}; SERVER='+SERVER+'; PORT='+PORT+'; DATABASE='+DATABASE+';UID='+UID+';PWD='+PWD+';')
            prices_data=pd.read_sql(prices_query,conn)
            conn.close()
            if portfolio_type=='General':
                prices_data=pd.merge(prices_data,Exposure_Data,on="Company").reset_index(drop=True)       
                prices_data['Net'] = prices_data['Price'] * prices_data['Quantity']       
                DailyPortfolio_Net=prices_data.groupby(['Date'])["Net"].agg('sum').reset_index()      
                DailyPortfolio_Net.columns=['Date','DailyPortfolioNet']       
                prices_data=pd.merge(prices_data,DailyPortfolio_Net,on='Date',how='inner')       
                prices_data["Net%"] = prices_data["Net"]/prices_data['DailyPortfolioNet']       
                prices_data['Date'] = pd.to_datetime(prices_data['Date'])   
                DailyPortfolio_Net['Date']=pd.to_datetime(DailyPortfolio_Net['Date'])
                weight_matrix=prices_data[prices_data['Date']==str(VarDates[i])]
                weight_matrix=weight_matrix[['Company','Net','Net%']]        
            else:  
                Exposure_Data=Exposure_Data[['Company','Net']]
                Exposure_Data['Net%']=Exposure_Data['Net']/Exposure_Data['Net'].sum()
                stocks=list(set(list(prices_data['Company'])))
                Exposure_Data=Exposure_Data[Exposure_Data['Company'].isin(stocks)]
                weight_matrix=Exposure_Data[['Company','Net','Net%']]
            prices_data['Date']=pd.to_datetime(prices_data['Date'])
            prices_data['Max_Date']= prices_data.groupby('Company')['Date'].transform(max)
            prices_data=prices_data[prices_data['Max_Date'].astype(str)==str(VarDates[i])]
            prices_data['Date']=prices_data['Date'].astype(str)
            returns_data=(prices_data.pivot(index='Date', columns='Company', values='Return')).reset_index(drop=True)            
            returns_data.fillna(0,inplace=True)    
            Returns=np.array(returns_data.values,dtype=float)
            significant_value=2.32635                                #value for the confidence of 99%       
            variance_covariance_matrix=np.matmul(Returns.T,Returns)/lookback      
            #variance_covariance_matrix=returns_data.cov()   
            weight_matrix.sort_values(by='Company',inplace=True)       
            weights=np.array(weight_matrix['Net'].values,dtype=float)      
            #Portfolio_Component_Var Calculation
            a=np.array(weight_matrix['Net%'].values,dtype=float)
            b=np.array(weight_matrix['Net%'].values,dtype=float).T
            portfolio_risk=np.matmul(np.matmul(a,variance_covariance_matrix),b)   
            portfolio_var=math.sqrt(portfolio_risk) * math.sqrt(horizon) * significant_value * np.sum(weights,axis=0)       
            Portfolio_VaR_Component.loc[i,"Date"]=VarDates[i]
            Portfolio_VaR_Component.loc[i,"VaR$"]=portfolio_var
            Portfolio_VaR_Component.loc[i,"Net$"]=np.sum(weights,axis=0)
            Portfolio_VaR_Component.loc[i,"VaR%"]=Portfolio_VaR_Component["VaR$"][i]/Portfolio_VaR_Component["Net$"][i]
            
    #        #Component Var Calculation
            if portfolio_type=='General':
                Stock_Values = prices_data[['Company','Net']][prices_data['Date']==str(VarDates[i])].reset_index(drop=True)    
            else:
                Stock_Values = Exposure_Data[['Company','Net']]
            Stock_Values.sort_values(by="Company",inplace=True)            
            #Individual var calculation
            temp=pd.DataFrame()
            Variance  =np.diag(variance_covariance_matrix).tolist()
            Stock_Values.sort_values(by="Company",inplace=True)
            Stock_Values['Variance']=Variance
            temp["Stock"]=Stock_Values['Company']
            temp["Date"]=VarDates[i]
            temp["VaR$"]=np.sqrt(Stock_Values['Variance']) * Stock_Values['Net'] * math.sqrt(horizon) * significant_value
            temp["Net$"]=Stock_Values['Net']
            temp["VaR%"]=temp["VaR$"]/ temp["Net$"]
            Individual_VaR=Individual_VaR.append(temp)
        return Portfolio_VaR_Component,Individual_VaR
    except Exception as e:
        return pd.DataFrame(),pd.DataFrame()
