# -*- coding: utf-8 -*-
"""
Created on Tue Apr 21 11:15:11 2020

@author: Jitendra.v
"""
import pyodbc
import re
import pandas as pd
import nltk
from  more_itertools import unique_everseen
from mysite.core.Appconfigproperties import *


def FilteredConditions(input_text,country):
    whereCondition=""
    input_text=input_text.replace("small cap","smallcap").replace("mid cap","midcap").replace("large cap","largecap")
    input_text=input_text.replace("communication services","communicationservices").replace("information technology","informationtechnology")
    input_text=input_text.replace("consumer discretionary","consumerdiscretionary").replace("consumer staples","consumerstaples")

    input_text=input_text.lower()
    marketcategoryIndex=-999;marketcategorybool="and";marketcategorycondition=""    
    if country=='usa':
        filters_data=pd.read_excel(r"Excel_files\Filters.xlsx",sheet_name="US")
    else:
        filters_data=pd.read_excel(r"Excel_files\Filters.xlsx",sheet_name="India")        
    possibleFilters=filters_data['Filter'].tolist()
    sql_columns=filters_data['SQL_Columns'].tolist()
    marketcaps=['smallcap','midcap','largecap']
    sql_conn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server}; SERVER='+SERVER+'; PORT='+PORT+'; DATABASE='+DATABASE+';UID='+UID+';PWD='+PWD+';')
    gics=pd.read_sql('''select distinct GICS from Security_Master where GICS is not null 
                        union
                        select distinct GICS from US_Security_Master where GICS is not null''',sql_conn)
    sql_conn.close()
    gics_data=gics['GICS'].tolist()
    gics_data=[sector.lower().replace(" ","") for sector in gics_data]
    tokens=nltk.word_tokenize(input_text)
    tokens = [word for word in tokens if word not in ['is','value',"with","having"]]
    pos_tag_tokens=nltk.pos_tag(tokens)
    foundedMetrics=[]
    for i in range (0,len(tokens)):
        if tokens[i].lower() in possibleFilters:
            if tokens[i-1].lower()=='portfolio' and i!=0:
                continue
            else:
                foundedMetrics.append((tokens[i].lower(),i))
    conditions=[]
    condition=""
    logicaloperator=[]
    if (any(substring in input_text.replace(" ","") for substring in marketcaps)):
        findIndex=0;flag=0
        for categ in marketcaps:
            if categ in tokens:
                if tokens[tokens.index(categ)-1].lower()!='nifty':
                    flag=1
        if flag==1:
            condition=" Market_Cap_Category in ("
        for i in range (0,len(pos_tag_tokens)):
            if pos_tag_tokens[i][0] in ['smallcap','midcap','largecap'] and pos_tag_tokens[i-1][0]!='nifty':
                condition=condition+"'"+ pos_tag_tokens[i][0] +"',"
                findIndex=i
        if condition!="":
            condition=condition+")"
            condition=condition.replace(",)",") ")
            if (len(foundedMetrics)==0):
                conditions.append(condition)                
            else:
                if (any([i[1]<findIndex for i in foundedMetrics])):
                    for j in range (findIndex,-1,-1):
                        if pos_tag_tokens[j][1]=='CD':
                            marketcategorycondition=condition
                            if pos_tag_tokens[j+1][0] in ['and','or']:
                                marketcategorybool=pos_tag_tokens[j+1][0]
                            marketcategoryIndex=len([True for i in foundedMetrics if i[1]<findIndex])
                            break
                else:
                    conditions.append(condition)
    if (len(foundedMetrics)>0):
        for i in range (0,len(foundedMetrics)):
            text=""
            startIndex=foundedMetrics[i][1]
            indexFound=startIndex
            if (len(conditions)>=1):
                if (pos_tag_tokens[indexFound-1][0] in ['and','or']):
                    logicaloperator.append((pos_tag_tokens[indexFound][0]," " + pos_tag_tokens[indexFound-1][0]+" "))
                else:
                    logicaloperator.append((pos_tag_tokens[indexFound][0]," and "))
            numeric_count=0
            for j in range (startIndex+1,len(pos_tag_tokens)):
                if (pos_tag_tokens[j][0] not in possibleFilters and pos_tag_tokens[j][0] not in marketcaps and pos_tag_tokens[j][0] not in ['from','since','to','for','of','till'] and numeric_count<2 and pos_tag_tokens[j][0] not in gics_data):
                    if (pos_tag_tokens[j][1]=='CD'):
                        numeric_count+=1
                        text=' '.join(str(token) for token in [pos_tag_tokens[k][0] for k in range (indexFound,j+1)])
                else:
                    break
            if (text!=""):
                conditions.append(text)                
    ordered_conditions=[];ordered_bools=[]
    ind=0
    for i in range (0,len(conditions)):
        if (ind==marketcategoryIndex-1):
            if ind==marketcategoryIndex-1:
                ordered_conditions.insert(ind,conditions[i])
                ordered_bools.insert(ind,marketcategorybool)
                ind+=1
                ordered_conditions.insert(ind,marketcategorycondition)
                if (len(foundedMetrics)>marketcategoryIndex):
                    ordered_bools.insert(ind,logicaloperator[i][1])
                ind+=1
        else:
             ordered_conditions.insert(ind,conditions[i])
             if (i!=len(conditions)-1):
                 ordered_bools.insert(ind,logicaloperator[i][1])
             ind+=1
                
    for i in range (0,len(ordered_conditions)):
        for filtervalue in possibleFilters:
            if filtervalue in ordered_conditions[i]:
                if (all(substring not in ordered_conditions[i].replace(" ","") for substring in ['greaterthanorequalsto','lessthanorequalsto','greaterthan','lessthan','equalsto','between','notequalto','notequalsto','>','<',"=",">=","<=","!="])):
                    ordered_conditions[i]=ordered_conditions[i].replace(filtervalue,' ' + filtervalue + " = " )
                else:
                    ordered_conditions[i]=ordered_conditions[i].replace(filtervalue,' ')
                    ordered_conditions[i]=re.sub(r'\band\b', '`and`', ordered_conditions[i])
                    ordered_conditions[i]= re.sub(r'\bor\b', '`or`', ordered_conditions[i])
                    ordered_conditions[i]=ordered_conditions[i].replace('>=',' ' + filtervalue+' >= ').replace('<=',' ' + filtervalue+' <= ').replace('>',' ' + filtervalue+' > ').replace('<',' ' + filtervalue+' < ').replace('=',' ' + filtervalue+' = ').replace('!=',' ' + filtervalue+' != ')
                    ordered_conditions[i]=ordered_conditions[i].replace(" ","").replace('greaterthan`or`equalsto',filtervalue+' >= ').replace('lessthan`or`equalsto',filtervalue+' <= ').replace('greaterthan',filtervalue+' > ').replace('lessthan',filtervalue+' < ').replace('equalsto',filtervalue+'=').replace('notequalsto',filtervalue+'!=').replace('notequalto',filtervalue+'!=')
                    ordered_conditions[i]=ordered_conditions[i].replace('between',' ' + filtervalue+' between ')
                    ordered_conditions[i].replace("`and`"," and ")
                    ordered_conditions[i].replace("`or`"," or ")                    
    spacejointext=""
    for i in range(0,len(ordered_conditions)):
        if (len(ordered_bools)>=1):
            if i!=0:
                spacejointext=spacejointext +" "+ ordered_bools[i-1] + ordered_conditions[i]
            else:
                spacejointext = ordered_conditions[i]
        else:
            spacejointext=" and " .join (str(token) for token in ordered_conditions)
    spacejointext=spacejointext.replace("between"," between ")
    spacejointext=re.sub(r'\bor\b', ' or ', spacejointext)
    spacejointext=re.sub(r'\band\b', ' and ', spacejointext)
    spacejointext=spacejointext.replace('`','')
    spacejointext=spacejointext.replace('smallcap','Small Cap').replace('midcap','mid cap').replace('largecap','large cap')
    spacejointext=spacejointext.replace("communicationservices","Communication Services").replace("informationtechnology","Information Technology")
    spacejointext=spacejointext.replace("consumerdiscretionary","Consumer Discretionary").replace("consumerstaples","Consumer Staples")
    for i in range (0,len(sql_columns)):
        spacejointext=spacejointext.replace(possibleFilters[i],sql_columns[i])
    whereCondition=spacejointext
    return whereCondition

def FilteredStocksByTags(input_text):
    if any(elem in input_text for elem in ['small','mid','large']):
        input_text=input_text.replace("cap","")
    sql_conn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server}; SERVER='+SERVER+'; PORT='+PORT+'; DATABASE='+DATABASE+';UID='+UID+';PWD='+PWD+';')
    tagging_data=pd.read_sql("select * from NLP_Tags2",sql_conn)
    sql_conn.close()
    stocks_list=tagging_data['FS Name'].tolist()
    tags_list=tagging_data['Tag'].tolist()
    gics_list=tagging_data['GICS'].tolist()
    tags_list=[word.lower().replace("india","") for word in tags_list]
    gics_list=[word.lower() for word in gics_list]
    tags_list=[re.split(" |,|;|&",word) for word in tags_list]
    all_tags=[]
    for i in tags_list:
        all_tags.extend(i)
    all_tags=list(set(all_tags))
    from nltk.corpus import stopwords
    stopwords=set(stopwords.words('english'))
    all_tags=[tag.replace("\xa0","") for tag in all_tags if tag not in stopwords and len(tag)>0]
    all_tags=list(set(all_tags))
    tokens=nltk.word_tokenize(input_text)
    tokens=[i for i in tokens if i not in stopwords]
    exception_words=['related']
    unique_words=[word for word in tokens if word in all_tags and word not in exception_words]
    tag_count=len(unique_words)
    filtered_stocks=[]
    bugs=[]
    if 'roe' in input_text:
        bugs.append("roe")
    if 'debt' in input_text:
        bugs.append("debt")
    tag_dictionary={}
    for k in range(0,len(tokens)):
        for i in range (0,len(tags_list)):
            for j in range (0,len(tags_list[i])):
                if tokens[k] in tags_list[i][j] and tokens[k] not in ['pe','pb'] and tokens[k] not in exception_words:
                    if j<len(tags_list[i])-1:
                        temp=tags_list[i][j+1]
                    if tokens[k]=='high':
                        if len(bugs)==1:
                            if temp==bugs[0]:
                                filtered_stocks.append(stocks_list[i])
                        elif len(bugs)==2:
                            if temp==tokens[k+1]:    
                                filtered_stocks.append(stocks_list[i])
                    elif tokens[k]=='low':
                        if len(bugs)==1:
                            if temp==bugs[0]:
                                filtered_stocks.append(stocks_list[i])
                        elif len(bugs)==2:
                            if temp==tokens[k+1]:
                                filtered_stocks.append(stocks_list[i])
                    else:
                        if stocks_list[i] not in (tag_dictionary.keys()):
                            tag_dictionary[stocks_list[i]]=[tokens[k]]
                            filtered_stocks.append(stocks_list[i])
                        else:
                            if tokens[k] not in tag_dictionary[stocks_list[i]]:
                                tag_dictionary[stocks_list[i]].append(tokens[k])
                                filtered_stocks.append(stocks_list[i])
    tag_matched_stocks = {i:filtered_stocks.count(i) for i in filtered_stocks}
    final_stocks=[]
    for stock,stock_count in tag_matched_stocks.items():
        if stock_count>=tag_count and tag_count!=0:
            final_stocks.append(stock)    
    return final_stocks,gics_list

def replace_words(input_text):
    input_text=re.sub(r"\bit\b","information technology",input_text)
    input_text=re.sub(r"\bindustry\b","industrials",input_text)
    input_text=re.sub(r"\bindustries\b","industrials",input_text)    
    input_text=re.sub(r"\bfinance\b","financial",input_text)
    input_text=re.sub(r"\bsbi\b","sbin",input_text)    
    input_text=re.sub(r"\bhyd\b","hyderabad",input_text)    
    input_text=input_text.replace("banks","bank")
    input_text=input_text.replace("india","*").replace("sector","*").replace("stock","*").replace("show","*").replace("give","").replace("consumers","consumer").replace("communications","communication")
    return input_text

def Main(input_text,country):
    input_text=input_text.lower()
    input_text=replace_words(input_text)
    whereCondition=""
    whereCondition=FilteredConditions(input_text,country)
    stocks_list_by_tag,gics_list=FilteredStocksByTags(input_text)
    tokens=input_text.split(" ")
    stocks_by_conditions= pd.DataFrame() 
    if country=='india':
        if whereCondition!="":
            sql_conn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server}; SERVER='+SERVER+'; PORT='+PORT+'; DATABASE='+DATABASE+';UID='+UID+';PWD='+PWD+';')
            stocks_by_conditions=pd.read_sql("select [FS Name] from security_master where flag='Yes' and " + whereCondition + " order by [FS Name]",sql_conn)
            sql_conn.close()
        else:
            sql_conn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server}; SERVER='+SERVER+'; PORT='+PORT+'; DATABASE='+DATABASE+';UID='+UID+';PWD='+PWD+';')
            stocks_by_conditions=pd.read_sql("select [FS Name] from security_master where flag='Yes'"  + " order by [FS Name]",sql_conn)
            sql_conn.close()
    stocks_by_conditions=stocks_by_conditions['FS Name'].tolist()
    final_stocks=[]
    final_output=[]
    if len(stocks_list_by_tag)>0:
        final_stocks=list(set(stocks_by_conditions).intersection(set(stocks_list_by_tag)))
        final_output=final_stocks
    if len(stocks_list_by_tag)==0:
        if whereCondition!="":
            final_output=stocks_by_conditions
    if len(final_output)>0:
        final_output.sort()
    return final_output

