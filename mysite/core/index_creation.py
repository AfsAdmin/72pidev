# -*- coding: utf-8 -*-
"""
Created on Mon Apr 20 17:07:20 2020

@author: Harshitha.b
"""


##importing required libraries
import requests
import os
from sqlalchemy import create_engine
import pandas as pd
from elasticsearch import Elasticsearch
from elasticsearch.helpers import bulk
import sqlalchemy as sal

def indexCreation():

##Setting up the working directory
#    os.chdir(r'I:/Harshita/version3/transcripts_text_files/')
    
    ##Checking whether elastic server is started or not
    res = requests.get('http://localhost:9200')
    
    ##Connection to ElasticSearch
    es = Elasticsearch([{'host': 'localhost', 'port': 9200}])
    
    ##Creating a index, index here refers a database in our language
    es.indices.create(index='earnings_transcripts_index_new3',body={})
    
    
    ##Connecting to MySQL for fetching the data
    engine=sal.create_engine('mssql+pyodbc://GHCDC01/datax10?driver=SQL Server?Trusted_Connection=yes')
    
    cnx = engine.connect()
    data = pd.read_sql('SELECT * FROM earnings_transcripts_dump', cnx)
    ##Bulk inserting documents. Each row in the DataFrame will be a document in ElasticSearch
    documents = data.to_dict(orient='records')
    bulk(es, documents, index='earnings_transcripts_index_new3',doc_type='foo', raise_on_error=True)
