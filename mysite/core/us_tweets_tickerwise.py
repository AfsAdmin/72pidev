# -*- coding: utf-8 -*-
"""
Created on Mon Mar 16 11:45:16 2020

@author: Harshitha.b
"""

import pandas as pd
import datetime


def tweetAnalysis(tweet_data,ticker):
  specified_ticker = tweet_data[tweet_data['Factset_Ticker']==ticker]
  specified_ticker.reset_index(drop=True,inplace=True)
  daily_sentiment_output=pd.DataFrame(columns=['Date','Positive','Negative'])
  dates = specified_ticker['Date'].unique().tolist()
  
  count_by_date=pd.DataFrame(columns=['Date','TweetCount'])
  
  for i in range(0,len(dates)):
    new_df = pd.DataFrame(columns = ['Date','Positive','Negative'])
    each_date= specified_ticker[specified_ticker['Date']==dates[i]]
    pos_count = each_date[each_date['Sentiment']=="Positive"]
    neg_count = each_date[each_date['Sentiment']=="Negative"]
    new_df.loc[i,"Date"]=dates[i]
    if len(pos_count)==0 and len(neg_count)==0:
      new_df.loc[i,"Positive"]= 0.5
      new_df.loc[i,"Negative"]= 0.5
    else:
      new_df.loc[i,"Positive"]= len(pos_count)/((len(pos_count)+len(neg_count)))
      new_df.loc[i,"Negative"]= len(neg_count)/((len(pos_count)+len(neg_count)))
    daily_sentiment_output = daily_sentiment_output.append(new_df)
    count_by_date.loc[i,'Date']=dates[i]
    count_by_date.loc[i,'TweetCount']=len(each_date)
  
  latest_date_output=daily_sentiment_output.iloc[-1:,]
  latest_date_output.reset_index(drop=True,inplace=True)
  
  sector_buzz=pd.DataFrame(columns=['Date','Ticker','# of tweets'])
  sector = specified_ticker.loc[0,'GICS']
  sector_wise = tweet_data[tweet_data['GICS']==sector]
  tickers_list= sector_wise['Factset_Ticker'].unique().tolist()
  l=0
  for j in range(0,len(tickers_list)):
    each_ticker = sector_wise[sector_wise['Factset_Ticker']==tickers_list[j]]
    each_ticker.reset_index(drop=True,inplace=True)
    each_ticker_dates = each_ticker['Date'].unique().tolist()
    for k in range(0,len(each_ticker_dates)):
      each_date_data = each_ticker[each_ticker['Date']==each_ticker_dates[k]]
      sector_buzz.loc[l,'Date']=each_ticker_dates[k]
      sector_buzz.loc[l,'Ticker'] = tickers_list[j]
      sector_buzz.loc[l,'# of tweets'] = len(each_date_data)
      l=l+1
      
  sector_buzz.sort_values(by='Date',ascending=True,inplace=True)
  sector_buzz.reset_index(drop=True,inplace=True)
  
  sector_sentiment=pd.DataFrame(columns = ['Date','Sector','Positive','Negative'])
  sectors_list=specified_ticker.loc[0,'GICS']
  m=0
  for u in range(0,1):
    each_sector = tweet_data[tweet_data['GICS']==sectors_list]
    each_sector.reset_index(drop=True,inplace=True)
    each_sector_date = each_sector['Date'].unique().tolist()
    for v in range(0,len(each_sector_date)):
      sector_date_datewise= each_sector[each_sector['Date']==each_sector_date[v]]
      positive_count = sector_date_datewise[sector_date_datewise['Sentiment']=='Positive']
      negative_count = sector_date_datewise[sector_date_datewise['Sentiment']=='Negative']
      sector_sentiment.loc[m,'Date']=each_sector_date[v]
      sector_sentiment.loc[m,'Sector']=sectors_list
      if len(pos_count)==0 and len(neg_count)==0 :
        sector_sentiment.loc[m,'Positive']=0.5
        sector_sentiment.loc[m,'Negative']=0.5    
      else:
        sector_sentiment.loc[m,'Positive']=len(positive_count)/(len(positive_count)+len(negative_count))
        sector_sentiment.loc[m,'Negative']=len(negative_count)/(len(positive_count)+len(negative_count))
      m=m+1
      
  sector_sentiment.sort_values(by=['Date','Sector'],ascending=True,inplace=True)
  sector_sentiment.reset_index(drop=True,inplace=True)
  
  return daily_sentiment_output,latest_date_output,count_by_date,sector_buzz,sector_sentiment

