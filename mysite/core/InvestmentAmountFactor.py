# -*- coding: utf-8 -*-
"""
Created on Tue Feb 25 13:18:28 2020

@author: Harshitha.b
"""

import pandas as pd
import pyodbc
import datetime
from datetime import timedelta
from .models import *


def factorsDashboard(Daily_Returns,factors_data,pnl_start_date,EndDate,portfolio,callFrom):
  try:
    factors_merge = pd.merge(Daily_Returns,portfolio ,on=['Factset_Ticker','Company'],how='inner')
    factors_output=factors_merge[['Date','Factset_Ticker','Company','Price','Quantity']]
    min_date = factors_output['Date'].min()
    previous_exposure = factors_output[factors_output['Date']==min_date]
    previous_exposure.reset_index(drop=True,inplace=True)
    previous_exposure = previous_exposure[['Company','Factset_Ticker','Price','Quantity']]
    previous_exposure['previous_exposure'] = previous_exposure['Price']*previous_exposure['Quantity']
    current_exposure = factors_output[factors_output['Date']==EndDate]
    current_exposure.reset_index(drop=True,inplace=True)
    current_exposure = current_exposure[['Company','Factset_Ticker','Price','Quantity']]
    current_exposure['current_exposure'] = current_exposure['Price']*current_exposure['Quantity']
    pnl_data1 = current_exposure[['Company','Factset_Ticker','current_exposure']]
    pnl_data2= previous_exposure[['Company','Factset_Ticker','previous_exposure']]
    pnl_data = pd.merge(pnl_data1,pnl_data2,on=['Company','Factset_Ticker'],how ='inner')
    pnl_data['PNL'] = (pnl_data['current_exposure']-pnl_data['previous_exposure'])
    Factors_output = pd.merge(pnl_data,factors_data ,on=['Company','Factset_Ticker'],how='inner')
    Final_output = Factors_output.copy()
    constraints = pd.DataFrame(list(FactorsConstraints.objects.values()))
    constraints.dropna(inplace=True)
    if callFrom!='view':
        constraints=constraints[constraints['Factor'].isin(['Beta_1Y','PE','EPS_Growth'])]
    constraints=constraints.sort_values(by='Factor',ascending ='True').reset_index(drop=True)
    fields = constraints['Factor'].tolist()
    fields.sort()
    for i in range(0,len(Final_output)):
      for j in range(0,len(fields)):
        if Final_output.loc[i,fields[j]] <= constraints.loc[j,'Low']: 
          Final_output.loc[i,fields[j]] =1
        elif Final_output.loc[i,fields[j]] >=constraints.loc[j,'High']:
          Final_output.loc[i,fields[j]] =3
        else:
          Final_output.loc[i,fields[j]] =2
      
    columns_list = Final_output.columns.tolist()
    for i in range(len(columns_list)):
      if columns_list[i] in fields:
        columns_list[i] = columns_list[i]+"_Bucket"
    Final_output.columns = columns_list
    
    columns_list = Final_output.columns.tolist()[4:]
    factor_count = pd.DataFrame(columns=['Factor','Bucket','count','Exposure','Return'])
    count_list = [1,2,3]
    bucket_list = ['Low','InLine','High']
    j=0
    for i in range(0,len(columns_list)):
      for k in range(0,3):
        factor_count.loc[j,'Factor'] = columns_list[i]
        required_bucket = Final_output[Final_output[columns_list[i]]==count_list[k]]
        required_bucket.reset_index(drop=True,inplace=True)
        factor_count.loc[j,'Bucket'] = bucket_list[k]
        factor_count.loc[j,'count'] = len(required_bucket)
        factor_count.loc[j,'Exposure'] = required_bucket['current_exposure'].sum()
        factor_count.loc[j,'Return'] = required_bucket['PNL'].sum()
        j=j+1
    return factor_count
  except Exception as e:
    emptyDF=pd.DataFrame()
    return emptyDF
