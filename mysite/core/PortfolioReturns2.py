import pandas as pd
from datetime import datetime
import pyodbc
import math
import numpy as np
from scipy import stats
from statistics import mean
from .Live_Cumulative_Returns_Dashboard import *
from .models import CustomerPortfolioDetails, UniversePricesMain, MarketIndex, RiskFreeRate, UsMarketIndex
def MacroImpactCumulativeReturns(Stocks_Data_with_Exposure,StartDate,EndDate,Daily_Returns):
    Stock_CumulativeReturns,DailyReturns =StockCumulativeReturns(Stocks_Data_with_Exposure,StartDate,EndDate,Daily_Returns)       
    Stock_CumulativeReturns.columns=['Trade_Dt','PNL']
    return Stock_CumulativeReturns

def CalcsMain(current_country,StartDate,pnl_start_date,EndDate,user,Stock_Data,Daily_Returns,index_cumulative_data,portfolio_name,risk_free_data,perType,callFrom=None):     
    try:
        country='India'
        if current_country=='US':
            country='US'            
        Stocks_Data_with_Exposure = ExposureCalculation(Stock_Data,StartDate,EndDate,Daily_Returns)  #calculating exposure by calling ExposureCalculation() function
        Risk_Free_Data = risk_free_data.copy()
        index_cumulative_data = index_cumulative_data.copy()
        Index_Ticker="Nifty 50 Index" ; Index_Ticker2="Nifty Smallcap 100 Index";Index_Ticker3="Nifty Midcap 100 Index"    #declaring benchmark tickers
        if current_country=='US':
            Index_Ticker = "S&P 500" ; Index_Ticker2="Russell 2000";Index_Ticker3="Dow Jones Industrial Average"    #declaring benchmark tickers
        if callFrom is not None and portfolio_name=='US Diversified-US':
            portfolio_name='US Diversified'
        #functions for calculating cumulative returns
        Stock_CumulativeReturns,DailyReturns =StockCumulativeReturns(Stocks_Data_with_Exposure,StartDate,EndDate,Daily_Returns)       
        Equal_Stock_CumulativeReturns,Equal_DailyReturns= Equal_Portfolio_Cumulative_Returns(StartDate,pnl_start_date,EndDate,user,portfolio_name,country,Stock_Data,Daily_Returns)
        Index1_CumulativeReturns,Index1_Returns = IndexCumulativeReturns (Index_Ticker,StartDate,EndDate,index_cumulative_data)
        Index2_CumulativeReturns,Index2_Returns = IndexCumulativeReturns (Index_Ticker2,StartDate,EndDate,index_cumulative_data)
        Index3_CumulativeReturns,Index3_Returns = IndexCumulativeReturns (Index_Ticker3,StartDate,EndDate,index_cumulative_data)
        Stock_CumulativeReturns['Date'] = pd.to_datetime(Stock_CumulativeReturns['Date'])
        Equal_Stock_CumulativeReturns['Date'] = pd.to_datetime(Equal_Stock_CumulativeReturns['Date'])
        Index1_CumulativeReturns['Date'] = pd.to_datetime(Index1_CumulativeReturns['Date'])
        Index2_CumulativeReturns['Date'] = pd.to_datetime(Index2_CumulativeReturns['Date'])
        Index3_CumulativeReturns['Date'] = pd.to_datetime(Index3_CumulativeReturns['Date'])
        DailyReturns['Date'] = pd.to_datetime(DailyReturns['Date'])
        Index1_Returns['Date'] = pd.to_datetime(Index1_Returns['Date'])
        Index2_Returns['Date'] = pd.to_datetime(Index2_Returns['Date'])
        Index3_Returns['Date'] = pd.to_datetime(Index3_Returns['Date'])
        Equal_DailyReturns['Date'] = pd.to_datetime(Equal_DailyReturns['Date'])
        DailyReturns.columns=["Date","PNL"]
        Index1_Returns.columns=["Date","PNL"]
        Index2_Returns.columns=["Date","PNL"]
        Index3_Returns.columns=["Date","PNL"] 
        Equal_DailyReturns.columns=['Date','PNL']


        #Annualize_Returns

        Fund_Annualize_Return = AnnualizeReturn (Stock_CumulativeReturns)
        Equal_Fund_Annualize_Return = AnnualizeReturn (Equal_Stock_CumulativeReturns)
        Index1_Annualize_Return = AnnualizeIndexReturn (Index_Ticker,StartDate,EndDate,index_cumulative_data)
        Index2_Annualize_Return = AnnualizeIndexReturn (Index_Ticker2,StartDate,EndDate,index_cumulative_data)
        Index3_Annualize_Return = AnnualizeIndexReturn (Index_Ticker3,StartDate,EndDate,index_cumulative_data)

        #Annualize_Volatiliy
        Annualize_Fund_Volatility = AnnualizeVolatility(DailyReturns)
        Equal_Annualize_Fund_Volatility = AnnualizeVolatility(Equal_DailyReturns)
        Annualize_Index1_Volatility = AnnualizeVolatility(Index1_Returns)
        Annualize_Index2_Volatility =AnnualizeVolatility(Index2_Returns)
        Annualize_Index3_Volatility =AnnualizeVolatility(Index3_Returns)

        #Annualize Risk Free Rate
        Annualize_Risk_Free_Rate=AnnualizeRiskFreeRate (Risk_Free_Data)

        #Max_DrawDown
        Fund_Max_DrawDown = Max_DrawDown(Stock_CumulativeReturns,StartDate,EndDate)
        Equal_Fund_Max_DrawDown = Max_DrawDown(Equal_Stock_CumulativeReturns,StartDate,EndDate)
        Index1_Max_DrawDown = Max_DrawDown(Index1_CumulativeReturns,StartDate,EndDate)
        Index2_Max_DrawDown = Max_DrawDown(Index2_CumulativeReturns,StartDate,EndDate)
        Index3_Max_DrawDown = Max_DrawDown(Index3_CumulativeReturns,StartDate,EndDate)

        #Sharpe
        Fund_Sharpe = (Fund_Annualize_Return - Annualize_Risk_Free_Rate)/Annualize_Fund_Volatility
        Equal_Fund_Sharpe = (Equal_Fund_Annualize_Return - Annualize_Risk_Free_Rate)/Equal_Annualize_Fund_Volatility
        Index1_Sharpe = (Index1_Annualize_Return - Annualize_Risk_Free_Rate)/Annualize_Index1_Volatility
        Index2_Sharpe = (Index2_Annualize_Return - Annualize_Risk_Free_Rate)/Annualize_Index2_Volatility
        Index3_Sharpe = (Index3_Annualize_Return - Annualize_Risk_Free_Rate)/Annualize_Index3_Volatility
        
        #Modified code for showing cumulative returns instead of annualized return added by jitendra
        if perType!='ITD':
            Equal_Fund_Annualize_Return=Equal_Stock_CumulativeReturns.iat[len(Equal_Stock_CumulativeReturns)-1,1]
            Fund_Annualize_Return=Stock_CumulativeReturns.iat[len(Stock_CumulativeReturns)-1,1]    # annualize returns by taking cumulative return
            Index1_Annualize_Return=IndexCumulativeReturn(Index_Ticker,StartDate,EndDate,index_cumulative_data)
            Index2_Annualize_Return=IndexCumulativeReturn(Index_Ticker2,StartDate,EndDate,index_cumulative_data)
            Index3_Annualize_Return=IndexCumulativeReturn(Index_Ticker3,StartDate,EndDate,index_cumulative_data)
        
        Results=pd.DataFrame(columns=["C","Annualized Return","Annualized Volatility","Sharpe","Maximum DrawDown"])
        Companies=['Portfolio','Equal Weighted Portfolio',Index_Ticker,Index_Ticker2,Index_Ticker3]
        Annnl_Return_values=[Fund_Annualize_Return,Equal_Fund_Annualize_Return,Index1_Annualize_Return,Index2_Annualize_Return,Index3_Annualize_Return]
        Annnl_Volatility_values= [Annualize_Fund_Volatility,Equal_Annualize_Fund_Volatility,Annualize_Index1_Volatility,Annualize_Index2_Volatility,Annualize_Index3_Volatility]
        Sharpe_values=[Fund_Sharpe,Equal_Fund_Sharpe,Index1_Sharpe,Index2_Sharpe,Index3_Sharpe]
        Max_DD_Values = [Fund_Max_DrawDown,Equal_Fund_Max_DrawDown,Index1_Max_DrawDown,Index2_Max_DrawDown,Index3_Max_DrawDown]
        Results['C']=Companies
        Results["Annualized Return"] = Annnl_Return_values
        Results["Annualized Volatility"] = Annnl_Volatility_values
        Results["Sharpe"] = Sharpe_values
        Results["Maximum DrawDown"] = Max_DD_Values
        Results.columns=["","Annualized Return","Annualized Volatility","Sharpe","Maximum DrawDown"]

        Equal_Stock_CumulativeReturns.columns=['Date','Equalweighted pnl']
        Index1_CumulativeReturns.columns=["Date","Company",Index_Ticker]
        Index2_CumulativeReturns.columns=["Date","Company",Index_Ticker2]
        Index3_CumulativeReturns.columns=["Date","Company",Index_Ticker3]

        Stock_CumulativeReturns['Date'] = pd.to_datetime(Stock_CumulativeReturns['Date'])
        Equal_Stock_CumulativeReturns['Date'] = pd.to_datetime(Equal_Stock_CumulativeReturns['Date'])
        Index1_CumulativeReturns['Date'] = pd.to_datetime(Index1_CumulativeReturns['Date'])
        Index2_CumulativeReturns['Date'] = pd.to_datetime(Index2_CumulativeReturns['Date'])
        Index3_CumulativeReturns['Date'] = pd.to_datetime(Index3_CumulativeReturns['Date'])

        Cumulative_Returns = pd.merge(Stock_CumulativeReturns,Equal_Stock_CumulativeReturns,on="Date",how="inner")   #merging all cumulative returns  
        Cumulative_Returns = pd.merge(Cumulative_Returns,Index1_CumulativeReturns,on="Date",how="inner")   #merging all cumulative returns  
        Cumulative_Returns = pd.merge(Cumulative_Returns,Index2_CumulativeReturns,on="Date",how="inner")
        Cumulative_Returns = pd.merge(Cumulative_Returns,Index3_CumulativeReturns,on="Date",how="inner")
        Cumulative_Returns =  Cumulative_Returns[["Date","PNL",'Equalweighted pnl',Index_Ticker,Index_Ticker2,Index_Ticker3]]  #extracting required columns
        Cumulative_Returns.columns=["Trade_Dt","PNL",'Equalweighted pnl',Index_Ticker,Index_Ticker2,Index_Ticker3]


        DailyReturns.columns=["Date","Portfolio Return"]
        Index1_Returns.columns=["Date",Index_Ticker+" Return"]
        Index2_Returns.columns=["Date",Index_Ticker2+" Return"]
        Index3_Returns.columns=["Date",Index_Ticker3+" Return"] 
        Equal_DailyReturns.columns=['Date','Equal Portfolio Return']
        Daily_Returns_values = pd.merge(DailyReturns,Equal_DailyReturns,on="Date",how="inner")   #merging all cumulative returns  
        Daily_Returns_values = pd.merge(Daily_Returns_values,Index1_Returns,on="Date",how="inner")   #merging all cumulative returns  
        Daily_Returns_values = pd.merge(Daily_Returns_values,Index2_Returns,on="Date",how="inner")
        Daily_Returns_values = pd.merge(Daily_Returns_values,Index3_Returns,on="Date",how="inner")
        Daily_Returns_values =  Daily_Returns_values[["Date","Portfolio Return",'Equal Portfolio Return',Index_Ticker+" Return",Index_Ticker2 + " Return",Index_Ticker3 + " Return"]]  #extracting required columns

        return Cumulative_Returns,Daily_Returns_values,Results
    except Exception as e:
        print(e)
        return pd.DataFrame(),pd.DataFrame(),pd.DataFrame()
        
def ExposureCalculation(Stocks_Data,StartDate,EndDate,Daily_Returns):     #function for exposure calc
    Exposure_Data=Stocks_Data.copy()
    Exposure_Data=Exposure_Data[['Company','Factset_Ticker','Quantity']]
    Daily_Returns = pd.merge(Daily_Returns,Exposure_Data,on=['Factset_Ticker','Company'],how='inner')    #merging returns data with exposure data on company
    Daily_Returns["Net"]=Daily_Returns["Price"] * Daily_Returns["Quantity"]    #multiplying price and quantity to get the net value
    Daily_Returns=Daily_Returns[['Company','Factset_Ticker','Date','Net']]
    return Daily_Returns

def StockCumulativeReturns(Stocks_Data_with_Exposure,StartDate,EndDate,Daily_Returns):    #function for calculating cumulative returns
    Exposure_Data = Stocks_Data_with_Exposure[["Company",'Factset_Ticker',"Date",'Net']]
    Exposure_Data=pd.merge(Exposure_Data,Daily_Returns,on=['Company','Factset_Ticker','Date'])                             
    Exposure_Data.fillna(0,inplace=True)
    Net_Data=Exposure_Data.groupby(['Date'])["Net"].agg('sum').reset_index()     #calculating day wise net
    Net_Data.columns=['Date','Denom']
    Exposure_Data=pd.merge(Exposure_Data,Net_Data,on='Date',how='inner')
    Exposure_Data["Net%"] = Exposure_Data["Net"]/Exposure_Data['Denom']         #calculating Net% by dividing day wise stock net with day wise portfolio net
    Exposure_Data['Date'] = pd.to_datetime(Exposure_Data['Date'])
    Exposure_Data['PNL%']=Exposure_Data['Return'] *  Exposure_Data.groupby(['Company','Factset_Ticker'])['Net%'].shift(1)     #calculating PNL% by multiplying todays return and yesterday net%
    PNL_data=Exposure_Data.groupby(['Date'])['PNL%'].agg('sum').reset_index()       #calculating day wise PNL%                  
    PNL_data.columns=["Date","Daily Ret"]
    PNL_data["PNL"] = (PNL_data['Daily Ret']+1).cumprod()-1       #calculating cumulative product
    Cumulative_Returns=PNL_data[["Date","PNL"]]                         
    PNL_data=PNL_data[["Date","Daily Ret"]]
    PNL_data.columns=["Date","Return"]
    return Cumulative_Returns,PNL_data 

def IndexCumulativeReturns (index_ticker,StartDate,EndDate,index_cumulative_data):   #function for calculating index cumulative returns
    index_cumulative_data=index_cumulative_data[index_cumulative_data['Company']==index_ticker]   #extracting the data of a given benchmark ticker
    index_cumulative_data.fillna(0,inplace=True)
    index_cumulative_data["Cumulative Return"] = (index_cumulative_data['Return']+1).cumprod()-1   #finding running cumulative 
    IndexCumulativeReturn=index_cumulative_data[["Date","Company","Cumulative Return"]]
    IndexCumulativeReturn.columns=["Date","Company","PNL"]
    IndexReturns=index_cumulative_data[["Date","Return"]]
    return IndexCumulativeReturn,IndexReturns
    
def AnnualizeReturn (CumulativeReturns_Daily):
    Stock_Returns=CumulativeReturns_Daily.copy()
    annualize_ret=((Stock_Returns.iat[len(Stock_Returns)-1,1] +1) ** (252/len(Stock_Returns)))-1   # annualize returns by taking cumulative returns
    return annualize_ret
    
def AnnualizeIndexReturn(index_ticker,StartDate,EndDate,index_cumulative_data):
    index_cumulative_data=index_cumulative_data[index_cumulative_data['Company']==index_ticker]
    index_cumulative_data["Index Cumulate Ret"] = (index_cumulative_data['Return']+1).cumprod()-1   #computing index annualize returns    
    annualize_index_ret=((index_cumulative_data.iat[len(index_cumulative_data)-1,3] +1) ** (252/len(index_cumulative_data)))-1
    return annualize_index_ret

def IndexCumulativeReturn(index_ticker,StartDate,EndDate,index_cumulative_data):
    index_cumulative_data=index_cumulative_data[index_cumulative_data['Company']==index_ticker]
    index_cumulative_data["Index Cumulate Ret"] = (index_cumulative_data['Return']+1).cumprod()-1   #computing index annualize returns    
    annualize_index_ret=index_cumulative_data.iat[len(index_cumulative_data)-1,3] 
    return annualize_index_ret


#Function for AnnualizeVolatility
def AnnualizeVolatility (Daily_Returns_data):
    Stock_Returns=Daily_Returns_data.copy()
    annualize_vol=Stock_Returns.loc[:,"PNL"].std()     #volatiity taking returns and applying standard deviation and then multiply with annualize number
    return annualize_vol*(math.sqrt(252))

#Function for AnnualizeRiskFreeRate
def AnnualizeRiskFreeRate(Input_Risk_Free_Rate):
    Input_Risk_Free_Rate_Data=Input_Risk_Free_Rate.copy()
    Input_Risk_Free_Rate_Data["Risk Free Rate_cumulate"] = (Input_Risk_Free_Rate_Data['Risk Free Rate']+1).cumprod()-1                               #Calculate the Cumulative PNL
    annualize_risk_free=((Input_Risk_Free_Rate_Data.iat[len(Input_Risk_Free_Rate_Data)-1,2] +1) ** (252/len(Input_Risk_Free_Rate_Data)))-1
    return annualize_risk_free

#Function for AnnualizeUpsideVolatility
def AnnualizeUpsideVolatility (Daily_Returns_data):
    Stock_Returns=Daily_Returns_data.copy()
    Stock_Returns= Stock_Returns.loc[Stock_Returns["PNL"]>0]   #upside volatility is taking positve returns and then apply std 
    annualize_upside_vol=Stock_Returns.loc[:,"PNL"].std()
    return annualize_upside_vol*(math.sqrt(252))

#Function for AnnualizeDownsideVolatility
def AnnualizeDownsideVolatility (Daily_Returns_data):
    Stock_Returns=Daily_Returns_data.copy()
    Stock_Returns= Stock_Returns.loc[Stock_Returns["PNL"]<0]  #downside volatility means taking negative returns and then apply std
    annualize_downside_vol=Stock_Returns.loc[:,"PNL"].std()
    return annualize_downside_vol * (math.sqrt(252))

#Function for Max_DrawDown
def Max_DrawDown(CumulativeReturns_Data,StartDate,EndDate):
    Stock_Returns=CumulativeReturns_Data.copy()
    Stock_Returns["Max Cumulative"]=Stock_Returns["PNL"].cummax(axis = 0)     #find max value of cumulatives at given point
    draw_down_input=pd.DataFrame()
    draw_down_input["Cumulative"]=Stock_Returns["PNL"]
    draw_down_input["Max Cumulative"]=Stock_Returns["Max Cumulative"]
    draw_down_input["Draw_down"]=1-((1+draw_down_input["Cumulative"])/(1+draw_down_input["Max Cumulative"]))
    max_drop_down_res=draw_down_input["Draw_down"].max()
    return max_drop_down_res

def Equal_Portfolio_Cumulative_Returns(StartDate,pnl_start_date,EndDate,user_name,portfolio_name,country,Stock_Data,Daily_Returns):
    stocks_data=Stock_Data[['Company','Factset_Ticker','Quantity']]
    stocks_data.columns=['Company','Factset_Ticker','Quantity']
    stock=list(stocks_data['Factset_Ticker'])

    if country=='India':
        input_returns  =Daily_Returns.copy()   
        DailyReturns=pd.merge(input_returns,stocks_data,on=['Factset_Ticker','Company'],how='inner')       
              
    else:
        input_returns  =Daily_Returns.copy()   
        DailyReturns=pd.merge(input_returns,stocks_data,on=['Factset_Ticker','Company'],how='inner')                

    if (len(DailyReturns)!=0):
        Equal_weighted_Portfolio= DailyReturns[['Date','Company','Factset_Ticker','Price','Return','Quantity']]
        Equal_weighted_Portfolio.sort_values(by=['Date','Company','Factset_Ticker'],ascending=[True,True,True],inplace=True)
        Equal_weighted_Portfolio.reset_index(drop=True,inplace=True)
        starting_prices =  Equal_weighted_Portfolio.groupby(['Company','Factset_Ticker']).first().reset_index()
        starting_prices=starting_prices[['Company','Factset_Ticker','Date','Price']]
        Stocks=Stock_Data[['Company','Factset_Ticker','Quantity','Price']].drop_duplicates()
        Stocks['Exposure']=Stocks['Quantity']*Stocks['Price']
        new_exposure = (Stocks['Exposure'].sum())/(len(Stocks))
        Stocks['New Quantity']=new_exposure/(Stocks['Price'])
        Stocks=Stocks[['Company','Factset_Ticker','New Quantity']]
        Stocks.columns=['Company','Factset_Ticker','Quantity']        
        new_stock_weigths=Stocks
        Equal_weighted_Portfolio=Equal_weighted_Portfolio[['Date','Company','Factset_Ticker','Price','Return']]
        Equal_weighted_Portfolio=pd.merge(Equal_weighted_Portfolio,new_stock_weigths,on=["Company","Factset_Ticker"],how="inner")
        Equal_weighted_Portfolio['StockDayNet']=Equal_weighted_Portfolio['Price']*Equal_weighted_Portfolio['Quantity']
        Equal_Net_Data=Equal_weighted_Portfolio.groupby(['Date'])["StockDayNet"].agg('sum').reset_index() 
        Equal_Net_Data.columns=['Date','PortfolioDayNet']
        Equal_weighted_Portfolio=pd.merge(Equal_weighted_Portfolio,Equal_Net_Data,on="Date",how="inner")
        Equal_weighted_Portfolio["Net%"] = Equal_weighted_Portfolio["StockDayNet"]/Equal_weighted_Portfolio['PortfolioDayNet']        
        Equal_weighted_Portfolio['PNL']= Equal_weighted_Portfolio['Return'] *  Equal_weighted_Portfolio.groupby(['Company','Factset_Ticker'])['Net%'].shift(1) 
        Equal_PNL_data=Equal_weighted_Portfolio.groupby(['Date'])['PNL'].agg('sum').reset_index()      
        Equal_PNL_data.columns=["Date","PNL"]
        Equal_PNL_data['Date']=pd.to_datetime(Equal_PNL_data['Date'])
        Equal_PNL_data["EqualCumulativePNL"] = (Equal_PNL_data['PNL']+1).cumprod()-1  
        # if str(StartDate)==str(minimum_date):
        Equal_Cumulative_PNL_data=Equal_PNL_data[['Date','EqualCumulativePNL']]
        Equal_Cumulative_PNL_data.columns=['Date','PNL']
        # else:
        #     Equal_Cumulative_PNL_data=Equal_PNL_data[['Date','EqualCumulativePNL']][1:] 
        #     Equal_Cumulative_PNL_data.columns=['Date','PNL']
        Equal_PNL_data=Equal_PNL_data[['Date','PNL']]
        Equal_PNL_data.columns=['Date','Daily_Return']
        return Equal_Cumulative_PNL_data,Equal_PNL_data

