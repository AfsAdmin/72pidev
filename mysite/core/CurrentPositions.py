import pandas as pd
import datetime
from datetime import date, timedelta

#Based on the current date we need to extract stock wise returns, exposure, pnl for ITD, YTD, MTD, LTD 

def CurrentPositionsMain(current_date,Daily_Returns,Stock_Data):

    try:
        Stock_Data = Stock_Data[['Company', 'Quantity']]
        current_data = Daily_Returns.sort_values('Date', ascending=False).groupby('Company', as_index=False).first()
        current_data = current_data[['Company', 'Date','Price']]

        #extracting stock wise starting business dates and corresponding prices of the current month
        pre_mtd_date = current_date.replace(day=1)
        pre_mtd_data = Daily_Returns[Daily_Returns['Date']>=pre_mtd_date]
        pre_mtd_data.sort_values(by=['Company','Date'],ascending=[True,True],inplace=True)
        pre_mtd_data = pre_mtd_data.groupby('Company').first().reset_index()
        pre_mtd_data = pre_mtd_data[['Company' , 'Date' , 'Price']]
        pre_mtd_data.columns = ['company', 'ITD_Date', 'price']   

        #extracting stock wise starting bussiness dates and corresponding prices of the current year
        pre_ytd_date = current_date.replace(month=1, day=1)
        pre_ytd_data = Daily_Returns[Daily_Returns['Date']>=pre_ytd_date]
        pre_ytd_data.sort_values(by=['Company','Date'],ascending=[True,True],inplace=True)
        pre_ytd_data = pre_ytd_data.groupby('Company').first().reset_index()
        pre_ytd_data = pre_ytd_data[['Company' , 'Date' , 'Price']]
        pre_ytd_data.columns = ['company', 'ITD_Date', 'price']

        #extracting stock wise starting bussiness dates and corresponding prices from inception
        pre_itd_data = Daily_Returns.sort_values(by=['Company','Date'],ascending=[True,True])
        pre_itd_data = pre_itd_data.groupby('Company').first().reset_index()
        pre_itd_data = pre_itd_data[['Company' , 'Date' , 'Price']]
        pre_itd_data.columns = ['company', 'ITD_Date', 'price']

        #extracting stock wise starting bussiness dates and corresponding prices for the last one year
        pre_one_year_date = current_date+timedelta(-365)
        pre_one_year_data = Daily_Returns[Daily_Returns['Date']>=pre_one_year_date]
        pre_one_year_data.sort_values(by=['Company','Date'],ascending=[True,True],inplace=True)
        pre_one_year_data = pre_one_year_data.groupby('Company').first().reset_index()
        pre_one_year_data = pre_one_year_data[['Company' , 'Date' , 'Price']]
        pre_one_year_data.columns = ['company', 'ITD_Date', 'price']

        output = pd.DataFrame()

        #calculating stockwise pnl, return based on current price and itd price
        itd_data = pnl_calc(Stock_Data, current_data, pre_itd_data)
        itd_data.columns=['Stock','current_exposure','ITD_PNL','ITD_Return']
        output['Stock']=itd_data['Stock']
        output['Current Exposure']=itd_data['current_exposure']
        output['ITD_PNL']=itd_data['ITD_PNL']
        output['ITD_Return']=itd_data['ITD_Return']

        #calculating stockwise pnl,return based on current price and ytd price
        ytd_data=pnl_calc(Stock_Data, current_data, pre_ytd_data)
        output['YTD_PNL']=ytd_data['PNL']
        output['YTD_Return']=ytd_data['Return']

        #calculating stockwise pnl,return based on current price and mtd price
        mtd_data=pnl_calc(Stock_Data, current_data, pre_mtd_data)
        output['MTD_PNL']=mtd_data['PNL']
        output['MTD_Return']=mtd_data['Return']

        #calculating stockwise pnl,return based on current price and ltd starting price
        year_data=pnl_calc(Stock_Data, current_data, pre_one_year_data)
        output['LTD_PNL']=year_data['PNL']
        output['LTD_Return']=year_data['Return']
        return output
    except Exception as e:
        emptyDF=pd.DataFrame()
        return emptyDF
        
#calc for finding stockwise return , pnl, current_exposure
def pnl_calc(Stock_Data, current_data, previous_data):
    current_data=pd.merge(current_data,Stock_Data,on='Company',how='inner')
    current_data=current_data[['Company','Quantity','Price']]
    current_data.columns=['Company','Quantity','current_price']
    previous_data=previous_data[['company','price']]
    previous_data.columns=['Company','previous_price']
    current_data=pd.merge(current_data,previous_data,on='Company',how='outer')
    current_data['current_exposure']=current_data['Quantity']*current_data['current_price']
    current_data['previous_exposure']=current_data['Quantity']*current_data['previous_price']
    current_data['PNL']=current_data['current_exposure']-current_data['previous_exposure']
    current_data['Return']=(current_data['current_price']-current_data['previous_price'])/current_data['previous_price']
    final_data=current_data[['Company','current_exposure','PNL','Return']]
    return final_data