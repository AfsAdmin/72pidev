from django.contrib.auth.decorators import login_required
from mysite.core.models import CustomerPriceMovementsAlerts, CustomerTechnicalIndicationAlerts, CustomerSubscription,DailyMarketData
from django.db.models import Q
import re
import json
import pandas as pd
from django.views.decorators.csrf import csrf_exempt
# context processors starts here

def getNotifications(request):
    if request.user.is_authenticated:
        notifications1 = CustomerPriceMovementsAlerts.objects.filter(customer_name=request.user,price_status='Notified').all().order_by('-price_triggeredat')
        notifications2 = CustomerPriceMovementsAlerts.objects.filter(customer_name=request.user,percent_status='Notified').all().order_by('-percent_triggeredat')
        notifications3 = CustomerPriceMovementsAlerts.objects.filter(customer_name=request.user,price52wh_status='Notified').all().order_by('-price52wh_triggeredat')
        notifications4 = CustomerTechnicalIndicationAlerts.objects.filter(customer_name=request.user,rsi_status='Notified').all().order_by('-rsi_triggeredat')
        notifications5 = CustomerTechnicalIndicationAlerts.objects.filter(customer_name=request.user,dma_status='Notified').all().order_by('-dma_triggeredat')
        notifications = list() #Empty List

        # Prive Movement Notifications
        for item in notifications1:
            # Conditions for Price in Rs.
            if item.price_value and item.price_value_condition:
                if item.price_value_condition == 'above':
                    item.condition = 'Last Price >= '+str(item.price_value)
                if item.price_value_condition == 'below':
                    item.condition = 'Last Price <= '+str(item.price_value)
                item.read = item.price_read
                item.triggeredat = item.price_triggeredat
                item.type = 'price'
            notifications.append(item)
        for item in notifications2:
            # Conditions for Price in %
            if item.price_percent and item.price_percent_condition:
                if item.price_percent_condition == 'above':
                    item.condition = 'Last Price Increased above '+str(item.price_percent)+'%'
                if item.price_percent_condition == 'below':
                    item.condition = 'Last Price Decreased below '+str(item.price_percent)+'%'
                item.read = item.percent_read
                item.triggeredat = item.percent_triggeredat
                item.type = 'percent'
            notifications.append(item)
        for item in notifications3:
            # Conditions for 52-week high
            if item.price_52wh_percent and item.price_52wh_condition:
                if item.price_52wh_condition == 'above':
                    item.condition = 'Last Price >= '+str(item.price_52wh_percent)+'% of 52-WH'
                if item.price_52wh_condition == 'below':
                    item.condition = 'Last Price <= '+str(item.price_52wh_percent)+'% of 52-WH'
                item.read = item.price52wh_read
                item.triggeredat = item.price52wh_triggeredat
                item.type = '52wh'
            notifications.append(item)

        # Technical Indication Notifications
        for item in notifications4:
            # Conditions for 14 Day RSI
            if item.rsi_operand1 and item.rsi_condition and item.rsi_metric:
                if item.rsi_condition == 'greaterthan':
                    item.condition = '14 Day RSI >= '+str(item.rsi_operand1)
                if item.rsi_condition == 'lessthan':
                    item.condition = '14 Day RSI <= '+str(item.rsi_operand1)
                item.read = item.rsi_read
                item.triggeredat = item.rsi_triggeredat
                item.type = 'rsi'
            notifications.append(item)
        for item in notifications5:
            # Conditions for DMA
            if item.dma_operand1 and item.dma_operand2 and item.dma_condition:
                if item.dma_condition == 'greaterthan':
                    item.condition = str(re.search(r'\d+', item.dma_operand1).group())+' Day MA >= '+str(re.search(r'\d+', item.dma_operand2).group())+' Day MA'
                if item.dma_condition == 'lessthan':
                    item.condition = str(re.search(r'\d+', item.dma_operand1).group())+' Day MA <= '+str(re.search(r'\d+', item.dma_operand2).group())+' Day MA'
                item.read = item.dma_read
                item.triggeredat = item.dma_triggeredat
                item.type = 'dma'
            notifications.append(item)

        #Making the arrangement for display
        # notifications.sort(key=sortByTriggeredDate, reverse=True)
        return {'notifications':notifications[:10]}  #Displaying only 10 notifications
    else:
        return {'notifications':[]}


def sortByTriggeredDate(item):
    return item.triggeredat


def getUnreadNotifications(request):
    if request.user.is_authenticated:
        count1 = CustomerPriceMovementsAlerts.objects.filter(customer_name=request.user,price_checked='No',price_status='Notified').count()
        count2 = CustomerPriceMovementsAlerts.objects.filter(customer_name=request.user,percent_checked='No',percent_status='Notified').count()
        count3 = CustomerPriceMovementsAlerts.objects.filter(customer_name=request.user,price52wh_checked='No',price52wh_status='Notified').count()
        count4 = CustomerTechnicalIndicationAlerts.objects.filter(customer_name=request.user,rsi_checked='No',rsi_status='Notified').count()
        count5 = CustomerTechnicalIndicationAlerts.objects.filter(customer_name=request.user,dma_checked='No',dma_status='Notified').count()
        total = count1+count2+count3+count4+count5
        subscription = CustomerSubscription.objects.filter(customer_name=request.user,subscription_status='ACTIVE').first()
        plan = None #Passing empty string for subscription, if not subscribed
        if subscription:
            plan = subscription.subscription_type
        return {'count':total,'subscription':plan}
    else:
        return {'count' : 0}

def marketIndices(request):
    df = pd.DataFrame(list(DailyMarketData.objects.all().values('ticker','current_price','net_change','percentage_change').order_by('order')))
    df.columns = ['CompanyName','ClosePrice','Change','PerChange']
    df=df.to_json(orient='records')
    return {'market_indices':df}


    

