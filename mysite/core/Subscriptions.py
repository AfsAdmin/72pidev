# Razorpay 
import razorpay
from mysite.core.models import *
from mysite.core.Forms import *
from django.views.generic import TemplateView, ListView
from django.views import View
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required
from django.utils import timezone
from datetime import datetime, timedelta, date
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.models import User, Group
from django.http import Http404


class PricingDetailsPageView(TemplateView):
    template_name = 'pricingPage.html'
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.request.user.is_authenticated:
            if CustomerSubscription.objects.filter(customer_name=self.request.user).exists():
                context['freesubscription'] = None
            else:
                context['freesubscription'] = 'Yes'
            return context
        else:
            context['freesubscription'] = 'Yes'
            return context

class CustomerFreeSubscriptionView(LoginRequiredMixin,View):

    def get(self, request, *args, **kwargs):
        if CustomerSubscription.objects.filter(customer_name=request.user).exists():
            return render(request, 'CannotTakeSubscription.html')
        else:
            subscriptionForm = CustomerFreeSubscriptionForm(request.GET or None)
            return render(request,'FreeSubscription.html',{'form' : subscriptionForm})

    def post(self, request, *args, **kwargs):
        form = CustomerFreeSubscriptionForm(request.POST)
        if form.is_valid():
            first_name = form.cleaned_data['first_name']
            last_name = form.cleaned_data['last_name']
            email = form.cleaned_data['email']
            contact_number = form.cleaned_data['contact_no']
            if CustomerSubscription.objects.filter(customer_name=request.user).count() == 0:
                from datetime import date
                endsubscription = date.today() + timedelta(days=30)
                startsubscription = date.today()
                CustomerSubscription(customer_name=request.user, first_name=first_name,last_name=last_name, email=email,contact_no=contact_number,
                subscription_type='FREE',subscription_starts=startsubscription,subscription_ends=endsubscription,
                subscription_status='ACTIVE', subscription_plan='30 DAY TRIAL', paid='NO', currency='NA').save()
                return redirect('SubscriptionSuccess')
        else:
            msg = "Something went wrong, please try again!"
            return render(request,'FreeSubscription.html',{'form' : form,'msg':msg})
        
class PaymentSignatureValidationView(LoginRequiredMixin, View):

    def get(self, request, *args, **kwargs):
        return render(request, 'order_failure.html', {'status': 'Payment Faliure!!!'})
    
    def post(self, request, *args, **kwargs):
        response = request.POST
        params_dict = {
            'razorpay_payment_id' : response['razorpay_payment_id'],
            'razorpay_order_id' : response['razorpay_order_id'],
            'razorpay_signature' : response['razorpay_signature']
        }
        client = razorpay.Client(auth=("rzp_live_lbU8W1Qyi9zpKo", "2hymwxMPdwzhl9ekmONzalvA"))
        # Signature Verification
        try:
            status = client.utility.verify_payment_signature(params_dict)
            #Creating subscripton
            plantype = request.session['rplantype']
            currency = request.session['rcurrency']
            subscription_enddate = date.today() #Consider today is the expiry day
            subscription_startdate = date.today()
            if CustomerSubscription.objects.filter(customer_name=request.user).exists():
                if CustomerSubscription.objects.filter(customer_name=request.user, subscription_status='ACTIVE').exists():
                    if CustomerSubscription.objects.filter(customer_name=request.user, subscription_status='UPCOMING').exists():
                        upcoming = CustomerSubscription.objects.filter(customer_name=request.user, subscription_status='UPCOMING').order_by('-subscription_starts').first()
                        subscription_startdate = upcoming.subscription_ends+timedelta(days=1)
                        if plantype == 'QUARTER':
                            subscription_enddate = subscription_startdate + timedelta(days=91)
                        elif plantype == 'ANNUAL':
                            subscription_enddate = subscription_startdate + timedelta(days=364)
                        CustomerSubscription(customer_name=request.user, first_name=request.session['rfirst_name'], last_name=request.session['rlast_name'],
                        email=request.session['remail'], contact_no=request.session['rcontact'],
                        subscription_type=request.session['rtype'], subscription_starts=subscription_startdate,
                        subscription_ends=subscription_enddate,subscription_status='UPCOMING', subscription_plan=plantype,paid='YES',currency=currency).save()
                    else:
                        active = CustomerSubscription.objects.filter(customer_name=request.user, subscription_status='ACTIVE').order_by('-subscription_starts').first()
                        subscription_startdate = active.subscription_ends+timedelta(days=1)
                        if plantype == 'QUARTER':
                            subscription_enddate = subscription_startdate + timedelta(days=91)
                        elif plantype == 'ANNUAL':
                            subscription_enddate = subscription_startdate + timedelta(days=364)
                        CustomerSubscription(customer_name=request.user, first_name=request.session['rfirst_name'], last_name=request.session['rlast_name'],
                        email=request.session['remail'], contact_no=request.session['rcontact'],
                        subscription_type=request.session['rtype'], subscription_starts=subscription_startdate,
                        subscription_ends=subscription_enddate,subscription_status='UPCOMING', subscription_plan=plantype,paid='YES',currency=currency).save()
                else:
                    if CustomerSubscription.objects.filter(customer_name=request.user, subscription_status='UPCOMING').exists():
                        upcoming = CustomerSubscription.objects.filter(customer_name=request.user, subscription_status='UPCOMING').order_by('-subscription_starts').first()
                        subscription_startdate = upcoming.subscription_ends+timedelta(days=1)
                        if plantype == 'QUARTER':
                            subscription_enddate = subscription_startdate + timedelta(days=91)
                        elif plantype == 'ANNUAL':
                            subscription_enddate = subscription_startdate + timedelta(days=364)
                        CustomerSubscription(customer_name=request.user, first_name=request.session['rfirst_name'], last_name=request.session['rlast_name'],
                        email=request.session['remail'], contact_no=request.session['rcontact'],
                        subscription_type=request.session['rtype'], subscription_starts=subscription_startdate,
                        subscription_ends=subscription_enddate,subscription_status='UPCOMING', subscription_plan=plantype,paid='YES',currency=currency).save()
                    else:
                        if plantype == 'QUARTER':
                            subscription_enddate = date.today()+timedelta(days=91)
                        elif plantype == 'ANNUAL':
                            subscription_enddate = date.today()+timedelta(days=364)
                        CustomerSubscription(customer_name=request.user, first_name=request.session['rfirst_name'], last_name=request.session['rlast_name'],
                        email=request.session['remail'], contact_no=request.session['rcontact'],
                        subscription_type=request.session['rtype'], subscription_starts=subscription_startdate,
                        subscription_ends=subscription_enddate,subscription_status='ACTIVE', subscription_plan=plantype,paid='YES',currency=currency).save()
            else:
                # This case not possible mostly
                if plantype == 'QUARTER':
                    subscription_enddate = date.today()+timedelta(days=91)
                elif plantype == 'ANNUAL':
                    subscription_enddate = date.today()+timedelta(days=364)
                CustomerSubscription(customer_name=request.user, first_name=request.session['rfirst_name'], last_name=request.session['rlast_name'],
                email=request.session['remail'], contact_no=request.session['rcontact'],
                subscription_type=request.session['rtype'], subscription_starts=subscription_startdate,
                subscription_ends=subscription_enddate,subscription_status='ACTIVE', subscription_plan=plantype,paid='YES',currency=currency).save()
            return render(request, 'order_success.html', {'endtime': subscription_enddate, 'subscription' : request.session['rtype']})
        except Exception as e:
            return render(request, 'order_failure.html', {'status': 'Payment Faliure!!!'})


class GetCustomerSubscriptionsLogView(LoginRequiredMixin,TemplateView):
    template_name = 'MySubscriptions.html'
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['activesubscriptions'] = CustomerSubscription.objects.filter(customer_name=self.request.user, subscription_status='ACTIVE').all()
        context['upcomingsubscriptions'] = CustomerSubscription.objects.filter(customer_name=self.request.user, subscription_status='UPCOMING').all().order_by('subscription_starts')
        context['expiredsubscriptions'] = CustomerSubscription.objects.filter(customer_name=self.request.user, subscription_status='EXPIRED').all().order_by('subscription_ends')
        return context

class SubscriptionSuccessView(LoginRequiredMixin, TemplateView):
    template_name = 'SubscriptionSuccess.html'
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        subscription = CustomerSubscription.objects.filter(customer_name=self.request.user).first()
        end_subscription = subscription.subscription_ends
        context['end_subscription'] = end_subscription
        return context

class CustomerINRSubscriptionView(LoginRequiredMixin,View):

    def get(self, request, *args, **kwargs):
        if CustomerSubscription.objects.filter(customer_name=request.user).exists():
            subscriber = CustomerSubscription.objects.filter(customer_name=request.user).first()
            subscriptionForm = CustomerINRSubscriptionForm(instance=subscriber)
            return render(request,'SubscriptionINR.html',{'form' : subscriptionForm})
        else:
            subscriptionForm = CustomerINRSubscriptionForm(request.GET or None)
            return render(request,'SubscriptionINR.html',{'form' : subscriptionForm})

    def post(self, request, *args, **kwargs):
        form = CustomerINRSubscriptionForm(request.POST)
        if form.is_valid():
            first_name = form.cleaned_data['first_name']
            last_name = form.cleaned_data['last_name']
            email = form.cleaned_data['email']
            contact_number = form.cleaned_data['contact_no']
            subscription = form.cleaned_data['subscription_type']
            order_amount = 0
            plan_type = ' '
            if subscription == 'premiuminr3':
                order_amount = 4705.84
                plan_type = 'QUARTER'
            elif subscription == 'premiuminr12':
                order_amount = 16992
                plan_type = 'ANNUAL'
            client = razorpay.Client(auth=("rzp_live_lbU8W1Qyi9zpKo", "2hymwxMPdwzhl9ekmONzalvA"))
            order_currency = 'INR'
            # CREAING ORDER
            response = client.order.create(dict(amount=order_amount*100, currency=order_currency, payment_capture='0'))
            request.session['rfirst_name'] = first_name
            request.session['rlast_name'] = last_name
            request.session['remail'] = email
            request.session['rcontact'] = contact_number
            request.session['rtype'] = 'PREMIUM'
            request.session['rplantype'] = plan_type
            request.session['rcurrency'] = order_currency
            order_id = response['id']
            order_status = response['status']
            context = dict()
            if order_status=='created':
                # Server data for user convinience
                context['product_id'] = '72PI Premium Subscription'
                context['price'] = order_amount
                context['currency'] = order_currency
                context['name'] = first_name
                context['phone'] = contact_number
                context['email'] = email
                context['payfor'] = plan_type
                # data that'll be send to the razorpay for
                context['order_id'] = order_id
                return render(request,'ConfirmSubscription.html',context)
            else:
                print("Order not created")
        else:
            msg = "Something went wrong, please try again!"
            return render(request,'SubscriptionINR.html',{'form' : form,'msg':msg})


class CustomerUSDSubscriptionView(LoginRequiredMixin,View):

    def get(self, request, *args, **kwargs):
        if CustomerSubscription.objects.filter(customer_name=request.user).exists():
            subscriber = CustomerSubscription.objects.filter(customer_name=request.user).first()
            subscriptionForm = CustomerUSDSubscriptionForm(instance=subscriber)
            return render(request,'SubscriptionUSD.html',{'form' : subscriptionForm})
        else:
            subscriptionForm = CustomerUSDSubscriptionForm(request.GET or None)
            return render(request,'SubscriptionUSD.html',{'form' : subscriptionForm})

    def post(self, request, *args, **kwargs):
        form = CustomerUSDSubscriptionForm(request.POST)
        if form.is_valid():
            first_name = form.cleaned_data['first_name']
            last_name = form.cleaned_data['last_name']
            email = form.cleaned_data['email']
            contact_number = form.cleaned_data['contact_no']
            subscription = form.cleaned_data['subscription_type']
            order_amount = 0
            plan_type = ' '
            if subscription == 'premiumusd3':
                order_amount = 56
                plan_type = 'QUARTER'
            elif subscription == 'premiumusd12':
                order_amount = 200
                plan_type = 'ANNUAL'
            client = razorpay.Client(auth=("rzp_live_lbU8W1Qyi9zpKo", "2hymwxMPdwzhl9ekmONzalvA"))
            order_currency = 'USD'
            # CREAING ORDER
            response = client.order.create(dict(amount=order_amount*100, currency=order_currency, payment_capture='0'))
            request.session['rfirst_name'] = first_name
            request.session['rlast_name'] = last_name
            request.session['remail'] = email
            request.session['rcontact'] = contact_number
            request.session['rtype'] = 'PREMIUM'
            request.session['rplantype'] = plan_type
            request.session['rcurrency'] = order_currency
            order_id = response['id']
            order_status = response['status']
            context = dict()
            if order_status=='created':
                # Server data for user convinience
                context['product_id'] = '72PI Premium Subscription'
                context['price'] = order_amount
                context['currency'] = order_currency
                context['name'] = first_name
                context['phone'] = contact_number
                context['email'] = email
                context['payfor'] = plan_type
                # data that'll be send to the razorpay for
                context['order_id'] = order_id
                return render(request,'ConfirmSubscription.html',context)
            else:
                print("Order not created")
        else:
            msg = "Something went wrong, please try again!"
            return render(request,'SubscriptionUSD.html',{'form' : form,'msg':msg})