# -*- coding: utf-8 -*-
"""
Created on Wed Apr 29 10:42:42 2020

@author: Jitendra.v
"""


import pyodbc
import pandas as pd
from matplotlib import pyplot as plt
from mysite.core.Appconfigproperties import *


def getMarketIndexCumulativeReturns(IndexTicker,country):
    if country=='India':
        sql_conn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server}; SERVER='+SERVER+'; PORT='+PORT+'; DATABASE='+DATABASE+';UID='+UID+';PWD='+PWD+';')
        Index_Returns=pd.read_sql("select Company,Date,[Return] from Market_Index where company in ('"+ IndexTicker+"') order by date",
                               sql_conn)
        sql_conn.close()
        Index_Returns[IndexTicker] = (Index_Returns['Return']+1).cumprod()-1
        
        Index_Returns=Index_Returns[['Date',IndexTicker]]
    else:
        sql_conn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server}; SERVER='+SERVER+'; PORT='+PORT+'; DATABASE='+DATABASE+';UID='+UID+';PWD='+PWD+';')
        Index_Returns=pd.read_sql("select Company,Date,[Return] from US_Market_Index where company in ('"+ IndexTicker+"') order by date",
                               sql_conn)
        sql_conn.close()
        Index_Returns[IndexTicker] = (Index_Returns['Return']+1).cumprod()-1
        
        Index_Returns=Index_Returns[['Date',IndexTicker]]
        
    return Index_Returns
def StockvsIndexMain(stockName):
    try:
        sql_conn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server}; SERVER='+SERVER+'; PORT='+PORT+'; DATABASE='+DATABASE+';UID='+UID+';PWD='+PWD+';')
        stocks=pd.read_sql("select [FS Name] from security_master where [FS Name] in ('"+stockName.replace("'","''") + "')",sql_conn)
        sql_conn.close()
        country="India"
        if len(stocks)==0:
            country="US"
        if country=="India":
            sql_conn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server}; SERVER='+SERVER+'; PORT='+PORT+'; DATABASE='+DATABASE+';UID='+UID+';PWD='+PWD+';')
            stock_data=pd.read_sql("select Company,Date,[Return] from universe_prices_main where company in ('" + stockName.replace("'","''")+"') order by date",
                                sql_conn)
            sql_conn.close()
        else:
            sql_conn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server}; SERVER='+SERVER+'; PORT='+PORT+'; DATABASE='+DATABASE+';UID='+UID+';PWD='+PWD+';')
            stock_data=pd.read_sql("select Company,Date,[Return] from us_universe_prices_main where company in ('" + stockName.replace("'","''")+"') order by date",
                                sql_conn)
            sql_conn.close()
        if (len(stock_data)!=0):
            stock_data[stockName] = (stock_data['Return']+1).cumprod()-1
            stock_data=stock_data[['Date',stockName]]
            indian_benchmarks=['Nifty 50 Index','Nifty Smallcap 100 Index','Nifty Midcap 100 Index']
            us_benchmarks=['S&P 500','Dow Jones Industrial Average','Russell 2000']
            if country=="India":
                Nifty_50_Returns=getMarketIndexCumulativeReturns(indian_benchmarks[0],country)
                Nifty_Smallcap_Returns=getMarketIndexCumulativeReturns(indian_benchmarks[1],country)
                Nifty_Midcap_Returns=getMarketIndexCumulativeReturns(indian_benchmarks[2],country)        
                output=pd.merge(stock_data,Nifty_50_Returns,on="Date",how="inner")
                output=pd.merge(output,Nifty_Smallcap_Returns,on="Date",how="inner")
                output=pd.merge(output,Nifty_Midcap_Returns,on="Date",how="inner")
                output.fillna(0,inplace=True)
            else:
                spxt_Returns=getMarketIndexCumulativeReturns(us_benchmarks[0],country)
                dowjones_Returns=getMarketIndexCumulativeReturns(us_benchmarks[1],country)
                rusell_Returns=getMarketIndexCumulativeReturns(us_benchmarks[2],country)        
                output=pd.merge(stock_data,spxt_Returns,on="Date",how="inner")
                output=pd.merge(output,dowjones_Returns,on="Date",how="inner")
                output=pd.merge(output,rusell_Returns,on="Date",how="inner")
                output.fillna(0,inplace=True)
            return output
        else:
            return pd.DataFrame()
    except Exception as e:
        return pd.DataFrame()
