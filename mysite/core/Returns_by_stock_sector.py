import pandas as pd
import datetime
from datetime import date, timedelta
import pyodbc
import math
def New_CurrentPositionsMain(start_date,current_date,Daily_Returns,Stock_Data,gics_data,sector_data,perType):
    try:
        if perType=='YTD':
            start_date=current_date.replace(month=1,day=1)
        elif perType=='MTD':
            start_date = current_date.replace(day=1)
        elif perType=='QTD':
            current_quarter = math.ceil(current_date.month/3)
            if current_quarter ==1:
                start_date  = datetime.date(current_date.year,1,1)
            elif current_quarter==2:
                start_date  = datetime.date(current_date.year,4,1)
            elif current_quarter==3:
                start_date  = datetime.date(current_date.year,7,1)
            else:
                start_date  = datetime.date(current_date.year,10,1)
        elif perType!='ITD':
            start_date = current_date+timedelta(-365)

        Daily_Returns=Daily_Returns[(Daily_Returns['Date']>=start_date) & (Daily_Returns['Date']<=current_date)]
        Stock_Data = Stock_Data[['Company', 'Factset_Ticker','Quantity','GICS']]
        Daily_Returns=pd.merge(Daily_Returns,Stock_Data,on=['Factset_Ticker','Company'],how="inner")
        Daily_Returns['InvestmentAmount'] = Daily_Returns ['Price'] * Daily_Returns['Quantity']
        input_data = Daily_Returns.copy()
        current_investment_amount= input_data.sort_values('Date', ascending=False).groupby(['Company','Factset_Ticker'], as_index=False).first()
        current_investment_amount=current_investment_amount[['Company','Factset_Ticker','GICS','InvestmentAmount']]
        current_investment_amount['InvestmentAmount%']=current_investment_amount['InvestmentAmount']/current_investment_amount['InvestmentAmount'].sum()
        investment_amount=current_investment_amount

        current_actual_data=input_data.sort_values('Date', ascending=False).groupby(['Company','Factset_Ticker'], as_index=False).first()
        current_actual_data=current_actual_data[['Company','Factset_Ticker','Date','InvestmentAmount','Price']]
        current_actual_data.columns=['Company','Factset_Ticker','CurrentDate','CurrentInvestmentAmount','CurrentPrice']

        start_actual_data=input_data.sort_values('Date', ascending=True).groupby(['Company','Factset_Ticker'], as_index=False).first()
        start_actual_data=start_actual_data[['Company','Factset_Ticker','Date','InvestmentAmount','Price']]
        start_actual_data.columns=['Company','Factset_Ticker','StartDate','StartInvestmentAmount','StartPrice']    

        actual_returns=pd.merge(current_actual_data,start_actual_data,on=['Company','Factset_Ticker'])   
        actual_returns_value=actual_returns.copy()

        actual_returns['ActualReturn_%']= (actual_returns['CurrentPrice'] -  actual_returns['StartPrice'])/actual_returns['StartPrice']
        actual_returns=actual_returns[['Company','Factset_Ticker','ActualReturn_%']]
        actual_returns_value['ActualReturn_$']=actual_returns_value['CurrentInvestmentAmount']-actual_returns_value['StartInvestmentAmount']
        actual_returns_value=actual_returns_value[['Company','Factset_Ticker','ActualReturn_$','StartInvestmentAmount']]    
        final_results=pd.merge(pd.merge(investment_amount,actual_returns,on=['Factset_Ticker','Company']),actual_returns_value,on=['Company','Factset_Ticker'])
        
        gics_final_results = final_results.groupby('GICS')['InvestmentAmount','InvestmentAmount%','StartInvestmentAmount','ActualReturn_$'].sum().reset_index()
        gics_final_results['ActualReturn_%']=gics_final_results['ActualReturn_$']/gics_final_results['StartInvestmentAmount']
        final_results=final_results[['Company','Factset_Ticker','InvestmentAmount','ActualReturn_%','ActualReturn_$']]
        final_results.columns=['Stock','Factset_Ticker','Current Exposure',perType + '_Return',perType + '_PNL']   
        gics_final_results=gics_final_results[['GICS','InvestmentAmount','ActualReturn_%','ActualReturn_$']]
        gics_final_results.columns=['GICS','Current Exposure',perType + '_Return',perType + '_PNL']
        return final_results,gics_final_results
    except Exception as e:
        return pd.DataFrame(),pd.DataFrame()
