# -*- coding: utf-8 -*-
"""
Created on Wed Jan  8 16:28:08 2020

@author: Shiva
"""

import pandas as pd
import numpy as np
import pyodbc
import math
from .models import UniversePricesMain, UsUniversePricesMain
def efficientFrontier(Portfolio,returns,portfolio_name,country):
    try:
        rfr = 0.065
        stock = set(Portfolio["Factset_Ticker"])
        returns = returns[['Date', 'Factset_Ticker','Company', 'Return']]
        returns["Date"] = pd.to_datetime(returns["Date"] )
        returns["Return"] = pd.to_numeric(returns["Return"] , errors='coerce')
        returns.fillna(0,inplace=True)
        Portfolio["Average_Return"]=""
        Portfolio["Annualized_Return"]=""
        Portfolio["Standard_Deviation"]=""
        Portfolio["Count"]=""
        for i in range(0,len(Portfolio)):
            Sub_returns=returns[returns["Factset_Ticker"]==Portfolio.loc[i,"Factset_Ticker"]]
            Sub_returns.reset_index(drop=True,inplace=True)
            Portfolio.loc[i,"Count"]=len(Sub_returns['Return'])
            Portfolio.loc[i,"Average_Return"]=Sub_returns['Return'].mean()
            Portfolio.loc[i,"Annualized_Return"]=Portfolio.loc[i,"Average_Return"]*252
            Portfolio.loc[i,"Standard_Deviation"]=Sub_returns['Return'].std()*math.sqrt(252)
        ##Covariance
        returns_trans=returns.pivot(index='Date',columns='Factset_Ticker')["Return"]
        covar_matrix=returns_trans.cov()*252
        covar=np.array(covar_matrix.values)
        Port_df = Portfolio[["Company","Factset_Ticker","Annualized_Return"]]
        final_ret=pd.DataFrame()        
        stdev=[]
        weights_list = []
        j=0
        while j<=18000:
            if j==0:
                weights=list(Portfolio["Weight"])
            else:
                rand_samp = np.random.dirichlet(np.ones(len(Port_df)),size=1).flatten()
                weights=list(rand_samp)
            Weight_Matrix=np.asarray(weights)
            st_dev = math.sqrt(np.matmul(np.matmul(Weight_Matrix.T,covar),Weight_Matrix))
            stdev.append(st_dev)
            weights_list.append(Weight_Matrix) 
            j+=1
        weights_list_df = pd.DataFrame(weights_list)
        stacked = weights_list_df.stack()
        final_ret = pd.DataFrame(stacked)
        final_ret.columns = ["Weight"]
        final_ret["Num"] = final_ret.index
        final_ret['Ind'] = final_ret['Num'].astype(str).str.split(',').str[0]
        final_ret['Ind'] =  final_ret.Ind.str.replace('(','')
        final_ret["Num"] =  final_ret['Num'].astype(str).str.split(',').str[1]
        final_ret['Num'] =  final_ret.Num.str.replace(')','')
        final_ret['Num'] =  final_ret.Num.str.replace(' ','')
        stdev_df = pd.DataFrame(stdev)
        stdev_df.columns = ["Std_Dev"]
        stdev_df["Ind"] = stdev_df.index
        stdev_df["Ind"] = stdev_df['Ind'].astype(str)
        final_ret = pd.merge(final_ret, stdev_df, how ='left', on ='Ind') 
        Port_df["Num"] = Port_df.index
        Port_df["Num"] = Port_df['Num'].astype(str)
        final_ret = pd.merge(final_ret, Port_df, how ='left')
    
        final_ret["Expected_Return"] = final_ret["Annualized_Return"]*final_ret["Weight"]
    
        new_data = final_ret.groupby('Ind')['Expected_Return'].sum()
        new_data = pd.DataFrame(new_data)
        new_data["Ind"] = new_data.index
        new_data.reset_index(drop=True,inplace=True)
        final_ret = final_ret.drop(['Expected_Return'], axis=1)
        final_ret = pd.merge(final_ret,new_data,how='left')
        
        final_ret["Sharpe_Ratio"] = (final_ret["Expected_Return"]-rfr)/final_ret["Std_Dev"]
        final_ret = final_ret[["Company","Factset_Ticker","Annualized_Return","Weight","Expected_Return","Std_Dev","Sharpe_Ratio","Ind"]]
        final_ret.columns = ["Company","Factset_Ticker","Annualized_Return","Weight","Expected_Return","Std_Dev","Sharpe_Ratio","Iter"]
                
        final_ret.sort_values("Sharpe_Ratio", axis = 0, ascending = False, inplace = True, na_position ='last')
        
        ret_df1 = final_ret[["Expected_Return","Sharpe_Ratio","Iter"]]
        ret_df1 = ret_df1.drop_duplicates(subset=["Expected_Return"],keep='first')
        
        ret_df=ret_df1[ret_df1["Iter"]!="0"]  
        
        ret_df["Expected_Return"]= round(ret_df["Expected_Return"]*100,1)
        ret_df = ret_df.drop_duplicates(subset=["Expected_Return"],keep='first')
        ret_df=ret_df.append(ret_df1[ret_df1["Iter"]=="0"])
    
        
        ret_df.sort_values("Expected_Return", axis = 0, ascending = True, inplace = True, na_position ='last')
        
        ret_df = ret_df[["Iter"]]
        ret_df.reset_index(drop=True,inplace=True)
        ret_df["Order"] = ret_df.index
        req_ret = pd.merge(ret_df, final_ret, how ='left')
        req_ret = req_ret[["Company","Factset_Ticker","Annualized_Return","Weight","Expected_Return","Std_Dev","Sharpe_Ratio","Iter","Order"]]
        return req_ret
    except Exception as e:
        print(e)
        emptyDF=pd.DataFrame()
        return emptyDF

