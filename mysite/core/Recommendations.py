import pandas as pd
import pyodbc
from .models import CustomerPortfolioDetails, UniversePricesMain, SecurityMaster, UsUniversePricesMain, UsSecurityMaster
from django.db.models import Max

def idealConstraints(user,portfolio_name,current_country,portfolio_data):
  try:
    universe_model = UniversePricesMain
    security_model = SecurityMaster
    if current_country=='US':
      universe_model = UsUniversePricesMain
      security_model = UsSecurityMaster
    stocks=portfolio_data['Factset_Ticker'].tolist()
    prices_data_query=universe_model.objects.filter(factset_ticker__in=stocks,date= universe_model.objects.all().aggregate(Max('date'))['date__max']).values('company','factset_ticker','price')
    prices_data=pd.DataFrame(list(prices_data_query))
    prices_data.columns=['Company','Factset_Ticker','Price']
    #stock wise exposure
    prices_data = pd.merge(portfolio_data,prices_data,on=['Company','Factset_Ticker'],how='inner')
    prices_data['Exposure']=prices_data['Quantity']*prices_data['Price']
    prices_data['Exposure%']=prices_data['Exposure']/(prices_data['Exposure'].sum())
    stock_wise_exposure = prices_data[['Company','Factset_Ticker','Exposure%']]
    stock_wise_exposure.sort_values(by=['Exposure%'],ascending=False,inplace=True)
    stock_wise_exposure.reset_index(drop=True,inplace=True)
    stock_wise_exposure['Exposure%'] = stock_wise_exposure['Exposure%']*100
    gics_query=security_model.objects.filter(fs_ticker__in= stocks).values('fs_name','fs_ticker','gics','security_code')
    gics_data = pd.DataFrame(list(gics_query))
    gics_data.columns = ['Company','Factset_Ticker','GICS','Security_Code']

    stock_wise_exposure = pd.merge(stock_wise_exposure,gics_data[['Factset_Ticker','Security_Code']],how='inner',on='Factset_Ticker')
    stock_wise_exposure ['Company'] = stock_wise_exposure['Company'] + " (" + stock_wise_exposure['Security_Code'] + ")"

    gics_data =pd.merge(prices_data,gics_data,on=['Company','Factset_Ticker'],how='inner')

    #gics_wise_exposure=gics_data.groupby(['GICS'])["Exposure"].agg('sum').reset_index()   
    gics_wise_exposure = gics_data.groupby(by=['GICS'])['Exposure'].sum().reset_index()
    gics_wise_exposure.columns=['GICS','Exposure']
    gics_wise_exposure['Exposure%']=gics_wise_exposure['Exposure']/(gics_wise_exposure['Exposure'].sum())
    gics_wise_exposure.sort_values(by=['Exposure%'],ascending=False,inplace=True)
    gics_wise_exposure.reset_index(drop=True,inplace=True)
    gics_wise_exposure['Exposure%'] = gics_wise_exposure['Exposure%']*100

    #calculation
    stocks = len(portfolio_data)
    max_stock_holding = stock_wise_exposure.loc[0,'Exposure%'].round(2)
    max_stock =  stock_wise_exposure.loc[0,'Company']
    if stocks <15:
      stock_result="Increase"
    elif stocks >=15 and stocks <=30:
      stock_result ="Ok"
    else:
      stock_result ='Decrease'
    
    #max stock holding  
    if max_stock_holding < 15:
      max_stock_holding_result = "Ok"
    else:
      max_stock_holding_result ="Decrease"
    
    #top3 stocks exposure sum
    if len(stock_wise_exposure)>2:
      top3_stocks_sum = stock_wise_exposure.loc[0:2,'Exposure%'].sum().round(2)
      top3_stocks = stock_wise_exposure.loc[0:2,['Company','Exposure%']]
    else:
      top3_stocks_sum = stock_wise_exposure.loc[0:len(stock_wise_exposure)-1,'Exposure%'].sum().round(2)
      top3_stocks = stock_wise_exposure.loc[0:len(stock_wise_exposure)-1,['Company','Exposure%']]
    if top3_stocks_sum <40:
      top3_stocks_sum_result = "Ok"
    else:
      top3_stocks_sum_result = "Decrease"
      
    #top exposure sector
    top_exposure_sector = gics_wise_exposure.loc[0,'Exposure%'].round(2)
    top_sector = gics_wise_exposure.loc[0,'GICS']
    if top_exposure_sector < 30:
      top_exposure_sector_result = 'Ok'
    else:
      top_exposure_sector_result = "Decrease"
      
    case1 ="You have "+str(stocks) +" stocks in your portfolio"
    case2 = max_stock+" is the stock with highest investment amount ("+str(max_stock_holding)+"%) in your portfolio"
    if len(top3_stocks)>2:
      case3 = top3_stocks.loc[0,'Company']+"("+str(top3_stocks.loc[0,'Exposure%'].round(2))+"%), "+top3_stocks.loc[1,'Company']+"("+str(top3_stocks.loc[1,'Exposure%'].round(2))+"%), "+top3_stocks.loc[2,'Company']+"("+str(top3_stocks.loc[2,'Exposure%'].round(2))+"%)."
    elif len(top3_stocks)==2:
      case3 = top3_stocks.loc[0,'Company']+"("+str(top3_stocks.loc[0,'Exposure%'].round(2))+"%), "+top3_stocks.loc[1,'Company']+"("+str(top3_stocks.loc[1,'Exposure%'].round(2))+"%)."
    elif len(top3_stocks)==1:
      case3 = top3_stocks.loc[0,'Company']+"("+str(top3_stocks.loc[0,'Exposure%'].round(2))+"%)."

    case4 = "Your portfolio has highest investment amount in "+ top_sector+"("+str(top_exposure_sector)+"%) "

    
    #output
    output_data=pd.DataFrame()
    output_data['Case']=['# of Stocks','Top Stock by Investment Amount','Top 3 Stocks holding  %','Top Sector by Investment Amount']
    output_data['Value']=[stocks,str(max_stock_holding)+'%', str(top3_stocks_sum)+"%", str(top_exposure_sector)+"%"]
    output_data['Ideal Range']=['15-30','Below 15%','Below 40%','Below 30%']
    output_data['Action'] = [stock_result,max_stock_holding_result,top3_stocks_sum_result,top_exposure_sector_result]
    output_data['Comment'] = [case1 , case2,case3,case4]
    return output_data
  except Exception as e:
    return pd.DataFrame()

