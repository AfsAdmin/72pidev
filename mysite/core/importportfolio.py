# -*- coding: utf-8 -*-
"""
Created on Thu Apr 16 17:20:00 2020

@author: sai.shiva
"""

import pandas as pd
import pyodbc
from .models import MoneyControlMapping,BseMapping

def importportfolio(selected_option,template1):
    required_fields=pd.DataFrame()
    stocklist=template1['Stock Name'].tolist()
    if selected_option=='MoneyControl':

        mapping=pd.DataFrame(list(MoneyControlMapping.objects.all().values('money_control_name','security_name','fs_name')))
        mapping.columns=['Money_Control_Name','Security Name','FS Name']
        if len(template1.columns)==15:
            template1['Stock Name'] = template1['Stock Name'].str.rstrip()
            l=pd.merge(mapping,template1,how='inner',left_on='Money_Control_Name',right_on='Stock Name')
            required_fields['Company']=l['FS Name']
            required_fields['Quantity']=l['Quantity']

        else:
            # template1.drop(['Broker','Notes'],axis=1,inplace=True)
            a=template1['Stock Name'].str.split(' - ',expand=True)
            template1['Stock Name']=a[0]
            k=pd.merge(mapping,template1,how='inner',left_on='Money_Control_Name',right_on='Stock Name')
            # k['Inv. Date']=pd.to_datetime(k['Inv. Date'], format='%d-%m-%Y').astype(str)
            required_fields['Company']=k['FS Name']
            required_fields['Quantity']=k['Quantity']

    elif selected_option=='NSE':
        BSE_mapping=pd.DataFrame(list(BseMapping.objects.all().values()))
        BSE_mapping.columns=['ISIN No','Security Name','FS Name','BSEIndia Name','S_No']
        bse=pd.merge(BSE_mapping,template1,how='inner',left_on='BSEIndia Name',right_on='Stock Name')
        required_fields['Company']=bse['FS Name']
        required_fields['Quantity']=bse['Quantity']

    return required_fields
        
            

        
        

    



 
    
    
    