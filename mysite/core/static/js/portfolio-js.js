$(document).ready(function(){
	$('[data-toggle="tooltip"]').tooltip();
	var actions = $("table td:last-child").html();
	// Append table with add row form on add new button click
    $(".add-new").click(function(){
        $('thead').show();
        $('#save_div').show();
		$(this).attr("disabled", "disabled");
		var index = $("table tbody tr:last-child").index();
        var row = '<tr>' +
            '<td class="text-center"><select  name="company" class="mdb-select md-form colorful-select dropdown-primary">'+
            '<option default>Select Company</option>'+
            '{% for universe_price in universe_prices %}'+
            '<option value={{universe_price.company}}>{{universe_price.company}}</option>'+
            '{% endfor %}'+
            '</select><span id="err1"></span></td>' +
            '<td class="text-center"><input type="text" class="inputquantity" name="quantity" placeholder="Enter Quantity" autocomplete="off"><span id="err2"></span></td>' +
            '<td class="text-center"><input  type="date" name="purchaseddate" placeholder="Enter Date"><span id="err3"></span></td>' +
            '<td><input  type="text" id="price" class="inputprice" name="price" placeholder="Enter/Get Price"  autocomplete="off">&nbsp;<button type="button" class="btn btn-sm btn-primary getprice">Get Price</button><br><span id="err4"></td>' +
			'<td class="text-center"> <a class="delete" title="Delete" data-toggle="tooltip"><i class="fa fa-trash" style="color:red"></i></a></td> '+
        '</tr>';

        // $("table").append(row);
        $('table > tbody:last').append(row);
		$("table tbody tr").eq(index + 1).find(".add, .edit").toggle();
        $('[data-toggle="tooltip"]').tooltip();
        $(".add-new").removeAttr("disabled");
    });
	// Add row on add button click
	$(document).on("click", ".add", function(){
		var empty = false;
		var input = $(this).parents("tr").find('input[type="text"]');
        input.each(function(){
			if(!$(this).val()){
				$(this).addClass("error");
				empty = true;
			} else{
                $(this).removeClass("error");
            }
		});
		$(this).parents("tr").find(".error").first().focus();
		if(!empty){
			input.each(function(){
				$(this).parent("td").html($(this).val());
			});			
			$(this).parents("tr").find(".add, .edit").toggle();
			$(".add-new").removeAttr("disabled");
		}		
    });
	// Edit row on edit button click
	$(document).on("click", ".edit", function(){		
        $(this).parents("tr").find("td:not(:last-child)").each(function(){
			$(this).html('<input type="text" class="form-control" value="' + $(this).text() + '">');
		});		
        $(this).parents("tr").find("td:not(:last-child)").each(function(){
			$(this).parents("tr");
		});	
		$(this).parents("tr").find(".add, .edit").toggle();
		$(".add-new").attr("disabled", "disabled");
    });
	// Delete row on delete button click
    $(document).on("click", ".delete", function(){
        $(this).parents("tr").remove();
		$(".add-new").removeAttr("disabled");
        }); 

    // Get Price based on company and trade date
    $(document).on("click", ".getprice", function(){
    var instance=jQuery(this);
    var inputDate = $(this).parents("tr").find('input[type="date"]').val()  ;
    var inputCompany = $(this).parents("tr").find("option:selected").text();
    if(!(inputCompany == "Select Company") || !(inputDate == "")){document.getElementById("err1").innerHTML="";document.getElementById("err3").innerHTML="";}
    if(inputCompany == "Select Company" || inputDate == ""){
    if(inputCompany == "Select Company")document.getElementById("err1").innerHTML="Company is required!";else document.getElementById("err1").innerHTML="";
    if(inputDate == "")document.getElementById("err3").innerHTML="Trading Date is required!";else document.getElementById("err3").innerHTML="";}else{
     $.ajax({
                    type: "POST",
                    url: "{% url 'Getstockprice'%}",
                    dataType : 'json',
                    csrfmiddlewaretoken: "{{ csrf_token }}",
                    data: {
                            'company': inputCompany,
                            'date' : inputDate, 
                    },
                    success:function (json) {
                        instance.parents("tr").find('[name="price"]').val(json.price);
                    },
                });}
            event.preventDefault(); 
    });
});

 // Input field "Quantity" validation
 $(document).on("keypress click input", ".inputquantity", function () {
              if(/^\+?(0|[1-9]\d*)$/.test($(this).val()))
              document.getElementById("err2").innerHTML=""
              else if($(this).val() == "")
              document.getElementById("err2").innerHTML=""
              else
              document.getElementById("err2").innerHTML="Not a valid quantity."
              
});

// INput field "Price" validation
 $(document).on("keypress click input", ".inputprice", function () {
              if(/^[0-9]+([,.][0-9]+)?$/g.test($(this).val()))
              document.getElementById("err4").innerHTML=""
              else if($(this).val() == "")
              document.getElementById("err4").innerHTML=""
              else
              document.getElementById("err4").innerHTML="Not a valid price."
              
});


// Saving the stock details
 $(document).on("click", ".submit", function(){
    var newFormData=[];
    jQuery('#table tr:not(:first)').each(function(i){
       	var tb=jQuery(this);
        var obj={};
        tb.find('select').each(function(){
             obj[this.name]=$(this).find("option:selected").text();
        });
        tb.find('input').each(function(){
           obj[this.name]=this.value;
        });
        
        newFormData.push(obj);
    });
             $.ajax({
                    type: "POST",
                    url: "{% url 'SavePortfolioData'%}",
                    dataType : 'json',
                    csrfmiddlewaretoken: "{{ csrf_token }}",
                    data: {
                            'result': JSON.stringify(newFormData) 
                    },
                    success:function (json) {
                        if(json.res_Msg == "Please fill all the fileds.")
                        document.getElementById("response").innerHTML=json.res_Msg;
                        else{document.getElementById("response").innerHTML=json.res_Msg;
                        setTimeout(function(){
                                    window.location = "{% url 'indexpage'%}";
                                    },1000);}
                        
                    },
                    error:function (json){
                        document.getElementById("response").innerHTML=json.res_Msg;
                    }
                });
            event.preventDefault(); 
    
    });