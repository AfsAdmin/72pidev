!(function (e, o) {
  "use strict";
  var t,
    n,
    s,
    i,
    a,
    l,
    r,
    h,
    c,
    d,
    u,
    p,
    f,
    v,
    m,
    b,
    $,
    g =
      ((t = "sf-breadcrumb"),
      (n = "sf-js-enabled"),
      (s = "sf-with-ul"),
      (i = "sf-arrows"),
      (a = (function () {
        var o = /^(?![\w\W]*Windows Phone)[\w\W]*(iPhone|iPad|iPod)/i.test(
          navigator.userAgent
        );
        return o && e("html").css("cursor", "pointer").on("click", e.noop), o;
      })()),
      (l = (function () {
        var e = document.documentElement.style;
        return (
          "behavior" in e &&
          "fill" in e &&
          /iemobile/i.test(navigator.userAgent)
        );
      })()),
      (r = !!o.PointerEvent),
      (h = function (e, o, t) {
        var s = n;
        o.cssArrows && (s += " " + i), e[t ? "addClass" : "removeClass"](s);
      }),
      (c = function (e, o) {
        var t = o ? "addClass" : "removeClass";
        e.children("a")[t](s);
      }),
      (d = function (e) {
        var o = e.css("ms-touch-action"),
          t = e.css("touch-action");
        (t = "pan-y" === (t = t || o) ? "auto" : "pan-y"),
          e.css({
            "ms-touch-action": t,
            "touch-action": t,
          });
      }),
      (u = function (e) {
        return e.closest("." + n);
      }),
      (p = function (e) {
        return u(e).data("sfOptions");
      }),
      (f = function () {
        var o = e(this),
          t = p(o);
        clearTimeout(t.sfTimer),
          o.siblings().superfish("hide").end().superfish("show");
      }),
      (v = function (o) {
        (o.retainPath = e.inArray(this[0], o.$path) > -1),
          this.superfish("hide"),
          this.parents("." + o.hoverClass).length ||
            (o.onIdle.call(u(this)), o.$path.length && e.proxy(f, o.$path)());
      }),
      (m = function () {
        var o = e(this),
          t = p(o);
        a
          ? e.proxy(v, o, t)()
          : (clearTimeout(t.sfTimer),
            (t.sfTimer = setTimeout(e.proxy(v, o, t), t.delay)));
      }),
      (b = function (o) {
        var t = e(this),
          n = p(t),
          s = t.siblings(o.data.popUpSelector);
        return !1 === n.onHandleTouch.call(s)
          ? this
          : void (
              s.length > 0 &&
              s.is(":hidden") &&
              (t.one("click.superfish", !1),
              "MSPointerDown" === o.type || "pointerdown" === o.type
                ? t.trigger("focus")
                : e.proxy(f, t.parent("li"))())
            );
      }),
      ($ = function (o, t) {
        var n = "li:has(" + t.popUpSelector + ")";
        e.fn.hoverIntent && !t.disableHI
          ? o.hoverIntent(f, m, n)
          : o.on("mouseenter.superfish", n, f).on("mouseleave.superfish", n, m);
        var s = "MSPointerDown.superfish";
        r && (s = "pointerdown.superfish"),
          a || (s += " touchend.superfish"),
          l && (s += " mousedown.superfish"),
          o
            .on("focusin.superfish", "li", f)
            .on("focusout.superfish", "li", m)
            .on(s, "a", t, b);
      }),
      {
        hide: function (o) {
          if (this.length) {
            var t = p(this);
            if (!t) return this;
            var n = !0 === t.retainPath ? t.$path : "",
              s = this.find("li." + t.hoverClass)
                .add(this)
                .not(n)
                .removeClass(t.hoverClass)
                .children(t.popUpSelector),
              i = t.speedOut;
            if (
              (o && (s.show(), (i = 0)),
              (t.retainPath = !1),
              !1 === t.onBeforeHide.call(s))
            )
              return this;
            s.stop(!0, !0).animate(t.animationOut, i, function () {
              var o = e(this);
              t.onHide.call(o);
            });
          }
          return this;
        },
        show: function () {
          var e = p(this);
          if (!e) return this;
          var o = this.addClass(e.hoverClass).children(e.popUpSelector);
          return !1 === e.onBeforeShow.call(o)
            ? this
            : (o.stop(!0, !0).animate(e.animation, e.speed, function () {
                e.onShow.call(o);
              }),
              this);
        },
        destroy: function () {
          return this.each(function () {
            var o,
              n = e(this),
              s = n.data("sfOptions");
            return (
              !!s &&
              ((o = n.find(s.popUpSelector).parent("li")),
              clearTimeout(s.sfTimer),
              h(n, s),
              c(o),
              d(n),
              n.off(".superfish").off(".hoverIntent"),
              o.children(s.popUpSelector).attr("style", function (e, o) {
                return o.replace(/display[^;]+;?/g, "");
              }),
              s.$path.removeClass(s.hoverClass + " " + t).addClass(s.pathClass),
              n.find("." + s.hoverClass).removeClass(s.hoverClass),
              s.onDestroy.call(n),
              void n.removeData("sfOptions"))
            );
          });
        },
        init: function (o) {
          return this.each(function () {
            var n = e(this);
            if (n.data("sfOptions")) return !1;
            var s = e.extend({}, e.fn.superfish.defaults, o),
              i = n.find(s.popUpSelector).parent("li");
            (s.$path = (function (o, n) {
              return o
                .find("li." + n.pathClass)
                .slice(0, n.pathLevels)
                .addClass(n.hoverClass + " " + t)
                .filter(function () {
                  return e(this).children(n.popUpSelector).hide().show().length;
                })
                .removeClass(n.pathClass);
            })(n, s)),
              n.data("sfOptions", s),
              h(n, s, !0),
              c(i, !0),
              d(n),
              $(n, s),
              i.not("." + t).superfish("hide", !0),
              s.onInit.call(this);
          });
        },
      });
  (e.fn.superfish = function (o, t) {
    return g[o]
      ? g[o].apply(this, Array.prototype.slice.call(arguments, 1))
      : "object" != typeof o && o
      ? e.error("Method " + o + " does not exist on jQuery.fn.superfish")
      : g.init.apply(this, arguments);
  }),
    (e.fn.superfish.defaults = {
      popUpSelector: "ul,.sf-mega",
      hoverClass: "sfHover",
      pathClass: "overrideThisToUse",
      pathLevels: 1,
      delay: 800,
      animation: {
        opacity: "show",
      },
      animationOut: {
        opacity: "hide",
      },
      speed: "normal",
      speedOut: "fast",
      cssArrows: !0,
      disableHI: !1,
      onInit: e.noop,
      onBeforeShow: e.noop,
      onShow: e.noop,
      onBeforeHide: e.noop,
      onHide: e.noop,
      onIdle: e.noop,
      onDestroy: e.noop,
      onHandleTouch: e.noop,
    });
})(jQuery, window),
  $(document).ready(function () {
    "use strict";
    $(window).width();
    var e = window.innerHeight,
      o = $(".default-header").height(),
      t = ($(".site-header.static").outerHeight(), e - o);
    if (
      ($(".fullscreen").css("height", 550),
      $(".fitscreen").css("height", t),
      document.getElementById("default-select") && $("select").niceSelect(),
      document.getElementById("service-select") && $("select").niceSelect(),
    
      $("#nav-menu-container").length)
    ) {
      var n = $("#nav-menu-container").clone().prop({
        id: "mobile-nav",
      });
      n.find("> ul").attr({
        class: "",
        id: "",
      }),
        $("body").append(n),
        $("body").prepend(
          '<button type="button" id="mobile-nav-toggle"><i class="fa fa-bars"></i></button>'
        ),
        $("body").append('<div id="mobile-body-overly"></div>'),
        $("#mobile-nav")
          .find(".menu-has-children")
          .prepend('<i class="fa fa-chevron-down"></i>'),
        $(document).on("click", ".menu-has-children i", function (e) {
          $(this).next().toggleClass("menu-item-active"),
            $(this).nextAll("ul").eq(0).slideToggle(),
            $(this).toggleClass("fa-chevron-up fa-chevron-down");
        }),
        $(document).on("click", "#mobile-nav-toggle", function (e) {
          $("body").toggleClass("mobile-nav-active"),
            $("#mobile-nav-toggle i").toggleClass("fa-times fa-bars"),
            $("#mobile-body-overly").toggle(),
            $("#mobile-nav").html("");
          var o = $("body").find(".dummydiv").clone();
          o.css("display", "block"), $("#mobile-nav").html(o);
        }),
        $(document).click(function (e) {
          var o = $("#mobile-nav, #mobile-nav-toggle");
          o.is(e.target) ||
            0 !== o.has(e.target).length ||
            ($("body").hasClass("mobile-nav-active") &&
              ($("body").removeClass("mobile-nav-active"),
              $("#mobile-nav-toggle i").toggleClass("fa-times fa-bars"),
              $("#mobile-body-overly").fadeOut()));
        });
    } else
      $("#mobile-nav, #mobile-nav-toggle").length &&
        $("#mobile-nav, #mobile-nav-toggle").hide();
    $(".nav-menu a, #mobile-nav a, .scrollto").on("click", function () {
      if (
        location.pathname.replace(/^\//, "") ==
          this.pathname.replace(/^\//, "") &&
        location.hostname == this.hostname
      ) {
        var e = $(this.hash);
        if (e.length) {
          var o = 0;
          return (
            $("#header").length &&
              ((o = $("#header").outerHeight()),
              $("#header").hasClass("header-fixed") || (o = o)),
            $("html, body").animate(
              {
                scrollTop: e.offset().top - o,
              },
              1500,
              "easeInOutExpo"
            ),
            $(this).parents(".nav-menu").length &&
              ($(".nav-menu .menu-active").removeClass("menu-active"),
              $(this).closest("li").addClass("menu-active")),
            $("body").hasClass("mobile-nav-active") &&
              ($("body").removeClass("mobile-nav-active"),
              $("#mobile-nav-toggle i").toggleClass("fa-times fa-bars"),
              $("#mobile-body-overly").fadeOut()),
            !1
          );
        }
      }
    }),
      $(document).ready(function () {
        $("html, body").hide(),
          window.location.hash
            ? setTimeout(function () {
                $("html, body").scrollTop(0).show(),
                  $("html, body").animate(
                    {
                      scrollTop: $(window.location.hash).offset().top - 108,
                    },
                    1e3
                  );
              }, 0)
            : $("html, body").show();
      }),
      $(window).scroll(function () {
        $(this).scrollTop() > 100
          ? $("#header").addClass("header-scrolled")
          : $("#header").removeClass("header-scrolled");
      }),
      $(".subnav").click(function () {
        $(".subnav .subnav-content").css("display", "block");
      });
  });
