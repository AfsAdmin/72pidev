import pandas as pd
import statistics
import pyodbc
from datetime import date,timedelta
import datetime
from scipy import stats
import math
import numpy as np
from .models import *
from django.db.models import Avg, Max, Min, Sum

import datetime
def stock_beta(StartDate, pnl_start_date,EndDate, user ,portfolio_name,Stock_Data,Daily_Returns,index_cumulative_data,beta_data,country,perType):
    # try:
    beta_data.columns=['Company','Factset_Ticker','Beta']
    beta=pd.merge(beta_data,Stock_Data,on=["Company",'Factset_Ticker'],how="inner")
    Stock_Data=Stock_Data[['Company','Factset_Ticker','Quantity']]
    Daily_Returns=pd.merge(Daily_Returns,Stock_Data,on=['Company','Factset_Ticker'],how='inner')
    Daily_Returns['Net']=Daily_Returns['Price'] * Daily_Returns['Quantity']
    Daily_Returns['Net%'] = Daily_Returns['Net']/Daily_Returns['Net']
    for i in range(0,len(Stock_Data)):
        fs_ticker=Stock_Data.loc[i,"Factset_Ticker"]      
        company=Stock_Data.loc[i,"Company"]      
        Stock_Daily_Returns=Daily_Returns[Daily_Returns["Factset_Ticker"]==fs_ticker]
        Stock_Daily_Returns.reset_index(drop=True,inplace=True)
        returns=stockwise_CalcsMain(StartDate, pnl_start_date, EndDate, user ,portfolio_name,Stock_Daily_Returns,perType)    
        output=returns
        output["Company"]=company
        output["Factset_Ticker"]=fs_ticker
        if i==0:
            output_master=output
        else:
            output_master=output_master.append(output) 
    output_master.reset_index(drop=True,inplace=True)
    output_master=pd.merge(output_master,beta_data,on=['Company','Factset_Ticker'],how='outer')
    output_master=output_master[["Company","Factset_Ticker",'Annualized Return', 'Annualized Volatility', 'Beta']]
    return output_master
    # except Exception as e:
    #     print(e)
    #     return pd.DataFrame()
        
def stockwise_CalcsMain(StartDate, pnl_start_date,EndDate, user ,portfolio_name,Stock_Daily_Returns,period):
    Stock_CumulativeReturns,DailyReturns =StockCumulativeReturns(Stock_Daily_Returns,pnl_start_date,EndDate)
    DailyReturns.columns=["Date","PNL"]
    DailyReturns=DailyReturns[1:]
    DailyReturns.reset_index(drop=True,inplace=True)    
    Fund_Annualize_Return = AnnualizeReturn (Stock_CumulativeReturns, StartDate,period)
    Annualize_Fund_Volatility = AnnualizeVolatility(DailyReturns)    
    Results=pd.DataFrame(columns=["C","Annualized Return","Annualized Volatility"])
    Companies=['Portfolio']
    Annnl_Return_values=[Fund_Annualize_Return]
    Annnl_Volatility_values= [Annualize_Fund_Volatility]
    Results['C']=Companies
    Results["Annualized Return"] = Annnl_Return_values
    Results["Annualized Volatility"] = Annnl_Volatility_values
    Results.columns=["","Annualized Return","Annualized Volatility"]
    return Results

    

def ExposureCalculation(Stocks_Data,StartDate,EndDate,Daily_Returns):     #function for exposure calc
    Exposure_Data=Stocks_Data.copy()
    Exposure_Data=Exposure_Data[['Company','Factset_Ticker','Quantity']]
    Daily_Returns = pd.merge(Daily_Returns,Exposure_Data,on=['Company','Factset_Ticker'],how='inner')    #merging returns data with exposure data on company
    Daily_Returns["Net"]=Daily_Returns["Price"] * Daily_Returns["Quantity"]    #multiplying price and quantity to get the net value
    Daily_Returns=Daily_Returns[['Company','Factset_Ticker','Date','Net']]
    return Daily_Returns

def StockCumulativeReturns(Stock_Daily_Returns,StartDate,EndDate,):    #function for calculating cumulative returns
    Stock_Daily_Returns['PNL%']=Stock_Daily_Returns['Return'] *  Stock_Daily_Returns.groupby(['Company','Factset_Ticker'])['Net%'].shift(1)     #calculating PNL% by multiplying todays return and yesterday net%
    PNL_data=Stock_Daily_Returns.groupby(['Date'])['PNL%'].agg('sum').reset_index()       #calculating day wise PNL%                  
    PNL_data.columns=["Date","Daily Ret"]
    PNL_data["PNL"] = (PNL_data['Daily Ret']+1).cumprod()-1       #calculating cumulative product
    Cumulative_Returns=PNL_data[["Date","PNL"]]                         
    PNL_data=PNL_data[["Date","Daily Ret"]]
    PNL_data.columns=["Date","Return"]
    return Cumulative_Returns,PNL_data 
  
def IndexCumulativeReturns (index_ticker,StartDate,EndDate,index_cumulative_data):   #function for calculating index cumulative returns
    index_cumulative_data=index_cumulative_data[index_cumulative_data['Company']==index_ticker]   #extracting the data of a given benchmark ticker
    index_cumulative_data.fillna(0,inplace=True)
    index_cumulative_data["Cumulative Return"] = (index_cumulative_data['Return']+1).cumprod()-1   #finding running cumulative 
    IndexCumulativeReturn=index_cumulative_data[["Date","Company","Cumulative Return"]]
    IndexCumulativeReturn.columns=["Date","Company","PNL"]
    IndexReturns=index_cumulative_data[["Date","Return"]]
    return IndexCumulativeReturn,IndexReturns

def AnnualizeReturn (CumulativeReturns_Daily, StartDate,period):
    Stock_Returns=CumulativeReturns_Daily.copy()
    Stock_Returns = Stock_Returns[Stock_Returns['Date'].astype(str) >= StartDate]
    if period!='ITD':
        if len(Stock_Returns)>0:
            annualize_ret=Stock_Returns.iat[len(Stock_Returns)-1,1]
        else:
            annualize_ret =0 
    else:
        if len(Stock_Returns)>0:
            annualize_ret=((Stock_Returns.iat[len(Stock_Returns)-1,1] +1) ** (252/len(Stock_Returns)))-1   # annualize returns by taking cumulative returns
        else:
            annualize_ret=0
    return annualize_ret
  
def AnnualizeVolatility (Daily_Returns_data):
    Stock_Returns=Daily_Returns_data.copy()
    annualize_vol=Stock_Returns.loc[:,"PNL"].std()     #volatiity taking returns and applying standard deviation and then multiply with annualize number
    return annualize_vol*(math.sqrt(252))
  
def AnnualizeIndexReturn(index_ticker,StartDate,EndDate,index_cumulative_data):
    index_cumulative_data=index_cumulative_data[index_cumulative_data['Company']==index_ticker]
    index_cumulative_data["Index Cumulate Ret"] = (index_cumulative_data['Return']+1).cumprod()-1   #computing index annualize returns
   
    annualize_index_ret=((index_cumulative_data.iat[len(index_cumulative_data)-1,3] +1) ** (252/len(index_cumulative_data)))-1
    return annualize_index_ret

