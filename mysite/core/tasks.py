from celery.task.schedules import crontab
from django.conf import settings
from django.core.mail import send_mail
from django.core import mail
from django.core.mail import EmailMessage
from django.template import loader
from mysite.celery import app
from mysite.core.models import *
from django.db.models import Avg, Max, Min, Sum, Q, Count
from datetime import datetime, timedelta
from django.utils import timezone
from django.contrib.sites.shortcuts import get_current_site
import re


@app.task
def startTask():  #Which sends start task mail everyday, just to know the setup is up and running
    mail_subject = "72PI Mail Triggering Started"
    mail_body  = loader.render_to_string('notifications/taskstarted.html')
    message = ''
    send_mail(mail_subject, message, 'alerts <alerts@72pi.ai>', ['vinaykumar.ch@goldenhillsindia.com'], fail_silently = True,html_message=mail_body)

@app.task
def endTask(): #Which sends end task mail everyday, just to know the setup is up and running
    mail_subject = "72PI Mail Triggering Ended"
    mail_body  = loader.render_to_string('notifications/taskended.html')
    message = ''
    send_mail(mail_subject, message, 'alerts <alerts@72pi.ai>', ['vinaykumar.ch@goldenhillsindia.com'], fail_silently = True,html_message=mail_body)

@app.task
def alertsPriceMovementCalculation():
    if CustomerPriceMovementsAlerts.objects.filter(Q(price_status='Waiting') | Q(percent_status='Waiting') | Q(price52wh_status='Waiting')).count():  #Check any alerts in waiting state or not
        customeralerts = CustomerPriceMovementsAlerts.objects.filter(Q(price_status='Waiting') | Q(percent_status='Waiting') | Q(price52wh_status='Waiting')).values('country','company_name', 'fs_ticker').distinct()
        #Get distinct tickers and company names for which alerts are in waiting state
        indiatickers, ustickers = set(), set()
        for alert in customeralerts:  #separate tickers on country wise
            if alert['country'] == 'India':
                indiatickers.add(alert['fs_ticker'])
            if alert['country'] == 'US':
                ustickers.add(alert['fs_ticker'])
        indiaTargetPrices = TargetPrices.objects.filter(ticker__in=indiatickers).values('ticker','company','current_price','number_52_week_high') #Fetch current price and 52wh prices for the list of tickers
        usTargetPrices = UsTargetPrices.objects.filter(ticker__in=ustickers).values('ticker','company','current_price','number_52_week_high') #Fetch current price and 52wh prices for the list of tickers
        for target in indiaTargetPrices: #Updte the current price and 52wh price on company wise
            CustomerPriceMovementsAlerts.objects.filter((Q(fs_ticker=target['ticker']) | Q(company_name=target['company']))&(Q(price_status='Waiting') | Q(percent_status='Waiting') | Q(price52wh_status='Waiting'))).update(current_price=target['current_price'],price_52wh=target['number_52_week_high'])
        for target in usTargetPrices:  #Updte the current price and 52wh price on company wise
            CustomerPriceMovementsAlerts.objects.filter((Q(fs_ticker=target['ticker']) | Q(company_name=target['company']))&(Q(price_status='Waiting') | Q(percent_status='Waiting') | Q(price52wh_status='Waiting'))).update(current_price=target['current_price'],price_52wh=target['number_52_week_high'])
        alerts = CustomerPriceMovementsAlerts.objects.filter(Q(price_status='Waiting') | Q(percent_status='Waiting') | Q(price52wh_status='Waiting')).all() #Fetch all the alerts in waiting state
        for alert in alerts:
            if alert.price_status == 'Waiting': #Check if the alert is in waiting state or not
                if alert.price_value_condition: #Check if the alert condition is present or not
                    if alert.price_value_condition == 'above':  #Chekc if the condition was above
                        if alert.current_price >= alert.price_value:  #Validate the condition
                            alertCondition = 'Last Price >= '+str(alert.price_value) #Prepare the condition to send mail
                            triggeredat = datetime.now().strftime("%d,%b %Y %H:%M%p")  #Get the triggered date
                            sendingPriceMovementEmailNotification(alert.company_name, alert.portfolio_name, alertCondition, alert.current_price, alert.customer_email, triggeredat) #Function call to send mail
                            alert.price_status = 'Notified'  #Make this alert is notified
                            alert.price_triggeredat = datetime.now() #Update the trigger date and time
                            alert.save() #Save the instance
                        else:
                            pass
                    if alert.price_value_condition == 'below': #Chekc if the condition was below
                        if alert.current_price <= alert.price_value: #Validate the condition 
                            alertCondition = 'Last Price <= '+str(alert.price_value) #Prepare the condition to send mail
                            triggeredat = datetime.now().strftime("%d,%b %Y %H:%M%p") #Get the triggered date
                            sendingPriceMovementEmailNotification(alert.company_name, alert.portfolio_name, alertCondition, alert.current_price, alert.customer_email, triggeredat) #Function call to send mail
                            alert.price_status = 'Notified' #Make this alert is notified
                            alert.price_triggeredat = datetime.now() #Update the trigger date and time
                            alert.save() #Save the instance       
                        else:
                            pass
            else:
                pass
            if alert.percent_status == 'Waiting': #Check if the alert is in waiting state or not
                if alert.price_percent_condition: #Check if the alert condition is present or not
                    if alert.price_percent_condition == 'above': #Chekc if the condition was above 
                        if alert.current_price >= (alert.last_price+(alert.last_price*(alert.price_percent/100))): #Validate the condition
                            alertCondition = 'Last Price Increased Above '+str(alert.price_percent)+'%' #Prepare the condition to send mail
                            triggeredat = datetime.now().strftime("%d,%b %Y %H:%M%p")  #Get the triggered date
                            sendingPriceMovementEmailNotification(alert.company_name, alert.portfolio_name, alertCondition, alert.current_price, alert.customer_email, triggeredat) #Function call to send mail
                            alert.percent_status = 'Notified' #make this alert is notified
                            alert.percent_triggeredat = datetime.now() #Update the trigger date and time
                            alert.save() #Save the instance
                        else:
                            pass
                    if alert.price_percent_condition == 'below':  #Check if the condition was below
                        if alert.current_price <= (alert.last_price-(alert.last_price*(alert.price_percent/100))): #Validate the condition
                            alertCondition = 'Last Price Decreased Below '+str(alert.price_percent)+'%' #Prepare the condition to send mail
                            triggeredat = datetime.now().strftime("%d,%b %Y %H:%M%p")  #Get the triggered date
                            sendingPriceMovementEmailNotification(alert.company_name, alert.portfolio_name, alertCondition, alert.current_price, alert.customer_email, triggeredat) #Function call to send mail
                            alert.percent_status = 'Notified' #make this alert is notified
                            alert.percent_triggeredat = datetime.now() #Update the trigger date and time
                            alert.save() #Save the instance
                        else:
                            pass
            else:
                pass
            if alert.price52wh_status == 'Waiting': #Check if the alert is in waiting state or not
                if alert.price_52wh_condition: #Check if the alert condition is present or not
                    if alert.price_52wh_condition == 'above': #Chekc if the condition was above 
                        if alert.current_price >= (alert.price_52wh+(alert.price_52wh*(alert.price_52wh_percent/100))): #Validate the condition
                            alertCondition = 'Last Price >= '+str(alert.price_52wh_percent)+'% of 52-week high' #Prepare the condition to send mail
                            triggeredat = datetime.now().strftime("%d,%b %Y %H:%M%p") #Get the triggered date
                            sendingPriceMovementEmailNotification(alert.company_name, alert.portfolio_name, alertCondition, alert.current_price, alert.customer_email, triggeredat) #Function call to send mail
                            alert.price52wh_status = 'Notified' #Make this alert is notified
                            alert.price52wh_triggeredat = datetime.now() #Update the trigger date and time
                            alert.save() #Save the instance
                        else:
                            pass
                    if alert.price_52wh_condition == 'below': #Check if the condition was below
                        if alert.current_price <= (alert.price_52wh-(alert.price_52wh*(alert.price_52wh_percent/100))): #Validate the condition
                            alertCondition = 'Last Price <= '+str(alert.price_52wh_percent)+'% of 52-week high' #Prepare the condition to send mail
                            triggeredat = datetime.now().strftime("%d,%b %Y %H:%M%p")  #Get the triggered date
                            sendingPriceMovementEmailNotification(alert.company_name, alert.portfolio_name, alertCondition, alert.current_price, alert.customer_email, triggeredat) #Function call to send mail
                            alert.price52wh_status = 'Notified' #Make this alert is notified
                            alert.price52wh_triggeredat = datetime.now() #Update the trigger date and time
                            alert.save() #Save the instance
                        else:
                            pass
            else:
                pass
    else:
        print("NO NOTIFICATIONS TODAY, AS NO PRICE MOVEMENTS PREFERENCES ARE IN WAITING STATE")
    print("TASK EXECUTION COMPLETED")

    
                

def sendingPriceMovementEmailNotification(companyName, portfolioName, condition,currentPrice, customerEmail, triggeredat):
    mail_subject = companyName+' alert triggered'  #Mail subject
    mail_body  = loader.render_to_string('notifications/notify.html',{  #Mail body which render to string from a html
        'companyName': companyName,
        'portfolioName' : portfolioName,
        'condition' : condition,
        'currentPrice' : currentPrice,
        'triggeredAt' : triggeredat,
    })
    to_email = [customerEmail] #To mail
    message = '' #Message nothing, this is mandatory
    send_mail(mail_subject, message, 'alerts <alerts@72pi.ai>', to_email, fail_silently = True,html_message=mail_body) #Calling send mail function


@app.task
def alertsTechnicalIndicationCalculation():
    if CustomerTechnicalIndicationAlerts.objects.filter(Q(rsi_status='Waiting') | Q(dma_status='Waiting')).count(): #Check if any alerts in waiting or not
        customeralerts = CustomerTechnicalIndicationAlerts.objects.filter(Q(rsi_status='Waiting') | Q(dma_status='Waiting')).values('country','company_name', 'fs_ticker').distinct() #Get tickers
        indiatickers, ustickers = set(), set()
        for alert in customeralerts: # Separate tickers on country wise
            if alert['country'] == 'India':
                indiatickers.add(alert['fs_ticker'])  
            if alert['country'] == 'US':
                ustickers.add(alert['fs_ticker'])
        indiamovingAverages = MovingAverage.objects.filter(date= 
				         MovingAverage.objects.all().aggregate(Max('date'))['date__max'], company__in=indiacompanies).all() #Fetch Moving averages for latest dates
        usmovingAverages = UsMovingAverage.objects.filter(date= 
				         UsMovingAverage.objects.all().aggregate(Max('date'))['date__max'], company__in=uscompanies).all()
        indiastockTickers, usstockTickers = list(),list()
        for average in indiamovingAverages:
            CustomerTechnicalIndicationAlerts.objects.filter((Q(fs_ticker=average.factset_ticker) | (Q(company_name=average.company))&(Q(rsi_status='Waiting') | Q(dma_status='Waiting'))).update(ma9=average.ma9,ma20=average.ma20,ma26=average.ma26,
                                                    ma50=average.ma50,ma100=average.ma100,ma200=average.ma200,current_price=average.price,factset_ticker=average.factset_ticker)
            indiastockTickers.append(average.factset_ticker)
            #Update moving averages and get list of tickers
        for average in usmovingAverages:
            CustomerTechnicalIndicationAlerts.objects.filter((Q(fs_ticker=average.factset_ticker) | (Q(company_name=average.company))&(Q(rsi_status='Waiting') | Q(dma_status='Waiting'))).update(ma9=average.ma9,ma20=average.ma20,ma26=average.ma26,
                                                    ma50=average.ma50,ma100=average.ma100,ma200=average.ma200,current_price=average.price,factset_ticker=average.factset_ticker)
            usstockTickers.append(average.factset_ticker)
        indiarsiValues = VolumeDataNew.objects.filter(ticker__in=indiastockTickers,
                date=VolumeDataNew.objects.filter(factor='RSI').all().aggregate(Max('date'))['date__max'], factor='RSI').values('value') #Get latest RSI values of list of tickers
        usrsiValues = UsVolumeDataNew.objects.filter(ticker__in=usstockTickers,
                date=UsVolumeDataNew.objects.filter(factor='RSI').all().aggregate(Max('date'))['date__max'], factor='RSI').values('value')
        for ticker in indiastockTickers:  #Update RSI values 
            for rsi in indiarsiValues:
                try:
                    rsivalue = rsi['value']
                    CustomerTechnicalIndicationAlerts.objects.filter(Q(factset_ticker=ticker)&(Q(rsi_status='Waiting') | Q(dma_status='Waiting'))).update(rsi_operand2=rsivalue)
                except KeyError:
                    pass
        for ticker in usstockTickers:   #Update RSI values
            for rsi in usrsiValues:
                try:
                    rsivalue = rsi['value']
                    CustomerTechnicalIndicationAlerts.objects.filter(Q(factset_ticker=ticker)&(Q(rsi_status='Waiting') | Q(dma_status='Waiting'))).update(rsi_operand2=rsivalue)
                except KeyError:
                    pass
        alerts = CustomerTechnicalIndicationAlerts.objects.filter(Q(rsi_status='Waiting') | Q(dma_status='Waiting')).values()  #Get waiting alerts
        for alert in alerts:
            if alert['rsi_status'] == 'Waiting':   #Check if the alert in waiting state or not
                if alert['rsi_metric']:  #Check if the condition exist or not
                    if alert['rsi_condition'] == 'greaterthan':  #Check the condition is greater than or not
                        if alert['rsi_operand1'] <= alert['rsi_operand2']: #Validate the condition
                            condition = alert['rsi_metric']+' >= '+str(alert['rsi_operand1']) #Prepare the mail condition
                            triggeredat = datetime.now() #Get the trigger date
                            sendingTechnicalIndicationEmailNotification(alert['company_name'], alert['portfolio_name'], condition, alert['current_price'], alert['customer_email'],triggeredat) #Function call to send mail
                            CustomerTechnicalIndicationAlerts.objects.filter(company_name=alert['company_name'], portfolio_name=alert['portfolio_name'],rsi_operand1=alert['rsi_operand1'],
                            rsi_operand2=alert['rsi_operand2']).update(rsi_status='Notified',rsi_triggeredat=datetime.now()) #Update alert as notified and save alert
                        else:
                            pass
                    if alert['rsi_condition'] == 'lessthan': #Check the condition is less than or not
                        if alert['rsi_operand1'] >= alert['rsi_operand2']:  #validate the condition
                            condition = alert['rsi_metric']+' <= '+str(alert['rsi_operand1']) #Prepare the mail condition
                            triggeredat = datetime.now() #Get the trigger date
                            sendingTechnicalIndicationEmailNotification(alert['company_name'], alert['portfolio_name'], condition, alert['current_price'], alert['customer_email'],triggeredat) #Function call to send mail
                            CustomerTechnicalIndicationAlerts.objects.filter(company_name=alert['company_name'], portfolio_name=alert['portfolio_name'],rsi_operand1=alert['rsi_operand1'],
                            rsi_operand2=alert['rsi_operand2']).update(rsi_status='Notified',rsi_triggeredat=datetime.now())  #Update alert as notified and save alert
                        else:
                            pass
            else:
                pass
            if alert['dma_status'] == 'Waiting':  #Check if the alert is in waiting or not
                if alert['dma_operand1'] and alert['dma_operand2']:  #Check the alert both operands
                    if alert['dma_condition'] == 'greaterthan': #Check if the condition was greater than
                        if alert[alert['dma_operand1']] >= alert[alert['dma_operand2']]: #Validate the condition
                            condition = str(re.search(r'\d+', alert['dma_operand1']).group())+' Day MA >= '+str(re.search(r'\d+', alert['dma_operand2']).group())+' Day MA' #Prepare the mail condition
                            triggeredat = datetime.now() # Get trigger time
                            sendingTechnicalIndicationEmailNotification(alert['company_name'], alert['portfolio_name'], condition, alert['current_price'], alert['customer_email'],triggeredat) #Function call to send mail
                            CustomerTechnicalIndicationAlerts.objects.filter(company_name=alert['company_name'], portfolio_name=alert['portfolio_name'],dma_operand1=alert['dma_operand1'],
                            dma_operand2=alert['dma_operand2']).update(dma_status='Notified',dma_triggeredat=datetime.now()) #Update alert as notified and save alert
                        else:
                            pass
                    if alert['dma_condition'] == 'lessthan': #Check if the condition was less than
                        if alert[alert['dma_operand1']] <= alert[alert['dma_operand2']]:  #Validate the condition
                            condition = str(re.search(r'\d+', alert['dma_operand1']).group())+' Day MA <= '+str(re.search(r'\d+', alert['dma_operand2']).group())+' Day MA' #Prepare the mail condition
                            triggeredat = datetime.now() # Get trigger time
                            sendingTechnicalIndicationEmailNotification(alert['company_name'], alert['portfolio_name'], condition, alert['current_price'], alert['customer_email'],triggeredat) #Function call to send mail
                            CustomerTechnicalIndicationAlerts.objects.filter(company_name=alert['company_name'], portfolio_name=alert['portfolio_name'],dma_operand1=alert['dma_operand1'],
                            dma_operand2=alert['dma_operand2']).update(dma_status='Notified',dma_triggeredat=datetime.now()) #Update alert as notified and save alert
                        else:
                            pass
            else:
                pass
    else:
        print("NO NOTIFICATIONS TODAY, AS NO TECHNICAL INDICATION PREFERENCES ARE IN WAITING STATE")
    print("TASK EXECUTION COMPLETED")



def sendingTechnicalIndicationEmailNotification(companyName, portfolioName, condition,currentPrice, customerEmail,triggeredat):
    mail_subject = companyName+' alert triggered' #Mail subject
    mail_body  = loader.render_to_string('notifications/notify.html',{  #Mail body which render to string from a html
        'companyName': companyName,
        'portfolioName' : portfolioName,
        'condition' : condition,
        'currentPrice' : currentPrice,
        'triggeredAt' : triggeredat
    })
    to_email = customerEmail #To mail
    message = ''  #Message nothing, this is mandatory
    send_mail(mail_subject, message, 'alerts <alerts@72pi.ai>', [to_email], fail_silently = True,html_message=mail_body)  #Calling send mail function


@app.task
def subscriptionExpireChecking():
    if CustomerSubscription.objects.count():   #Check if any subscriptions exist or not
        subscriptions = CustomerSubscription.objects.all()  #Get all customer subscriptions
        from datetime import date
        currentDay = date.today()  #Get current date
        for subscription in subscriptions:
            endDay = subscription.subscription_ends  #Get subscription end date for each customer
            if currentDay > endDay: #Check if subscription already expired
                if subscription.subscription_status == 'ACTIVE':  #If subscription status is still ACTIVE mark id EXPIRED
                    subscription.subscription_status = 'EXPIRED'
                    subscription.save()
                daysLeft = int(abs(currentDay - endDay).days)  #Get the difference dates
                if daysLeft%3 == 0: #send mail only for multiples of 3 (ex: 3,6,9,12...)
                    SendEmailForSubscriptionExpire(subscription.customer_name, subscription.email, subscription.subscription_type, subscription.subscription_plan, 0, subscription.subscription_ends)
                else:
                    pass
            else: #If subscription is not expired 
                daysLeft = int(abs(currentDay - endDay).days)  #Get the difference days
                if subscription.subscription_type == 'FREE':  #If the subscription type is FREE
                    if daysLeft == 5 or daysLeft == 3 or daysLeft == 1 or daysLeft == 0: #Send Reminder mail for 5,3,1 day before
                        SendEmailForSubscriptionExpire(subscription.customer_name, subscription.email, subscription.subscription_type, subscription.subscription_plan, daysLeft, subscription.subscription_ends)
                elif subscription.subscription_type == 'PREMIUM': #If the subscription type is PREMIUM
                    if daysLeft == 30 or daysLeft == 14 or daysLeft == 5 or daysLeft == 3 or daysLeft == 1 or daysLeft == 0: #Send Reminder mail for 30,14,5,3,1 day before
                        SendEmailForSubscriptionExpire(subscription.customer_name, subscription.email, subscription.subscription_type, subscription.subscription_plan, daysLeft, subscription.subscription_ends)
                else:
                    pass
        print("TASK EXECUTION COMPLETED")
    else:
        print("NO SUBSCRIPTIONS TO CHECK")


def SendEmailForSubscriptionExpire(user_name, user_email, user_subscription, user_plan, user_daysLeft, user_expireDate):
    mail_subject = '72PI Subscription is Expiring'  #Defailt mail subject for reminder mail
    if user_daysLeft == 0: # If subscription already expired
        user_daysLeft = None #No days left
        mail_subject = '72PI Subscription is Expired' #Change subject
    else:
        user_expireDate = None
    if user_subscription == 'FREE':  #If subscription type is free , display it as free trial
        user_subscription = 'Free Trial'
        user_plan = ''
    mail_body  = loader.render_to_string('notifications/SubscriptionExpiring.html',{  #Compose mail from the template
        'user_name': user_name,
        'user_subscription' : user_subscription,
        'user_plan' : user_plan,
        'user_daysLeft' : user_daysLeft,
        'user_expireDate' : user_expireDate,
        'domain': 'www.72pi.ai',
        'protocol' : 'https' #We consider always secure
    })
    to_email = user_email   #To mail
    message = '' #No message
    send_mail(mail_subject, message, 'subscribe <subscribe@72pi.ai>', [to_email], fail_silently = True,html_message=mail_body) #Send mail function calling


@app.task
def dailyStockAlertsReport():
    price_waiting, price_notified, price_percent_waiting, price_percent_notified, price_52wh_waiting, price_52wh_notified =0,0,0,0,0,0
    rsi_waiting, rsi_notified, dma_waiting, dma_notified = 0,0,0,0
    dateTimeThreshold = datetime.now() - timedelta(hours=6)
    pricealerts = CustomerPriceMovementsAlerts.objects.all()
    techalerts = CustomerTechnicalIndicationAlerts.objects.all()
    for alert in pricealerts:
        if alert.price_status == 'Notified':
            if alert.price_triggeredat >= dateTimeThreshold:
               price_notified +=1
            else:
                pass
        else:
            if alert.price_status == 'Waiting':
                price_waiting +=1
            else:
                pass
        if alert.percent_status == 'Notified':
            if alert.percent_triggeredat >= dateTimeThreshold:
                price_percent_notified +=1
            else:
                pass
        else:
            if alert.percent_status == 'Waiting':
                price_percent_waiting +=1
            else:
                pass
        if alert.price52wh_status == 'Notified':
            if alert.price52wh_triggeredat >= dateTimeThreshold:
                price_52wh_notified +=1
            else:
                pass
        else:
            if alert.price52wh_status == 'Waiting':
                price_52wh_waiting +=1
            else:
                pass
    for alert in techalerts:
        if alert.rsi_status == 'Notified':
            if alert.rsi_triggeredat >= dateTimeThreshold:
               rsi_notified +=1
            else:
                pass
        else:
            if alert.rsi_status == 'Waiting':
                rsi_waiting +=1
        if alert.dma_status == 'Notified':
            if alert.dma_triggeredat >= dateTimeThreshold:
                dma_notified +=1
            else:
                pass
        else:
            if alert.dma_status == 'Waiting':
                dma_waiting +=1
    mail_subject = '71PI Stock Notification Report'
    mail_body  = loader.render_to_string('notifications/alertsreport.html',{
        'price_waiting': price_waiting,'price_notified':price_notified,'price_percent_waiting':price_percent_waiting,
        'price_percent_notified':price_percent_notified,'price_52wh_waiting':price_52wh_waiting,'price_52wh_notified':price_52wh_notified,
        'rsi_waiting' : rsi_waiting, 'rsi_notified':rsi_notified, 'dma_waiting':dma_waiting,'dma_notified':dma_notified
    })
    send_mail(mail_subject, '', 'alerts <subscribe@72pi.ai>', ['vinaykumar.ch@goldenhillsindia.com'], fail_silently = True,html_message=mail_body)



## Email Test Campaign to GHC Employees using AWS
@app.task
def EmailCampaignTask(*args, **kwargs):
    recipients = EmailCampaignDetails.objects.none()
    try:
        condition = kwargs['batch']
        batches = list()
        if condition in ['all', 'All', 'ALL', 'aLL', 'alL', 'aLl']:
            recipients = EmailCampaignDetails.objects.all()
        else:
            if condition.find('&'):
                batches = condition.split('&')
            else:
                batches.append(condition)
            recipients = EmailCampaignDetails.objects.filter(batch__in=batches).all()
    except:
        mail_subject = '[ACTION REQUIRED] 72PI Email Campaign Failed '
        send_mail(mail_subject, 'Hello, Internal server error occured, please check and try again', 'no-reply@72pi.ai', ['vinaykumar.ch@goldenhillsindia.com'], fail_silently = True)
        send_mail(mail_subject, 'Hello, Internal server error occured, please check and try again', 'no-reply@72pi.ai', ['ravi.gundu@goldenhillsindia.com'], fail_silently = True)
        send_mail(mail_subject, 'Hello, Internal server error occured, please check and try again', 'no-reply@72pi.ai', ['pavani@goldenhillsindia.com'], fail_silently = True)
    if recipients.count():
        emails = recipients.count()
        unsubscribers = recipients.filter(unsubscribed='NO').count()
        emailsactive = recipients.filter(active='YES').count()
        emailstriggered = emailsactive
        emailssent = 0
        mail_subject = '[BEFORE] 72PI Email Campaign Report '
        mail_body  = loader.render_to_string('notifications/emailcampaignreport.html',{
            'emails': emails,'unsubscribers':unsubscribers,'emailsactive':emailsactive,
            'emailstriggered':emailstriggered,'emailssent':emailssent})
        message = ''    
        send_mail(mail_subject, message, 'no-reply@72pi.ai', ['vinaykumar.ch@goldenhillsindia.com'], fail_silently = True,html_message=mail_body)
        send_mail(mail_subject, message, 'no-reply@72pi.ai', ['ravi.gundu@goldenhillsindia.com'], fail_silently = True,html_message=mail_body)
        send_mail(mail_subject, message, 'no-reply@72pi.ai', ['pavani@goldenhillsindia.com'], fail_silently = True,html_message=mail_body)
        for recipient in recipients:
            if recipient.email:
                try:
                    h_message = loader.render_to_string('notifications/campaigntemplate.html',{'randomness' : datetime.now()})
                    send_mail('Portfolio Up 41% in 5 months with 72pi.ai','', 'no-reply@72pi.ai',[recipient.email], html_message=h_message,fail_silently = False)
                    emailssent += 1
                    print("MAIL SENT  ", emailssent)
                except Exception as e:
                    print("EMAIL NOT SENT - REMARKS UPDATED")
                    recipient.active = 'NO'
                    recipient.remarks = str(e)
                    recipient.save()
            else:
                print("INVALID EMAIL")
        emails = recipients.count()
        unsubscribers = recipients.filter(unsubscribed='NO').count()
        emailsactive = recipients.filter(active='YES').count()
        emailstriggered = emailsactive
        emailssent = emailssent
        mail_subject = '[AFTER] 72PI Email Campaign Report '
        mail_body  = loader.render_to_string('notifications/emailcampaignreport.html',{
            'emails': emails,'unsubscribers':unsubscribers,'emailsactive':emailsactive,
            'emailstriggered':emailstriggered,'emailssent':emailssent})
        message = ''
        send_mail(mail_subject, message, 'no-reply@72pi.ai', ['vinaykumar.ch@goldenhillsindia.com'], fail_silently = True,html_message=mail_body)
        send_mail(mail_subject, message, 'no-reply@72pi.ai', ['ravi.gundu@goldenhillsindia.com'], fail_silently = True,html_message=mail_body)
        send_mail(mail_subject, message, 'no-reply@72pi.ai', ['pavani@goldenhillsindia.com'], fail_silently = True,html_message=mail_body)
    else:
        print("No EMAILS FOUND TO SEND")
        mail_subject = '[ACTION REQUIRED] 72PI Email Campaign Failed '
        send_mail(mail_subject, 'Hello, No Emails found to send, please check and try again the same', 'no-reply@72pi.ai', ['vinaykumar.ch@goldenhillsindia.com'], fail_silently = True)
        send_mail(mail_subject, 'Hello, No Emails found to send, please check and try again the same', 'no-reply@72pi.ai', ['ravi.gundu@goldenhillsindia.com'], fail_silently = True)
        send_mail(mail_subject, 'Hello, No Emails found to send, please check and try again the same', 'no-reply@72pi.ai', ['pavani@goldenhillsindia.com'], fail_silently = True)
    print("TASK DONE")



@app.task
def TestEmailCampaignTask():
    print("TASK STARTED")
    h_message = loader.render_to_string('notifications/campaigntemplate.html',{'randomness' : datetime.now()})
    subject = 'Portfolio Up 41% in 5 months with 72pi.ai'
    send_mail(subject,'', '72PI TestMail <no-reply@72pi.ai>',['vinaykumar.ch@goldenhillsindia.com'], html_message=h_message,fail_silently = True)
    send_mail(subject,'', '72PI TestMail <no-reply@72pi.ai>',['ravi.gundu@goldenhillsindia.com'], html_message=h_message,fail_silently = True)
    send_mail(subject,'', '72PI TestMail <no-reply@72pi.ai>',['pavani@goldenhillsindia.com'], html_message=h_message,fail_silently = True)
    send_mail(subject,'', '72PI TestMail <no-reply@72pi.ai>',['suresh@goldenhillsindia.com'], html_message=h_message,fail_silently = True)
    send_mail(subject,'', '72PI TestMail <no-reply@72pi.ai>',['yugandhar.s@goldenhillsindia.com'], html_message=h_message,fail_silently = True)
    print("TASK DONE")