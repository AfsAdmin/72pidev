# -*- coding: utf-8 -*-
"""
Created on Mon Apr 20 12:34:02 2020

@author: sai.shiva
"""

import pandas as pd
import pyodbc
import math
from scipy import stats
from datetime import timedelta
import datetime

# def style_measures_us():
def style_measures_us(current_date,customer_portfolio_details,prices,market_data,sec_master,upm,mi):

    stocks_sec_master=sec_master[sec_master['Factset_Ticker'].isin(customer_portfolio_details['Factset_Ticker'])]
    n=pd.merge(prices,stocks_sec_master,how='inner',left_on=['Company','Factset_Ticker'],right_on=['FS Name','Factset_Ticker'])


    # n1=pd.merge(n,customer_portfolio_details,how='inner',left_on='FS Name',right_on='Company Name')
    n1=pd.merge(n,customer_portfolio_details,how='inner',on=['Company','Factset_Ticker'])


    o=n1.copy()
    p=n1.copy()
    q=n1.copy()
    r=n1.copy()
    s=n1.copy()
    t=n1.copy()

    o.dropna(subset=['PB'],inplace=True)
    p.dropna(subset=['PE'],inplace=True)
    q.dropna(subset=['Div_Yield'],inplace=True)
    r.dropna(subset=['Sales_Growth'],inplace=True)
    s.dropna(subset=['EPS_Growth'],inplace=True)
    t.dropna(subset=['ROE'],inplace=True)

    o['Inv_Amount']=o['Price']*o['Quantity']
    p['Inv_Amount']=p['Price']*p['Quantity']
    q['Inv_Amount']=q['Price']*q['Quantity']
    r['Inv_Amount']=r['Price']*r['Quantity']
    s['Inv_Amount']=s['Price']*s['Quantity']
    t['Inv_Amount']=t['Price']*t['Quantity']

    o['PB_Product']=o['Price']*o['Quantity']*o['PB']
    p['PE_Product']=p['Price']*p['Quantity']*p['PE']
    q['Div_Yield_Product']=q['Price']*q['Quantity']*q['Div_Yield']
    r['Sales_Growth_Product']=r['Price']*r['Quantity']*r['Sales_Growth']
    s['EPS_Growth_Product']=s['Price']*s['Quantity']*s['EPS_Growth']
    t['ROE_Product']=t['Price']*t['Quantity']*t['ROE']

    #Market Indices

    #a=sec_master[sec_master['Nifty 50']=='Yes']
    a=sec_master[sec_master['S&P 500']=='Yes']
    a1=a.copy()
    a2=a.copy()
    a3=a.copy()
    a4=a.copy()
    a5=a.copy()
    a6=a.copy()

    a1.dropna(subset=['PB'],inplace=True)
    a2.dropna(subset=['PE'],inplace=True)
    a3.dropna(subset=['Div_Yield'],inplace=True)
    a4.dropna(subset=['Sales_Growth'],inplace=True)
    a5.dropna(subset=['EPS_Growth'],inplace=True)
    a6.dropna(subset=['ROE'],inplace=True)

    #b=sec_master[sec_master['Nifty Small Cap 100']=='Yes']
    b=sec_master[sec_master['Dow Jones 30']=='Yes']
    b1=b.copy()
    b2=b.copy()
    b3=b.copy()
    b4=b.copy()
    b5=b.copy()
    b6=b.copy()

    b1.dropna(subset=['PB'],inplace=True)
    b2.dropna(subset=['PE'],inplace=True)
    b3.dropna(subset=['Div_Yield'],inplace=True)
    b4.dropna(subset=['Sales_Growth'],inplace=True)
    b5.dropna(subset=['EPS_Growth'],inplace=True)
    b6.dropna(subset=['ROE'],inplace=True)


    #SPXT

    a1['PB_Product']=a1['Market Cap']*a1['PB']
    a2['PE_Product']=a2['Market Cap']*a2['PE']
    a3['Div_Yield_Product']=a3['Market Cap']*a3['Div_Yield']
    a4['Sales_Growth_Product']=a4['Market Cap']*a4['Sales_Growth']
    a5['EPS_Growth_Product']=a5['Market Cap']*a5['EPS_Growth']
    a6['ROE_Product']=a6['Market Cap']*a6['ROE']

    #Dow Jones

    b1['PB_Product']=b1['Market Cap']*b1['PB']
    b2['PE_Product']=b2['Market Cap']*b2['PE']
    b3['Div_Yield_Product']=b3['Market Cap']*b3['Div_Yield']
    b4['Sales_Growth_Product']=b4['Market Cap']*b4['Sales_Growth']
    b5['EPS_Growth_Product']=b5['Market Cap']*b5['EPS_Growth']
    b6['ROE_Product']=b6['Market Cap']*b6['ROE']



    def AnnualizeVolatility (Daily_Returns_data):
        Stock_Returns=Daily_Returns_data.copy()
        annualize_vol=Stock_Returns.loc[:,"Return"].std()     #volatiity taking returns and applying standard deviation and then multiply with annualize number
        return annualize_vol*(math.sqrt(252))
    def ExposureCalculation(Stocks_Data,Daily_Returns):     #function for exposure calc
        Exposure_Data=Stocks_Data.copy()
        Exposure_Data=Exposure_Data[['Company','Factset_Ticker','Quantity']]
        Daily_Returns = pd.merge(Daily_Returns,Exposure_Data,on=['Company','Factset_Ticker'],how='inner')    #merging returns data with exposure data on company
        Daily_Returns["Net"]=Daily_Returns["Price"] * Daily_Returns["Quantity"]    #multiplying price and quantity to get the net value
        Daily_Returns=Daily_Returns[['Company','Factset_Ticker','Date','Net']]
        return Daily_Returns

    def StockCumulativeReturns(Stocks_Data_with_Exposure,Daily_Returns):    #function for calculating cumulative returns
        Exposure_Data = Stocks_Data_with_Exposure[["Company","Factset_Ticker","Date",'Net']]
        Exposure_Data=pd.merge(Exposure_Data,Daily_Returns,on=['Company',"Factset_Ticker",'Date'])                             
        Exposure_Data.fillna(0,inplace=True)
        Net_Data=Exposure_Data.groupby(['Date'])["Net"].agg('sum').reset_index()     #calculating day wise net
        Net_Data.columns=['Date','Denom']
        Exposure_Data=pd.merge(Exposure_Data,Net_Data,on='Date',how='inner')
        Exposure_Data["Net%"] = Exposure_Data["Net"]/Exposure_Data['Denom']         #calculating Net% by dividing day wise stock net with day wise portfolio net
        # Exposure_Data['Date'] = pd.to_datetime(Exposure_Data['Date'])
        Exposure_Data['PNL%']=Exposure_Data['Return'] *  Exposure_Data.groupby(['Company',"Factset_Ticker"])['Net%'].shift(1)     #calculating PNL% by multiplying todays return and yesterday net%
        PNL_data=Exposure_Data.groupby(['Date'])['PNL%'].agg('sum').reset_index()       #calculating day wise PNL%                  
        PNL_data.columns=["Date","Daily Ret"]
        PNL_data["PNL"] = (PNL_data['Daily Ret']+1).cumprod()-1       #calculating cumulative product
        PNL_data=PNL_data[["Date","Daily Ret"]]
        PNL_data.columns=["Date","Return"]
        return PNL_data


    cpd=customer_portfolio_details[['Company',"Factset_Ticker",'Quantity']]
    stocks_with_exposure=ExposureCalculation(cpd,upm)
    DailyReturns=StockCumulativeReturns(stocks_with_exposure,upm)
    portfolio_volatility=AnnualizeVolatility(DailyReturns)

    spxt=mi[mi['Company']=='S&P 500']
    spxt.reset_index(drop=True,inplace=True)
    spxt_returns = spxt[['Date','Return']]
    spxtReturns =spxt_returns.groupby(['Date'])['Return'].agg('sum').reset_index()  
    spxt_volatility= AnnualizeVolatility(spxtReturns) 

    dow=mi[mi['Company']=='Dow Jones Industrial Average']
    dow.reset_index(drop=True,inplace=True)
    dow_returns = dow[['Date','Return']]
    dowReturns =dow_returns.groupby(['Date'])['Return'].agg('sum').reset_index()  
    dow_volatility= AnnualizeVolatility(dowReturns)    #calculating day wise PNL%     
        
    if len(DailyReturns["Return"].values) < len(spxtReturns["Return"].values):
            portfolio_beta = 1
    else:
        portfolio_beta = stats.linregress(spxtReturns["Return"].values,DailyReturns["Return"].values)[0:1][0]
    
    if len(spxtReturns["Return"].values) < len(spxtReturns["Return"].values):
            spxt_beta = 1
    else:
        spxt_beta = stats.linregress(spxtReturns["Return"].values,spxtReturns["Return"].values)[0:1][0]
    
    if len(dowReturns["Return"].values) < len(spxtReturns["Return"].values):
            dow_beta = 1
    else:
        dow_beta = stats.linregress(spxtReturns["Return"].values,dowReturns["Return"].values)[0:1][0]
    

    factor_list=['Beta','Volatility','PE','PB','Div Yield','Sales Growth','EPS Growth','ROE']

    portfolio_list=[portfolio_beta,portfolio_volatility,
                    p['PE_Product'].sum()/p['Inv_Amount'].sum(),
                    o['PB_Product'].sum()/o['Inv_Amount'].sum(),
                    q['Div_Yield_Product'].sum()/q['Inv_Amount'].sum(),
                    r['Sales_Growth_Product'].sum()/r['Inv_Amount'].sum(),
                    s['EPS_Growth_Product'].sum()/s['Inv_Amount'].sum(),
                    t['ROE_Product'].sum()/t['Inv_Amount'].sum()
                    ]         

    snp500=[spxt_beta,spxt_volatility,
                  a2['PE_Product'].sum()/a2['Market Cap'].sum(),
                  a1['PB_Product'].sum()/a1['Market Cap'].sum(),
                  a3['Div_Yield_Product'].sum()/a3['Market Cap'].sum(),
                  a4['Sales_Growth_Product'].sum()/a4['Market Cap'].sum(),
                  a5['EPS_Growth_Product'].sum()/a5['Market Cap'].sum(),
                  a6['ROE_Product'].sum()/a6['Market Cap'].sum()
                    ]           

    dowjones=[dow_beta,dow_volatility,
                    b2['PE_Product'].sum()/b2['Market Cap'].sum(),
                    b1['PB_Product'].sum()/b1['Market Cap'].sum(),
                    b3['Div_Yield_Product'].sum()/b3['Market Cap'].sum(),
                    b4['Sales_Growth_Product'].sum()/b4['Market Cap'].sum(),
                    b5['EPS_Growth_Product'].sum()/b5['Market Cap'].sum(),
                    b6['ROE_Product'].sum()/b6['Market Cap'].sum()
                    ]
    


    final_df=pd.DataFrame({'Factor':factor_list,'Portfolio':portfolio_list,'S&P 500':snp500,
                            'Dow Jones 30':dowjones})


    return final_df