from django.views.generic import TemplateView, ListView
from django.views import View
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse, Http404
from mysite.core.Forms import EmailNotificationsForm, TradeSiganlsIdeasForm
from django.contrib.auth.models import User, Group
from django.core.mail import send_mass_mail, send_mail
from django.conf import settings
from mysite.core.models import CustomerSubscription
from django.template.loader import render_to_string
from datetime import datetime


class SendEmailNotificationsAdminView(LoginRequiredMixin,View):
    def get(self, request, *args, **kwargs):
        template_name = 'subscriptionadmin/emails.html'
        emailform = EmailNotificationsForm()
        context = {'form': emailform}
        return render(request,template_name, context)

    def post(self, request, *args, **kwargs):
        emailform = EmailNotificationsForm(request.POST)
        template_name = 'subscriptionadmin/emails.html'
        if emailform.is_valid():
            customer_type = emailform.cleaned_data['customer_type']
            subject = emailform.cleaned_data['subject']
            message = emailform.cleaned_data['message']
            users = User.objects.none()  # Making as global variable
            if customer_type == 'all':
                freesubscribers = Group.objects.get(name='freesubscribers')
                goldsubscribers = Group.objects.get(name='goldsubscribers')
                diamondsubscribers = Group.objects.get(name='diamondsubscribers')
                users = freesubscribers.user_set.all().union(goldsubscribers.user_set.all()).union(diamondsubscribers.user_set.all())
            elif customer_type == 'free':
                freesubscribers = Group.objects.get(name='freesubscribers')
                users = freesubscribers.user_set.all()
            elif customer_type == 'gold':
                goldsubscribers = Group.objects.get(name='goldsubscribers')
                users = goldsubscribers.user_set.all()
            elif customer_type == 'diamond':
                diamondsubscribers = Group.objects.get(name='diamondsubscribers')
                users = diamondsubscribers.user_set.all()
            from_email=settings.EMAIL_HOST_USER
            for user in users:
                h_message = render_to_string('subscriptionadmin/emailtemplate.html', {'message':message, 'sentat': datetime.now()})
                send_mail(subject, message, from_email, [user.email],html_message=h_message)
            emailform = EmailNotificationsForm(request.GET or None)
            context = {'form': emailform}
        else:
            context = {'form': emailform}
        return render(request, template_name, context)


class SendTradeIdeasSignalsAdminView(LoginRequiredMixin,View):
    def get(self, request, *args, **kwargs):
        template_name = 'subscriptionadmin/ideas.html'
        ideasform = TradeSiganlsIdeasForm()
        context = {'form': ideasform}
        return render(request,template_name, context)

    def post(self, request, *args, **kwargs):
        ideasform = TradeSiganlsIdeasForm(request.POST)
        template_name = 'subscriptionadmin/ideas.html'
        if ideasform.is_valid():
            customer_type = ideasform.cleaned_data['customer_type']
            company_name = ideasform.cleaned_data['company_name']
            fundamental = ideasform.cleaned_data['fundamental']
            fundamental_rating = ideasform.cleaned_data['fundamental_rating']
            quantitative = ideasform.cleaned_data['quantitative']
            quantitative_rating = ideasform.cleaned_data['quantitative_rating']
            technical = ideasform.cleaned_data['technical']
            technical_rating = ideasform.cleaned_data['technical_rating']
            situation = ideasform.cleaned_data['situation']
            situation_rating = ideasform.cleaned_data['situation_rating']
            purchase_price = ideasform.cleaned_data['purchase_price']
            target_price = ideasform.cleaned_data['target_price']
            time_period = ideasform.cleaned_data['time_period']
            comment = ideasform.cleaned_data['comment']
            users = User.objects.none()  # Making as global variable
            if customer_type == 'all':
                freesubscribers = Group.objects.get(name='freesubscribers')
                goldsubscribers = Group.objects.get(name='goldsubscribers')
                diamondsubscribers = Group.objects.get(name='diamondsubscribers')
                users = freesubscribers.user_set.all().union(goldsubscribers.user_set.all()).union(diamondsubscribers.user_set.all())
            elif customer_type == 'free':
                freesubscribers = Group.objects.get(name='freesubscribers')
                users = freesubscribers.user_set.all()
            elif customer_type == 'gold':
                goldsubscribers = Group.objects.get(name='goldsubscribers')
                users = goldsubscribers.user_set.all()
            elif customer_type == 'diamond':
                diamondsubscribers = Group.objects.get(name='diamondsubscribers')
                users = diamondsubscribers.user_set.all()
            from_email=settings.EMAIL_HOST_USER
            subject = 'Trade Idea Notification for '+company_name
            for user in users:
                h_message = render_to_string('subscriptionadmin/ideastemplate.html',
                 { 'fundamental':fundamental,
                  'fundamental_rating': fundamental_rating,
                  'quantitative' : quantitative,
                  'quantitative_rating' : quantitative_rating,
                  'technical':technical,
                  'technical_rating':technical_rating,
                  'situation':situation,
                  'situation_rating':situation_rating,
                  'purchase_price':purchase_price,
                  'target_price':target_price,
                  'time_period':time_period,
                  'comment':comment,
                  'sentat': datetime.now()})
                send_mail(subject, '', from_email, [user.email],html_message=h_message)
            ideasform = TradeSiganlsIdeasForm(request.GET or None)
            context = {'form': ideasform}
        else:
            context = {'form': ideasform}
        return render(request, template_name, context)

class SubscriptionAdminHomePageView(LoginRequiredMixin,TemplateView):
    template_name = 'subscriptionadmin/SubscriptionAdmin.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        freesubscribers = Group.objects.get(name='freesubscribers')
        goldsubscribers = Group.objects.get(name='goldsubscribers')
        diamondsubscribers = Group.objects.get(name='diamondsubscribers')
        allusers = freesubscribers.user_set.all().union(goldsubscribers.user_set.all()).union(diamondsubscribers.user_set.all())
        subscriptions = CustomerSubscription.objects.all()
        for user in allusers:
            for item in subscriptions:
                if item.customer_name_id == user.id:
                    user.valid_from = item.subscription_starts
                    user.valid_till = item.subscription_ends
        context['users'] = allusers
        context['alluserscount'] = allusers.count()
        context['freeuserscount'] = freesubscribers.user_set.all().count()
        context['golduserscount'] = goldsubscribers.user_set.all().count()
        context['diamonduserscount'] = diamondsubscribers.user_set.all().count()
        return context

class AllCustomersListView(LoginRequiredMixin,TemplateView):
    template_name = 'subscriptionadmin/SubscriptionAdmin.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        freesubscribers = Group.objects.get(name='freesubscribers')
        goldsubscribers = Group.objects.get(name='goldsubscribers')
        diamondsubscribers = Group.objects.get(name='diamondsubscribers')
        allusers = freesubscribers.user_set.all().union(goldsubscribers.user_set.all()).union(diamondsubscribers.user_set.all())
        subscriptions = CustomerSubscription.objects.all()
        for user in allusers:
            for item in subscriptions:
                if item.customer_name_id == user.id:
                    user.valid_from = item.subscription_starts
                    user.valid_till = item.subscription_ends
        context['users'] = allusers
        context['alluserscount'] = allusers.count()
        context['freeuserscount'] = freesubscribers.user_set.all().count()
        context['golduserscount'] = goldsubscribers.user_set.all().count()
        context['diamonduserscount'] = diamondsubscribers.user_set.all().count()
        return context

class FreeSubscribersListView(LoginRequiredMixin,TemplateView):
    template_name = 'subscriptionadmin/SubscriptionAdmin.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        freesubscribers = Group.objects.get(name='freesubscribers')
        goldsubscribers = Group.objects.get(name='goldsubscribers')
        diamondsubscribers = Group.objects.get(name='diamondsubscribers')
        allusers = freesubscribers.user_set.all()
        users = [user.id for user in allusers]
        subscriptions = CustomerSubscription.objects.filter(customer_name_id__in=users).all()
        for user in allusers:
            for item in subscriptions:
                if item.customer_name_id == user.id:
                    user.valid_from = item.subscription_starts
                    user.valid_till = item.subscription_ends
        context['users'] = allusers
        context['alluserscount'] = freesubscribers.user_set.all().count()+goldsubscribers.user_set.all().count()+diamondsubscribers.user_set.all().count()
        context['freeuserscount'] = freesubscribers.user_set.all().count()
        context['golduserscount'] = goldsubscribers.user_set.all().count()
        context['diamonduserscount'] = diamondsubscribers.user_set.all().count()
        return context

class GoldSubscribersListView(LoginRequiredMixin,TemplateView):
    template_name = 'subscriptionadmin/SubscriptionAdmin.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        freesubscribers = Group.objects.get(name='freesubscribers')
        goldsubscribers = Group.objects.get(name='goldsubscribers')
        diamondsubscribers = Group.objects.get(name='diamondsubscribers')
        allusers = goldsubscribers.user_set.all()
        users = [user.id for user in allusers]
        subscriptions = CustomerSubscription.objects.filter(customer_name_id__in=users).all()
        for user in allusers:
            for item in subscriptions:
                if item.customer_name_id == user.id:
                    user.valid_from = item.subscription_starts
                    user.valid_till = item.subscription_ends
        context['users'] = allusers
        context['alluserscount'] = freesubscribers.user_set.all().count()+goldsubscribers.user_set.all().count()+diamondsubscribers.user_set.all().count()
        context['freeuserscount'] = freesubscribers.user_set.all().count()
        context['golduserscount'] = goldsubscribers.user_set.all().count()
        context['diamonduserscount'] = diamondsubscribers.user_set.all().count()
        return context

class DiamondSubscribersListView(LoginRequiredMixin,TemplateView):
    template_name = 'subscriptionadmin/SubscriptionAdmin.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        freesubscribers = Group.objects.get(name='freesubscribers')
        goldsubscribers = Group.objects.get(name='goldsubscribers')
        diamondsubscribers = Group.objects.get(name='diamondsubscribers')
        allusers = diamondsubscribers.user_set.all()
        users = [user.id for user in allusers]
        subscriptions = CustomerSubscription.objects.filter(customer_name_id__in=users).all()
        for user in allusers:
            for item in subscriptions:
                if item.customer_name_id == user.id:
                    user.valid_from = item.subscription_starts
                    user.valid_till = item.subscription_ends
        context['users'] = allusers
        context['alluserscount'] = freesubscribers.user_set.all().count()+goldsubscribers.user_set.all().count()+diamondsubscribers.user_set.all().count()
        context['alluserscount'] = allusers.count()
        context['freeuserscount'] = freesubscribers.user_set.all().count()
        context['golduserscount'] = goldsubscribers.user_set.all().count()
        context['diamonduserscount'] = diamondsubscribers.user_set.all().count()
        return context


class ManageSubscriptionsAdminView(LoginRequiredMixin, View):
    context = dict()
    def get(self, request, *args, **kwargs):
        template_name = 'subscriptionadmin/manageaccounts.html'
        self.context['subscriptions'] = CustomerSubscription.objects.all()
        return render(request,template_name, self.context)

    def post(self, request, *args, **kwargs):
        template_name = 'subscriptionadmin/manageaccounts.html'
        context = {}
        return render(request, template_name, context)


