import pandas as pd
import pyodbc
import datetime
from datetime import date, datetime, timedelta
from mysite.core.models import *
from django.db.models import Avg, Max, Min, Sum

def RiskMonitorMain(customer_name,portfolio_name,country,StartDate,Stock_Data,Daily_Returns,IndexData,EMA_Data,MA50_Data,RSI_Data,week_52_data):
    # try:
    stock = list(Stock_Data['Factset_Ticker'])
    benchmarks={'India':'Nifty 50 Index','US':'SPXT Index'}
    index_ticker=benchmarks[country]
    Daily_Returns['Date']=pd.to_datetime(Daily_Returns['Date'])
    RSI_Data=pd.merge(RSI_Data,Daily_Returns[['Company','Factset_Ticker']].drop_duplicates().reset_index(drop=True),how='inner',on='Factset_Ticker')
    RSI_Data=RSI_Data[['Company','Factset_Ticker','value']]
    RSI_Data.columns=['Company','Factset_Ticker','RSI']
    latest_prices=Daily_Returns[Daily_Returns['Date'].dt.date==StartDate].reset_index(drop=True)
    IndexData['Date']=pd.to_datetime(IndexData['Date'])
    trailing_1_month=StartDate+timedelta(-30)
    trailing_3_month=StartDate+timedelta(-90)
    trailing_1_year=StartDate+timedelta(-365)
    trailing_1_month_returns=Daily_Returns[Daily_Returns['Date'].dt.date>=trailing_1_month].reset_index(drop=True)
    trailing_3_month_returns=Daily_Returns[Daily_Returns['Date'].dt.date>=trailing_3_month].reset_index(drop=True)
    trailing_1_year_returns=Daily_Returns[Daily_Returns['Date'].dt.date>=trailing_1_year].reset_index(drop=True)
    trailing_1_month_returns['Return']=trailing_1_month_returns['Return']+1
    trailing_1_month_returns["1M Cumulative Return"]=trailing_1_month_returns.groupby(['Company','Factset_Ticker'])["Return"].cumprod()-1
    trailing_3_month_returns['Return']=trailing_3_month_returns['Return']+1
    trailing_3_month_returns["3M Cumulative Return"]=trailing_3_month_returns.groupby(['Company','Factset_Ticker'])["Return"].cumprod()-1
    trailing_1_year_returns['Return']=trailing_1_year_returns['Return']+1
    trailing_1_year_returns["1Y Cumulative Return"]=trailing_1_year_returns.groupby(['Company','Factset_Ticker'])["Return"].cumprod()-1
    trailing_1_month_returns=trailing_1_month_returns.groupby(['Company','Factset_Ticker'])['Company','Factset_Ticker','1M Cumulative Return'].tail(1).reset_index(drop=True)
    trailing_3_month_returns=trailing_3_month_returns.groupby(['Company','Factset_Ticker'])['Company','Factset_Ticker','3M Cumulative Return'].tail(1).reset_index(drop=True)
    trailing_1_year_returns=trailing_1_year_returns.groupby(['Company','Factset_Ticker'])['Company','Factset_Ticker','1Y Cumulative Return'].tail(1).reset_index(drop=True)
    output1=pd.merge(trailing_1_year_returns,trailing_1_month_returns,on=["Company",'Factset_Ticker'],how="outer")
    trailing_1_month_index_returns=IndexData[IndexData['Date'].dt.date>=trailing_1_month].reset_index(drop=True)
    trailing_3_month_index_returns=IndexData[IndexData['Date'].dt.date>=trailing_3_month].reset_index(drop=True)
    trailing_1_year_index_returns=IndexData[IndexData['Date'].dt.date>=trailing_1_year].reset_index(drop=True)
    trailing_1_month_index_returns['cumulative']=(trailing_1_month_index_returns['Return']+1).cumprod()-1
    trailing_3_month_index_returns['cumulative']=(trailing_3_month_index_returns['Return']+1).cumprod()-1
    trailing_1_year_index_returns['cumulative']=(trailing_1_year_index_returns['Return']+1).cumprod()-1
    excess_return_1_month=trailing_1_month_returns
    excess_return_3_month=trailing_3_month_returns
    excess_return_1_year=trailing_1_year_returns
    excess_return_1_month['1M Excess Return'] = trailing_1_month_returns['1M Cumulative Return'] - trailing_1_month_index_returns.loc[len(trailing_1_month_index_returns)-1,'cumulative']
    excess_return_3_month['3M Excess Return'] = trailing_3_month_returns['3M Cumulative Return'] - trailing_3_month_index_returns.loc[len(trailing_1_month_index_returns)-1,'cumulative']
    excess_return_1_year['1Y Excess Return'] = trailing_1_year_returns['1Y Cumulative Return'] - trailing_1_year_index_returns.loc[len(trailing_1_month_index_returns)-1,'cumulative']
    output2=pd.merge(excess_return_1_year[['Company','Factset_Ticker','1Y Excess Return']],excess_return_3_month[['Company','Factset_Ticker','3M Excess Return']],on=["Company","Factset_Ticker"],how="outer")
    output2=pd.merge(output2,excess_return_1_month[['Company',"Factset_Ticker",'1M Excess Return']],on=["Company","Factset_Ticker"],how="outer")
    prices_data_1M = Daily_Returns[Daily_Returns['Date'].dt.date>=trailing_1_month].reset_index(drop=True)
    prices_data_1Y = Daily_Returns[Daily_Returns['Date'].dt.date>=trailing_1_year].reset_index(drop=True)
    if country=='India':
        Low_High_1M = week_52_data[['Company',"Factset_Ticker",'1 Month Low','1 Month High']]        
    else:
        Low_High_1M = prices_data_1M.groupby(['Company',"Factset_Ticker"])['Price'].agg([min,max]).reset_index()
    # Low_High_1Y = prices_data_1Y.groupby('Company')['Price'].agg([min,max]).reset_index()
    Low_High_1Y=week_52_data[['Company',"Factset_Ticker",'52_Week_Low','52_Week_High']]
    Low_High_1M.columns=['Company',"Factset_Ticker",'Min_Price_1M','Max_Price_1M']
    Low_High_1Y.columns=['Company',"Factset_Ticker",'Min_Price_1Y','Max_Price_1Y']
    output3=pd.merge(Low_High_1Y,Low_High_1M,on=['Company',"Factset_Ticker"],how='outer')
    output3=pd.merge(output3,latest_prices[['Company',"Factset_Ticker",'Price']],on=['Company',"Factset_Ticker"],how='inner')
    output3['VFX']=(output3['Max_Price_1M']-output3['Price'])/output3['Max_Price_1M']
    final_output=pd.merge(output1,output2,on=['Company',"Factset_Ticker"],how='inner')
    final_output=pd.merge(final_output,output3,on=['Company',"Factset_Ticker"],how='inner')
    final_output=pd.merge(final_output,EMA_Data,on=['Company',"Factset_Ticker"],how='inner')
    final_output=pd.merge(final_output,MA50_Data,on=['Company',"Factset_Ticker"],how='inner')
    final_output=pd.merge(final_output,RSI_Data,on=['Company',"Factset_Ticker"],how='inner')
    return final_output
    # except Exception as e:
    #     print(e)
    #     return pd.DataFrame()