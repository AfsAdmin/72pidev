import pandas as pd
import pyodbc
from datetime import datetime,date
from statistics import mean
import statsmodels.api as sm
import numpy as np
from scipy import stats
from mysite.core.Appconfigproperties import *
pd.options.mode.chained_assignment = None 

def FamaFrenchModel(PNL_data,Fama_Data,Trade_date):    
    fama_data=PNL_data.copy()        
    fama_data["Date"]=fama_data["Date"].astype(str)    
    fama_data["Date"]=fama_data["Date"].str.replace('-','')    
    fama_data["Date"]=fama_data["Date"].astype(int)
    fama_data=pd.merge(fama_data,Fama_Data,on="Date",how="left")    
    fama_data.columns=["Date","Return","SMB","HML","WML","Rm"]
    fama_data["Count"]=fama_data.index+1

    end=int(fama_data[fama_data['Date']==Trade_date].iloc[0]['Count'])
    start=end-251    

    required_data=fama_data[(fama_data['Count']>=start) & (fama_data['Count']<=end)]
    required_data.reset_index(drop=True,inplace=True)
    required_data["Count"]=required_data.index+1
    results=[]    
    required_data.fillna(0,inplace=True)

    Independent_variables = required_data[['SMB','HML','WML','Rm']]
    Dependent_variables= required_data[["Return"]]
    X=Independent_variables.values
    X=sm.add_constant(X)
    mod = sm.OLS(Dependent_variables,X)
    res = mod.fit()
    resu=[]
    resu.extend(res.params)
    resu.append(Trade_date)
    results.append(resu)
    res1=pd.DataFrame(results)
    res1.columns=["Intercept",'SMB_Beta','HML_Beta','WML_Beta','Rm_Beta',"Date"]

    required_data=pd.merge(required_data,res1,on=["Date"],how="left")
    required_data=required_data[["Date","Return","SMB","HML","WML","Rm","Intercept",'SMB_Beta','HML_Beta','WML_Beta','Rm_Beta']]
    required_data.fillna(0,inplace=True)

    required_data["Cum Intercept"]=pow(1+required_data["Intercept"],252)-1
    
    cumulatives=[]
    cumulatives.append(((required_data['SMB']+1).cumprod()-1).iloc[-1])
    cumulatives.append(((required_data['HML']+1).cumprod()-1).iloc[-1])
    cumulatives.append(((required_data['WML']+1).cumprod()-1).iloc[-1])
    cumulatives.append(((required_data['Rm']+1).cumprod()-1).iloc[-1])
    cumulatives.append(((required_data['Return']+1).cumprod()-1).iloc[-1])
    cumulatives.append(Trade_date)
    results1=[]
    results1.append(cumulatives)
    res2=pd.DataFrame(results1)
    res2.columns=['SMB Cum',"HML Cum", "WML Cum","Rm Cum","Fund Cum",'Date']

    required_data=pd.merge(required_data,res2,on=["Date"],how="left")
    required_data.fillna(0,inplace=True)
    
    required_data["SMB Cum Beta"]=required_data["SMB Cum"]  * required_data["SMB_Beta"]
    required_data["HML Cum Beta"]=required_data["HML Cum"]  * required_data["HML_Beta"]
    required_data["WML Cum Beta"]=required_data["WML Cum"]  * required_data["WML_Beta"]
    required_data["Rm Cum Beta"]=required_data["Rm Cum"]  * required_data["Rm_Beta"]
    required_data["Intercept Cum"]= required_data["Cum Intercept"]    
    required_data["Error Term"]= required_data["Fund Cum"]-( required_data["Intercept Cum"]+required_data["SMB Cum Beta"]+required_data["HML Cum Beta"]+required_data["WML Cum Beta"]+required_data["Rm Cum Beta"])

    required_data["Size"] =required_data["SMB Cum Beta"]     
    required_data["Value"] =required_data["HML Cum Beta"]     
    required_data["Momentum"] = required_data["WML Cum Beta"]     
    required_data["Market"] =required_data["Rm Cum Beta"]     
    required_data["Alpha"] =required_data["Intercept Cum"] + required_data["Error Term"]    
    required_data.fillna(0,inplace=True)

    Final_FAMA_Results=required_data[["Alpha","Size","Value","Momentum","Market","Fund Cum"]]    
    Final_FAMA_Results = pd.concat([Final_FAMA_Results.reset_index(drop=True), required_data[["Date"]]], axis=1)    
    Final_FAMA_Results=Final_FAMA_Results[Final_FAMA_Results['Date']==Trade_date]
    Final_FAMA_Results=Final_FAMA_Results[['Date',"Market","Size","Value","Momentum","Alpha","Fund Cum"]]        
    Final_FAMA_Results.columns=['Date',"Market","Size","Value","Momentum","Alpha","Portfolio"]    

    return Final_FAMA_Results

def FamaMain(PNL_data):
    try:
        PNL_data.columns=['Date','PNL']
        country='US'
        if country=='IN':
            Trade_date=20191231   
            sql_conn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server}; SERVER='+SERVER+'; PORT='+PORT+'; DATABASE='+DATABASE+';UID='+UID+';PWD='+PWD+';')  
            Fama_Data = pd.read_sql("select Date,SMB,HML,WML,Rm from Fama_Data",sql_conn)         #Getting Fama Data (Factors Data)
            sql_conn.close()
        else:
            Trade_date=20200430
            sql_conn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server}; SERVER='+SERVER+'; PORT='+PORT+'; DATABASE='+DATABASE+';UID='+UID+';PWD='+PWD+';')         
            Fama_Data = pd.read_sql("select Date,SMB,HML,WML,Rm from US_Fama_Data",sql_conn)         #Getting Fama Data (Factors Data)
            sql_conn.close()
        Fama_Data['Date']=Fama_Data['Date'].astype(int)
        Fama_Final_Results=FamaFrenchModel(PNL_data,Fama_Data,Trade_date)    
        Fama_Final_Results.reset_index(drop=True,inplace=True)
        return Fama_Final_Results
    except Exception as e:
        return pd.DataFrame()
