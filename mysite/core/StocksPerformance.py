
import pyodbc
import pandas as pd

def StockReturnsCalcMain(StartDate,current_date,stock,country,Input_data):
    try:
        Input_Data = Input_data.copy()
        Input_Data["Return"] =Input_Data["Return"]+1
        Input_Data["Cumulative Return"]=Input_Data.groupby(['Company'])["Return"].cumprod()-1    
        StockReturns=Input_Data.groupby(['Company']).tail(1)
        StockReturns=StockReturns[['Company','Cumulative Return']].reset_index(drop=True)                              #taking last day cumulative
        return StockReturns
    except Exception as e:
        return pd.DataFrame()


