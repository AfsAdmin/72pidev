# -*- coding: utf-8 -*-
"""
Created on Mon Apr 20 12:34:02 2020

@author: sai.shiva
"""

import pandas as pd
import pyodbc
import math
from scipy import stats
from datetime import timedelta
import datetime
from mysite.core.Appconfigproperties import *


def style_measures_india(start_date,current_date,user_name,portfolio_name,portfolio_type,customer_portfolio_details,prices,market_data,sec_master,upm,mi):
    try:
        stocks_sec_master=sec_master[sec_master['Factset_Ticker'].isin(customer_portfolio_details['Factset_Ticker'])]
        n=pd.merge(prices,stocks_sec_master,how='inner',left_on=['Company','Factset_Ticker'],right_on=['FS Name','Factset_Ticker'])

        n1=pd.merge(n,customer_portfolio_details,how='inner',on=['Company','Factset_Ticker'])

        o=n1.copy()
        p=n1.copy()
        q=n1.copy()
        r=n1.copy()
        s=n1.copy()
        t=n1.copy()

        o.dropna(subset=['PB'],inplace=True)
        p.dropna(subset=['PE'],inplace=True)
        q.dropna(subset=['Div_Yield'],inplace=True)
        r.dropna(subset=['Sales_Growth'],inplace=True)
        s.dropna(subset=['EPS_Growth'],inplace=True)
        t.dropna(subset=['ROE'],inplace=True)
        
        o['Inv_Amount']=o['Price']*o['Quantity']
        p['Inv_Amount']=p['Price']*p['Quantity']
        q['Inv_Amount']=q['Price']*q['Quantity']
        r['Inv_Amount']=r['Price']*r['Quantity']
        s['Inv_Amount']=s['Price']*s['Quantity']
        t['Inv_Amount']=t['Price']*t['Quantity']

        o['PB_Product']=o['Price']*o['Quantity']*o['PB']
        p['PE_Product']=p['Price']*p['Quantity']*p['PE']
        q['Div_Yield_Product']=q['Price']*q['Quantity']*q['Div_Yield']
        r['Sales_Growth_Product']=r['Price']*r['Quantity']*r['Sales_Growth']
        s['EPS_Growth_Product']=s['Price']*s['Quantity']*s['EPS_Growth']
        t['ROE_Product']=t['Price']*t['Quantity']*t['ROE']

        #Market Indices

        a=sec_master[sec_master['Nifty 50']=='Yes']
        a1=a.copy()
        a2=a.copy()
        a3=a.copy()
        a4=a.copy()
        a5=a.copy()
        a6=a.copy()

        a1.dropna(subset=['PB'],inplace=True)
        a2.dropna(subset=['PE'],inplace=True)
        a3.dropna(subset=['Div_Yield'],inplace=True)
        a4.dropna(subset=['Sales_Growth'],inplace=True)
        a5.dropna(subset=['EPS_Growth'],inplace=True)
        a6.dropna(subset=['ROE'],inplace=True)

        b=sec_master[sec_master['Nifty Small Cap 100']=='Yes']
        b1=b.copy()
        b2=b.copy()
        b3=b.copy()
        b4=b.copy()
        b5=b.copy()
        b6=b.copy()

        b1.dropna(subset=['PB'],inplace=True)
        b2.dropna(subset=['PE'],inplace=True)
        b3.dropna(subset=['Div_Yield'],inplace=True)
        b4.dropna(subset=['Sales_Growth'],inplace=True)
        b5.dropna(subset=['EPS_Growth'],inplace=True)
        b6.dropna(subset=['ROE'],inplace=True)

        c=sec_master[sec_master['Nifty Mid Cap 100']=='Yes']
        c1=c.copy()
        c2=c.copy()
        c3=c.copy()
        c4=c.copy()
        c5=c.copy()
        c6=c.copy()

        c1.dropna(subset=['PB'],inplace=True)
        c2.dropna(subset=['PE'],inplace=True)
        c3.dropna(subset=['Div_Yield'],inplace=True)
        c4.dropna(subset=['Sales_Growth'],inplace=True)
        c5.dropna(subset=['EPS_Growth'],inplace=True)
        c6.dropna(subset=['ROE'],inplace=True)

        #Nifty 50

        a1['PB_Product']=a1['Market Cap']*a1['PB']
        a2['PE_Product']=a2['Market Cap']*a2['PE']
        a3['Div_Yield_Product']=a3['Market Cap']*a3['Div_Yield']
        a4['Sales_Growth_Product']=a4['Market Cap']*a4['Sales_Growth']
        a5['EPS_Growth_Product']=a5['Market Cap']*a5['EPS_Growth']
        a6['ROE_Product']=a6['Market Cap']*a6['ROE']

        #Nifty Small Cap 100

        b1['PB_Product']=b1['Market Cap']*b1['PB']
        b2['PE_Product']=b2['Market Cap']*b2['PE']
        b3['Div_Yield_Product']=b3['Market Cap']*b3['Div_Yield']
        b4['Sales_Growth_Product']=b4['Market Cap']*b4['Sales_Growth']
        b5['EPS_Growth_Product']=b5['Market Cap']*b5['EPS_Growth']
        b6['ROE_Product']=b6['Market Cap']*b6['ROE']

        #Nifty Mid Cap 100

        c1['PB_Product']=c1['Market Cap']*c1['PB']
        c2['PE_Product']=c2['Market Cap']*c2['PE']
        c3['Div_Yield_Product']=c3['Market Cap']*c3['Div_Yield']
        c4['Sales_Growth_Product']=c4['Market Cap']*c4['Sales_Growth']
        c5['EPS_Growth_Product']=c5['Market Cap']*c5['EPS_Growth']
        c6['ROE_Product']=c6['Market Cap']*c6['ROE']

        def AnnualizeVolatility (Daily_Returns_data):
            Stock_Returns=Daily_Returns_data.copy()
            annualize_vol=Stock_Returns.loc[:,"Return"].std()     #volatiity taking returns and applying standard deviation and then multiply with annualize number
            return annualize_vol*(math.sqrt(252))

        def ExposureCalculation(Stocks_Data,Daily_Returns):     #function for exposure calc
            Exposure_Data=Stocks_Data.copy()
            Exposure_Data=Exposure_Data[['Company','Factset_Ticker','Quantity']]
            Daily_Returns = pd.merge(Daily_Returns,Exposure_Data,on=['Company','Factset_Ticker'],how='inner')    #merging returns data with exposure data on company
            Daily_Returns["Net"]=Daily_Returns["Price"] * Daily_Returns["Quantity"]    #multiplying price and quantity to get the net value
            Daily_Returns=Daily_Returns[['Company','Factset_Ticker','Date','Net']]
            return Daily_Returns

        def StockCumulativeReturns(Stocks_Data_with_Exposure,Daily_Returns):    #function for calculating cumulative returns
            Exposure_Data = Stocks_Data_with_Exposure[["Company",'Factset_Ticker',"Date",'Net']]
            Exposure_Data=pd.merge(Exposure_Data,Daily_Returns,on=['Company','Factset_Ticker','Date'],how='inner')                             
            Exposure_Data.fillna(0,inplace=True)
            Net_Data=Exposure_Data.groupby(['Date'])["Net"].agg('sum').reset_index()     #calculating day wise net
            Net_Data.columns=['Date','Denom']
            Exposure_Data=pd.merge(Exposure_Data,Net_Data,on='Date',how='inner')
            Exposure_Data["Net%"] = Exposure_Data["Net"]/Exposure_Data['Denom']         #calculating Net% by dividing day wise stock net with day wise portfolio net
            # Exposure_Data['Date'] = pd.to_datetime(Exposure_Data['Date'])
            Exposure_Data['PNL%']=Exposure_Data['Return'] *  Exposure_Data.groupby(['Company','Factset_Ticker'])['Net%'].shift(1)     #calculating PNL% by multiplying todays return and yesterday net%
            PNL_data=Exposure_Data.groupby(['Date'])['PNL%'].agg('sum').reset_index()       #calculating day wise PNL%                  
            PNL_data.columns=["Date","Daily Ret"]
            # PNL_data["PNL"] = (PNL_data['Daily Ret']+1).cumprod()-1       #calculating cumulative product
            PNL_data=PNL_data[["Date","Daily Ret"]]
            PNL_data.columns=["Date","Return"]        
            return PNL_data

        
        def Live_PortfolioCumulativeReturns(StartDate,EndDate,user_name,portfolio_name,country):
            if country=='IN':
                sql_conn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server}; SERVER='+SERVER+'; PORT='+PORT+'; DATABASE='+DATABASE+';UID='+UID+';PWD='+PWD+';')
                input_data=pd.read_sql('''select Date,c.[Company name] as Company,[P&LRealized],[P&LUnrealized],AUM_BOD from Daily_Positions d join Customer_Portfolio_Details c 
                                    on c.[customer name]=d.customer_name and c.[portfolio name]=d.portfolio_name and c.[company name]=d.stock 
                                    where c.[customer name]='''+"'"+user_name+"' and c.[portfolio name]='"+
                                    portfolio_name+"' and date>='"  + str(StartDate) + "' and date<='" + str(EndDate)+"' order by date",sql_conn)
                sql_conn.close()
            input_data.sort_values(by=['Date','Company'],inplace=True)
            input_data['PNL%'] = (input_data['P&LRealized']+input_data['P&LUnrealized'])/input_data['AUM_BOD']
            daily_portfolio_pnl=input_data.groupby(['Date'])['PNL%'].sum().reset_index()
            daily_portfolio_pnl["Cumulative_PNL%"] = (daily_portfolio_pnl['PNL%']+1).cumprod()-1   
            daily_portfolio_cumulative_pnl=daily_portfolio_pnl[['Date','Cumulative_PNL%']]
            daily_portfolio_pnl=daily_portfolio_pnl[['Date','PNL%']]
            daily_portfolio_cumulative_pnl.columns=['Date','Portfolio']
            daily_portfolio_cumulative_pnl['Date'] = pd.to_datetime(daily_portfolio_cumulative_pnl['Date'])
            daily_portfolio_pnl['Date'] = pd.to_datetime(daily_portfolio_pnl['Date'])
            return daily_portfolio_cumulative_pnl,daily_portfolio_pnl


        if portfolio_type=='General':
            cpd=customer_portfolio_details[['Company','Factset_Ticker','Quantity']]
        
            stocks_with_exposure=ExposureCalculation(cpd,upm)
            DailyReturns=StockCumulativeReturns(stocks_with_exposure,upm)
            portfolio_volatility=AnnualizeVolatility(DailyReturns)
        else:
            Portfolio_CumulativePNL,DailyReturns =  Live_PortfolioCumulativeReturns(start_date,current_date,user_name,portfolio_name,'IN')
            DailyReturns.columns=['Date','Return']
            portfolio_volatility=AnnualizeVolatility(DailyReturns)

        nifty_50=mi[mi['Company']=='Nifty 50 Index']
        nifty_50.reset_index(drop=True,inplace=True)
        niftyReturns = nifty_50.copy()
        niftyReturns=niftyReturns[['Date','Return']]

        #niftyReturns =nifty_returns.groupby(['Date'])['Return'].agg('sum').reset_index()  
        nifty_volatility= AnnualizeVolatility(niftyReturns) 

        nifty_small=mi[mi['Company']=='Nifty Smallcap 100 Index']
        nifty_small.reset_index(drop=True,inplace=True)
        niftySmallReturns = nifty_small[['Date','Return']]
        #niftySmallReturns =nifty_small_returns.groupby(['Date'])['Return'].agg('sum').reset_index()  
        nifty_small_volatility= AnnualizeVolatility(niftySmallReturns)    #calculating day wise PNL%     

        nifty_mid_cap=mi[mi['Company']=='Nifty Midcap 100 Index']
        nifty_mid_cap.reset_index(drop=True,inplace=True)
        niftyMidReturns = nifty_mid_cap[['Date','Return']]
        #niftyMidReturns =nifty_mid_returns.groupby(['Date'])['Return'].agg('sum').reset_index()  
        nifty_mid_volatility= AnnualizeVolatility(niftyMidReturns)    #calculating day wise PNL% 

        niftyReturns=niftyReturns.sort_values(by='Date').reset_index(drop=True)
        niftySmallReturns=niftySmallReturns.sort_values(by='Date').reset_index(drop=True)
        niftyMidReturns=niftyMidReturns.sort_values(by='Date').reset_index(drop=True)
        DailyReturns=DailyReturns.sort_values(by='Date').reset_index(drop=True)



        dailyreturns=DailyReturns.copy()
        dailyreturns.columns=['Date','Portfolio_Pnl']
        niftyReturns.columns=['Date','nifty50_returns']
        niftySmallReturns.columns=['Date','nifty_smallcap_returns']
        niftyMidReturns.columns=['Date','nifty_midcap_returns']
        returns = pd.merge(dailyreturns,niftyReturns,on='Date',how='inner')
        index1_returns = pd.merge(niftyReturns,niftySmallReturns,on='Date',how='inner')
        index2_returns = pd.merge(niftyReturns,niftyMidReturns,on='Date',how='inner')
        
        portfolio_beta = 1
        nifty50_beta = 1
        nifty_small_cap =1
        nifty_mid_cap = 1
        if abs(len(dailyreturns)-len(niftyReturns))<=5:
            portfolio_beta = stats.linregress(returns["nifty50_returns"].values,returns["Portfolio_Pnl"].values)[0:1][0]

        if abs(len(niftySmallReturns)-len(niftyReturns))<=5:
            nifty_small_cap = stats.linregress(index1_returns["nifty50_returns"].values,index1_returns["nifty_smallcap_returns"].values)[0:1][0]

        if abs(len(niftyMidReturns)-len(niftyReturns))<=5:
            nifty_mid_cap = stats.linregress(index2_returns["nifty50_returns"].values,index2_returns["nifty_midcap_returns"].values)[0:1][0]




        factor_list=['Beta','Volatility','PE','PB','Div Yield','Sales Growth','EPS Growth','ROE']

        portfolio_list=[portfolio_beta,portfolio_volatility,
                        p['PE_Product'].sum()/p['Inv_Amount'].sum(),
                        o['PB_Product'].sum()/o['Inv_Amount'].sum(),
                        q['Div_Yield_Product'].sum()/q['Inv_Amount'].sum(),
                        r['Sales_Growth_Product'].sum()/r['Inv_Amount'].sum(),
                        s['EPS_Growth_Product'].sum()/s['Inv_Amount'].sum(),
                        t['ROE_Product'].sum()/t['Inv_Amount'].sum()
                        ]

        nifty50_list=[nifty50_beta,nifty_volatility,
                    a2['PE_Product'].sum()/a2['Market Cap'].sum(),
                    a1['PB_Product'].sum()/a1['Market Cap'].sum(),
                    a3['Div_Yield_Product'].sum()/a3['Market Cap'].sum(),
                    a4['Sales_Growth_Product'].sum()/a4['Market Cap'].sum(),
                    a5['EPS_Growth_Product'].sum()/a5['Market Cap'].sum(),
                    a6['ROE_Product'].sum()/a6['Market Cap'].sum()
                        ]           

        niftysmall_list=[nifty_small_cap,nifty_small_volatility,
                        b2['PE_Product'].sum()/b2['Market Cap'].sum(),
                        b1['PB_Product'].sum()/b1['Market Cap'].sum(),
                        b3['Div_Yield_Product'].sum()/b3['Market Cap'].sum(),
                        b4['Sales_Growth_Product'].sum()/b4['Market Cap'].sum(),
                        b5['EPS_Growth_Product'].sum()/b5['Market Cap'].sum(),
                        b6['ROE_Product'].sum()/b6['Market Cap'].sum()
                        ]
        
        niftymid_list=[nifty_mid_cap,nifty_mid_volatility,
                        c2['PE_Product'].sum()/c2['Market Cap'].sum(),
                        c1['PB_Product'].sum()/c1['Market Cap'].sum(),
                        c3['Div_Yield_Product'].sum()/c3['Market Cap'].sum(),
                        c4['Sales_Growth_Product'].sum()/c4['Market Cap'].sum(),
                        c5['EPS_Growth_Product'].sum()/c5['Market Cap'].sum(),
                        c6['ROE_Product'].sum()/c6['Market Cap'].sum()
                        ]


        final_df=pd.DataFrame({'Factor':factor_list,'Portfolio':portfolio_list,'Nifty 50':nifty50_list,
                                'Nifty Small Cap 100':niftysmall_list,'Nifty Mid Cap 100':niftymid_list})


        return final_df
    except Exception as e:
        return pd.DataFrame()   

