from django.contrib.sitemaps import Sitemap
from django.shortcuts import reverse
from django.urls.resolvers import RegexPattern,RoutePattern
from mysite import urls


class StaticViewSitemap(Sitemap):

    def items(self):
        try:
            url_list=[u.name for u in urls.urlpatterns if hasattr(u,'name') and u.name]
        except Exception:
            pass
        return url_list

    def location(self, item):
        return reverse(item)

