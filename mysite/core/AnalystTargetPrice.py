import pandas as pd
import pyodbc
from datetime import date
from .models import CustomerPortfolioDetails, UniversePricesMain, TargetPrices, MarketIndex, UsUniversePricesMain, UsMarketIndex, UsTargetPrices, PortfolioV5
from django.db.models import Max
#target prices data calculation
def analystTargetPricesData(date,user,portfolio_name,portfolio_type,current_country,Stock_Data,universe_prices,target_data):
    try:
        stock = list(Stock_Data['Factset_Ticker'])
        if target_data.empty:
            return target_data        
        else:
            target_data = target_data[['company','ticker','current_price','avg_upside','avg_downside','number_52_week_high','number_52_week_low']]
            target_data.columns = ['Company', 'Factset_Ticker','Price','Avg_Upside','Avg_Downside','Week High 52','Week Low 52']
            prices_data=pd.merge(Stock_Data,target_data,on=['Company','Factset_Ticker'],how='left')
            if portfolio_type=='General':
                prices_data['current_exposure']=prices_data['Price']*prices_data['Quantity']
                prices_data['weights']=prices_data['current_exposure']/(prices_data['current_exposure'].sum())
            else:
                exposure_data = pd.DataFrame(list(PortfolioV5.objects.filter(customer_name=str(user),portfolio_name=portfolio_name).values('company','value_mkt_price')))
                exposure_data.columns=['Company','VALUE_MKT_PRICE']
                exposure_data['weights']=exposure_data['VALUE_MKT_PRICE']/(exposure_data['VALUE_MKT_PRICE'].sum())
                exposure_data=exposure_data[['Company','weights']]
                prices_data=pd.merge(prices_data,exposure_data,on='Company',how='inner')                
            prices_data['up_return']=(prices_data['Avg_Upside']/prices_data['Price'])-1
            prices_data['down_return']=(prices_data['Avg_Downside']/prices_data['Price'])-1
            prices_data['up_down_ratio']=prices_data['up_return']/abs(prices_data['down_return'])
            prices_data=prices_data[['Company','Factset_Ticker', 'Price','weights','Avg_Upside',  'Avg_Downside',  'Week High 52',  'Week Low 52','up_return',  'down_return' , 'up_down_ratio']]
            prices_data.fillna(0,inplace=True)
            return prices_data
    except Exception as e:
        emptyDataFrame=pd.DataFrame()
        return emptyDataFrame