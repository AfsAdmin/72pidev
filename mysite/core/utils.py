from io import BytesIO 
from django.http import HttpResponse
from django.template.loader import get_template
from xhtml2pdf import pisa  
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from mysite.core.Decorators import SubscriptionValidation
from django.http import  HttpResponse
from django.shortcuts import render, redirect
from mysite.core.CacheBuilder import getPortfolioWiseDataFromCacheBuilder
import json
from datetime import datetime, date
from mysite.core.models import CustomerSubscription, MyProfile

#utils start here
@login_required
@csrf_exempt
def daemonCreation(request):
    if not request.is_ajax():
        if not CustomerSubscription.objects.filter(customer_name=request.user).exists():
            myprofile = MyProfile.objects.filter(user=request.user).all()
            first_name, last_name, email , contact_number = None,None,None,None
            for item in myprofile:
                first_name = item.first_name
                last_name = item.last_name
                email = item.email
                contact_number = item.contact_number
            if first_name:
                pass
            else:
                first_name = request.user.first_name
            if last_name:
                pass
            else:
                last_name = request.user.last_name
            if email:
                pass
            else:
                email = request.user.email
            from datetime import date, timedelta
            endsubscription = date.today() + timedelta(days=30)
            startsubscription = date.today()
            CustomerSubscription(customer_name=request.user, first_name=first_name,last_name=last_name, email=email,contact_no=contact_number,
            subscription_type='FREE',subscription_starts=startsubscription,subscription_ends=endsubscription,
            subscription_status='ACTIVE', subscription_plan='30 DAY TRIAL', paid='NO',currency='NA').save()
        else:
            pass #Nothing to do
        redirect_url = request.session['redirect_url']
        context = {'redirect_url': redirect_url}
        return render(request, 'cachebuilder.html',context)
    else:
        redirect_url = request.session['redirect_url']
        context = {'redirect_url': redirect_url}
        try:
            import threading
            bg_thread = threading.Thread(target=getPortfolioWiseDataFromCacheBuilder,
                                    args=(request.user.username,))   #Building the cache for the user to all his portfolios
            bg_thread.start()
            import time
            time.sleep(5)
            bg_thread.join()
        except Exception as e:
            print(e)
        return HttpResponse(json.dumps(context), content_type='application/json')



def render_to_pdf(template_src, context_dict={}):
     template = get_template(template_src)
     html  = template.render(context_dict)
     result = BytesIO()
     pdf = pisa.pisaDocument(BytesIO(html.encode("ISO-8859-1")), result)
     if not pdf.err:
         return HttpResponse(result.getvalue(), content_type='application/pdf')
     return None

