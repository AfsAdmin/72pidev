# User defined exceptions

class InvalidQuantityError(Exception):

    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return(repr(self.msg))


class NoStocksMatchedError(Exception):

    def __init__(self,msg):
        self.msg = msg
    
    def __str__(self):
        return (repr(self.msg))

class PortfolioExistsError(Exception):
    def __init__(self,msg):
        self.msg = msg
    
    def __str__(self):
        return (repr(self.msg))


class InvalidFormError(Exception):

    def __init__(self,msg):
        self.msg = msg
    
    def __str__(self):
        return (repr(self.msg))

class IncompleteFieldsException(Exception):

    def __init__(self,msg):
        self.msg = msg
    
    def __str__(self):
        return (repr(self.msg))

