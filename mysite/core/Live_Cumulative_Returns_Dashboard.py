# -*- coding: utf-8 -*-

import pandas as pd
import pyodbc
import math
from mysite.core.models import *
from django.db.models import Avg, Max, Min, Sum

# sql_conn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server}; SERVER=GHCDC01; DATABASE=datax10;Trusted_Connection=yes') 




def LivePortfolioCumulativeReturns(StartDate,EndDate,user_name,portfolio_name,country):
    if country=='India':
        daily_positions = DailyPositions.objects.filter(date__range=(StartDate,EndDate),customer_name=user_name,portfolio_name=portfolio_name).values('date','stock','p_lrealized','p_lunrealized','aum_bod')
        input_data = pd.DataFrame(list(daily_positions))
        input_data.columns = ['Date','Company','P&LRealized','P&LUnrealized','AUM_BOD']
    else:
        daily_positions = UsDailyPositions.objects.filter(date__range=(StartDate,EndDate),customer_name=user_name,portfolio_name=portfolio_name).values('date','stock','p_lrealized','p_lunrealized','aum_bod')
        input_data = pd.DataFrame(list(daily_positions))
        input_data.columns = ['Date','Company','P&LRealized','P&LUnrealized','AUM_BOD']

    input_data.sort_values(by=['Date','Company'],inplace=True)

    input_data['PNL%'] = (input_data['P&LRealized']+input_data['P&LUnrealized'])/input_data['AUM_BOD']
    daily_portfolio_pnl=input_data.groupby(['Date'])['PNL%'].sum().reset_index()

    daily_portfolio_pnl["Cumulative_PNL%"] = (daily_portfolio_pnl['PNL%']+1).cumprod()-1   
    daily_portfolio_cumulative_pnl=daily_portfolio_pnl[['Date','Cumulative_PNL%']]
    daily_portfolio_pnl=daily_portfolio_pnl[['Date','PNL%']]
    daily_portfolio_cumulative_pnl.columns=['Date','Portfolio']
    daily_portfolio_cumulative_pnl['Date'] = pd.to_datetime(daily_portfolio_cumulative_pnl['Date'])
    daily_portfolio_pnl['Date'] = pd.to_datetime(daily_portfolio_pnl['Date'])
    return daily_portfolio_cumulative_pnl,daily_portfolio_pnl



def EqualPortfolioReturns(StartDate,EndDate,user_name,portfolio_name,country):
    if country=='India':
        minimum_date = UniversePricesMain.objects.all().aggregate(Min('date'))['date__min']
        StartDate = DailyPositions.objects.filter(date__lte=StartDate,customer_name=user_name,portfolio_name=portfolio_name).all().aggregate(Max('date'))['date__max']
    if country=='India':
        customer_stocks=list(CustomerPortfolioDetails.objects.filter(customer_name=user_name,portfolio_name=portfolio_name).values ('company_name','quantity'))
        stocks_data=pd.DataFrame(customer_stocks)
        stocks_data.columns=['Company','Quantity']
        stock=list(stocks_data['Company'])
        returns_data=list(UniversePricesMain.objects.filter(date__range=(StartDate,EndDate),company__in=stock).values('date','company','price','return_field'))
        input_returns=pd.DataFrame(returns_data)
        input_returns.columns=['Date','Company','Price','Return']
        DailyReturns=pd.merge(input_returns,stocks_data,on='Company',how='inner')                
    if (len(DailyReturns)!=0):
        Equal_weighted_Portfolio= DailyReturns[['Date','Company','Price','Return','Quantity']]
        Equal_weighted_Portfolio.sort_values(by=['Date','Company'],ascending=[True,True],inplace=True)
        Equal_weighted_Portfolio.reset_index(drop=True,inplace=True)
        starting_prices =  Equal_weighted_Portfolio.groupby('Company').first().reset_index()
        starting_prices=starting_prices[['Company','Date','Price']]
        Stocks=Equal_weighted_Portfolio[['Company','Quantity']].drop_duplicates()
        starting_prices.reset_index(inplace=True)
        new_stock_weigths = pd.merge(Stocks[['Company','Quantity']],starting_prices,on='Company',how='inner')
        new_stock_weigths['Exposure']=new_stock_weigths['Quantity']*new_stock_weigths['Price']
        new_exposure = (new_stock_weigths['Exposure'].sum())/(len(new_stock_weigths))
        new_stock_weigths['New Quantity']=new_exposure/(new_stock_weigths['Price'])
        new_stock_weigths=new_stock_weigths[['Company','New Quantity']]
        new_stock_weigths.columns=['Company','Quantity']        
        Equal_weighted_Portfolio=Equal_weighted_Portfolio[['Date','Company','Price','Return']]
        Equal_weighted_Portfolio=pd.merge(Equal_weighted_Portfolio,new_stock_weigths,on="Company",how="inner")
        Equal_weighted_Portfolio['StockDayNet']=Equal_weighted_Portfolio['Price']*Equal_weighted_Portfolio['Quantity']
        Equal_Net_Data=Equal_weighted_Portfolio.groupby(['Date'])["StockDayNet"].agg('sum').reset_index() 
        Equal_Net_Data.columns=['Date','PortfolioDayNet']
        Equal_weighted_Portfolio=pd.merge(Equal_weighted_Portfolio,Equal_Net_Data,on="Date",how="inner")
        Equal_weighted_Portfolio["Net%"] = Equal_weighted_Portfolio["StockDayNet"]/Equal_weighted_Portfolio['PortfolioDayNet']        
        Equal_weighted_Portfolio['PNL']= Equal_weighted_Portfolio['Return'] *  Equal_weighted_Portfolio.groupby(['Company'])['Net%'].shift(1) 
        Equal_PNL_data=Equal_weighted_Portfolio.groupby(['Date'])['PNL'].agg('sum').reset_index()      
        Equal_PNL_data.columns=["Date","PNL%"]
        Equal_PNL_data['Date']=pd.to_datetime(Equal_PNL_data['Date'])
        Equal_PNL_data["EqualCumulativePNL"] = (Equal_PNL_data['PNL%']+1).cumprod()-1  
        if str(StartDate)==str(minimum_date):
            Equal_Cumulative_PNL_data=Equal_PNL_data[['Date','EqualCumulativePNL']]
            Equal_Cumulative_PNL_data.columns=['Date','Equal_Portfolio']
        else:
            Equal_Cumulative_PNL_data=Equal_PNL_data[['Date','EqualCumulativePNL']][1:] 
            Equal_Cumulative_PNL_data.columns=['Date','Equal_Portfolio']
        Equal_PNL_data=Equal_PNL_data[['Date','PNL%']]
        return Equal_Cumulative_PNL_data,Equal_PNL_data
    
    
def IndexCumulativeReturns (index_ticker,StartDate,EndDate,country):
    if country=='India':
        market_indexes = MarketIndex.objects.filter(date__range=(StartDate,EndDate),company=index_ticker).values('date','company','return_field').order_by('date')
    else:
        market_indexes = UsMarketIndex.objects.filter(date__range=(StartDate,EndDate),company=index_ticker).values('date','company','return_field').order_by('date')

    index_cumulative_data = pd.DataFrame(list(market_indexes))
    index_cumulative_data = index_cumulative_data[['date', 'company', 'return_field']]
    index_cumulative_data.columns = ['Date' , 'Company' , 'Return']   
    index_cumulative_data.fillna(0,inplace=True)   
    index_cumulative_data["Cumulative Return"] = (index_cumulative_data['Return']+1).cumprod()-1 
    IndexCumulativeReturn=index_cumulative_data[["Date","Cumulative Return"]]
    IndexReturns=index_cumulative_data[['Date','Return']]
    IndexCumulativeReturn.columns=['Date',index_ticker]
    IndexCumulativeReturn['Date'] = pd.to_datetime(IndexCumulativeReturn['Date'])
    IndexReturns['Date'] = pd.to_datetime(IndexReturns['Date'])
    return IndexCumulativeReturn,IndexReturns

def AnnualizeReturn (CumulativePNL_Daily):
    Daily_Portfolio_Cumulative_PNL=CumulativePNL_Daily.copy()
    annualize_ret=((Daily_Portfolio_Cumulative_PNL.iat[len(Daily_Portfolio_Cumulative_PNL)-1,1] +1) ** (252/len(Daily_Portfolio_Cumulative_PNL)))-1   # annualize returns by taking cumulative returns
    return annualize_ret

def AnnualizeIndexReturn(index_cumulative_data):
    index_cumulative=index_cumulative_data.copy()
    annualize_index_ret=((index_cumulative.iat[len(index_cumulative)-1,1] +1) ** (252/len(index_cumulative)))-1
    return annualize_index_ret

#Function for AnnualizeVolatility
def AnnualizeVolatility (Daily_pnl_data):
    Portfolio_pnl=Daily_pnl_data.copy()
    Portfolio_pnl.columns=['Date','PNL%']
    annualize_vol=Portfolio_pnl.loc[:,"PNL%"].std()     
    return annualize_vol*(math.sqrt(252))

#Function for AnnualizeRiskFreeRate
def AnnualizeRiskFreeRate(Input_Risk_Free_Rate):
    Input_Risk_Free_Rate_Data=Input_Risk_Free_Rate.copy()
    Input_Risk_Free_Rate_Data["Risk Free Rate_cumulate"] = (Input_Risk_Free_Rate_Data['Risk Free Rate']+1).cumprod()-1                               #Calculate the Cumulative PNL
    annualize_risk_free=((Input_Risk_Free_Rate_Data.iat[len(Input_Risk_Free_Rate_Data)-1,2] +1) ** (252/len(Input_Risk_Free_Rate_Data)))-1
    return annualize_risk_free

#Function for Max_DrawDown
def Max_DrawDown(CumulativePNL_Daily,StartDate,EndDate):
    portfolio_pnl=CumulativePNL_Daily.copy()
    portfolio_pnl.columns=['Date','PNL']
    portfolio_pnl["Max Cumulative"]=portfolio_pnl["PNL"].cummax(axis = 0)     #find max value of cumulatives at given point
    draw_down_input=pd.DataFrame()
    draw_down_input["Cumulative"]=portfolio_pnl["PNL"]
    draw_down_input["Max Cumulative"]=portfolio_pnl["Max Cumulative"]
    draw_down_input["Draw_down"]=1-((1+draw_down_input["Cumulative"])/(1+draw_down_input["Max Cumulative"]))
    max_drop_down_res=draw_down_input["Draw_down"].max()
    return max_drop_down_res

def PortfolioReturnsMain(StartDate,EndDate,user_name,portfolio_name,country,perType):
    try:
        if country=='India':
            Index_Ticker1,Index_Ticker2,Index_Ticker3="Nifty 50 Index","Nifty Smallcap 100 Index","Nifty Midcap 100 Index" 
        else:
            Index_Ticker1,Index_Ticker2,Index_Ticker3='S&P 500','Russell 2000','Dow Jones Industrial Average'
            
        Portfolio_CumulativePNL,Portfolio_daily_pnl =  LivePortfolioCumulativeReturns(StartDate,EndDate,user_name,portfolio_name,country)
        Equal_Portfolio_CumulativePNL,Equal_Portfolio_daily_pnl=EqualPortfolioReturns(StartDate,EndDate,user_name,portfolio_name,country)
        Index1_CumulativeReturns,Index1_Daily_Return = IndexCumulativeReturns (Index_Ticker1,StartDate,EndDate,country)
        Index2_CumulativeReturns,Index2_Daily_Return = IndexCumulativeReturns (Index_Ticker2,StartDate,EndDate,country)
        Index3_CumulativeReturns,Index3_Daily_Return = IndexCumulativeReturns (Index_Ticker3,StartDate,EndDate,country)
        
        #Annualize_Returns        
        Portfolio_Annualize_Return = AnnualizeReturn (Portfolio_CumulativePNL)
        Equal_Portfolio_Annualize_Return=AnnualizeReturn(Equal_Portfolio_CumulativePNL)
        Index1_Annualize_Return = AnnualizeIndexReturn (Index1_CumulativeReturns)
        Index2_Annualize_Return = AnnualizeIndexReturn (Index2_CumulativeReturns)
        Index3_Annualize_Return = AnnualizeIndexReturn (Index3_CumulativeReturns)
        
        #Annualize_Volatiliy

        Annualize_Portfolio_Volatility = AnnualizeVolatility(Portfolio_daily_pnl)
        Annualize_Equal_Portfolio_Volatility = AnnualizeVolatility(Equal_Portfolio_daily_pnl)
        Annualize_Index1_Volatility = AnnualizeVolatility(Index1_Daily_Return)
        Annualize_Index2_Volatility =AnnualizeVolatility(Index2_Daily_Return)
        Annualize_Index3_Volatility =AnnualizeVolatility(Index3_Daily_Return)
        
        #Annualize Risk Free Rate
        if country=='India':
            risk_data = RiskFreeRate.objects.filter(date__range=(StartDate,EndDate)).values('date','risk_free_rate').order_by('date')
            Risk_Free_Data=pd.DataFrame(list(risk_data))
            Risk_Free_Data.columns=['Date','Risk Free Rate']
        else:
            risk_data = USRiskFreeRate.objects.filter(date__range=(StartDate,EndDate)).values('date','risk_free_rate').order_by('date')
            Risk_Free_Data=pd.DataFrame(list(risk_data))
            Risk_Free_Data.columns=['Date','Risk Free Rate']

        Annualize_Risk_Free_Rate=AnnualizeRiskFreeRate (Risk_Free_Data)
        
        #Max_DrawDown
        Fund_Max_DrawDown = Max_DrawDown(Portfolio_CumulativePNL,StartDate,EndDate)
        Equal_Fund_Max_DrawDown = Max_DrawDown(Equal_Portfolio_CumulativePNL,StartDate,EndDate)
        Index1_Max_DrawDown = Max_DrawDown(Index1_CumulativeReturns,StartDate,EndDate)
        Index2_Max_DrawDown = Max_DrawDown(Index2_CumulativeReturns,StartDate,EndDate)
        Index3_Max_DrawDown = Max_DrawDown(Index3_CumulativeReturns,StartDate,EndDate)
        
        #Sharpe
        Fund_Sharpe = (Portfolio_Annualize_Return - Annualize_Risk_Free_Rate)/Annualize_Portfolio_Volatility
        Equal_Fund_Sharpe =(Equal_Portfolio_Annualize_Return - Annualize_Risk_Free_Rate)/Annualize_Equal_Portfolio_Volatility
        Index1_Sharpe = (Index1_Annualize_Return - Annualize_Risk_Free_Rate)/Annualize_Index1_Volatility
        Index2_Sharpe = (Index2_Annualize_Return - Annualize_Risk_Free_Rate)/Annualize_Index2_Volatility
        Index3_Sharpe = (Index3_Annualize_Return - Annualize_Risk_Free_Rate)/Annualize_Index3_Volatility
        
        if perType!='ITD':
            Portfolio_Annualize_Return=Portfolio_CumulativePNL.iat[len(Portfolio_CumulativePNL)-1,1]    # annualize returns by taking cumulative return
            Equal_Portfolio_Annualize_Return=Equal_Portfolio_CumulativePNL.iat[len(Equal_Portfolio_CumulativePNL)-1,1]    # annualize returns by taking cumulative return
            Index1_Annualize_Return=Index1_CumulativeReturns.iat[len(Index1_CumulativeReturns)-1,1]
            Index2_Annualize_Return=Index2_CumulativeReturns.iat[len(Index2_CumulativeReturns)-1,1]
            Index3_Annualize_Return=Index3_CumulativeReturns.iat[len(Index3_CumulativeReturns)-1,1]
        


        Cumulative_Returns = pd.merge(Portfolio_CumulativePNL,Equal_Portfolio_CumulativePNL,on="Date",how="inner")    
        Cumulative_Returns = pd.merge(Cumulative_Returns,Index1_CumulativeReturns,on="Date",how="inner") 
        Cumulative_Returns = pd.merge(Cumulative_Returns,Index2_CumulativeReturns,on="Date",how="inner")
        Cumulative_Returns = pd.merge(Cumulative_Returns,Index3_CumulativeReturns,on="Date",how="inner")
        Cumulative_Returns =  Cumulative_Returns[["Date","Portfolio",'Equal_Portfolio',Index_Ticker1,Index_Ticker2,Index_Ticker3]]


        Portfolio_daily_pnl.columns=['Date','Portfolio Return']
        Equal_Portfolio_daily_pnl.columns=['Date','Equal Portfolio Return']
        Index1_Daily_Return.columns=['Date',Index_Ticker1 + " Return"]
        Index2_Daily_Return.columns=['Date',Index_Ticker2 + " Return"]
        Index3_Daily_Return.columns=['Date',Index_Ticker3 + " Return"]

        Daily_Returns_Values = pd.merge(Portfolio_daily_pnl,Equal_Portfolio_daily_pnl,on="Date",how="inner")    
        Daily_Returns_Values = pd.merge(Daily_Returns_Values,Index1_Daily_Return,on="Date",how="inner") 
        Daily_Returns_Values = pd.merge(Daily_Returns_Values,Index2_Daily_Return,on="Date",how="inner")
        Daily_Returns_Values = pd.merge(Daily_Returns_Values,Index3_Daily_Return,on="Date",how="inner")


        Results=pd.DataFrame(columns=["C","Annualized Return","Annualized Volatility","Sharpe","Maximum DrawDown"])
        Companies=['Portfolio','Equal_Portfolio',Index_Ticker1,Index_Ticker2,Index_Ticker3]
        Annnl_Return_values=[Portfolio_Annualize_Return,Equal_Portfolio_Annualize_Return,Index1_Annualize_Return,Index2_Annualize_Return,Index3_Annualize_Return]
        Annnl_Volatility_values= [Annualize_Portfolio_Volatility,Annualize_Equal_Portfolio_Volatility,Annualize_Index1_Volatility,Annualize_Index2_Volatility,Annualize_Index3_Volatility]
        Sharpe_values=[Fund_Sharpe,Equal_Fund_Sharpe,Index1_Sharpe,Index2_Sharpe,Index3_Sharpe]
        Max_DD_Values = [Fund_Max_DrawDown,Equal_Fund_Max_DrawDown,Index1_Max_DrawDown,Index2_Max_DrawDown,Index3_Max_DrawDown]
        Results['C']=Companies
        Results["Annualized Return"] = Annnl_Return_values
        Results["Annualized Volatility"] = Annnl_Volatility_values
        Results["Sharpe"] = Sharpe_values
        Results["Maximum DrawDown"] = Max_DD_Values
        Results.columns=["","Annualized Return","Annualized Volatility","Sharpe","Maximum DrawDown"]
        Cumulative_Returns.columns=['Trade_Dt','PNL','Equal_PNL',Index_Ticker1,Index_Ticker2,Index_Ticker3]
        return Cumulative_Returns,Daily_Returns_Values,Results
    except Exception as e:
        return pd.DataFrame(),pd.DataFrame(),pd.DataFrame()
