from django.contrib import admin
from import_export.admin import ImportExportModelAdmin
from rangefilter.filter import DateRangeFilter
from mysite.core.models import *
from django.contrib.auth.models import User

# Register your models here.
admin.site.site_header = '72PI Adminstration'                    
admin.site.index_title = 'Welcome to 72PI admin portal'                
admin.site.site_title = '72PI Admin Portal'
admin.site.unregister(User)


@admin.register(DailyMarketUpdateEmail)
class DailyMarketUpdateEmailAdmin(admin.ModelAdmin):
    list_display = ('plan_date', 'subject', 'to_be_sent', 'sent')
    list_filter = (('plan_date', DateRangeFilter),'to_be_sent', 'sent')

@admin.register(EmailCampaignDetails)
class EmailCampaignAdmin(ImportExportModelAdmin):
    list_display = ('first_name', 'email', 'contact_no', 'unsubscribed','unsubscribe_reason', 'active','remarks')
    list_filter = ('unsubscribed', 'active',)

@admin.register(User)
class UserAdmin(ImportExportModelAdmin):
    search_fields = ('username','first_name', 'last_name', 'email',)
    list_display = ('username','first_name', 'last_name', 'email', 'date_joined','is_active', 'is_superuser')
    list_filter = (('date_joined', DateRangeFilter),'is_active', 'is_superuser',('last_login',DateRangeFilter))
    filter_horizontal = ()
    fieldsets = ()


@admin.register(CustomerSubscription)
class CustomerSubscriptionAdmin(admin.ModelAdmin):
    search_fields = ('email','contact_no','subscription_type','subscription_plan','subscription_status','customer_name__id',)
    list_display = ('customer_name', 'first_name', 'email', 'contact_no','subscription_type','subscription_plan','subscription_status', 'paid','currency',)
    list_filter = ('subscription_status', 'subscription_type', 'subscription_plan', 'paid', 'currency',)
    filter_horizontal = ()
    fieldsets = ()



@admin.register(MyProfile)
class MyProfileAdmin(admin.ModelAdmin):
    list_display = ('user', 'first_name','last_name', 'contact_number')

@admin.register(TradeSignals)
class TradeSignalsAdmin(admin.ModelAdmin):
    list_display = ('company_name','fundamental','fund_rating','quantitative','quant_rating','technical','technical_rating','situations','situations_rating')

@admin.register(TradeIdeasPerformance)
class TradeIdeasPerformanceAdmin(admin.ModelAdmin):
    list_display = ('company_name','buy_sell','issue_date','purchased_price','target_price','time_period','performance','action','signal_message')
    list_filter = ('buy_sell',)
    
@admin.register(USTradeSignals)
class USTradeSignalsAdmin(admin.ModelAdmin):
    list_display = ('company_name','fundamental','fund_rating','quantitative','quant_rating','technical','technical_rating','situations','situations_rating')

@admin.register(USTradeIdeasPerformance)
class USTradeIdeasPerformanceAdmin(admin.ModelAdmin):
    list_display = ('company_name','buy_sell','issue_date','purchased_price','target_price','time_period','performance','action','signal_message')
    list_filter = ('buy_sell',)

@admin.register(FuturesAndOptions)
class FuturesAndOptionsAdmin(admin.ModelAdmin):
    list_display = ('company_name','begin_price','High_Probability_Range','Remark_1','signal','end_price','Signal_Efficiency')
    list_filter = ('month', 'Signal_Efficiency','Remark_1')

@admin.register(CustomerPriceMovementsAlerts)
class CustomerPriceMovementsAlertsAdmin(admin.ModelAdmin):
    list_display = ('customer_name','portfolio_name','company_name','last_price','current_price','price_52wh','price_value_condition','price_value',
                    'price_percent_condition','price_percent','price_52wh_condition','price_52wh_percent',
                    'price_status','percent_status','price52wh_status')
    list_filter = ('price_status', 'percent_status','price52wh_status','price_value_condition','price_percent_condition','price_52wh_condition')

@admin.register(CustomerTechnicalIndicationAlerts)
class CustomerTechnicalIndicationAlertsAdmin(admin.ModelAdmin):
    list_display = ('customer_name','portfolio_name','company_name','rsi_metric','rsi_condition','rsi_operand1','rsi_operand2','dma_operand1',
                    'dma_condition','dma_operand2', 'current_price','rsi_status','dma_status')
    list_filter = ('rsi_status', 'rsi_condition','dma_status','dma_condition')

@admin.register(CustomerFeedback)
class CustomerFeedbackAdmin(admin.ModelAdmin):
    list_display = ('customer_name','rating','category','feedback')
    list_filter = ('rating', 'category')
