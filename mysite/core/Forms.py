from django import forms
from mysite.core.models import *
from mysite.core.validators import *
 

class SignupForm(forms.Form):
    first_name = forms.CharField(max_length=30, label='First Name')
    last_name = forms.CharField(max_length=30, required=False,label='Last Name')
    contact_number = forms.CharField(max_length=15,required=False, label='Contact No',validators=[contactValidation])
    
    def signup(self, request, user):
        user.first_name = self.cleaned_data['first_name']
        user.last_name = self.cleaned_data['last_name']
        MyProfile(user=user,first_name=self.cleaned_data['first_name'],last_name=self.cleaned_data['last_name'],email=self.cleaned_data['email'],contact_number=self.cleaned_data['contact_number']).save()
        user.save()
    

class MyProfileForm(forms.ModelForm):
    class Meta:
        model = MyProfile
        fields = ['first_name', 'last_name', 'email', 'contact_number']

    first_name = forms.CharField(max_length=30, label='First Name')
    last_name = forms.CharField(max_length=30, label='Last Name')
    email = forms.EmailField(max_length=255, label='Email')
    contact_number = forms.CharField(max_length=15,label='Contact No',validators=[contactValidation])
    

        
class RegistrationForm(forms.Form):
    first_name = forms.CharField(label='First Name', required=False, max_length=15,  
    error_messages={'required': 'Firstname is required.',
                    'invalid': 'Enter a your firstname'},
                         widget=forms.TextInput(attrs={'placeholder':'Enter your firstname','autocomplete': 'off'}))
    last_name = forms.CharField(label='Last Name', required=False,max_length=15,
    error_messages={'required': 'Firstname is required.',
                    'invalid': 'Enter a your firstname'},
                         widget=forms.TextInput(attrs={'placeholder':'Enter your lastname','autocomplete': 'off'}))
    email = forms.EmailField(label='Email', required=False, 
    error_messages={'required': 'Firstname is required.',
                    'invalid': 'Enter a your firstname'},
                         widget=forms.TextInput(attrs={'placeholder':'Enter your email','autocomplete': 'off'}))
    username = forms.CharField(label='Username', required=False, max_length=12,
    error_messages={'required': 'Firstname is required.',
                    'invalid': 'Enter a your firstname'},
                         widget=forms.TextInput(attrs={'placeholder':'Enter your username','autocomplete': 'off'}))
    password1 = forms.CharField(label='Password', required=False,
    error_messages={'required': 'Firstname is required.',
                    'invalid': 'Enter a your firstname'},
                         widget=forms.PasswordInput(attrs={'placeholder':'Set your password','autocomplete': 'off'}))
    password2 = forms.CharField(label='Confirm Password', required=False, 
    error_messages={'required': 'Firstname is required.',
                    'invalid': 'Enter a your firstname'},
                         widget=forms.PasswordInput(attrs={'placeholder':'Confirm your password','autocomplete': 'off'}))
 
    

    def clean_first_name(self):
        firstname = self.cleaned_data['first_name']
        if firstname == '':
            raise ValidationError('Firstname is required.')
        return firstname
    
    def clean_last_name(self):
        lastname = self.cleaned_data['last_name']
        return lastname

    def clean_username(self):
        username = self.cleaned_data['username'].lower()
        if username == '':
            raise  ValidationError('Username is required.')
        else:
            if  len(username) < 4:
                raise ValidationError('Username must be minimum 4 characters.')
        if User.objects.filter(username=username).count():
            raise  ValidationError('Username already exists')
        return username
 
    def clean_email(self):
        email = self.cleaned_data['email'].lower()
        if email == '':
            raise ValidationError('Email is required.')
        if User.objects.filter(email=email).count():
            raise  ValidationError('Email already exists')
        return email
 
    def clean_password1(self):
        password1 = self.cleaned_data.get('password1')
        if password1 == '':
            raise ValidationError('Password is required')
        return password1

    def clean_password2(self):
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')
        
        if password2 =='':
            raise ValidationError('Confirm Password is required.')

        if password1 and password2 and password1 != password2:
            raise ValidationError('Passwords not matched')
        return password2    
 
    def save(self, commit=True):
        user = User.objects.create_user(
            first_name = self.cleaned_data['first_name'],
            last_name = self.cleaned_data['last_name'],
            username = self.cleaned_data['username'],
            email = self.cleaned_data['email'],
            password = self.cleaned_data['password1'],
            is_active = 'False'
        )
        return user
           

class UserLoginForm(forms.Form):
      username = forms.CharField(label='Username',  required=False, 
                        error_messages={'required': 'Username is required.',
                                        'invalid': 'Enter a valid username'},
                        widget=forms.TextInput(attrs={'placeholder': 'Enter your username','autocomplete': 'off'}))
      password = forms.CharField(label='Password', required=False, 
                        error_messages={'required': 'Password is required.',
                                        'invalid': 'Enter a valid password'},
                        widget=forms.PasswordInput(attrs={'placeholder': 'Enter your password','autocomplete': 'off'}))

     

      def clean_username(self):
        username = self.cleaned_data['username'].lower()
        if username == '':
            raise  ValidationError('Username is required.')
        return username

     
      def clean_password(self):
        password = self.cleaned_data['password']
        if password == '':
            raise  ValidationError('Password is required.')
        return password



class FundManagerLoginForm(forms.Form):
    username = forms.CharField(label='Username',  required=False, 
                error_messages={'required': 'Username is required.',
                                'invalid': 'Enter a valid username'},
                widget=forms.TextInput(attrs={'placeholder': 'Enter your username','autocomplete': 'off'}))
    password = forms.CharField(label='Password', required=False, 
                error_messages={'required': 'Password is required.',
                                'invalid': 'Enter a valid password'},
                widget=forms.PasswordInput(attrs={'placeholder': 'Enter your password','autocomplete': 'off'}))

    def clean_username(self):
        username = self.cleaned_data['username'].lower()
        if username == '':
            raise  ValidationError('Username is required.')
        return username
    def clean_password(self):
        password = self.cleaned_data['password']
        if password == '':
            raise  ValidationError('Password is required.')
        return password




class TradeNotificationForm(forms.Form):
    options = [
          ('BUY', 'BUY'),
          ('SELL', 'SELL'),
    ]
    finalcompanies = SecurityMaster.objects.filter(flag='yes').values_list('fs_name','fs_name').distinct()
    companies = (finalcompanies)
    buy_sell = forms.ChoiceField(choices=options)
    company_name = forms.ChoiceField(choices=companies)
    latest_price = forms.CharField()
    limit_price = forms.CharField()
    # issued_date = forms.DateField(initial=datetime.date.today)

    def __init__(self, *args,**kwargs):
        super(TradeNotificationForm, self).__init__(*args, **kwargs)
    



class CustomerFreeSubscriptionForm(forms.Form):

    first_name = forms.CharField(label='First Name', required=True, max_length=255,
                            error_messages={'required': 'Firstname is required.',
                                            'invalid': 'Enter your firstname'},
                            widget=forms.TextInput(attrs={'placeholder': 'First Name','autocomplete': 'off'}))
    last_name = forms.CharField(label='Last Name', required=False, max_length=255,
                        error_messages={'required': 'Lastname is required.',
                                        'invalid': 'Enter your lastname'},
                        widget=forms.TextInput(attrs={'placeholder': 'Last Name','autocomplete': 'off'}))
    email = forms.EmailField(label='Email ID', required=True,  max_length=255,validators=[emailValidation],
                            widget=forms.TextInput(attrs={'placeholder': 'Email','autocomplete': 'off'}))
    contact_no = forms.CharField(label='Mobile No', required=True,  max_length=10, validators=[contactValidation],
                            widget=forms.TextInput(attrs={'placeholder': 'Mobile Number','autocomplete': 'off'}))
    


class CustomerGoldSubscriptionForm(forms.ModelForm):
    class Meta:
        model = CustomerSubscription
        fields = ['first_name', 'last_name','email','contact_no']

    subscription_choices = [('gold3', 'Gold (Quarterly)- Rs. 999/month + GST 18%'), ('gold12', 'Gold (Annual)- Rs. 999/month + GST 18%')]
    first_name = forms.CharField(label='First Name', required=True, max_length=255,
                            error_messages={'required': 'Firstname is required.',
                                            'invalid': 'Enter your firstname'},
                            widget=forms.TextInput(attrs={'placeholder': 'First Name','autocomplete': 'off'}))
    last_name = forms.CharField(label='Last Name', required=False, max_length=255,
                        error_messages={'required': 'Lastname is required.',
                                        'invalid': 'Enter your lastname'},
                        widget=forms.TextInput(attrs={'placeholder': 'Last Name','autocomplete': 'off'}))
    email = forms.EmailField(label='Email ID', required=True, max_length=255,validators=[emailValidation],
                            widget=forms.TextInput(attrs={'placeholder': 'Email','autocomplete': 'off'}))
    contact_no = forms.CharField(label='Mobile No', required=True, max_length=10,validators=[contactValidation],
                            widget=forms.TextInput(attrs={'placeholder': 'Mobile Number','autocomplete': 'off'}))
    
    subscription_type = forms.CharField(label='Subscription', required=False, 
                            error_messages={'required': 'Choose your plan',
                                            'invalid': 'Choose your plan'},
                            widget=forms.Select(choices=subscription_choices))

class CustomerGoldUpgradeForm(forms.ModelForm):

    class Meta:
        model = CustomerSubscription
        fields = ['first_name', 'last_name','email','contact_no']

    subscription_choices = [('gold3', 'Gold (Quarterly)- Rs. 999/month + GST 18%'), ('gold12', 'Gold (Annual)- Rs. 999/month + GST 18%')]
    first_name = forms.CharField(label='First Name', required=True, max_length=255,
                            error_messages={'required': 'Firstname is required.',
                                            'invalid': 'Enter your firstname'},
                            widget=forms.TextInput(attrs={'placeholder': 'First Name','autocomplete': 'off'}))
    last_name = forms.CharField(label='Last Name', required=False, max_length=255,
                        error_messages={'required': 'Lastname is required.',
                                        'invalid': 'Enter your lastname'},
                        widget=forms.TextInput(attrs={'placeholder': 'Last Name','autocomplete': 'off'}))
    email = forms.EmailField(label='Email ID', required=True, max_length=255,validators=[emailValidation],
                            widget=forms.TextInput(attrs={'placeholder': 'Email','autocomplete': 'off'}))
    contact_no = forms.CharField(label='Mobile No', required=True, max_length=10,validators=[contactValidation],
                            widget=forms.TextInput(attrs={'placeholder': 'Mobile Number','autocomplete': 'off'}))
    
    subscription_type = forms.CharField(label='Subscription', required=False, 
                            error_messages={'required': 'Choose your plan',
                                            'invalid': 'Choose your plan'},
                            widget=forms.Select(choices=subscription_choices))

class CustomerDiamondUpgradeForm(forms.ModelForm):

    class Meta:
        model = CustomerSubscription
        fields = ['first_name', 'last_name','email','contact_no']

    subscription_choices = [('diamond3', 'Diamond (Quarterly)- Rs. 1440/month + GST 18%'),('diamond12', 'Diamond (Annual)- Rs. 1440/month + GST 18%') ]
    first_name = forms.CharField(label='First Name', required=True, max_length=255,
                            error_messages={'required': 'Firstname is required.',
                                            'invalid': 'Enter your firstname'},
                            widget=forms.TextInput(attrs={'placeholder': 'First Name','autocomplete': 'off'}))
    last_name = forms.CharField(label='Last Name', required=False, max_length=255,
                        error_messages={'required': 'Lastname is required.',
                                        'invalid': 'Enter your lastname'},
                        widget=forms.TextInput(attrs={'placeholder': 'Last Name','autocomplete': 'off'}))
    email = forms.EmailField(label='Email ID', required=True, max_length=255,validators=[emailValidation],
                            widget=forms.TextInput(attrs={'placeholder': 'Email','autocomplete': 'off'}))
    contact_no = forms.CharField(label='Mobile No', required=True, max_length=10,validators=[contactValidation],
                            widget=forms.TextInput(attrs={'placeholder': 'Mobile Number','autocomplete': 'off'}))
    
    subscription_type = forms.CharField(label='Subscription', required=True, 
                            error_messages={'required': 'Choose your plan',
                                            'invalid': 'Choose your plan'},
                            widget=forms.Select(choices=subscription_choices))



class CustomerDiamondSubscriptionForm(forms.ModelForm):
    class Meta:
        model = CustomerSubscription
        fields = ['first_name', 'last_name','email','contact_no']

    subscription_choices = [('diamond3', 'Diamond (Quarterly)- Rs. 1440/month + GST 18%'),('diamond12', 'Diamond (Annual)- Rs. 1440/month + GST 18%') ]
    first_name = forms.CharField(label='First Name', required=True, max_length=255,
                            error_messages={'required': 'Firstname is required.',
                                            'invalid': 'Enter your firstname'},
                            widget=forms.TextInput(attrs={'placeholder': 'First Name','autocomplete': 'off'}))
    last_name = forms.CharField(label='Last Name', required=False, max_length=255,
                        error_messages={'required': 'Lastname is required.',
                                        'invalid': 'Enter your lastname'},
                        widget=forms.TextInput(attrs={'placeholder': 'Last Name','autocomplete': 'off'}))
    email = forms.EmailField(label='Email ID', required=True, max_length=255,validators=[emailValidation],
                            widget=forms.TextInput(attrs={'placeholder': 'Email','autocomplete': 'off'}))
    contact_no = forms.CharField(label='Mobile No', required=True, max_length=10,validators=[contactValidation],
                            widget=forms.TextInput(attrs={'placeholder': 'Mobile Number','autocomplete': 'off'}))
    
    subscription_type = forms.CharField(label='Subscription', required=False, 
                            error_messages={'required': 'Choose your plan',
                                            'invalid': 'Choose your plan'},
                            widget=forms.Select(choices=subscription_choices))

    

class CustomerSubscriptionUpgradeForm(forms.ModelForm):
    class Meta:
            model = CustomerSubscription
            fields = ['first_name', 'last_name','email','contact_no']
    subscription_choices = [('diamond3', 'Diamond (Quarterly)- Rs. 1440/month + GST 18%'),('diamond12', 'Diamond (Annual)- Rs. 1440/month + GST 18%') ]
    first_name = forms.CharField(label='First Name', required=True, max_length=255,
                            error_messages={'required': 'Firstname is required.',
                                            'invalid': 'Enter your firstname'},
                            widget=forms.TextInput(attrs={'placeholder': 'First Name','autocomplete': 'off'}))
    last_name = forms.CharField(label='Last Name', required=False, max_length=255,
                        error_messages={'required': 'Lastname is required.',
                                        'invalid': 'Enter your lastname'},
                        widget=forms.TextInput(attrs={'placeholder': 'Last Name','autocomplete': 'off'}))
    email = forms.EmailField(label='Email ID', required=True, max_length=255,
                        error_messages={'required': 'Email is required.',
                                            'invalid': 'Enter your email'},
                            widget=forms.TextInput(attrs={'placeholder': 'Email','autocomplete': 'off'}))
    contact_no = forms.CharField(label='Mobile No', required=True, max_length=10,
                            error_messages={'required': 'mobile number is required.',
                                            'invalid': 'Enter your mobile number'},
                            widget=forms.TextInput(attrs={'placeholder': 'Mobile Number','autocomplete': 'off'}))
    
    subscription_type = forms.CharField(label='Subscription', required=False, 
                            error_messages={'required': 'Choose your plan',
                                            'invalid': 'Choose your plan'},
                            widget=forms.Select(choices=subscription_choices))
    
class CustomerContactForm(forms.Form):
   
    first_name = forms.CharField(label='First Name',max_length=255,
                            widget=forms.TextInput(attrs={'placeholder': 'First Name','autocomplete': 'off'}))
    last_name = forms.CharField(label='Last Name', required=False, max_length=255,
                        widget=forms.TextInput(attrs={'placeholder': 'Last Name','autocomplete': 'off'}))
    email = forms.EmailField(label='Email ID',max_length=255, validators=[emailValidation],
                            widget=forms.TextInput(attrs={'placeholder': 'Email','autocomplete': 'off'}))
    contact_no = forms.CharField(label='Mobile No',max_length=15, validators=[contactValidation],
                            widget=forms.TextInput(attrs={'placeholder': 'Mobile Number','autocomplete': 'off'}))
    subject = forms.CharField(label='Subject',max_length=255,
                            widget=forms.TextInput(attrs={'placeholder': 'Subject','autocomplete': 'off'}))
    message = forms.CharField(label='Message',max_length=255,
                            widget=forms.Textarea(attrs={'placeholder': 'Message','autocomplete': 'off'}))

   
class CustomerBugReportForm(forms.Form):
    bug_categories = (('A','A'),('B','B'),('C','C'))
   
    bug_type = forms.CharField(label='Category', required=False, 
                            error_messages={'required': 'Select issue category',
                                            'invalid': 'Select issue category'},
                            widget=forms.Select(choices=bug_categories))
    bug_title = forms.CharField(label='Title',max_length=255,
                            widget=forms.TextInput(attrs={'placeholder': 'Title of the issue','autocomplete': 'off'}))
    
    bug_description = forms.CharField(label='Description',max_length=255,
                            widget=forms.Textarea(attrs={'placeholder': 'Issue Description','autocomplete': 'off'}))

    bug_image = forms.ImageField(label='Screenshot')
    

class InvestorsRiskProfileForm(forms.ModelForm):
    class Meta:
            model = InvestorRiskProfile
            fields = ['name','age','risk_type','investor_approch','current_stage','income_source','portfolio_investment','financial_status','investment_experience',
            'asset_classes','risk_understanding','risk_awareness','portfolio_corrects','trade_objective','liquidate_period','annual_return_investment']

    risk_choices = [('Risk Averse','Risk Averse'),('Conservative','Conservative'),('Aggressive','Aggressive'),('Very Aggressive', 'Very Aggressive')]
    approch_choices = [('Active Investor (Commentary/News for ideas','Active Investor (Commentary/News for ideas)'),
    ('Systematic Investor (Data/Quantitative metrics for trading ideas)','Systematic Investor (Data/Quantitative metrics for trading ideas)'),
    ('Quality & Income Investor (Buy for long term holding)','Quality & Income Investor (Buy for long term holding)'),
    ('Trader (Price/Technical triggers to trade regularly)','Trader (Price/Technical triggers to trade regularly)'),
    ('New Investor (Yet to decide on strategy)','New Investor (Yet to decide on strategy)')]
    current_stage_choices =[('retired','a) Retired'),('preparing_for_retirement','b) Preparing for retirement'),('mature_family','c) Mature family'),
    ('young_couple','d) A young couple'),('single','e) Single')]
    income_source_choices=[('very_unstable','a) Very unstable'),('unstable','b) Unstable'),('somewhat_stable','c) Somewhat Stable'),
    ('stable','d) Stable'),('very_stable','e) Very Stable')]
    portfolio_investment_choices = [('less_than_10_lakhs','a) Less than Rs. 10 Laks'),('10lakhs_to_1crore','b) Rs 10 Laks  – Rs. 1 Crore'),
    ('1crore_to_5crores','c) Rs. 1 Crore – Rs. 5 Crores'),('more_than_5crores','d) More than Rs. 5 Crores')]
    financial_status_choices = [('you_just_manage_to_make_ends_meet_with_negligible_savings','a) You just manage to make ends meet with negligible savings'),
    ('you_save_less_than_10%_of_your_income','b) You save less than 10% of your income'),
    ('you_save_around_10%_to_20%_of_your_income','c) You save around 10% to 20% of your income'),
    ('you_save_around_20%_to_30%_of_your_income','d) You save around 20% to 30% of your income'),
    ('you_save_more_than_30%_of_your_income','e) You save more than 30% of your income')]
    investment_experience_choices = [('no_prior_experience', 'a) No Prior Experience'), ('0_3_years','b) 0 to 3 years'),('3_5_years','c) 3 to 5 years'), ('more_than_5_years','d) More than 5 years')]
    asset_classes_choices = [('fixed_deposits_bonds_and_money_market_funds','a) Fixed deposits, bonds and money market funds'),
    ('debt_hybrid_mutual_funds','b)	Debt/Hybrid Mutual Funds'), ('equity_mutual_funds','c)	Equity mutual funds'),
    ('direct_equities','d)	Direct equities'),('private_equity_venture_capital_funds','e) Private equity/venture capital funds'),
    ('derivatives','f) Derivatives')]
    risk_understanding_choices = [('none_at_all','a) None at all'), ('minimal','b) Minimal'), ('fair','c) Fair'),('good','d) Good'),
    ('very_good','e) Very good')]
    risk_awareness_choices = [('yes', 'a) Yes'), ('no','b) No')]
    portfolio_corrects_choices = [('sell_all_of_the_investments','a) Sell all of the investments'),
    ('sell_a_position_of_your_portfolio_to_cut_your_losses_and_reinvest_into_more_secure_investment_assets','b)	Sell a portion of your portfolio to cut your losses and reinvest into more secure investment assets'),
    ('hold_the_investment_and_sell_nothing_expecting_performance_to_improve','c) Hold the investment and sell nothing, expecting performance to improve'),
    ('invest_more_funds_to_lower_your_average_investment_price','d) Invest more funds to lower your average investment price')]
    investment_objective_choices = [('capital_appreciation','a)	Capital appreciation'),
    ('capital_appreciation_and_regular_income','b) Capital appreciation and regular income'),
    ('regular_income','c) Regular income')]
    liquidate_period_choices = [('up_to_two_years','a) Up to two years'),('two_and_three_years','b) Two and three years'),
    ('three_and_five_years','c) Three and five years'),('five_and_ten_years','d) Five and Ten years'),('ten_and_more','e) Ten years and more')]
    annual_return_investment_choices = [('low_between_5-8%','a)	Low, between 5-8% (“I focus on protecting capital and steady income though it may be limited. I am not looking for significant capital appreciation”)'),
    ('medium_between_9_15%','b)	Medium, between 9-15% (“I am interested in moderate capital growth or moderate income. I am willing to tolerate infrequent and moderate negative return”)'),
    ('high_more_than_15%','c)	High, more than 15% (“I am interested in generating high returns. To do so, I understand that I will have to take high risk and acceptable probable high negative returns”)')]
    name = forms.CharField(label='Investor Name :', required=True, max_length=25,
                            widget=forms.TextInput(attrs={'placeholder': 'Investor Name','autocomplete': 'off'}))
    age = forms.IntegerField(label='Age :', required=True,
                            widget=forms.TextInput(attrs={'placeholder': 'Investor Age','autocomplete': 'off'}))
    risk_type = forms.ChoiceField(choices=risk_choices,widget=forms.RadioSelect)
    investor_approch = forms.ChoiceField(choices=approch_choices, widget=forms.RadioSelect)
    current_stage = forms.ChoiceField(label='1. Which of the following best describes your current stage of life?', choices=current_stage_choices, widget=forms.RadioSelect)
    income_source = forms.ChoiceField(label='2.	Are your current and future income sources (example: salary, business income, etc.)',choices=income_source_choices,widget=forms.RadioSelect)
    portfolio_investment = forms.ChoiceField(label='3. What is the value at cost of your total investment portfolio (excluding real estate and bank deposits)?', choices=portfolio_investment_choices, widget=forms.RadioSelect)
    financial_status = forms.ChoiceField(label='4. Describe your financial status in terms of savings.', choices=financial_status_choices, widget=forms.RadioSelect)
    investment_experience = forms.ChoiceField(label='5.	What is your total investment experience (excluding real estate and bank deposits)?', choices=investment_experience_choices, widget=forms.RadioSelect)
    asset_classes = forms.MultipleChoiceField(label='6.	Which of the following asset classes do you have an investment experience in (tick all)?', choices=asset_classes_choices, widget=forms.CheckboxSelectMultiple)
    risk_understanding = forms.ChoiceField(label='7. What is the level of your understanding of the risks associated with investing in equity markets?', choices=risk_understanding_choices, widget=forms.RadioSelect)
    risk_awareness = forms.ChoiceField(label='8. Are you aware that investments in equity markets are subject to risk, and that the return of capital cannot be guaranteed?', choices=risk_awareness_choices, widget=forms.RadioSelect)
    portfolio_corrects = forms.ChoiceField(label='9. Assume your portfolio corrects by 25% in a month, would you:', choices=portfolio_corrects_choices, widget=forms.RadioSelect)
    trade_objective = forms.ChoiceField(label='10.	What is your investment objective?', choices=investment_objective_choices, widget=forms.RadioSelect)
    liquidate_period = forms.ChoiceField(label='11.	How long before you would need to liquidate, partially or fully, the money you are planning on investing needing access to it?', choices=liquidate_period_choices, widget=forms.RadioSelect)
    annual_return_investment = forms.ChoiceField(label='12.	What is your expectation of annual return on investments?', choices=annual_return_investment_choices, widget=forms.RadioSelect)


class EmailNotificationsForm(forms.Form):
    choices = (('all','All'),('free','Free'),('gold','Gold'),('diamond','Diamond'))
    customer_type = forms.CharField(label='Select Customers',widget=forms.Select(choices=choices))
    subject = forms.CharField(label='Subject',max_length=255, 
                            widget=forms.TextInput(attrs={'placeholder': 'Type subject here','autocomplete': 'off'}))
    message= forms.CharField(widget=forms.Textarea(attrs={'placeholder': 'Type message here','autocomplete': 'off',"rows":10, "cols":60}))

    def __init__(self, *args, **kwargs):
        super(EmailNotificationsForm, self).__init__(*args, **kwargs)
        self.fields['message'].strip = False



class TradeSiganlsIdeasForm(forms.Form):
    choices = (('all','All'),('free','Free'),('gold','Gold'),('diamond','Diamond'))
    customer_type = forms.CharField(label='Select Customers',widget=forms.Select(choices=choices))
    companies = (SecurityMaster.objects.filter(flag='yes').values_list('fs_name','fs_name').distinct())
    company_name = forms.CharField(label='Company Name',widget=forms.Select(choices=companies))
    fundamental = forms.CharField(label='Fundamental',widget=forms.Textarea(attrs={'placeholder': 'Fundamentals','autocomplete': 'off',"rows":5, "cols":100}))
    fundamental_rating = forms.IntegerField(label='Fundamental Rating', min_value=1, max_value=5, widget=forms.NumberInput(attrs={'placeholder': 'Fundamental rating '}))
    quantitative = forms.CharField(label='Quantitative',widget=forms.Textarea(attrs={'placeholder': 'Quantitative','autocomplete': 'off',"rows":5, "cols":100}))
    quantitative_rating = forms.IntegerField(label='Quantitative Rating', min_value=1, max_value=5, widget=forms.NumberInput(attrs={'placeholder': 'Quantitative rating '}))
    technical = forms.CharField(label='Technical',widget=forms.Textarea(attrs={'placeholder': 'Technical','autocomplete': 'off',"rows":5, "cols":100}))
    technical_rating = forms.IntegerField(label='Technical Rating', min_value=1, max_value=5, widget=forms.NumberInput(attrs={'placeholder': 'Technical rating '}))
    situation = forms.CharField(label='Situations',widget=forms.Textarea(attrs={'placeholder': 'Situation','autocomplete': 'off',"rows":5, "cols":100}))
    situation_rating = forms.IntegerField(label='Situation Rating', min_value=1, max_value=5, widget=forms.NumberInput(attrs={'placeholder': 'Situation rating '}))
    purchase_price = forms.CharField(label='Purchase Price',max_length=255, 
                            widget=forms.TextInput(attrs={'placeholder': 'Enter purchase price','autocomplete': 'off'}))
    target_price = forms.CharField(label='Target Price',max_length=255, 
                            widget=forms.TextInput(attrs={'placeholder': 'Enter target price','autocomplete': 'off'}))
    time_period = forms.CharField(label='Time Period',max_length=255, 
                            widget=forms.TextInput(attrs={'placeholder': 'Enter time period','autocomplete': 'off'}))
    comment = forms.CharField(label='Comment',max_length=255, 
                            widget=forms.TextInput(attrs={'placeholder': 'Enter comment','autocomplete': 'off'}))
    



class CustomerINRSubscriptionForm(forms.ModelForm):
    class Meta:
        model = CustomerSubscription
        fields = ['first_name', 'last_name','email','contact_no']

    subscription_choices = [('premiuminr3', 'Premium (Quarterly) - Rs. 1440 /month + GST 18%'), ('premiuminr12', 'Premium (Annual) - Rs. 1440 /month + GST 18%')]
    first_name = forms.CharField(label='First Name', required=True, max_length=255,
                            error_messages={'required': 'Firstname is required.',
                                            'invalid': 'Enter your firstname'},
                            widget=forms.TextInput(attrs={'placeholder': 'First Name','autocomplete': 'off'}))
    last_name = forms.CharField(label='Last Name', required=False, max_length=255,
                        error_messages={'required': 'Lastname is required.',
                                        'invalid': 'Enter your lastname'},
                        widget=forms.TextInput(attrs={'placeholder': 'Last Name','autocomplete': 'off'}))
    email = forms.EmailField(label='Email ID', required=True, max_length=255,validators=[emailValidation],
                            widget=forms.TextInput(attrs={'placeholder': 'Email','autocomplete': 'off'}))
    contact_no = forms.CharField(label='Mobile No', required=True, max_length=10,validators=[contactValidation],
                            widget=forms.TextInput(attrs={'placeholder': 'Mobile Number','autocomplete': 'off'}))
    
    subscription_type = forms.CharField(label='Subscription', required=False, 
                            error_messages={'required': 'Choose your plan',
                                            'invalid': 'Choose your plan'},
                            widget=forms.Select(choices=subscription_choices))


class CustomerUSDSubscriptionForm(forms.ModelForm):
    class Meta:
        model = CustomerSubscription
        fields = ['first_name', 'last_name','email','contact_no']

    subscription_choices = [('premiumusd3', 'Premium (Quarterly) - 20$ /month'), ('premiumusd12', 'Premium (Annual) - 20$ /month')]
    first_name = forms.CharField(label='First Name', required=True, max_length=255,
                            error_messages={'required': 'Firstname is required.',
                                            'invalid': 'Enter your firstname'},
                            widget=forms.TextInput(attrs={'placeholder': 'First Name','autocomplete': 'off'}))
    last_name = forms.CharField(label='Last Name', required=False, max_length=255,
                        error_messages={'required': 'Lastname is required.',
                                        'invalid': 'Enter your lastname'},
                        widget=forms.TextInput(attrs={'placeholder': 'Last Name','autocomplete': 'off'}))
    email = forms.EmailField(label='Email ID', required=True, max_length=255,validators=[emailValidation],
                            widget=forms.TextInput(attrs={'placeholder': 'Email','autocomplete': 'off'}))
    contact_no = forms.CharField(label='Mobile No', required=True, max_length=10,validators=[contactValidation],
                            widget=forms.TextInput(attrs={'placeholder': 'Mobile Number','autocomplete': 'off'}))
    
    subscription_type = forms.CharField(label='Subscription', required=False, 
                            error_messages={'required': 'Choose your plan',
                                            'invalid': 'Choose your plan'},
                            widget=forms.Select(choices=subscription_choices))