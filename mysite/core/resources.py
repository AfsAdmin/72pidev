from import_export import resources
from mysite.core.models import EmailCampaignDetails
 
class EmailCampaignResource(resources.ModelResource):
    class Meta:
        model = EmailCampaignDetails