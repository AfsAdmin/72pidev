#optimisation
import pandas as pd
import datetime
from scipy import stats
from datetime import timedelta
from statistics import mean
import statsmodels.api as sm
import math
from .models import *
from .Fama_French import *
from .InvestmentAmountFactor import *

def newSummary( current_date, user,current_country,Stock_Data,Daily_Returns,factors_data,Daily_Returns_values):
  currency_symbols = {'India':'Rs.','US':'$'}
  currency = currency_symbols[current_country]
  try:
    pnl_start_date = Daily_Returns[Daily_Returns['Date']< current_date+timedelta(-364) ]
    pnl_start_date.sort_values(by='Date',ascending=False,inplace=True)
    pnl_start_date.reset_index(drop=True,inplace=True)
    pnl_start_date=pnl_start_date.loc[0,'Date']
    start_date=current_date+timedelta(-364)
    Daily_Returns1 = Daily_Returns[(Daily_Returns['Date']>= pnl_start_date) & (Daily_Returns['Date']<=current_date)]
    Daily_Returns1.reset_index(drop=True,inplace=True)
    Daily_Returns1 = Daily_Returns1[['Company', 'Factset_Ticker', 'Date', 'Price', 'Return']]

    factors_list = []
    Fama_Current= FamaMain(Daily_Returns_values[['Date','Portfolio Return']])
    del Fama_Current['Date']
    Fama_Current=Fama_Current.stack().reset_index().rename(columns={'level_1':'name',0:'value'})
    del Fama_Current['level_0']
    positive = Fama_Current[Fama_Current['value'] >=0]
    positive = positive['name'].values.tolist()
    negative = Fama_Current[Fama_Current['value'] <0]
    negative = negative['name'].values.tolist()
    factors_list.append([','.join(positive),','.join(negative)])

    bucket_output = factorsDashboard(Daily_Returns1,factors_data,pnl_start_date,current_date,Stock_Data,'analyser')
    beta_res = bucket_output[bucket_output['Factor']=='Beta_1Y_Bucket']
    beta_res.sort_values(['Return'],ascending=False,inplace=True)
    beta_res.reset_index(drop=True,inplace=True)
    beta_res = beta_res[0:1]
    factors_list.append([str(beta_res['Bucket'][0])+" BETA",str(round(beta_res['Return'][0],2))])

    pe_res = bucket_output[bucket_output['Factor']=='PE_Bucket']
    pe_res.sort_values(['Return'],ascending=False,inplace=True)
    pe_res.reset_index(drop=True,inplace=True)
    pe_res = pe_res[0:1]
    factors_list.append([str(pe_res['Bucket'][0])+" PE",str(round(pe_res['Return'][0],2))])

    eps_res = bucket_output[bucket_output['Factor']=='EPS_Growth_Bucket']
    eps_res.sort_values(['Return'],ascending=False,inplace=True)
    eps_res.reset_index(drop=True,inplace=True)
    eps_res = eps_res[0:1]  
    factors_list.append([str(eps_res['Bucket'][0])+" EPS",str(round(eps_res['Return'][0],2))])

    return factors_list
  except Exception as e:
    emptyList=[]
    return emptyList 

