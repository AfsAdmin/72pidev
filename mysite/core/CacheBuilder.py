# -*- coding: utf-8 -*-
"""
Created on Wed Dec  9 17:23:20 2020

@author: jitendra.v
"""

import pandas as pd
import pickle
from django.core.cache import cache
import pyodbc
from mysite.core.Appconfigproperties import *
from mysite.core.models import CustomerPortfolioDetails
from datetime import datetime
from django.shortcuts import render


def getWizardTableDataFromCacheBuilder(country, selectedIndex,stocks=None):
    wizardDataKey = country+"_wizard"
    tableObjects={'India':['wizardTable'],'US':['US_wizardTable']}
    conn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server}; SERVER='+SERVER+'; PORT='+PORT+'; DATABASE='+DATABASE+';UID='+UID+';PWD='+PWD+';')
    wizardTable=tableObjects[country][0]   
    if cache.get(wizardDataKey):
        wizardData = pickle.loads(cache.get(wizardDataKey))
    else:
        try:
            wizardData  = pd.read_sql(""" select * from """ + wizardTable ,conn)
            wizardDataDump = pickle.dumps(wizardData)
            cache.set(wizardDataKey, wizardDataDump, 1*60*60)
        except Exception as e:
            print(e)
    conn.close()
    if stocks is not None:
        wizardData=wizardData[(wizardData['index_name']==selectedIndex) & (wizardData['Factset_Ticker'].isin(stocks))].reset_index()
    else:
        wizardData=wizardData[wizardData['index_name']==selectedIndex]
    if country=='US':
        wizardData.rename(columns={'GICS':'gics'},inplace=True)
    return wizardData

def getWizardIndicesDataFromCacheBuilder(country):
    indicesKey=country+"_indices"
    if cache.get(indicesKey):
        indices=pickle.loads(cache.get(indicesKey))
    else:
        conn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server}; SERVER='+SERVER+'; PORT='+PORT+'; DATABASE='+DATABASE+';UID='+UID+';PWD='+PWD+';')

        if country=='India':
            indices=list(pd.read_sql('''select distinct index_name from Indexconstituents where flag='yes' union 
                                    select distinct index_name from IndexConstituents_bse where flag='yes' ''',conn)['index_name'])
        else:
            indices=list(pd.read_sql('''select distinct index_name from Us_Indexconstituents where flag='yes' ''',conn)['index_name'])
        conn.close()
        cache.set(indicesKey,pickle.dumps(indices),1*60*60)
    return indices


def getPortfolioStocksDataFromCacheBuilder(country):
    portfolioStockKey = country+'_portfolio_stocks_data'
    if cache.get(portfolioStockKey):
        stockData=pickle.loads(cache.get(portfolioStockKey))
    else:
        requiredTableObjects={'India':['Universe_Prices_Main','Security_Master'," where flag='yes'"],'US':['Us_Universe_Prices_Main','Us_Security_Master',""]}
        conn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server}; SERVER='+SERVER+'; PORT='+PORT+'; DATABASE='+DATABASE+';UID='+UID+';PWD='+PWD+';')
        stockData=pd.read_sql('''select [FS Ticker],[FS Name],[Market Cap],Market_Cap_Category,GICS,a.Price,[Security Code] from ''' + requiredTableObjects[country][1]+ ''' s join 
                            (select Company,date,Factset_Ticker,Price from ''' +  requiredTableObjects[country][0] +''') a join
                            (select factset_ticker,max(date) as mdate from ''' +  requiredTableObjects[country][0] + ''' group by factset_ticker) u
                            on a.Factset_Ticker=u.Factset_Ticker and a.Date=u.mdate
                            on s.[FS Ticker] = u.Factset_Ticker ''' +  requiredTableObjects[country][2] ,conn)
        conn.close()
        cache.set(portfolioStockKey,pickle.dumps(stockData),1*60*60)
    return stockData


def getDataForCreatingStocksFromCacheBuilder(country):
    creatingStocksKey = country+'_portfolio_creating_by_stocks'
    if cache.get(creatingStocksKey):
        stockData=pickle.loads(cache.get(creatingStocksKey))
    else:
        requiredTableObjects={'India':['Universe_Prices_Main','Security_Master'," where flag='yes'"],'US':['Us_Universe_Prices_Main','Us_Security_Master',""]}
        conn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server}; SERVER='+SERVER+'; PORT='+PORT+'; DATABASE='+DATABASE+';UID='+UID+';PWD='+PWD+';')
        sec_master_data=pd.read_sql("""select [FS Ticker] as factset_ticker,[FS Name] as company,[Market Cap] as market_cap,Market_Cap_Category
                            as market_cap_category,GICS as gics, [Security Code] as security_code from """ + requiredTableObjects[country][1] + " " + requiredTableObjects[country][2], conn)
        min_max_prices=pd.read_sql("""select date,company,a.factset_ticker,price from """ + requiredTableObjects[country][0] + """ a join 
        (select Factset_Ticker,min(date) as mindate,max(date) as maxdate from """ + requiredTableObjects[country][0] + """ where Date>='2019-01-01' group by Factset_Ticker) b on
        (a.date=b.mindate or a.date=b.maxdate) and a.factset_ticker=b.factset_ticker order by Company,date desc
        """, conn)
        min_prices=min_max_prices.groupby(['company','factset_ticker'])['company','factset_ticker','price'].tail(1).reset_index(drop=True)
        min_prices.columns=['company','factset_ticker','min_price']
        max_prices=min_max_prices.groupby(['company','factset_ticker'])['company','factset_ticker','price'].head(1).reset_index(drop=True)
        min_max_prices=pd.merge(min_prices,max_prices,on=['company','factset_ticker'])
        stockData= pd.merge(sec_master_data,min_max_prices,on=['company','factset_ticker'],how='inner')
        conn.close()
        cache.set(creatingStocksKey,pickle.dumps(stockData),1*60*60)
    return stockData

def getEODMarketDataFromCacheBuilder():
    eodMarketKey = "eod_market_key"
    if cache.get(eodMarketKey):
        eodMarketData = pickle.loads(cache.get(eodMarketKey))
    else:
        conn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server}; SERVER='+SERVER+'; PORT='+PORT+'; DATABASE='+DATABASE+';UID='+UID+';PWD='+PWD+';')
        eodMarketData = pd.read_sql("select ticker,date,[adjusted_close] from eod_market where date>='2017-01-01' order by date",conn)
        conn.close()
        cache.set(eodMarketKey,pickle.dumps(eodMarketData),1*60*60)
    return eodMarketData

def getMarketDataFromCacheBuilder():
    conn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server}; SERVER='+SERVER+'; PORT='+PORT+'; DATABASE='+DATABASE+';UID='+UID+';PWD='+PWD+';') #Establishing the DB connection    
    marketTables = {'India':'Market_Index','US':'Us_Market_Index'}
    countries = ['India','US']
    for country in countries:
        marketKey = country+"_market"
        if cache.get(marketKey):
            pass
        else:
            try:
                marketTable = marketTables[country]
                index_returns  = pd.read_sql(""" select * from """ + marketTable + ' order by date',conn)
                index_prices_dump = pickle.dumps(index_returns)
                cache.set(marketKey, index_prices_dump, 1*60*60)
            except Exception as e:
                print(e)
    conn.close()
def getPricesDataForCacheBuilder(username):    
    india_prices_key = username+"_India_universe"
    us_prices_key = username+"_US_universe"
    conn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server}; SERVER='+SERVER+'; PORT='+PORT+'; DATABASE='+DATABASE+';UID='+UID+';PWD='+PWD+';') #Establishing the DB connection
    india_daily_returns = pd.read_sql(""" select * from Universe_Prices_Main where factset_ticker in ( select distinct fs_ticker from Customer_Portfolio_Details 
                            where [Customer Name]='"""+username+"""' and Country='India') and date >= '2019-01-01' ORDER by company,date""",conn)
    cache.set(india_prices_key,pickle.dumps(india_daily_returns),1*60*60)
    us_daily_returns = pd.read_sql(""" select * from Us_Universe_Prices_Main where factset_ticker in ( select distinct fs_ticker from Customer_Portfolio_Details 
                            where [Customer Name]='"""+username+"""' and Country='US') and date >= '2019-01-01' ORDER by company,date""",conn)
    cache.set(us_prices_key,pickle.dumps(us_daily_returns),1*60*60)
    conn.close()
    return india_daily_returns,us_daily_returns

def getPortfolioWiseDataFromCacheBuilder(*args):
    username = args[0]
    tableObjects={'India':'Universe_Prices_Main','US':'US_Universe_Prices_Main'}  # Definig the DB Tables based on country
    conn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server}; SERVER='+SERVER+'; PORT='+PORT+'; DATABASE='+DATABASE+';UID='+UID+';PWD='+PWD+';') #Establishing the DB connection
    portfolio_stocks = pd.read_sql("select [company name] as company_name,fs_ticker as factset_ticker,[portfolio name] as portfolio_name,country from customer_portfolio_details where [customer name]='" + username + "'",conn)    
    portfolios=portfolio_stocks[['country','portfolio_name']].drop_duplicates()
    portfolios=portfolios.to_dict(orient='records')
    if len(portfolios):
        india_key = username + "_India_universe"
        us_key = username + "_US_universe"
        if cache.get(india_key):
            india_daily_returns = pickle.loads(cache.get(india_key))
        if cache.get(us_key):
            us_daily_returns = pickle.loads(cache.get(us_key))
        else:
            india_daily_returns,us_daily_returns = getPricesDataForCacheBuilder(username)
        universeDF = {'India':india_daily_returns, 'US':us_daily_returns}        
        for portfolio in portfolios:
            universeKey = username+"_"+portfolio['portfolio_name']+"_"+portfolio['country']+"_universe"            
            if cache.get(universeKey):
                pass
            else:
                pricesData = universeDF[portfolio['country']]
                portfolio_wise_stocks=portfolio_stocks[portfolio_stocks['portfolio_name']==portfolio['portfolio_name']]
                portfolio_wise_stocks = list(portfolio_wise_stocks['factset_ticker'].unique())
                daily_returns=pricesData[pricesData['Factset_Ticker'].isin(portfolio_wise_stocks)]
                stock_prices_dump = pickle.dumps(daily_returns)
                cache.set(universeKey, stock_prices_dump, 1*60*60)
        portfolios=[{'portfolio_name':'72PI India Portfolio','country':'India'},{'portfolio_name':'72PI US Portfolio','country':'US'}]
        for portfolio in portfolios:
            universeKey = username+"_"+portfolio['portfolio_name']+"_"+portfolio['country']+"_universe"
            if cache.get(universeKey):
                pass
            else:
                pricesTable=tableObjects[portfolio['country']]
                daily_returns = pd.read_sql(""" select * from """ + pricesTable +""" where factset_ticker in ( select distinct [fs_ticker] from Model_Portfolio_Details 
                where [Model Portfolio Name]='"""+portfolio['portfolio_name'] +"""' and Country='"""+portfolio['country']+"""') ORDER by company,date""",conn)
                stock_prices_dump = pickle.dumps(daily_returns)
                cache.set(universeKey, stock_prices_dump, 1*60*60)
    else:
        portfolios=[{'portfolio_name':'72PI India Portfolio','country':'India'},{'portfolio_name':'72PI US Portfolio','country':'US'}]
        for portfolio in portfolios:
            universeKey = username+"_"+portfolio['portfolio_name']+"_"+portfolio['country']+"_universe"
            if cache.get(universeKey):
                pass
            else:
                pricesTable=tableObjects[portfolio['country']]
                daily_returns = pd.read_sql(""" select * from """ + pricesTable +""" where factset_ticker in ( select distinct [fs_ticker] from Model_Portfolio_Details 
                where [Model Portfolio Name]='"""+portfolio['portfolio_name'] +"""' and Country='"""+portfolio['country']+"""') ORDER by company,date""",conn)
                stock_prices_dump = pickle.dumps(daily_returns)
                cache.set(universeKey, stock_prices_dump, 1*60*60)
    conn.close()  #Closing the DB Connection

def createCacheForNewPortfolio(username, country, portfolio_name):
    pricesTables={'India':'Universe_Prices_Main','US':'Us_Universe_Prices_Main'}
    pricesTable = pricesTables[country]
    universeKey = username+"_"+portfolio_name+"_"+country+"_universe"
    cache.delete(universeKey)  #delete cache key if exists
    conn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server}; SERVER='+SERVER+'; PORT='+PORT+'; DATABASE='+DATABASE+';UID='+UID+';PWD='+PWD+';') #Establishing the DB connection
    daily_returns = pd.read_sql(""" select * from """ + pricesTable +""" where factset_ticker in ( select distinct [fs_ticker] from Customer_Portfolio_Details 
    where [Customer Name]='""" + username +"""' and [Portfolio Name]='"""+ portfolio_name +"""' and Country='"""+ country +"""') ORDER by company,date""",conn)
    stock_prices_dump = pickle.dumps(daily_returns)
    cache.set(universeKey, stock_prices_dump, 1*60*60)
    conn.close()  #Closing the DB Connection


def updatePortfolioDataInCacheBuilder(username, country, portfolio_name, newStocks):
    pricesTables={'India':'Universe_Prices_Main','US':'Us_Universe_Prices_Main'}
    pricesTable = pricesTables[country]
    universeKey = username+"_"+portfolio_name+"_"+country+"_universe"
    if cache.get(universeKey):
        existingStockReturns = pickle.loads(cache.get(universeKey))
        filterstocks =', '.join('"{0}"'.format(w) for w in [newStocks[i].get('stockname').replace("'","''") for i in range (0,len(newStocks))] ).replace('"',"'")
        conn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server}; SERVER='+SERVER+'; PORT='+PORT+'; DATABASE='+DATABASE+';UID='+UID+';PWD='+PWD+';') #Establishing the DB connection
        newStockReturns = pd.read_sql(""" select * from """ + pricesTable +""" where factset_ticker in ( """+filterstocks+""") ORDER by company,date""",conn)
        newDailyReturns = pd.concat([existingStockReturns,newStockReturns])
        stock_prices_dump = pickle.dumps(newDailyReturns)
        cache.set(universeKey, stock_prices_dump, 1*60*60)
    else:
        createCacheForNewPortfolio(username, country, portfolio_name) #Create data into redis cache
