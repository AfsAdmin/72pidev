# -*- coding: utf-8 -*-
"""
Created on Fri Feb 21 11:56:31 2020

@author: Harshitha.b
"""
import pandas as pd
import statistics
import pyodbc
from datetime import date,timedelta
import datetime
from scipy import stats
import math
from .models import UniversePricesMain, CustomerPortfolioDetails, MarketIndex, UsUniversePricesMain
from django.db.models import Avg, Max, Min, Sum
import numpy as np
# StartDate, pnl_start_date,EndDate, user ,Stock_Data,Daily_Returns,index_cumulative_data,Daily_Returns1
def portfolio_CalcsMain(StartDate, pnl_start_date,EndDate, user ,portfolio_name,Stock_Data,stock_daily_returns,index_Returns,country,period):
    # try:
    Stocks_Data_with_Exposure = ExposureCalculation(Stock_Data,StartDate,EndDate,stock_daily_returns)
    benchmark_indices = {'India':'Nifty 50 Index','US':'SPXT Index'}
    Index_Ticker=benchmark_indices[country]  
    Stock_CumulativeReturns,DailyReturns =StockCumulativeReturns(Stocks_Data_with_Exposure,pnl_start_date,EndDate,stock_daily_returns)       
    Index_CumulativeReturns,Index_Returns = IndexCumulativeReturns (Index_Ticker,StartDate,EndDate,index_Returns)
    DailyReturns.columns=["Date","PNL"]
    Index_Returns.columns=["Date","PNL"]
    Index_Returns.reset_index(drop=True,inplace=True)
    DailyReturns.reset_index(drop=True,inplace=True)

    dailyReturns = DailyReturns.copy()
    dailyReturns.columns=['Date','portfolio_pnl']
    indexReturns =Index_Returns.copy()
    indexReturns.columns=['Date','index_pnl']
    returns = pd.merge(dailyReturns,indexReturns,on=['Date'],how='inner')
    beta = 1
    if abs(len(dailyReturns)-len(indexReturns))<=5:
        beta = stats.linregress(returns["index_pnl"].values,returns["portfolio_pnl"].values)[0:1][0]


    #Annualize_Returns
    Fund_Annualize_Return = AnnualizeReturn (Stock_CumulativeReturns, StartDate,period)
    Annualize_Fund_Volatility = AnnualizeVolatility(DailyReturns)
    Results=pd.DataFrame(columns=["C","Annualized Return","Annualized Volatility"])
    Results['C']=['Portfolio']
    Results["Annualized Return"] = [Fund_Annualize_Return]
    Results["Annualized Volatility"] = [Annualize_Fund_Volatility]
    Results.columns=["","Annualized Return","Annualized Volatility"]
    Portfolio_VaR_Component,Individual_VaR=portfolioVaR(EndDate,user,portfolio_name,Stock_Data,stock_daily_returns)
    return Results,beta,Portfolio_VaR_Component,Individual_VaR
    # except Exception as e:
    #     print(e)
    #     return pd.DataFrame(),pd.DataFrame(),pd.DataFrame(),pd.DataFrame()


def ExposureCalculation(Stocks_Data,StartDate,EndDate,Daily_Returns):     #function for exposure calc
    Exposure_Data=Stocks_Data.copy()
    Exposure_Data=Exposure_Data[['Company','Factset_Ticker','Quantity']]
    Daily_Returns = pd.merge(Daily_Returns,Exposure_Data,on=['Company','Factset_Ticker'],how='inner')    #merging returns data with exposure data on company
    Daily_Returns["Net"]=Daily_Returns["Price"] * Daily_Returns["Quantity"]    #multiplying price and quantity to get the net value
    Daily_Returns=Daily_Returns[['Company','Factset_Ticker','Date','Net']]
    return Daily_Returns

def StockCumulativeReturns(Stocks_Data_with_Exposure,StartDate,EndDate,Daily_Returns):    #function for calculating cumulative returns
    Exposure_Data = Stocks_Data_with_Exposure[["Company","Factset_Ticker","Date",'Net']]
    Exposure_Data=pd.merge(Exposure_Data,Daily_Returns,on=['Company','Factset_Ticker','Date'])                             
    Exposure_Data.fillna(0,inplace=True)
    Net_Data=Exposure_Data.groupby(['Date'])["Net"].agg('sum').reset_index()     #calculating day wise net
    Net_Data.columns=['Date','Denom']
    Exposure_Data=pd.merge(Exposure_Data,Net_Data,on='Date',how='inner')
    Exposure_Data["Net%"] = Exposure_Data["Net"]/Exposure_Data['Denom']         #calculating Net% by dividing day wise stock net with day wise portfolio net
    Exposure_Data['PNL%']=Exposure_Data['Return'] *  Exposure_Data.groupby(['Company','Factset_Ticker'])['Net%'].shift(1)     #calculating PNL% by multiplying todays return and yesterday net%
    PNL_data=Exposure_Data.groupby(['Date'])['PNL%'].agg('sum').reset_index()       #calculating day wise PNL%                  
    PNL_data.columns=["Date","Daily Ret"]
    PNL_data["PNL"] = (PNL_data['Daily Ret']+1).cumprod()-1       #calculating cumulative product
    Cumulative_Returns=PNL_data[["Date","PNL"]]                         
    PNL_data=PNL_data[["Date","Daily Ret"]]
    PNL_data.columns=["Date","Return"]
    return Cumulative_Returns,PNL_data 

def IndexCumulativeReturns (index_ticker,StartDate,EndDate,index_cumulative_data):   #function for calculating index cumulative returns
    index_cumulative_data=index_cumulative_data[index_cumulative_data['Company']==index_ticker]   #extracting the data of a given benchmark ticker
    index_cumulative_data.fillna(0,inplace=True)
    index_cumulative_data["Cumulative Return"] = (index_cumulative_data['Return']+1).cumprod()-1   #finding running cumulative 
    IndexCumulativeReturn=index_cumulative_data[["Date","Company","Cumulative Return"]]
    IndexCumulativeReturn.columns=["Date","Company","PNL"]
    IndexReturns=index_cumulative_data[["Date","Return"]]
    return IndexCumulativeReturn,IndexReturns

def AnnualizeReturn (CumulativeReturns_Daily, StartDate,period):
    Stock_Returns=CumulativeReturns_Daily.copy()
    Stock_Returns = Stock_Returns[pd.to_datetime(Stock_Returns['Date']).astype(str) >= StartDate]
    if period!='ITD':
        annualize_ret=Stock_Returns.iat[len(Stock_Returns)-1,1] 
    else:
        annualize_ret=((Stock_Returns.iat[len(Stock_Returns)-1,1] +1) ** (252/len(Stock_Returns)))-1   # annualize returns by taking cumulative returns
    return annualize_ret

def AnnualizeVolatility (Daily_Returns_data):

    Stock_Returns=Daily_Returns_data.copy()
    annualize_vol=Stock_Returns.loc[:,"PNL"].std()     #volatiity taking returns and applying standard deviation and then multiply with annualize number
    return annualize_vol*(math.sqrt(252))

def portfolioVaR(Trade_Dt,customerName,portfolioName,Stock_Data,Daily_Returns):
    VarDates=[Trade_Dt]  
    Exposure_Data=Stock_Data.copy()
    Exposure_Data=Exposure_Data[['Company','Factset_Ticker','Quantity']]    
    stock_list = set((Exposure_Data["Factset_Ticker"]))
    lookback=252;horizon=1
    Individual_VaR=pd.DataFrame()
    Portfolio_VaR_Component=pd.DataFrame()
    Portfolio_VaR_Component["Date"]=""
    Portfolio_VaR_Component["Portfolio"]=""
    Portfolio_VaR_Component["VaR$"]=""
    Portfolio_VaR_Component["Net$"]=""
    Portfolio_VaR_Component["VaR%"]=""
        
    i=0
    start_Date = VarDates[i]+timedelta(-365)
    End_Date = VarDates[i]
    prices_data = Daily_Returns[(Daily_Returns ['Date']>=start_Date)&(Daily_Returns['Date']<=End_Date)]
    prices_data.reset_index(drop=True,inplace=True)
    prices_data.columns = ['Company', 'Factset_Ticker','Date','Price','Return']
    prices_data=pd.merge(prices_data,Stock_Data,on=["Company","Factset_Ticker"]).reset_index(drop=True)  
    prices_data['Net'] = prices_data['Price'] * prices_data['Quantity']       
    DailyPortfolio_Net=prices_data.groupby(['Date'])["Net"].agg('sum').reset_index()      
    DailyPortfolio_Net.columns=['Date','DailyPortfolioNet']       
    prices_data=pd.merge(prices_data,DailyPortfolio_Net,on='Date',how='inner')       
    prices_data["Net%"] = prices_data["Net"]/prices_data['DailyPortfolioNet']       
    weight_matrix=prices_data[prices_data['Date']==VarDates[i]]
    weight_matrix=weight_matrix[['Company','Factset_Ticker','Net','Net%']]
    prices_data['Date']=pd.to_datetime(prices_data['Date'])
    prices_data['Max_Date']= prices_data.groupby(['Company',"Factset_Ticker"])['Date'].transform(max)
    prices_data=prices_data[prices_data['Max_Date'].astype(str)==str(VarDates[i])]
    returns_data=(prices_data.pivot(index='Date', columns="Factset_Ticker", values='Return')).reset_index(drop=True)
    returns_data.fillna(0,inplace=True)    
    Returns=np.array(returns_data.values,dtype=float)
    significant_value=2.32635                                #value for the confidence of 99%       
    variance_covariance_matrix=np.matmul(Returns.T,Returns)/lookback      
    #variance_covariance_matrix=returns_data.cov()   
    weight_matrix.sort_values(by=['Company','Factset_Ticker'],inplace=True)       
    weights=np.array(weight_matrix['Net'].values,dtype=float)      

    #Portfolio_Component_Var Calculation
    portfolio_risk=np.matmul(np.matmul(np.array(weight_matrix['Net%'].values,dtype=float),variance_covariance_matrix),np.array(weight_matrix['Net%'].values,dtype=float).T)   
    portfolio_var=math.sqrt(portfolio_risk) * math.sqrt(horizon) * significant_value * np.sum(weights,axis=0)       
    Portfolio_VaR_Component.loc[i,"Date"]=VarDates[i]
    Portfolio_VaR_Component.loc[i,"Portfolio"]=portfolioName
    Portfolio_VaR_Component.loc[i,"VaR$"]=portfolio_var
    Portfolio_VaR_Component.loc[i,"Net$"]=np.sum(weights,axis=0)
    Portfolio_VaR_Component.loc[i,"VaR%"]=Portfolio_VaR_Component["VaR$"][i]/Portfolio_VaR_Component["Net$"][i]
        
    #Individual var calculation
    temp=pd.DataFrame()
    Variance  =np.diag(variance_covariance_matrix).tolist()
    Stock_Values = prices_data[['Company','Factset_Ticker','Net']][prices_data['Date'].astype(str)==str(VarDates[i])].reset_index(drop=True)    
    Stock_Values.sort_values(by=["Company","Factset_Ticker"],inplace=True)
    Stock_Values['Variance']=Variance
    temp["Stock"]=Stock_Values['Company']
    temp["Factset_Ticker"]=Stock_Values['Factset_Ticker']
    temp["Date"]=VarDates[i]
    temp["VaR$"]=np.sqrt(Stock_Values['Variance']) * Stock_Values['Net'] * math.sqrt(horizon) * significant_value
    temp["Net$"]=Stock_Values['Net']
    temp["VaR%"]=temp["VaR$"]/ temp["Net$"]
    Individual_VaR=Individual_VaR.append(temp)

    Individual_VaR=Individual_VaR[['Date','Stock','Factset_Ticker','VaR$','Net$','VaR%']]
    return Portfolio_VaR_Component,Individual_VaR
    
