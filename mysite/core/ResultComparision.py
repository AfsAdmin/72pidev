import pandas as pd
from .models import OptSavedPortfolioEff, OptSavedPortfolioMont, OptSavedPortfolioVol

def opt_summary_data(user,base_portfolio):
  try:
    eff_objs = OptSavedPortfolioEff.objects.filter(user=user,Base_Portfolio=base_portfolio).values('portfolio_name', 'company', 'model', 'actual')
    eff_data = pd.DataFrame(list(eff_objs))
    vol_objs = OptSavedPortfolioVol.objects.filter(user=user,Base_Portfolio=base_portfolio).values('portfolio_name', 'company', 'model', 'actual')
    vol_data = pd.DataFrame(list(vol_objs))    
    output = pd.DataFrame()    
    p=0
    if (not eff_data.empty) and (not vol_data.empty):
      eff_data.columns = ['Portfolio Name', 'Company', 'Model', 'Actual_Portfolio']
      portfolio_names = eff_data['Portfolio Name'].unique().tolist()
      for i in portfolio_names:  
        each_portfolio = eff_data[eff_data['Portfolio Name']==i]
        if p==0:
            output=each_portfolio[['Company','Actual_Portfolio']]
        each_portfolio.reset_index(drop=True,inplace=True)
        each_portfolio =each_portfolio[['Model','Company']]
        each_portfolio.columns = [i+"",'Company']
        output = pd.merge(output,each_portfolio,on='Company',how='inner')
        p=p+1
      
    if (not vol_data.empty) and (not eff_data.empty):
      vol_data.columns = ['Portfolio Name', 'Company', 'Model', 'Actual_Portfolio']
      portfolio_names = vol_data['Portfolio Name'].unique().tolist()
      for i in portfolio_names:
        each_portfolio = vol_data[vol_data['Portfolio Name']==i]
        each_portfolio.reset_index(drop=True,inplace=True)
        each_portfolio =each_portfolio[['Model','Company']]
        each_portfolio.columns = [i+"",'Company']
        output = pd.merge(output,each_portfolio,on='Company',how='inner')
    return output
  except Exception as e:
    return pd.DataFrame()      
       

      
       
