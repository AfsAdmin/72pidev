# Django
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.models import User, Group
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.decorators import login_required
from django.views.generic import TemplateView, ListView
from django.views import View
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page
from django.contrib.auth.mixins import LoginRequiredMixin
from mysite.core.Forms import *
from mysite.core.models import *
from mysite.home.models import *
from django.contrib.auth.views import LoginView
from django.contrib.auth import authenticate
from django.contrib.auth import login as user_login, logout as user_logout
from django.views.generic import CreateView, RedirectView
from django.contrib.auth.tokens import PasswordResetTokenGenerator as default_token_generator
from django.template.response import TemplateResponse
from django.conf import settings 
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse, Http404
from django.contrib import messages
from django.core.mail import send_mail
from django.core import mail
from django.core.mail import EmailMessage
from django.template.loader import render_to_string
from django.contrib.sites.shortcuts import get_current_site
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.utils.encoding import force_bytes, force_text
from .tokens import account_activation_token
from django.contrib.admin.models import LogEntry
from django.views.decorators.csrf import csrf_exempt
from django.core.exceptions import ValidationError
from django.db.utils import DataError, DatabaseErrorWrapper, DatabaseError
from django.db.models import Avg, Max, Min, Sum
from django.utils import timezone
from django.utils.timezone import localdate
from xlutils.copy import copy 
from xlrd import open_workbook 
from django.db.models import Q
from io import BytesIO
from django.template.loader import get_template
from django.core.serializers.json import DjangoJSONEncoder
from mysite.core.Decorators import SubscriptionValidation
from mysite.core.Mixins import SubscriptionValidationMixin
import xlwt
import os
# Userdefined exceptions 
from .Exceptions import *
from mysite.core.ContextProcessors import *
#database conncection
import pyodbc
from mysite.core.Appconfigproperties import *
# python 
import sys
import pandas as pd
import numpy as np
import datetime
import json
import dateutil.parser
import calendar
from datetime import datetime, date
import operator
import random 
from collections import OrderedDict
from itertools import chain
from django.core.cache import cache
import math

# Python files for the calculation
##########            Portfolio Analyzer
from mysite.core.PortfolioCheck import *    #used in all dashbaords for checking the portfolio's
###     Returns
# from .PortfolioReturns1 import *                    # used in Portfolio_Returns (Portfolio returns)
from .PortfolioReturns2 import *                       # used in Portfolio_Returns (Portfolio returns)
from .Returns_by_stock_sector import *              #used in new Returns_by_stock_sector
from .MonthlyPerformance1 import *             # used in Monthly_Return (Monthly Returns)
from .MonthlyPerformance2 import *          # used in Monthly_Return (Monthly Returns)
# from .ActualvsExpected import *                     # used in Actual_vs_Expected (Actual_vs_Expected)
# from .QoQStockReturns import *                           # used in QoQStockReturns (Optimized Portfolio)
###     Investment amount 
from .Investment_amount_sector_mktcap import *      #used in Investment_amount_sector_mktcap dashboard
###     Analyst Target Price
from .AnalystTargetPrice import *                          # used in AnalystTargetPrice (Price Target)
###     Risk
from .Risk_overview_portfolio import *              #used in factors dashboard
from .Risk_overview_stockwise import *              #used in factors dashboard
###     Factor Analysis
from .Fama_French import *                          # used in FamaFrench_Current (Fama French Current)
from .InvestmentAmountFactor import *                              #used in investment_amount_factors dashboard
###     Recommendations
from .Recommendations import *                              #used in Recommendations
from .Analyzer_summary import *                     #used in Analyzer_summary
from .StocksPerformance import *
##########                Portfolio Optimizaion   
from .CurrentPositions import *                     # used in Current_Positions (Current Positions)
from .Efficient_Frontier import *                   # used in Efficient_Frontier (Optimized Portfolio)
from .target_return_model import *    # used in Portfolio Optimization Volatility (Optimized Portfolio)
from .ResultComparision import *                 # used in opt_summary
######## Conversational AI ##########
#########              Version 2
from .us_tweets_tickerwise import *
from .NLPStockSearch import *
from .Stylemetrics_India import *                     #used in Dashboardsummary
from .Stylemetrics_US import *                     #used in Dashboardsummary
from .importportfolio import *
# from .elastic_search import *
# from .elastic_search_new import *
from .index_creation import *
from .Stock_vs_Index_Returns import *
from .Live_Cumulative_Returns_Dashboard import *
from .RiskMonitorCalcs import *
import time
from babel.numbers import format_currency
from mysite.core.CacheBuilder import *
# --------------------------------------        views start        ------------------------------------------
symbol=''

@csrf_exempt
def countryChange(request):
    context={}
    if request.method == 'POST':
        request.session['country'] = request.POST.get("country")
        country = request.session['country']
        if request.user.is_authenticated:
            stocks = CustomerPortfolioDetails.objects.filter(customer_name=request.user.username,country=request.session['country']).values().order_by('created_at')
            if stocks.count() == 0:
                port_names={'India':'72PI India Portfolio','US':'72PI US Portfolio'}
                stocks = ModelPortfolioDetails.objects.filter(country=country,model_portfolio_name=port_names[country]).values().order_by('company_name')
                stocksDF = pd.DataFrame(list(stocks))
                portfolio_list = list(stocksDF['model_portfolio_name'].unique())
            else:
                stocksDF = pd.DataFrame(list(stocks))
                portfolio_list=list(stocksDF['portfolio_name'].unique())
            try:
                if any(elem in portfolio_list for elem in ['72PI India Portfolio','72PI US Portfolio']):
                    portfolioSet = list(stocksDF['model_portfolio_name'].unique())
                    request.session['portfoliolist'] = portfolioSet
                    request.session['portfolio_name'] = (portfolioSet)[0]
                else:
                    portfolioSet = list(stocksDF['portfolio_name'].unique())
                    request.session['portfoliolist'] = portfolioSet
                    request.session['portfolio_name'] = (portfolioSet)[0]
            except:
                pass
        context={'country':country}
    return HttpResponse(json.dumps(context), content_type='application/json')

def checkPortfolioName(username,country,portfolio_name):
    stocks = CustomerPortfolioDetails.objects.filter(customer_name=username,country=country).values().order_by('company_name')
    stocksDF = pd.DataFrame(list(stocks))
    if len(stocksDF) > 0:
        portfolioSet = list(stocksDF['portfolio_name'].unique())
    else:
        portfolio_names={'India':'72PI India Portfolio','US':'72PI US Portfolio'}
        stocks = ModelPortfolioDetails.objects.filter(country=country,model_portfolio_name=portfolio_names[country]).values().order_by('company_name')
        stocksDF = pd.DataFrame(list(stocks))
        portfolioSet=[portfolio_names[country]]  
    if(portfolio_name not in (portfolioSet)):
        return True
    else:
        return False  


class HomePageView(TemplateView):
    template_name = "home.html"

    def get_context_data(self,**kwargs):
        context = super().get_context_data(**kwargs)
        try:
            current_country=self.request.session['country']
        except KeyError:
            self.request.session['country'] = 'India'
            current_country=self.request.session['country']
        dist_companies=pd.DataFrame(list(UniversePricesMain.objects.filter(price__isnull=True).values('company').distinct()))
        filters=list(dist_companies['company'])
        us_filters=[]
        us_dist_companies=pd.DataFrame(list(UsUniversePricesMain.objects.filter(price__isnull=True).values('company').distinct()))
        if len(us_dist_companies)>0:
            us_filters = list(us_dist_companies['company'])
        #india
        gainer_loser_df_india = pd.DataFrame(list(GainersLosers.objects.all().values()))    
        gainer_loser_df_india['return_field']=(gainer_loser_df_india['return_field']*100).round(2)
        index_gainers_india = gainer_loser_df_india.loc[(gainer_loser_df_india["companytype"] == 'Yes') & (gainer_loser_df_india["type"] == 'Top 5 Gainers'), ['nsesymbol','price','return_field']].values.tolist()
        index_losers_india = gainer_loser_df_india.loc[(gainer_loser_df_india["companytype"] == 'Yes') & (gainer_loser_df_india["type"] == 'Top 5 Losers'), ['nsesymbol','price','return_field']].values.tolist()
        gainers_india = gainer_loser_df_india.loc[(gainer_loser_df_india["companytype"] == 'No'), ['nsesymbol','return_field']].sort_values(by=['return_field'], ascending=False).values.tolist()
        #usa
        gainer_loser_df_usa= pd.DataFrame(list(UsGainersLosers.objects.all().values()))        
        gainer_loser_df_usa['return_field']=(gainer_loser_df_usa['return_field']*100).round(2)
        index_gainers_usa = gainer_loser_df_usa.loc[(gainer_loser_df_usa["companytype"] == 'Yes') & (gainer_loser_df_usa["type"] == 'Top 5 Gainers'), ['factset_ticker','price','return_field']].values.tolist()
        index_losers_usa = gainer_loser_df_usa.loc[(gainer_loser_df_usa["companytype"] == 'Yes') & (gainer_loser_df_usa["type"] == 'Top 5 Losers'), ['factset_ticker','price','return_field']].values.tolist()
        gainers_usa = gainer_loser_df_usa.loc[(gainer_loser_df_usa["companytype"] == 'No'), ['factset_ticker','return_field']].sort_values(by=['return_field'], ascending=False).values.tolist()
        gainer_date_india=list(set(gainer_loser_df_india['date'].astype(str)))
        gainer_date_usa=list(set(gainer_loser_df_usa['date'].astype(str)))
        market_byte_df_india= pd.DataFrame(list(MarketByte.objects.all().values('country','ticker','dailypercentchange','monthly_price_change_percent','yearly_price_change_percent')))
        market_byte_df_usa= pd.DataFrame(list(UsMarketByte.objects.all().values('country','ticker','dailypercentchange','monthly_price_change_percent','yearly_price_change_percent').order_by('order')))
        def Adv_Dec(universeModel,indexconstituents,index):
            Universe_Prices_Main = pd.DataFrame(list(universeModel.objects.filter(date = universeModel.objects.all().aggregate(Max('date'))['date__max']).values()))
            IndexConstituents = pd.DataFrame(list(indexconstituents.objects.filter(index_name = index).values()))
            Adv_dec=pd.DataFrame()
            columns={'Nifty 500':['company','lname'],'S&P 500':['factset_ticker','fs_ticker']}
            left_on_column = columns[index][0]
            right_on_column = columns[index][1]
            Advances1=pd.merge(Universe_Prices_Main,IndexConstituents,how='inner',left_on=left_on_column,right_on=right_on_column)
            Adv_dec.loc[0,'type']='Advances'
            Adv_dec.loc[0,'count']=len(Advances1[Advances1['return_field']>=0])
            Declines1=pd.merge(Universe_Prices_Main,IndexConstituents,how='inner',left_on=left_on_column,right_on=right_on_column)
            Adv_dec.loc[1,'type']='Declines'
            Adv_dec.loc[1,'count']=len(Declines1[Declines1['return_field']<0])
            return Adv_dec
        #india
        adv_dec_bar_india = pd.DataFrame(list(AdvancesDeclines.objects.all().values('date','count','type')))
        prev_10_adv_india = adv_dec_bar_india.loc[adv_dec_bar_india["type"]=="Advances",['count']].values.tolist()
        prev_10_dec_india = adv_dec_bar_india.loc[adv_dec_bar_india["type"]=="Declines",['count']].values.tolist()
        prev_10_dec_india = [[ (-1) * i for i in inner ] for inner in prev_10_dec_india]
        prev_10_categories_india = adv_dec_bar_india['date'].values.tolist()
        #usa
        adv_dec_bar_usa = pd.DataFrame(list(UsAdvancesDeclines.objects.all().values('date','count','type')))
        
        prev_10_adv_usa = adv_dec_bar_usa.loc[adv_dec_bar_usa["type"]=="Advances",['count']].values.tolist()
        prev_10_dec_usa = adv_dec_bar_usa.loc[adv_dec_bar_usa["type"]=="Declines",['count']].values.tolist()
        prev_10_dec_usa = [[ (-1) * i for i in inner ] for inner in prev_10_dec_usa]
        prev_10_categories_usa = adv_dec_bar_usa['date'].values.tolist()
        #india
        df=pd.DataFrame(list(NiftyIndexesCorelation.objects.all().values()))
        
        df.columns = ['s_no','Index', 'Nifty 50', 'Auto','Bank','Energy','Fin Serv','FMCG','Infra','IT','Media','Metal','MidCap 100','Pharma','PVT Bank','PSU Bank','Realty','SmallCap 100']
        df = df[['Index', 'Nifty 50', 'Auto','Bank','Energy','Fin Serv','FMCG','Infra','IT','Media','Metal','MidCap 100','Pharma','PVT Bank','PSU Bank','Realty','SmallCap 100']]
        header_list_india=df.columns.tolist()
        data_list_india=df.values.tolist()
        #usa
        df=pd.DataFrame(list(UsIndicesCorrelation.objects.all().values()))
        
        df.columns = ['Index', 'S&P500', 'Russell 2000','Dow30','XLB-US','XLC-US','XLE-US','XLF-US','XLI-US','XLK-US','XLP-US','XLRE-US','XLU-US','XLV-US','XLY-US','date','s_no']
        df = df[['Index', 'S&P500', 'Russell 2000','Dow30','XLB-US','XLC-US','XLE-US','XLF-US','XLI-US','XLK-US','XLP-US','XLRE-US','XLU-US','XLV-US','XLY-US']]
        header_list_usa=df.columns.tolist()
        data_list_usa=df.values.tolist()
        
        
        
        if(current_country=='US'):
            us_companies = UsSecurityMaster.objects.exclude(fs_name__in=us_filters).values('fs_name','security_code').distinct()
            us_companies = us_companies.order_by('fs_name')
            context['companies'] = us_companies
            context['default']='Find Stock Details (S&P 500,  Russell 1000 and Russell 2000 traded stocks)'
        else:
            companies = SecurityMaster.objects.exclude(fs_name__in=filters).filter(flag='yes').values('fs_name','security_code').distinct()
            companies = companies.order_by('fs_name')
            context['companies'] = companies
            context['default']='Find Stock details (only NSE traded)'
        
        def nifty500_sp500(screening):
            input_df=pd.DataFrame(list(screening.objects.values('fs_name','market_cap','sales','beta_1y','volatility_1y','pe','pb','roe','debt_equity','ev_sales','sales_growth','eps_growth','security_code')))
        
            input_df.columns = ['fs_name','market_cap','value','beta_1y','volatility_1y','pe','pb','roe','debt_equity','ev_sales','sales_growth','eps_growth','security_code']
            input_df =input_df[input_df['debt_equity'].notna()]
            input_df.fillna(0,inplace=True)
            # input_df['current_price_vs_52_week_high']=input_df['current_price_vs_52_week_high'].apply(lambda x: round(x*100,2))
            input_df['sales_growth']=input_df['sales_growth'].apply(lambda x: round(x*100,2))
            input_df['eps_growth']=input_df['eps_growth'].apply(lambda x: round(x*100,2))
            input_df=input_df[(input_df['debt_equity'] <=0.4) & (input_df['sales_growth'] >=5 )]
            return input_df
        #india nifty500
        input_df_india=nifty500_sp500(Screening)
        
        nse_data=pd.DataFrame(list(Indexconstituents.objects.filter(flag='yes').values('lname','index_name').distinct()))
        bse_data=pd.DataFrame(list(IndexconstituentsBse.objects.filter(flag='yes').values('lname','index_name').distinct()))        
        data=pd.concat([nse_data,bse_data])
        data=pd.pivot(data,columns='index_name',values='lname')
        data.columns=[col.replace("&","") for col in data.columns]
        output={}
        for column in data.columns:
            output[column]=data[column].dropna().unique().tolist()
        input_df_india=input_df_india[input_df_india['fs_name'].isin(output['Nifty 500'])]
        #usa smp500
        input_df_usa=nifty500_sp500(UsScreening)        
        data=pd.DataFrame(list(UsIndexconstituents.objects.filter(flag='yes').values('lname','index_name').distinct()))
        
        data=pd.pivot(data,columns='index_name',values='lname')
        data.columns=[col.replace("&","") for col in data.columns]
        output={}
        for column in data.columns:
            output[column]=data[column].dropna().unique().tolist()
        input_df_usa=input_df_usa[input_df_usa['fs_name'].isin(output['SP 500'])]
    


        #heatmap
        sql_conn=pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server}; SERVER='+SERVER+'; PORT='+PORT+'; DATABASE='+DATABASE+';UID='+UID+';PWD='+PWD+';')

        def MultiLayerHeapmap(query,requiredColumn,securityCode):
            daily_stock_returns = pd.read_sql(query,sql_conn)
            daily_stock_returns=daily_stock_returns.fillna(0) 
            daily_stock_returns['DailyPercentChange']=daily_stock_returns['DailyPercentChange']*100
            daily_stock_returns  = daily_stock_returns[[securityCode,'DailyPercentChange','GICS_Sector','Gics_SubIndustry_Name','Market Cap',requiredColumn]]
            daily_stock_returns=daily_stock_returns[daily_stock_returns[requiredColumn]=='Yes']
            data = daily_stock_returns.groupby(['GICS_Sector','Gics_SubIndustry_Name',securityCode])['Market Cap','DailyPercentChange'].sum().reset_index()
            gics_sub_group = daily_stock_returns.groupby(['GICS_Sector','Gics_SubIndustry_Name'])['Market Cap','DailyPercentChange'].sum().reset_index()
            gics_main_group  = daily_stock_returns.groupby(['GICS_Sector'])['Market Cap','DailyPercentChange'].sum().reset_index()
            Final_output = data.copy()
            
    
            gics_sub_group.columns=['GICS_Sector', 'Gics_SubIndustry_Name', 'SUB_GICS_size', 'SUB_GICS_FrequencyPercentChange']
            gics_main_group.columns=['GICS_Sector', 'GICS_size', 'GICS_FrequencyPercentChange']

            Final_output = Final_output.merge(gics_sub_group,on=['GICS_Sector', 'Gics_SubIndustry_Name'],how='inner')
            Final_output = Final_output.merge(gics_main_group,on=['GICS_Sector'],how='inner')
            final_dict_list=[]
            gics = Final_output['GICS_Sector'].unique().tolist()
            gics_list = []
            mcap=0
            colorRange=[-3,-2,-1,0,1,2,3]
            for main_gics in gics:
                i=0
                gics_wise = Final_output[Final_output['GICS_Sector']==main_gics]
                gics_wise.reset_index(drop=True,inplace=True)
                sub_gics = gics_wise['Gics_SubIndustry_Name'].unique().tolist()
                sub_gics_list=[]
                for each_gics in sub_gics:
                    j=0
                    sub_gics_wise = gics_wise[gics_wise['Gics_SubIndustry_Name']==each_gics]
                    sub_gics_wise.reset_index(drop=True,inplace=True)
                    sub_gics_companies_list=[]
                    for k in range(0,len(sub_gics_wise)):
                        if sub_gics_wise.loc[k,'DailyPercentChange']>=colorRange[6]:
                            color='35764E'
                        elif sub_gics_wise.loc[k,'DailyPercentChange']>=colorRange[5]:
                            color='2F9E4F'
                        elif sub_gics_wise.loc[k,'DailyPercentChange']>=colorRange[4]:
                            color='30CC5A'
                        elif sub_gics_wise.loc[k,'DailyPercentChange']>=colorRange[3]:
                            color='414554'
                        elif sub_gics_wise.loc[k,'DailyPercentChange']>=colorRange[2]:
                            color='F63538'
                        elif sub_gics_wise.loc[k,'DailyPercentChange']>=colorRange[1]:
                            color='BF4045'
                        else:
                            color='8B444E'
                        if(sub_gics_wise.loc[k,'DailyPercentChange']>=0):
                            sub_gics_wise_dict={'label':sub_gics_wise.loc[k,securityCode]+"<br/> +"+str(round(sub_gics_wise.loc[k,'DailyPercentChange'],2))+"%",'value':sub_gics_wise.loc[k,'Market Cap'],'svalue':round(sub_gics_wise.loc[k,'DailyPercentChange'],2),'fillcolor':color}
                        else:
                            sub_gics_wise_dict={'label':sub_gics_wise.loc[k,securityCode]+"<br/>"+str(round(sub_gics_wise.loc[k,'DailyPercentChange'],2))+"%",'value':sub_gics_wise.loc[k,'Market Cap'],'svalue':round(sub_gics_wise.loc[k,'DailyPercentChange'],2),'fillcolor':color}
                    
                        sub_gics_companies_list.append(sub_gics_wise_dict)
                        mcap=mcap+sub_gics_wise.loc[k,'Market Cap']
            
                        if sub_gics_wise.loc[j,'DailyPercentChange']<=colorRange[0]:
                            color='F63538'
                        elif sub_gics_wise.loc[j,'DailyPercentChange']>colorRange[0] and sub_gics_wise.loc[j,'DailyPercentChange']<=colorRange[1]:
                            color='BF4045'
                        elif sub_gics_wise.loc[j,'DailyPercentChange']>colorRange[1] and sub_gics_wise.loc[j,'DailyPercentChange']<=colorRange[2]:
                            color='8B444E'
                        elif sub_gics_wise.loc[j,'DailyPercentChange']>colorRange[2] and sub_gics_wise.loc[j,'DailyPercentChange']<=colorRange[3]:
                            color='414554'
                        elif sub_gics_wise.loc[j,'DailyPercentChange']>colorRange[3] and sub_gics_wise.loc[j,'DailyPercentChange']<=colorRange[4]:
                            color='35764E'
                        elif sub_gics_wise.loc[j,'DailyPercentChange']>colorRange[4] and sub_gics_wise.loc[j,'DailyPercentChange']<=colorRange[5]:
                            color='2F9E4F'
                        else:
                            color='30CC5A'
                
                    sub_gics_dict = { "fillcolor": color,'label':sub_gics_wise.loc[j,'Gics_SubIndustry_Name'],'value':sub_gics_wise.loc[j,'SUB_GICS_size'],'svalue':sub_gics_wise.loc[j,'SUB_GICS_FrequencyPercentChange']/len(sub_gics_wise),'data':sub_gics_companies_list}
                    sub_gics_list.append(sub_gics_dict)
                    j=j+1

                gics_dict = { "fillcolor": "20261b",'label':gics_wise.loc[i,'GICS_Sector'],'value':gics_wise.loc[i,'GICS_size'],'svalue':gics_wise.loc[i,'GICS_FrequencyPercentChange'],'data':sub_gics_list}
                gics_list.append(gics_dict)
                i=i+1
            return gics_list,mcap

        gics_list_snp500,mcap_snp500=MultiLayerHeapmap("select  * from US_Daily_Stock_Returns","SnP_500",'Ticker')
        gics_list_dow30,mcap_dow30=MultiLayerHeapmap("select  * from US_Daily_Stock_Returns","DOW_30",'Ticker')
        gics_list_nifty50,mcap_nifty50=MultiLayerHeapmap("select  * from Daily_Stock_Returns","Nifty_50",'NSE_Symbol')
        sql_conn.close()

        context['gics_list_snp500']=gics_list_snp500
        context['mcap_snp500']=mcap_snp500
        context['gics_list_dow30']=gics_list_dow30
        context['mcap_dow30']=mcap_dow30
        context['gics_list_nifty50']=gics_list_nifty50
        context['mcap_nifty50']=mcap_nifty50
        context['nifty500_bubble']=input_df_india.to_json(orient='records')
        context['sp500_bubble']=input_df_usa.to_json(orient='records')
        context['correlation_india']=[header_list_india]+data_list_india
        context['correlation_usa']=[header_list_usa]+data_list_usa
        context['index_gainers_india'] = index_gainers_india
        context['index_losers_india'] =index_losers_india
        context['index_gainers_usa'] = index_gainers_usa
        context['index_losers_usa'] = index_losers_usa
        context['gainers_india'] = gainers_india
        context['gainers_usa'] = gainers_usa
        context['gainer_date_india'] = gainer_date_india
        context['gainer_date_usa'] = gainer_date_usa
        context['prev_10_adv_india'] = prev_10_adv_india
        context['prev_10_dec_india'] = prev_10_dec_india
        context['prev_10_categories_india'] = prev_10_categories_india
        context['prev_10_adv_usa'] = prev_10_adv_usa
        context['prev_10_dec_usa'] = prev_10_dec_usa
        context['prev_10_categories_usa'] = prev_10_categories_usa
        context['market_byte_df_india'] = market_byte_df_india.values.tolist()
        context['market_byte_df_usa'] = market_byte_df_usa.values.tolist()
        context['adv_dec_india'] =  Adv_Dec(UniversePricesMain,Indexconstituents,'Nifty 500').values.tolist()
        context['adv_dec_usa'] =  Adv_Dec(UsUniversePricesMain,UsIndexconstituents,'S&P 500').values.tolist()
        context['tastybytes'] = TastyByte.objects.live().order_by('-posted_at')[:3]
        context['country']=current_country
        return context

class AboutUsView(TemplateView):
    template_name = 'Aboutus.html'


class PrivacyPolicyView(TemplateView):
    template_name = 'Privacypolicy.html'

class TermsofUseView(TemplateView):
    template_name = 'TermsOfUse.html'

class TrustAndSecurityView(TemplateView):
    template_name = 'TrustSecurity.html'

class FAQView(TemplateView):
    template_name='FAQ.html'

def portfolioconstruction(request):
    return render(request, 'Portfolioconstruction.html')

def portfolioanalyzer(request):
    return render(request, 'Portfolioanalyzer.html')

def portfoliooptimization(request):
    return render(request, 'Portfoliooptimization.html')

def mlprediction(request):
    return render(request, 'MLPredection.html')


class ListAllWaitingAlertsView(LoginRequiredMixin,TemplateView):
    template_name = "Myalerts.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        alerts = list() #Empty List
        notifications1 = CustomerPriceMovementsAlerts.objects.filter(customer_name=self.request.user,price_status='Waiting').all()
        notifications2 = CustomerPriceMovementsAlerts.objects.filter(customer_name=self.request.user,percent_status='Waiting').all() 
        notifications3 = CustomerPriceMovementsAlerts.objects.filter(customer_name=self.request.user,price52wh_status='Waiting').all()
        notifications4 = CustomerTechnicalIndicationAlerts.objects.filter(customer_name=self.request.user,rsi_status='Waiting').all()
        notifications5 = CustomerTechnicalIndicationAlerts.objects.filter(customer_name=self.request.user,dma_status='Waiting').all()
        # Price Movement Notifications
        for item in notifications1:
            # Conditions for Price in Rs.
            if item.price_value and item.price_value_condition:
                item.left = 'Last Price'
                item.condition = item.price_value_condition
                item.right = item.price_value
                item.type = 'pricealert'
                item.metric = '₹'
                if item.country == 'US':
                    item.metric = '$'
                item.buy_sell = item.pricealert_for
            alerts.append(item)
        for item in notifications2:
            # Conditions for Price in %
            if item.price_percent and item.price_percent_condition:
                item.left = 'Last Price'
                item.condition = item.price_percent_condition
                item.right = item.price_percent
                item.type = 'percentalert'
                item.metric = '%'
                item.buy_sell = item.percentalert_for
            alerts.append(item)
        for item in notifications3:
            # Conditions for 52-week high
            if item.price_52wh_percent and item.price_52wh_condition:
                item.left = 'Last Price'
                item.condition = item.price_52wh_condition
                item.right = item.price_52wh_percent
                item.type = '52whalert'
                item.metric = '% of 52-WH'
                item.buy_sell = item.price52alert_for
            alerts.append(item)
        # Technical Indication Notifications
        for item in notifications4:
            # Conditions for 14 Day RSI
            if item.rsi_condition and item.rsi_metric:
                item.left = item.rsi_metric
                item.condition = item.rsi_condition
                item.right = item.rsi_operand1
                item.type = 'rsialert'
                item.metric = ''
                item.buy_sell = item.rsialert_for
            alerts.append(item)
        for item in notifications5:
            # Conditions for DMA
            if item.dma_operand1 and item.dma_operand2 and item.dma_condition:
                item.left = item.dma_operand1
                item.condition = item.dma_condition
                item.right = item.dma_operand2
                item.type = 'dmaalert'
                item.metric = ''
                item.buy_sell = item.dmaalert_for
            alerts.append(item)
        context['alerts'] = alerts
        return context

class StockWiseWaitingAlertsView(LoginRequiredMixin,View):

    def get(self, request, *args, **kwargs):
        template_name = 'Myalerts.html'
        companyName = request.GET.get('stock',None)
        portfolioName = request.GET.get('portfolio',None)
        companyName = re.sub(r'\(.*?\)', '', companyName)
        alerts = list() #Empty List
        companyName = companyName.replace('pi72pi', '&')
        notifications1 = CustomerPriceMovementsAlerts.objects.filter(customer_name=self.request.user,portfolio_name=portfolioName,company_name=companyName,price_status='Waiting').all()
        notifications2 = CustomerPriceMovementsAlerts.objects.filter(customer_name=self.request.user,portfolio_name=portfolioName,company_name=companyName,percent_status='Waiting').all() 
        notifications3 = CustomerPriceMovementsAlerts.objects.filter(customer_name=self.request.user,portfolio_name=portfolioName,company_name=companyName,price52wh_status='Waiting').all()
        notifications4 = CustomerTechnicalIndicationAlerts.objects.filter(customer_name=self.request.user,portfolio_name=portfolioName,company_name=companyName,rsi_status='Waiting').all()
        notifications5 = CustomerTechnicalIndicationAlerts.objects.filter(customer_name=self.request.user,portfolio_name=portfolioName,company_name=companyName,dma_status='Waiting').all()
        # Price Movement Notifications
        for item in notifications1:
            # Conditions for Price in Rs.
            if item.price_value and item.price_value_condition:
                item.left = 'Last Price'
                item.condition = item.price_value_condition
                item.right = item.price_value
                item.type = 'pricealert'
                item.metric = '₹'
                if item.country == 'US':
                    item.metric = '$'
                item.buy_sell = item.pricealert_for
            alerts.append(item)
        for item in notifications2:
            # Conditions for Price in %
            if item.price_percent and item.price_percent_condition:
                item.left = 'Last Price'
                item.condition = item.price_percent_condition
                item.right = item.price_percent
                item.type = 'percentalert'
                item.metric = '%'
                item.buy_sell = item.percentalert_for
            alerts.append(item)
        for item in notifications3:
            # Conditions for 52-week high
            if item.price_52wh_percent and item.price_52wh_condition:
                item.left = 'Last Price'
                item.condition = item.price_52wh_condition
                item.right = item.price_52wh_percent
                item.type = '52whalert'
                item.metric = '% of 52-WH'
                item.buy_sell = item.price52alert_for
            alerts.append(item)
        # Technical Indication Notifications
        for item in notifications4:
            # Conditions for 14 Day RSI
            if item.rsi_condition and item.rsi_metric:
                item.left = item.rsi_metric
                item.condition = item.rsi_condition
                item.right = item.rsi_operand1
                item.type = 'rsialert'
                item.metric = ''
                item.buy_sell = item.rsialert_for
            alerts.append(item)
        for item in notifications5:
            # Conditions for DMA
            if item.dma_operand1 and item.dma_operand2 and item.dma_condition:
                item.left = item.dma_operand1
                item.condition = item.dma_condition
                item.right = item.dma_operand2
                item.type = 'dmaalert'
                item.metric = ''
                item.buy_sell = item.dmaalert_for
            alerts.append(item)
        return render(request, template_name,{'alerts':alerts,'display':'yes'})

    def post(self, request, *args, **kwargs):
        template_name = 'Myalerts.html'
        return render(request, template_name)


@login_required
@csrf_exempt
def updateMyStockAlerts(request):
    response = {}
    if request.method == 'POST':
        _id = request.POST['id']
        company = request.POST['companyname']
        portfolio = request.POST['portfolio']
        alerttype = request.POST['alerttype']
        leftvalue = request.POST['leftvalue']
        condition = request.POST['condition']
        rightvalue = request.POST['rightvalue']
        subtype = request.POST['subtype']
        if alerttype == 'Price Movement':
            if subtype == 'pricealert':
                CustomerPriceMovementsAlerts.objects.filter(id=_id).update(price_value_condition = condition,price_value = rightvalue)
                response['status'] = True
                response['result'] = 'Success'
            elif subtype == 'percentalert':
                CustomerPriceMovementsAlerts.objects.filter(id=_id).update(price_percent_condition = condition,price_percent = rightvalue)
                response['status'] = True
                response['result'] = 'Success'
            elif subtype == '52whalert':
                CustomerPriceMovementsAlerts.objects.filter(id=_id).update(price_52wh_condition = condition,price_52wh_percent = rightvalue)
                response['status'] = True
                response['result'] = 'Success'
            else:
                response['status'] = False
                response['result'] = 'Could not update the alert at this moment, try again!'
        elif alerttype == 'Technical Indicator':
            if subtype == 'rsialert':
                CustomerTechnicalIndicationAlerts.objects.filter(id=_id).update(rsi_condition=condition,rsi_operand1=rightvalue)
                response['status'] = True
                response['result'] = 'Success'
            elif subtype == 'dmaalert':
                CustomerTechnicalIndicationAlerts.objects.filter(id=_id).update(dma_condition=condition,dma_operand1=leftvalue,dma_operand2=rightvalue)
                response['status'] = True
                response['result'] = 'Success'
            else:
                response['status'] = False
                response['result'] = 'Could not update the alert at this moment, try again!'
        else:
            response['status'] = False
            response['result'] = 'Could not update the alert at this moment, try again!'
    else:
        response['status'] = False
        response['result'] = 'Something went wrong, try again'
    return HttpResponse(json.dumps(response), content_type='application/json')


@login_required
@csrf_exempt
def deleteMyStockAlerts(request):
    response = {}
    if request.method == 'POST':
        _id = request.POST['id']
        alerttype = request.POST['alerttype']
        subtype = request.POST['subtype']
        if alerttype == 'Price Movement':
            if subtype == 'pricealert':
                CustomerPriceMovementsAlerts.objects.filter(id=_id).update(price_value_condition = None,price_value = None,price_status='Deleted',price_read='NA')
                response['status'] = True
                response['result'] = 'Success'
            elif subtype == 'percentalert':
                CustomerPriceMovementsAlerts.objects.filter(id=_id).update(price_percent_condition = None,price_percent = None,percent_status='Deleted',percent_read='NA')
                response['status'] = True
                response['result'] = 'Success'
            elif subtype == '52whalert':
                CustomerPriceMovementsAlerts.objects.filter(id=_id).update(price_52wh_condition = None,price_52wh_percent = None,price52wh_status='Deleted',price52wh_read='No')
                response['status'] = True
                response['result'] = 'Success'
            else:
                response['status'] = False
                response['result'] = 'Could not delete the alert at this moment, try again!'
        elif alerttype == 'Technical Indicator':
            if subtype == 'rsialert':
                CustomerTechnicalIndicationAlerts.objects.filter(id=_id).update(rsi_condition=None,rsi_operand1=None,rsi_metric=None,rsi_status='Deleted',rsi_read='No')
                response['status'] = True
                response['result'] = 'Success'
            elif subtype == 'dmaalert':
                CustomerTechnicalIndicationAlerts.objects.filter(id=_id).update(dma_condition=None,dma_operand1=None,dma_operand2=None,dma_status='Deleted',dma_read='No')
                response['status'] = True
                response['result'] = 'Success'
            else:
                response['status'] = False
                response['result'] = 'Could not delete the alert at this moment, try again!'
        else:
            response['status'] = False
            response['result'] = 'Could not delete the alert at this moment, try again!'
    else:
        response['status'] = False
        response['result'] = 'Something went wrong, try again'
    return HttpResponse(json.dumps(response), content_type='application/json')


class GetStockPriceWithSectorView(LoginRequiredMixin, View):
    sectors = dict()
    prices = dict() 
    backprices = dict()
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(GetStockPriceWithSectorView, self).dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        self.prices['price'] = ''
        self.sectors['sector'] = ''
        self.backprices['backprice'] = ''
        context = {'price':self.prices, 'sector': self.sectors, 'backprice':self.backprices}
        return HttpResponse(json.dumps(context), content_type='application/json')

    def post(self, request, *args, **kwargs):
        inputCompany = self.request.POST.get('company')
        context = dict()
        try:
            if request.session['country'] == 'US':
                sectors = UsSecurityMaster.objects.filter(fs_name=inputCompany).first()
                prices = UsUniversePricesMain.objects.filter(company=inputCompany, date=(UsUniversePricesMain.objects.filter(company=inputCompany).aggregate(Max('date'))['date__max'])).first()
                backprices = UsUniversePricesMain.objects.filter(company=inputCompany, date=(UsUniversePricesMain.objects.filter(date__gte='2019-01-02').all().aggregate(Min('date'))['date__min'])).first()
                context = {'price':prices.price, 'sector':sectors.gics,'backprice':backprices.price,'security_code':sectors.security_code,'fsticker':sectors.fs_ticker}
            else:
                sectors = SecurityMaster.objects.filter(fs_name=inputCompany).first()
                prices = UniversePricesMain.objects.filter(company=inputCompany, date=(UniversePricesMain.objects.filter(company=inputCompany).aggregate(Max('date'))['date__max'])).first()
                backprices = UniversePricesMain.objects.filter(company=inputCompany, date__gte=(UniversePricesMain.objects.filter(date__gte='2019-01-01').all().aggregate(Min('date'))['date__min'])).first()
                context = {'price':prices.price, 'sector':sectors.gics,'backprice':backprices.price,'security_code':sectors.security_code,'fsticker':sectors.fs_ticker}
        except Exception as e:
            self.prices['price'] = ''
            self.sectors['sector'] = ''
            self.backprices['backprice'] = ''
        return HttpResponse(json.dumps(context), content_type='application/json')




class ListAllNotificationsView(LoginRequiredMixin, TemplateView):
    template_name = 'MyNotifications.html'
   

class MyPortfolioView(LoginRequiredMixin, SubscriptionValidationMixin, View):
    template_name = 'PortfolioDetails.html'

    def get(self, request, *args, **kwargs):
        try:
            try:
                country = request.session['country']
            except:
                request.session['country'] =  'India'
            country = request.session['country']
            symbols = {'India':'₹','US':'$'}
            symbol =symbols[country]
            cacheStocksData = getPortfolioStocksDataFromCacheBuilder(country)
            companies = list(cacheStocksData['FS Name'].unique())
            userStocks = CustomerPortfolioDetails.objects.filter(customer_name=request.user.username,country=country).values().order_by('created_at')
            if userStocks.count() == 0:
                customerName = request.user.username
                portfolios={'India':'72PI India Portfolio','US':'72PI US Portfolio'}
                request.session['portfolio_name'] = portfolios[request.session['country']]  #save the portfolio to the session
                request.session['portfoliolist'] = [request.session['portfolio_name']]
                return render(request, 'EmptyPortfolio.html',{'customer':customerName})
            else:
                notifications1 = CustomerPriceMovementsAlerts.objects.filter(Q(customer_name=request.user) & Q(price_status='Notified') | Q(percent_status='Notified') | Q(price52wh_status='Notified')).values('company_name','portfolio_name')
                notifications2 = CustomerTechnicalIndicationAlerts.objects.filter(Q(customer_name=request.user) & Q(rsi_status='Notified') | Q(dma_status='Notified')).values('company_name','portfolio_name')
                notifications = notifications1.union(notifications2)
                alerts1 = CustomerPriceMovementsAlerts.objects.filter(Q(customer_name=request.user) & Q(price_status='Waiting') | Q(percent_status='Waiting') | Q(price52wh_status='Waiting')).values('company_name','portfolio_name')
                alerts2 = CustomerTechnicalIndicationAlerts.objects.filter(Q(customer_name=request.user) & Q(rsi_status='Waiting') | Q(dma_status='Waiting')).values('company_name','portfolio_name')
                alerts = alerts1.union(alerts2)
                stocksDF = pd.DataFrame(list(userStocks))
                myStocks = list(stocksDF['company_name'].unique())
                portfolioSet = list(stocksDF['portfolio_name'].unique())
                notificationsDF = pd.DataFrame(list(notifications))
                alertsDF = pd.DataFrame(list(alerts))
                try:
                    alertsStocks = list(alertsDF['company_name'].unique())
                    alertsPortfolios = list(alertsDF['portfolio_name'].unique())
                except:
                    alertsStocks = []
                    alertsPortfolios = []
                try:
                    notificationsStocks = list(notificationsDF['company_name'].unique())
                    notificationsPortfolios = list(notificationsDF['portfolio_name'].unique())
                except:
                    notificationsStocks = []
                    notificationsPortfolios = []
                cacheStocksData = cacheStocksData[cacheStocksData['FS Name'].isin(myStocks)]
                cacheStocksData.reset_index(drop=True,inplace=True)
                cacheStocksData = cacheStocksData[['FS Name','Price','Market_Cap_Category','Security Code']]
                cacheStocksData.columns=['company_name','current_price','mcap_cat','security_code']
                finalDF = pd.merge(stocksDF,cacheStocksData, how='inner', on='company_name')
                finalDF['isAlertSent'] = 'No'
                finalDF['isAlertExist'] = 'No'
                for ind in finalDF.index:
                    if len(alertsStocks) and len(alertsPortfolios):
                        if finalDF['company_name'][ind] in alertsStocks and finalDF['portfolio_name'][ind] in alertsPortfolios:
                            finalDF['isAlertExist'][ind] = 'Yes'
                    if len(notificationsStocks) and len(notificationsPortfolios):
                        if finalDF['company_name'][ind] in notificationsStocks and finalDF['portfolio_name'][ind] in notificationsPortfolios:
                            finalDF['isAlertSent'][ind] = 'Yes'
                finalDF['company_name'] = finalDF['company_name']+" ("+finalDF['security_code']+")"
                request.session['portfoliolist'] = portfolioSet
                request.session['portfolio_name'] = finalDF['portfolio_name'].iat[-1]
                context = {'stocks' : finalDF.T.to_dict(),'companies':companies,'symbol':symbol,'recentportfolio':request.session['portfolio_name'],'portfolios':portfolioSet}
                return render(request,'PortfolioDetails.html' , context)
        except Exception as e:
            print(e)
            pass
   
            
@login_required
@csrf_exempt
def customerFeedback(request):
    if request.method == 'POST':
        status = dict()
        try:
            rating = request.POST['rating']
            category = request.POST['feed_type']
            feedback = request.POST['feed_text']
            CustomerFeedback(customer_name=request.user.username, rating=rating, category=category, feedback=feedback).save()
            mail_subject = '72PI Customer Feedback'
            mail_body = render_to_string('feedbackreply.html', {'customer': request.user.username,'rating': rating,'category':category,'feedback':feedback})
            email = request.user.email
            message = ''
            send_mail(mail_subject, message, 'feedback <support@72pi.ai>', [email], fail_silently = True,html_message=mail_body)
            send_mail(mail_subject, message, 'feedback <support@72pi.ai>', ['support@72pi.ai'], fail_silently = True,html_message=mail_body)
            status['msg'] = "Thank you, your feedback has been submitted to us"
        except Exception as e:
            print(e)
            status['msg'] = "Please fill all the fields and try again"
        return HttpResponse(json.dumps(status), content_type='application/json')
    else:
        return render(request, 'feedback.html')


@csrf_exempt
def newsLetterSubscription(request):
    status = dict()
    if request.method == 'POST':
        try:
            newsEmail = request.POST['email']
            if newsEmail:
                regex = r'(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)'
                check = re.search(regex, newsEmail)
                if check is None:
                    status['msg'] = 'Please provide valid email!'
                else:
                    if NewsLetterSubscription.objects.filter(email=newsEmail).exists():
                        status['msg'] = 'Email already subscribed to us!'
                    else:
                        NewsLetterSubscription(email=newsEmail).save()
                        status['msg'] = 'You have subscribed to our newsletters'
            else:
                status['msg'] = 'Something went wrong, try again!'
        except:
            status['msg'] = 'Something went wrong, try again!'
    else:
        status['msg'] = 'Internal server error, try again!'
    return HttpResponse(json.dumps(status), content_type='application/json')


    
@login_required
@csrf_exempt
def portfolioChanger(request):
    Status = {}
    if request.method == 'POST':
        portfolioName = request.POST['portfolio']
        if portfolioName is not None:
            try:
                Status = {}
                result = CustomerPortfolioLog.objects.filter(customer_name=request.user.username,portfolio_name=portfolioName).values('created_at','last_modified_at','last_action')
                if result:
                    for item in result:
                        if item['last_action'] == 'portfolio_created':
                            Status['createdAt'] = str(item['created_at'].strftime("%d-%m-%Y %H:%M:%S"))
                            Status['modifiedAt'] = "not_modified_at"
                        else:
                            Status['createdAt'] = str(item['created_at'].strftime("%d-%m-%Y %H:%M:%S"))
                            Status['modifiedAt'] = str(item['last_modified_at'].strftime("%d-%m-%Y %H:%M:%S"))
                else:
                    Status['createdAt'] = ""
                    Status['modifiedAt'] = ""
                request.session['portfolio_name'] = portfolioName
            except:
                Status['createdAt'] = ""
                Status['modifiedAt'] = ""
                request.session['portfolio_name'] = portfolioName
    Status['result'] = "Successfully changed portfolio!"
    return HttpResponse(json.dumps(Status), content_type='application/json')

def handler401(request):
    return render(request, '401.html', status=401)
def handler403(request):
    return render(request, '403.html', status=403)
def handler403(request):
    return render(request, '403.html', status=403)
def handler405(request):
    return render(request, '405.html', status=405)
def handler406(request):
    return render(request, '406.html', status=406)
def handler412(request):
    return render(request, '412.html', status=412)
def handler500(request):
    return render(request, '500.html', status=500)
def handler501(request):
    return render(request, '501.html', status=501)
def handler502(request):
    return render(request, '502.html', status=502) 

def SearchByStockInfo(request, cname):
    stockName = cname
    stock = SecurityMaster.objects.filter(fs_name__startswith=stockName).first()
    if stock:
        stockName = stock.fs_name
        fsTicker=stock.fs_ticker
    else:
        stock = UsSecurityMaster.objects.filter(fs_name__startswith=stockName).first()
        if stock:
            stockName = stock.fs_name
            fsTicker=stock.fs_ticker
    if stockName:
        securityModel = SecurityMaster
        universeModel = UniversePricesMain
        movingavg_model = MovingAverage
        indexes=['Nifty 50 Index','Nifty Midcap 100 Index','Nifty Smallcap 100 Index']
        symbol='₹'
        flag=0
        companyDescription=CompanyDescriptions
        companyAddressDetails2=CompanyAddressDetails2
        stockRanking=StockRanking
        format="in Cr"
        if UsSecurityMaster.objects.filter(fs_name=stockName).exists():
            securityModel = UsSecurityMaster
            universeModel = UsUniversePricesMain
            indexes=['S&P 500','Dow Jones Industrial Average','Russell 2000']
            symbol='$'
            flag=1
            movingavg_model = UsMovingAverage
            companyDescription=UsCompanyDescriptions
            companyAddressDetails2=UsCompanyAddressDetails2
            stockRanking=UsStockRanking
            format="M"
        factors = securityModel.objects.filter(fs_name=stockName,fs_ticker=fsTicker).all()
        details = companyAddressDetails2.objects.filter(lname=stockName).all()
        for item in factors:
            for det in details:
                if(flag==1):
                    item.industry = det.ind_l_name
                    item.address = det.regdist+', '+det.regstate
                    item.phone = det.tel1
                    item.website = det.internet
                else:
                    item.chairman = det.chairman
                    item.industry = det.ind_l_name
                    item.founded = det.inc_dt
                    item.address = det.regdist+', '+det.regstate
                    item.phone = det.tel1
                    item.email = det.email
                    item.website = det.internet
        company_description = companyDescription.objects.filter(company=stockName,co_code=fsTicker).values('description')
        if(len(company_description)>0):
            company_description1='<br /> '.join(company_description[0]['description'].splitlines())
        else:
            company_description1=''
        df=StockvsIndexMain(stockName)
        prices = universeModel.objects.filter(company=stockName,factset_ticker=fsTicker).values('date','price').order_by('date')
        prices_df = pd.DataFrame(list(prices))
        if(len(prices_df)>0):
            prices_df['date']=prices_df['date'].astype(str)
            prices_list = prices_df.values.tolist()
            df['Date']=df['Date'].astype(str)
            df=df.dropna().reset_index(drop=True)
            lineColor=['#02BCD4','#28a745','#6610F2','#ffc107','#dc3545']
            output_df=[{"name":"Return","lineWidth": 3,'color':lineColor[0],'data':df[['Date',stockName]].values.tolist()}, 
                {"name":indexes[0],'lineWidth': 2,'color':lineColor[1],'data':df[['Date',indexes[0]]].values.tolist()},
                {"name":indexes[1],"lineWidth": 2,'color':lineColor[2],'data':df[['Date',indexes[1]]].values.tolist()},
                {"name":indexes[2],"lineWidth": 2,'color':lineColor[3],'data':df[['Date',indexes[2]]].values.tolist()}
            ]
        else:
            prices_list=[]
            output_df=[]
        df=pd.DataFrame(list(movingavg_model.objects.filter(company=stockName,factset_ticker=fsTicker).values('date','price','ma20','ma50')))
        if(len(df)>0):
            df.columns=['Date','Price','MA20','MA50']
            df=df.reset_index(drop=True).sort_values(by='Date')
            df=df[['Date','Price','MA20','MA50']]
            df['Date'] = df['Date'].astype(str)
            df['Date'] = df['Date'].str[0:11]
            output_pricevsDMA=[]
            temp_list=[]
            lineColor=['#6610F2','#28a745','#02BCD4','#ffc107','#dc3545','#5A6268','#23272B']
            i=0
            for col in df.columns: 
                if(col!='Date'):
                    pricevsDMA_df=df[['Date',col]].dropna()
                    my_list=pricevsDMA_df.values.tolist()
                    if(col!='Price'):
                        x={"name":col,"lineWidth": 2,'color':lineColor[i],"data":my_list}
                    else:
                        x={"name":"Price","lineWidth": 3,'color':lineColor[i],"data":my_list}
                    output_pricevsDMA.append(x)
                    i=i+1
        else:
            output_pricevsDMA=[]
        stock_ranking=pd.DataFrame(list(stockRanking.objects.filter(fs_name=stockName,fs_ticker=fsTicker).values('nsesymbol','q_sales','q_piotroski_score','q_roe','q_debt_equity','q_fcf_margin','q_beta','g_eps_growth',
        'g_sales_growth','g_sales_qoq','t_price_vs_ema20','t_price_vs_ema50','t_price_vs_ema200','t_rsi','t_ema50_vs_ema200','t_current_price_vs_52_week_high','quality_score',
        'growth_score','technical_score','stock_rank','price','net_change','return_field')))
        stock_ranking= stock_ranking.fillna(0)
        ranking=stock_ranking.values.tolist()
        context = {'symbol':symbol,'stockName':stockName,'output_df':output_df,'factors':factors,'index1':indexes[0],'index2':indexes[1],'index3':indexes[2],
                    'prices':prices_list,'companyDescription':company_description,
                    'output_pricevsDMA':output_pricevsDMA,'stocks':factors,'companyDesc':company_description1,'ranking':ranking,'format':format}
        return render(request, 'StockInfo.html',context)
       

def StockInfoFromStockList(request):
    stockNameSymbol = request.GET.get('stock-name')
    stockName = re.sub(r'\(.*?\)', '', stockNameSymbol)
    stockName = stockName.strip()
    ticker_val = stockNameSymbol[stockNameSymbol.find("(")+1 : stockNameSymbol.find(")")] 
    stock = SecurityMaster.objects.filter(security_code=ticker_val).first()
    fsTicker = ''
    if stock:
        stockName = stock.fs_name
        fsTicker=stock.fs_ticker
    else:
        stock = UsSecurityMaster.objects.filter(security_code=ticker_val).first()
        if stock:
            stockName = stock.fs_name
            fsTicker=stock.fs_ticker
    if stockName:
        securityModel = SecurityMaster
        universeModel = UniversePricesMain
        movingavg_model = MovingAverage
        indexes=['Nifty 50 Index','Nifty Midcap 100 Index','Nifty Smallcap 100 Index']
        symbol='₹'
        flag=0
        companyDescription=CompanyDescriptions
        companyAddressDetails2=CompanyAddressDetails2
        stockRanking=StockRanking
        format="in Cr"
        if UsSecurityMaster.objects.filter(fs_ticker=fsTicker).exists():
            securityModel = UsSecurityMaster
            universeModel = UsUniversePricesMain
            indexes=['S&P 500','Dow Jones Industrial Average','Russell 2000']
            symbol='$'
            flag=1
            movingavg_model = UsMovingAverage
            companyDescription=UsCompanyDescriptions
            companyAddressDetails2=UsCompanyAddressDetails2
            stockRanking=UsStockRanking
            format="M"
        factors = securityModel.objects.filter(fs_name=stockName,fs_ticker=fsTicker).all()
        details = companyAddressDetails2.objects.filter(ticker=fsTicker).all()
        for item in factors:
            for det in details:
                if(flag==1):
                    item.industry = det.ind_l_name
                    item.address = det.regdist+', '+det.regstate
                    item.phone = det.tel1
                    item.website = det.internet
                else:
                    item.chairman = det.chairman
                    item.industry = det.ind_l_name
                    item.founded = det.inc_dt
                    item.address = det.regdist+', '+det.regstate
                    item.phone = det.tel1
                    item.email = det.email
                    item.website = det.internet
        company_description = companyDescription.objects.filter(company=stockName,co_code=fsTicker).values('description')
        if(len(company_description)>0):
            company_description1='<br /> '.join(company_description[0]['description'].splitlines())
        else:
            company_description1=''
        df=StockvsIndexMain(stockName)
        prices = universeModel.objects.filter(company=stockName,factset_ticker=fsTicker).values('date','price').order_by('date')
        prices_df = pd.DataFrame(list(prices))
        if(len(prices_df)>0):
            prices_df['date']=prices_df['date'].astype(str)
            prices_list = prices_df.values.tolist()
            df['Date']=df['Date'].astype(str)
            df=df.dropna().reset_index(drop=True)
            lineColor=['#02BCD4','#28a745','#6610F2','#ffc107','#dc3545']
            output_df=[{"name":"Return","lineWidth": 3,'color':lineColor[0],'data':df[['Date',stockName]].values.tolist()}, 
                {"name":indexes[0],'lineWidth': 2,'color':lineColor[1],'data':df[['Date',indexes[0]]].values.tolist()},
                {"name":indexes[1],"lineWidth": 2,'color':lineColor[2],'data':df[['Date',indexes[1]]].values.tolist()},
                {"name":indexes[2],"lineWidth": 2,'color':lineColor[3],'data':df[['Date',indexes[2]]].values.tolist()}
            ]
        else:
            prices_list=[]
            output_df=[]
        df=pd.DataFrame(list(movingavg_model.objects.filter(company=stockName,factset_ticker=fsTicker).values('date','price','ma20','ma50')))
        if(len(df)>0):
            df.columns=['Date','Price','MA20','MA50']
            df=df.reset_index(drop=True).sort_values(by='Date')
            df=df[['Date','Price','MA20','MA50']]
            df['Date'] = df['Date'].astype(str)
            df['Date'] = df['Date'].str[0:11]
            output_pricevsDMA=[]
            temp_list=[]
            lineColor=['#6610F2','#28a745','#02BCD4','#ffc107','#dc3545','#5A6268','#23272B']
            i=0
            for col in df.columns: 
                if(col!='Date'):
                    pricevsDMA_df=df[['Date',col]].dropna()
                    my_list=pricevsDMA_df.values.tolist()
                    if(col!='Price'):
                        x={"name":col,"lineWidth": 2,'color':lineColor[i],"data":my_list}
                    else:
                        x={"name":"Price","lineWidth": 3,'color':lineColor[i],"data":my_list}
                    output_pricevsDMA.append(x)
                    i=i+1
        else:
            output_pricevsDMA=[]
        stock_ranking=pd.DataFrame(list(stockRanking.objects.filter(fs_name=stockName,fs_ticker=fsTicker).values('nsesymbol','q_sales','q_piotroski_score','q_roe','q_debt_equity','q_fcf_margin','q_beta','g_eps_growth',
        'g_sales_growth','g_sales_qoq','t_price_vs_ema20','t_price_vs_ema50','t_price_vs_ema200','t_rsi','t_ema50_vs_ema200','t_current_price_vs_52_week_high','quality_score',
        'growth_score','technical_score','stock_rank','price','net_change','return_field')))
        stock_ranking= stock_ranking.fillna(0)
        ranking=stock_ranking.values.tolist()

        context = {'symbol':symbol,'stockName':stockNameSymbol,'output_df':output_df,'factors':factors,'index1':indexes[0],'index2':indexes[1],'index3':indexes[2],
                    'prices':prices_list,'companyDescription':company_description,
                    'output_pricevsDMA':output_pricevsDMA,'stocks':factors,'companyDesc':company_description1,'ranking':ranking,'format':format}
        return render(request, 'StockInfo.html',context)
      

@login_required
def createPortfolio(request):
    condition = request.GET.get('mcap', None)
    market = 'all'
    country=request.session['country']
    symbols_formats_investments={'India':['₹','Cr',100000],'US':['$','M',5000]}
    symbol= symbols_formats_investments[country][0]
    format= symbols_formats_investments[country][1]
    defaultInvestment=symbols_formats_investments[country][2]
    result = getDataForCreatingStocksFromCacheBuilder(country)
    if condition is None or condition == 'default':
        market = 'default'
        result = result[result['market_cap'] > 500]
    result.fillna("N/A",inplace=True)
    result['quantity'] = (round(defaultInvestment / result['price'].apply(np.ceil),0)).astype(int)
    result  =result[['company','market_cap','gics','market_cap_category','min_price','price','quantity']]
    portfolioSet = set(list(pd.DataFrame(list(CustomerPortfolioDetails.objects.filter(customer_name=request.user.username,country=request.session['country']).values('portfolio_name').order_by('portfolio_name')))['portfolio_name']))
    context = {'market': market,'format':format,'companies' : result.T.to_dict(), 'username' : request.user.username,'portfolios':portfolioSet,'symbol':symbol,'defaultInvestment':defaultInvestment,'country':request.session['country']}
    return render(request, 'NewPortfolio.html', context)



class CreatePortfolioView(LoginRequiredMixin, SubscriptionValidationMixin, TemplateView):
    template_name = 'NewPortfolio.html'
    def get_context_data(self,**kwargs):
        context = super().get_context_data(**kwargs)
        condition = self.request.GET.get('mcap', None)
        market = 'all'
        country=self.request.session['country']
        symbols_formats_investments={'India':['₹','Cr',100000],'US':['$','M',5000]}
        symbol= symbols_formats_investments[country][0]
        format= symbols_formats_investments[country][1]
        defaultInvestment=symbols_formats_investments[country][2]
        result = getDataForCreatingStocksFromCacheBuilder(country)
        if condition is None or condition == 'default':
            market = 'default'
            result = result[result['market_cap'] > 500]
        result.fillna("N/A",inplace=True)
        result['company'] = result['company']+" ("+result['security_code']+")"
        # result['quantity'] = (round(defaultInvestment / result['price'].apply(np.ceil),0)).astype(int)
        result  =result[['company','market_cap','gics','market_cap_category','min_price','price','factset_ticker']]
        try:
            portfolioSet = set(list(pd.DataFrame(list(CustomerPortfolioDetails.objects.filter(customer_name=self.request.user.username,country=self.request.session['country']).values('portfolio_name').order_by('portfolio_name')))['portfolio_name']))
        except:
            portfolioSet=set()
        context = {'market': market,'format':format,'companies' : result.T.to_dict(), 'username' : self.request.user.username,'portfolios':portfolioSet,'symbol':symbol,'defaultInvestment':defaultInvestment,'country':self.request.session['country']}
        return context


@login_required
def countryPreference(request):
    mcap = request.GET.get('mcap', None)
    context = {}
    country=request.session['country']
    symbols_formats_investments={'India':['₹','Cr',100000],'US':['$','M',5000]}
    symbol= symbols_formats_investments[country][0]
    format= symbols_formats_investments[country][1]
    defaultInvestment=symbols_formats_investments[country][2]
    result = getDataForCreatingStocksFromCacheBuilder(country)
    if mcap == 'default':
        result = result[result['market_cap']>500]
    result['company'] = result['company']+" ("+result['security_code']+")"
    # result['quantity'] = (round(defaultInvestment / result['price'].apply(np.ceil),0)).astype(int)
    result  =result[['company','market_cap','gics','market_cap_category','min_price','price']]
    try:
        portfolioSet = set(list(pd.DataFrame(list(CustomerPortfolioDetails.objects.filter(customer_name=request.user.username,country=request.session['country']).values('portfolio_name').order_by('portfolio_name')))['portfolio_name']))
    except:
        portfolioSet = set()
    context = {'market':'default','format':format,'companies' : result.values.tolist(), 'username' : request.user.username,'portfolios':portfolioSet,'symbol':symbol,'defaultInvestment':defaultInvestment,'country':request.session['country']}
    return render(request, 'NewPortfolio.html', context)
        

class UserCreateView(CreateView):
    def get(self, request):
        user_form = RegistrationForm()
        template_name = 'registration/signup.html'
        context = {'form' : user_form}
        return render(request, template_name, context)
    def post(self, request, *args, **kwargs):
        user_form = RegistrationForm(data = request.POST)
        template_name = 'registration/signup.html'
        if user_form.is_valid():
            user = user_form.save()
            user.save()
            mail_subject = 'Activate your Datax10 account.'
            message = render_to_string('registration/account_activation_email.html', {
                'user': user,
                'domain': get_current_site(request).domain,
                'uid':user.id,
                'token':account_activation_token.make_token(user),
            })
            # 'protocol': 'http https if use', Removing due to to social authentication, will be added at DNS config level
            to_email = user_form.cleaned_data.get('email')
            from_email=settings.EMAIL_HOST_USER
            send_mail(mail_subject, message, from_email, [to_email], fail_silently = True)
            messages.success(request, 'Your account is created. Please confirm email for account activation.')
            context = {'form' : user_form}
            # return render(request, template_name, context)
            return redirect('login')
        else:
            context = {'form' : user_form,'errors':user_form.errors}
            return render(request, template_name, context)


class UserLoginView(LoginView):
    def get(self, request):
        global return_list
        return_list = UserLoginView.randomCaptcha()
        login_form = UserLoginForm()
        template_name='registration/login.html'
        context = {'form' : login_form, 'captcha_qs' :return_list[0]}
        return render(request, template_name, context)
    def post(self, request, *args, **kwargs):
        global return_list  # Making the return_list as global variable
        login_form = UserLoginForm(data = request.POST)  # Creating the UserLoginForm instance
        template_name='registration/login.html'   # Template which is to be rendered
        if login_form.is_valid():
            try:
                captcha_ans =request.POST['captchans']
                if captcha_ans is not None:
                    username = login_form.cleaned_data['username']
                    password = login_form.cleaned_data['password']
                    authuser = authenticate(username=username, password=password)
                    try:
                        user = User.objects.get(username=username)
                        if authuser is not None:
                            if return_list[1] == int(captcha_ans): # Validating the captcha answer
                                user_login(request, authuser)
                                # return redirect('indexpage')
                                return redirect('homepage_new')
                            else: # Invalid captcha answer
                                messages.error(request, 'Incorrect value, Please try again!')
                                return_list = UserLoginView.randomCaptcha()
                                context = {'form' : login_form,'captcha_qs' :return_list[0]}
                                return render(request, template_name, context)
                        else:
                            messages.error(request, 'Invalid Username or Password!')
                            return_list = UserLoginView.randomCaptcha()
                            context = {'form' : login_form,'captcha_qs' :return_list[0]}
                            return render(request, template_name, context)
                    except (User.DoesNotExist): # Handling Exception for user does not exist
                        messages.error(request, "User doesn't exist!")
                        return_list = UserLoginView.randomCaptcha()
                        context = {'form' : login_form,'captcha_qs' :return_list[0]}
                        return render(request, template_name, context)
                else: # Captcha is None
                        messages.error(request, 'Captcha answer is required!')
                        return_list = UserLoginView.randomCaptcha()
                        context = {'form' : login_form,'captcha_qs' :return_list[0]}
                        return render(request, template_name, context)
            except ValueError: # Handling Exception for wrong captcha answer
                messages.error(request, 'Captcha answer is always numeric!')
                return_list = UserLoginView.randomCaptcha()
                context = {'form' : login_form,'captcha_qs' :return_list[0]}
                return render(request, template_name, context)
            except NameError:  # Handling Exception for class variable 'return_list not defined'
                return_list = UserLoginView.randomCaptcha()
                context = {'form' : login_form,'captcha_qs' :return_list[0]}
                return render(request, template_name, context)
        else:
            return_list = UserLoginView.randomCaptcha()
            context = {'form' : login_form,'captcha_qs' :return_list[0]}
            return render(request, template_name, context)

    # This is for captcha while login
    def randomCaptcha():
        ops = {'+':operator.add,
              } # Captcha always contains add and sub operations
        num1 = random.randint(1,9)
        num2 = random.randint(1,9)   
        op = random.choice(list(ops.keys()))
        question = '{} {} {}    ='.format(num1, op, num2)
        answer = ops.get(op)(num1,num2)
        return [question, answer]


def activate(request, uidb, token):
    try:
        # uid = force_text(urlsafe_base64_decode(uidb))
        user = User.objects.get(pk=uidb)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.save()
        # return HttpResponse('Thank you for your email confirmation. Now you can login your account.')
        messages.success(request,'Thank you for your email confirmation. Now you can login your account.')
        return redirect('login')
    else:
        return HttpResponse('Ooops, activation link is invalid!')




@login_required
@csrf_exempt
@SubscriptionValidation
def saveNewPortfolio(request):
    saveStatus = {}
    if request.method == 'POST': # Checking whether the request is POST or not.
        datastr = request.POST.get('portfolio_list')  # taking the newly added stock data as string from the client POST request
        datalist = json.loads(datastr) # De-Serializing the stock data
        portfolio_name = request.POST.get('portfolio_name')  # Taking the portfolio name from the client POST request
        try:
            request.session['country'] = request.session['country']
        except:
            request.session['country'] = 'India'
        if datalist:  # Checking if newly added stock data is exists or not
            try:
                if CustomerPortfolioDetails.objects.filter(customer_name=request.user.username,country=request.session['country'], portfolio_name=portfolio_name).count() == 0: # Checking if the portfolio name already exists or not
                    for data in datalist:   # Iterating the stocks (if more than one stock is adding at a time)
                        if CustomerPortfolioDetails.objects.filter(customer_name=request.user.username,country=request.session['country'], portfolio_name=portfolio_name,company_name=data['companyName']).count() == 0: # To avoid the duplicates in portfolio database
                            CustomerPortfolioDetails(customer_name=request.user.username, portfolio_name=portfolio_name, company_name=data['companyName'],
                            quantity=int(data['stockQuantity']), price=data['lt_price'], sector_name=data['sectorName'],model_portfolio='no', portfolio_type='General',
                            country=request.session['country'], fs_ticker=data['fsTicker']).save()  # Saving the stock
                    CustomerPortfolioLog(customer_name=request.user.username,portfolio_name=portfolio_name,last_action='portfolio_created',created_at=timezone.now(),status='active').save()
                    saveStatus['res_Status'] = True
                    saveStatus['portfolio'] = portfolio_name
                    createCacheForNewPortfolio(request.user.username, request.session['country'], portfolio_name)
                    saveStatus['res_Msg'] = 'Your Portfolio has been saved successfully!'
                    return HttpResponse(json.dumps(saveStatus), content_type='application/json')
                else:  # If the portfolio name already exists, raise error as exception
                    raise PortfolioExistsError('Portfolio name is already taken!')
            # Handling the exceptions
            except PortfolioExistsError as p_error:  
                saveStatus['res_Status'] = False
                saveStatus['portfolio'] = portfolio_name
                saveStatus['res_Msg'] = p_error.msg
            except Exception as e:
                print(e)
                saveStatus['res_Status'] = False
                saveStatus['portfolio'] = portfolio_name
                saveStatus['res_Msg'] = 'Something went wrong, try again!'
        else:
            saveStatus['res_Status'] = False
            saveStatus['portfolio'] = portfolio_name
            saveStatus['res_Msg'] = 'Invalid input, try again!'
    else:
        saveStatus['res_Status'] = False
        saveStatus['res_Msg'] = 'Invalid input, try again!'
    return HttpResponse(json.dumps(saveStatus), content_type='application/json')
           


@login_required
@csrf_exempt
@SubscriptionValidation
def SavePortfolioData(request):
    addStatus = {}
    if request.method == 'POST': # Checking whether the request is POST or not.
        portfolio_name = request.POST.get("portfolioname")
        stockinput = request.POST.get("result")
        datalist = json.loads(stockinput)
        try:
            request.session['country'] = request.session['country']
        except:
            request.session['country'] = 'India'
        for data in datalist:
            try :
                quantity = int(data['stockquantity'])  # Coverting all the quantity fields to integer fields
                if quantity <= 0 or quantity > 2147483647: # Quantity should not be negative value, else raise InvalidQuantityError Exception
                    raise InvalidQuantityError('Please enter a valid quantity and try again!')
                else:
                    count = CustomerPortfolioDetails.objects.filter(customer_name=request.user.username,country=request.session['country'], portfolio_name= portfolio_name, company_name=data['stockname']).count()
                    if count == 0: # If it is new stock, add stock directly
                        CustomerPortfolioDetails(customer_name=request.user.username, portfolio_name= portfolio_name, company_name=data['stockname'] ,quantity=quantity, price=data['stockprice'],sector_name=data['sectorname'],
                        model_portfolio='no', portfolio_type='General',country=request.session['country'], fs_ticker=data['fsTicker']).save()
                        CustomerPortfolioLog.objects.filter(customer_name=request.user.username,
                        portfolio_name=portfolio_name).update(last_action='stock_added',last_modified_at=timezone.now(),status='active')
                    else: # If any stock exist, update the stock with old+new quantity
                        exist_stock_quantity = CustomerPortfolioDetails.objects.filter(customer_name=request.user.username,country=request.session['country'], portfolio_name= portfolio_name, 
                        company_name = data['stockname']).values('quantity')[0]
                        new_stock_quantity = exist_stock_quantity['quantity'] + quantity
                        if (new_stock_quantity > 2147483647): # What if quantity is reaches max value after adding old+new, raise InvalidQuantityError Exception
                            raise InvalidQuantityError('Please enter a valid quantity and try again!')
                        else: # Else update the quantity with old+new quantity
                            CustomerPortfolioDetails.objects.filter(customer_name=request.user.username,country=request.session['country'],company_name = data['stockname'],
                            portfolio_name= portfolio_name,quantity=exist_stock_quantity['quantity']).update(quantity=new_stock_quantity)
                            CustomerPortfolioLog.objects.filter(customer_name=request.user.username,
                            portfolio_name=portfolio_name).update(last_action='stock_added',last_modified_at=timezone.now(),status='active')
            except InvalidQuantityError as q_error:
                addStatus['res_Status'] = False
                addStatus['portfolio'] = portfolio_name
                addStatus['res_Msg'] = q_error.msg
            except Exception as e:
                print(e)
                addStatus['res_Status'] = False
                addStatus['portfolio'] = portfolio_name
                addStatus['res_Msg'] = 'Something went wrong, try again!'
        addStatus['res_Status'] = True
        addStatus['portfolio'] = portfolio_name
        addStatus['res_Msg'] = 'Stock has been added successfully!'
        updatePortfolioDataInCacheBuilder(request.user.username, request.session['country'], portfolio_name, datalist)
    else: # Any request method other than POST, will be handled here
        addStatus['res_Status'] = False
        addStatus['res_Msg'] = 'Something went wrong, try again!'
    return HttpResponse(json.dumps(addStatus), content_type='application/json')

  

@login_required
@csrf_exempt
@SubscriptionValidation
def CustomerTradeNotifications(request):
    saveStatus = {}
    try:
        request.session['country'] = request.session['country']
    except:
        request.session['country'] = 'India'
    if request.method == 'POST': # Checking whether the request is POST or not.
        trades = request.POST.get("tradesignals")
        tradesignals = json.loads(trades)
        for tradesignal in tradesignals:
            try:
                count = CustomerTradeSignals.objects.filter(buy_sell=tradesignal['Buy_Sell'],customer_name=tradesignal['CustomerName'],company_name=tradesignal['CompanyName'],limited_price=tradesignal['LimitPrice'],quantity=tradesignal['Quantity'],expiry_date=tradesignal['IssuedDate']).count()
                if count == 0:
                    CustomerTradeSignals(buy_sell=tradesignal['Buy_Sell'],customer_name=tradesignal['CustomerName'],company_name=tradesignal['CompanyName'],limited_price=tradesignal['LimitPrice'],quantity=tradesignal['Quantity'],expiry_date=tradesignal['IssuedDate']).save()
                    saveStatus['res_Status'] = True
                    saveStatus['res_Msg'] = 'Trades Signals have been notified successfully!'
            except (Exception):
                saveStatus['res_Status'] = False
                saveStatus['res_Msg'] = 'Something went wrong, try again!'
    else: # Any request method other than POST, will be handled here
        saveStatus['res_Status'] = False
        saveStatus['res_Msg'] = 'Something went wrong, try again!'
    return HttpResponse(json.dumps(saveStatus), content_type='application/json')
          
@login_required
@csrf_exempt
@SubscriptionValidation
def deleteCustomerPortfolio(request):
    deleteStatus = {}
    try:
        request.session['country'] = request.session['country']
    except:
        request.session['country'] = 'India'
    if request.method == 'POST':  # Checking whether the request is POST or not.
        portfolio_tobedeleted = str(request.POST.get('portfolioname')) # Converting portfolio value to string, (not needed)
        try:
            countCheck = CustomerPortfolioDetails.objects.filter(customer_name=request.user.username,country=request.session['country'], portfolio_name=portfolio_tobedeleted).count()
            if countCheck:
                CustomerPortfolioDetails.objects.filter(customer_name=request.user.username,country=request.session['country'], portfolio_name=portfolio_tobedeleted).delete()
                CustomerPortfolioLog.objects.filter(customer_name=request.user.username,
                portfolio_name=portfolio_tobedeleted).update(last_action='portfolio_deleted',last_modified_at=timezone.now(),status='inactive')
                CustomerPriceMovementsAlerts.objects.filter(customer_name=request.user, portfolio_name=portfolio_tobedeleted).delete()
                CustomerTechnicalIndicationAlerts.objects.filter(customer_name=request.user, portfolio_name=portfolio_tobedeleted).delete()
                portfolios = CustomerPortfolioDetails.objects.filter(customer_name=request.user.username,country=request.session['country']).values('portfolio_name').distinct().order_by('portfolio_name')
                deleteStatus['status'] = True
                if portfolios:
                    deleteStatus['portfolio'] = portfolios.last()['portfolio_name']
                else:
                    deleteStatus['portfolio'] = ''
                deleteStatus['result'] = "Successfully deleted portfolio!"
                cache.delete(request.user.username+"_"+portfolio_tobedeleted+"_"+request.session['country']+"_universe")
                return HttpResponse(json.dumps(deleteStatus), content_type='application/json')
            else:
                raise NoPortfolioExistsError("Portfolio to be deleted not found, try again!")
        except NoPortfolioExistsError as p_error:
            deleteStatus['status'] = False
            deleteStatus['portfolio'] = portfolio_tobedeleted
            deleteStatus['result']  = p_error.msg
        except (Exception):
            deleteStatus['status'] = False
            deleteStatus['portfolio'] = portfolio_tobedeleted
            deleteStatus['result'] = "Couldn't delete portfolio, try again!"
    else: # Any request method other than POST, will be handled here
        deleteStatus['status'] = False
        deleteStatus['result'] = "Something went wrong, try again!"
    return HttpResponse(json.dumps(deleteStatus), content_type='application/json')


@login_required
@csrf_exempt
@SubscriptionValidation
def deleteSavedStockFromPortfolio(request):
    deleteStatus = {}
    try:
        request.session['country'] = request.session['country']
    except:
        request.session['country'] = 'India'
    if request.method == 'POST': # Checking whether the request is POST or not.
        portfolio_name = request.POST.get('portfolioname') # Taking the portfolio name from the client POST request
        company_name = request.POST.get('companyname') # Taking the company name  from the client POST request
        company_name = re.sub(r'\(.*?\)', '', company_name)
        if (portfolio_name or company_name or quantity):  # Checking if any required input is missed
            try: # What if any Exception is raised 
                CustomerPortfolioDetails.objects.filter(customer_name=request.user.username,country=request.session['country'], portfolio_name=portfolio_name,
                company_name = company_name).delete()  #Deleting the requested company only when it it matched
                if CustomerPortfolioDetails.objects.filter(customer_name=request.user.username,country=request.session['country'], portfolio_name=portfolio_name).count():
                    deleteStatus['portfolio'] = portfolio_name
                    CustomerPortfolioLog.objects.filter(customer_name=request.user.username,
                    portfolio_name=portfolio_name).update(last_action='stock_deleted',last_modified_at=timezone.now(),status='active')
                else:
                    CustomerPortfolioLog.objects.filter(customer_name=request.user.username,
                    portfolio_name=portfolio_name).update(last_action='portfolio_deleted',last_modified_at=timezone.now(),status='inactive')
                    portfolios = CustomerPortfolioDetails.objects.filter(customer_name=request.user.username,country=request.session['country']).values('portfolio_name').distinct().order_by('portfolio_name')
                    deleteStatus['portfolio'] = portfolios.last()['portfolio_name']
                deleteStatus['status'] = True
                deleteStatus['result'] = 'Stock has been deleted successfully!'
            except (ValueError, KeyError, ): # Handling all the possible exceptions
                deleteStatus['status'] = False
                deleteStatus['portfolio'] = portfolio_name
                deleteStatus['result'] = 'Unable to delete stock, try again!'
            except (Exception):  # Handling exceptions other than the mentioned above, with super exception.
                deleteStatus['status'] = False
                deleteStatus['portfolio'] = portfolio_name
                deleteStatus['result'] = 'Unable to delete stock, try again!'
        else:   # What if any input is missed
            deleteStatus['status'] = False
            deleteStatus['portfolio'] = portfolio_name
            deleteStatus['result'] = 'Something went wrong, try again!'
    else: # Any request method other than POST, will be handled here
        deleteStatus['status'] = False
        deleteStatus['result'] = 'Something went wrong, try again!'
    return HttpResponse(json.dumps(deleteStatus), content_type='application/json')


@login_required
@csrf_exempt
@SubscriptionValidation
def updateSavedStockFromPortfolio(request):
    updateStatus = {}
    try:
        request.session['country'] = request.session['country'] 
    except:
        request.session['country'] = 'India'
    if request.method == 'POST':  # Checking whether the request is POST or not.
        quantity = int(request.POST.get('quantity')) # Converting quantity value to integer
        portfolio_name = request.POST.get('portfolioname')
        price = request.POST.get('price')
        company_name = request.POST.get('companyname')
        company_name = re.sub(r'\(.*?\)', '', company_name)
        print(company_name)
        print(quantity, portfolio_name, price)
        try:
            if quantity:  # If quntity doesn't exixt (Rarely) raise InvalidQuantityError Exception
                if quantity <= 0 or quantity >2147483647: # Quantity should not be negative value, else raise InvalidQuantityError Exception
                    raise InvalidQuantityError('Invalid quantity, try again!')
                else:
                    try: # If Block Scope is missed
                        CustomerPortfolioDetails.objects.filter(customer_name=request.user.username,country=request.session['country'],portfolio_name=portfolio_name,
                        company_name=company_name).update(quantity = quantity, price=price)  #Update the stock with new quantity
                        CustomerPortfolioLog.objects.filter(customer_name=request.user.username,
                        portfolio_name=portfolio_name).update(last_action='stock_updated',last_modified_at=timezone.now(),status='active')
                        updateStatus['status'] = True
                        updateStatus['portfolio'] = portfolio_name
                        updateStatus['result'] ='Stock has been updated successfully!'
                    except Exception as e:  # If Block Scope is missed 
                        updateStatus['status'] = False
                        updateStatus['portfolio'] = portfolio_name
                        updateStatus['result'] = "Couldn't update the stock, try again!"
            else:
                raise InvalidQuantityError('Invalid quantity, try again!')
        except InvalidQuantityError as q_error:
            updateStatus['status'] = False
            updateStatus['portfolio'] = portfolio_name
            updateStatus['result']  = q_error.msg
        except (ValueError, KeyError):
            updateStatus['status'] = False
            updateStatus['portfolio'] = portfolio_name
            updateStatus['result'] = "Couldn't update the stock, try again!"
        except Exception as e:
            updateStatus['status'] = False
            updateStatus['portfolio'] = portfolio_name
            updateStatus['result'] = "Couldn't update the stock, try again!"
    else: # Any request method other than POST, will be handled here
        updateStatus['status'] = False
        updateStatus['result'] = "Something went wrong, try again!"
    return HttpResponse(json.dumps(updateStatus), content_type='application/json')
            



class ReadALLCustomerNotificationsView(LoginRequiredMixin,View):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(ReadALLCustomerNotificationsView, self).dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        return HttpResponse(json.dumps({'read':'ERROR'}), content_type='application/json')

    def post(self, request, *args, **kwargs):
        CustomerPriceMovementsAlerts.objects.filter(customer_name=request.user, price_status='Notified',price_read='No').update(price_read='Yes')
        CustomerPriceMovementsAlerts.objects.filter(customer_name=request.user, percent_status='Notified',percent_read='No').update(percent_read='Yes')
        CustomerPriceMovementsAlerts.objects.filter(customer_name=request.user, price52wh_status='Notified',price52wh_read='No').update(price52wh_read='Yes')
        CustomerTechnicalIndicationAlerts.objects.filter(customer_name=request.user,rsi_status='Notified').update(rsi_read='Yes')
        CustomerTechnicalIndicationAlerts.objects.filter(customer_name=request.user,dma_status='Notified').update(dma_read='Yes')
        return HttpResponse(json.dumps({'read':'SUCCESS'}), content_type='application/json')

class CheckALLCustomerNotificationsView(LoginRequiredMixin,View):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(CheckALLCustomerNotificationsView, self).dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        return HttpResponse(json.dumps({'checked':'ERROR'}), content_type='application/json')

    def post(self, request, *args, **kwargs):
        CustomerPriceMovementsAlerts.objects.filter(customer_name=request.user,price_checked='No', price_status='Notified').update(price_checked='Yes')
        CustomerPriceMovementsAlerts.objects.filter(customer_name=request.user,percent_checked='No',percent_status='Notified').update(percent_checked='Yes')
        CustomerPriceMovementsAlerts.objects.filter(customer_name=request.user,price52wh_checked='No',price52wh_status='Notified').update(price52wh_checked='Yes')
        CustomerTechnicalIndicationAlerts.objects.filter(customer_name=request.user,rsi_checked='No',rsi_status='Notified').update(rsi_checked='Yes')
        CustomerTechnicalIndicationAlerts.objects.filter(customer_name=request.user,dma_checked='No',dma_status='Notified').update(dma_checked='Yes')
        return HttpResponse(json.dumps({'checked':'SUCCESS'}), content_type='application/json')

class ReadCustomerNotificationView(LoginRequiredMixin,View):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(ReadCustomerNotificationView, self).dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        return HttpResponse(json.dumps({'read':'ERROR'}), content_type='application/json')

    def post(self, request, *args, **kwargs):
        company = request.POST['company']
        portfolio = request.POST['portfolio']
        alert_type = request.POST['type']
        _id = request.POST['id']
        if alert_type == 'price':
            CustomerPriceMovementsAlerts.objects.filter(id=_id,customer_name=request.user,company_name=company, portfolio_name=portfolio, price_status='Notified',price_read='No').update(price_read='Yes')
        if alert_type == 'percent':
            CustomerPriceMovementsAlerts.objects.filter(id=_id,customer_name=request.user,company_name=company, portfolio_name=portfolio, percent_status='Notified',percent_read='No').update(percent_read='Yes')
        if alert_type == '52wh':
            CustomerPriceMovementsAlerts.objects.filter(id=_id,customer_name=request.user,company_name=company, portfolio_name=portfolio, price52wh_status='Notified',price52wh_read='No').update(price52wh_read='Yes')
        if alert_type == 'rsi':
            CustomerTechnicalIndicationAlerts.objects.filter(id=_id,customer_name=request.user,company_name=company, portfolio_name=portfolio,rsa_status='Notified',rsi_read='No').update(rsi_read='Yes')
        if alert_type == 'dma':
            CustomerTechnicalIndicationAlerts.objects.filter(id=_id,customer_name=request.user,company_name=company, portfolio_name=portfolio,dma_status='Notified',dma_read='No').update(dma_read='Yes')
        return HttpResponse(json.dumps({'read':'SUCCESS'}), content_type='application/json')



@login_required
@SubscriptionValidation
def export_user_portfolio(request):
    try:
        request.session['country'] = request.session['country'] 
    except:
        request.session['country'] = 'India'
    response = HttpResponse(content_type='application/ms-excel')
    response['Content-Disposition'] = 'attachment; filename="Myportfolio.xls"'
    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('Customer Portfolio') 
    row_num = 0
    font_style = xlwt.XFStyle()
    font_style.font.bold = True
    columns = ['Customer Name', 'Portfolio Name', 'Company Name', 'Quantity','Last Traded Price (Rs)' ]
    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)
    font_style = xlwt.XFStyle()
    portfolio = request.GET.get("portfolio",None)
    rows = []
    if portfolio == 'allportfolios':
        rows = CustomerPortfolioDetails.objects.filter(customer_name=request.user.username,country=request.session['country']).all().values_list('customer_name', 'portfolio_name', 'company_name', 'quantity','price').order_by('portfolio_name')
    else:
        rows = CustomerPortfolioDetails.objects.filter(customer_name=request.user.username,country=request.session['country'],portfolio_name=portfolio).all().values_list('customer_name', 'portfolio_name', 'company_name', 'quantity','price').order_by('portfolio_name')
    for row in rows:
        row_num += 1
        for col_num in range(len(row)):
            ws.write(row_num, col_num, row[col_num], font_style)
    wb.save(response)
    return response
            

@login_required
@SubscriptionValidation
def exportStocksExcel(request):
    response = HttpResponse(content_type='application/ms-excel')
    response['Content-Disposition'] = 'attachment; filename="Listed_stocks.xls"'
    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('Stocks Details') 
    row_num = 0
    font_style = xlwt.XFStyle()
    font_style.font.bold = True
    columns = ['Security Code', 'Security ID', 'ISIN ','Company Name', 'Sector Name', 'Market Cap Category' ]
    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)
    font_style = xlwt.XFStyle()
    rows = SecurityMaster.objects.filter(flag='yes').all().values_list('security_code','security_id','isin_no','fs_name','gics','market_cap_category').distinct().order_by('fs_name')
    for row in rows:
        row_num += 1
        for col_num in range(len(row)):
            ws.write(row_num, col_num, row[col_num], font_style)
    wb.save(response)
    return response
#                             ---------------------------   CHARTS VIEWS    -----------------------------------

current_date= UniversePricesMain.objects.all().aggregate(Max('date'))['date__max']

@login_required
@SubscriptionValidation
def getPortfolio(request):
    try:
        request.session['country'] = request.session['country']  
    except:  
        request.session['country'] = 'India'  
    portfolio_name = request.GET.get('portfoliochanged-to', None)
    userPortfolio = pd.DataFrame(list(CustomerPortfolioDetails.objects.filter(customer_name=request.user.username,country=request.session['country']).values('portfolio_name')))
    if len(userPortfolio)>0:       
        if portfolio_name is not None and portfolio_name not in list(userPortfolio['portfolio_name'].unique()):
            portfolio_name=None
    if portfolio_name:
        request.session['portfolio_name'] = portfolio_name 
    else:
        try:
            portfolio_name = request.session['portfolio_name']
        except:
            portfolio_name = None
        if portfolio_name is None:
            portfolios = CustomerPortfolioDetails.objects.filter(customer_name=request.user.username,country=request.session['country']).order_by('created_at').values('portfolio_name').distinct() #take the first portfolio from the database
            portfolioList = list()
            if portfolios:
                for item in portfolios:
                    request.session['portfolio_name'] = item['portfolio_name']
                    if item['portfolio_name'] not in portfolioList:
                        portfolioList.append(item['portfolio_name'])
                request.session['portfoliolist'] = portfolioList
            else:
                portfolios={'India':'72PI India Portfolio','US':'72PI US Portfolio'}
                request.session['portfolio_name'] = portfolios[request.session['country']] 
    portfolio_type='General'
    try:
        portfolioType=CustomerPortfolioDetails.objects.filter(customer_name=request.user.username,country=request.session['country'], portfolio_name=portfolio_name).values('portfolio_type')[0]
        portfolio_type = portfolioType['portfolio_type']
    except:
        portfolio_type='General'
    portfolio_name = request.session['portfolio_name'] 
    return portfolio_name,portfolio_type

@login_required
@SubscriptionValidation
def setPortfolio(request):
    try:
        portfolio_names = request.session['portfoliolist']
        if len(portfolio_names)==0:
            portfolios={'India':'72PI India Portfolio','US':'72PI US Portfolio'}
            portfolio_names=[portfolios[request.session['country']]]
            request.session['portfoliolist']=portfolio_names
        return portfolio_names
    except Exception:
        portfolios = list(CustomerPortfolioDetails.objects.filter(customer_name=request.user.username,country=request.session['country']).values('portfolio_name').distinct())
        if len(portfolios):
            portfolio_names= ([y['portfolio_name'] for y in portfolios])
            request.session['portfolio_name'] = portfolio_names[len(portfolio_names)-1]
            request.session['portfoliolist'] = portfolio_names
        else:
            portfolios={'India':'72PI India Portfolio','US':'72PI US Portfolio'}
            portfolio_names=[portfolios[request.session['country']]]
            request.session['portfolio_name'] = portfolios[request.session['country']]  #save the portfolio to the session
            request.session['portfoliolist'] = portfolio_names
        return portfolio_names



@login_required
@SubscriptionValidation
def getRequiredModelsObjects(request):
    requiredTableObjects={'India':[UniversePricesMain,SecurityMaster,MarketIndex,RiskFreeRate,DailyPositions],
                            'US':[UsUniversePricesMain,UsSecurityMaster,UsMarketIndex,UsRiskFreeRate,UsDailyPositions]}
    country = request.session['country']                          
    return requiredTableObjects[country][0] ,requiredTableObjects[country][1] ,requiredTableObjects[country][2] ,requiredTableObjects[country][3],requiredTableObjects[country][4] 

@login_required
@SubscriptionValidation
def getStockData(request,universe_model,portfolio_name):
    Stock_Data = CustomerPortfolioDetails.objects.filter(customer_name=request.user.username,country=request.session['country'],portfolio_name=portfolio_name).values()
    Stock_Data = pd.DataFrame(list(Stock_Data))
    if len(Stock_Data)==0:
        Stock_Data = ModelPortfolioDetails.objects.filter(model_portfolio_name=portfolio_name,country=request.session['country'],add_on='Yes').values('company_name','fs_ticker','quantity','sector_name')
        Universe_Prices_Main = pd.DataFrame(list(universe_model.objects.filter(date = universe_model.objects.all().aggregate(Max('date'))['date__max']).values()))
        Universe_Prices_Main = Universe_Prices_Main[['company','factset_ticker','price']]
        Universe_Prices_Main.columns=['company_name','fs_ticker','price']
        Stock_Data = pd.DataFrame(list(Stock_Data))
        Stock_Data = pd.merge(Stock_Data,Universe_Prices_Main,on=['company_name','fs_ticker'],how='inner')
    return Stock_Data


@login_required
@SubscriptionValidation
def getDates(request,required_value,universe_model,daily_postions_object,portfolio_type,dynamicStartDate=None):    
    if dynamicStartDate is None:
        if(required_value =='2019'):
            if portfolio_type=='General':
                start_date = universe_model.objects.filter(date__gte='2019-01-01').all().aggregate(Min('date'))['date__min']
                pnl_start_date=start_date
            else:
                start_date = daily_postions_object.objects.filter(customer_name=request.user.username, portfolio_name=portfolio_name).all().aggregate(Min('date'))['date__min']
                pnl_start_date=start_date
        elif(required_value =='YTD'):  
            pnl_start_date = universe_model.objects.filter(date__lt=str(current_date.year)+'-01-01').all().aggregate(Max('date'))['date__max']
            start_date= str(current_date.year)+'-01-01'
        elif(required_value =='MTD'):   
            pnl_start_date = universe_model.objects.filter(date__lt=str(current_date.year)+'-'+str(current_date.month)+'-01').all().aggregate(Max('date'))['date__max']
            start_date=str(current_date.year)+'-'+str(current_date.month)+'-01'
        else:
            pnl_start_date = universe_model.objects.filter(date__lt=str(current_date.year-1)+'-'+str(current_date.month)+'-'+str(current_date.day)).all().aggregate(Max('date'))['date__max']
            start_date= str(current_date.year-1)+'-'+str(current_date.month)+'-'+str(current_date.day)
    else:
        dynamicStartDate = datetime.strptime(dynamicStartDate, '%d-%m-%Y').date()
        pnl_start_date = universe_model.objects.filter(date__lt=str(dynamicStartDate.year)+'-'+str(dynamicStartDate.month)+'-'+str(dynamicStartDate.day)).all().aggregate(Max('date'))['date__max']
        start_date= str(dynamicStartDate)
    return start_date,pnl_start_date

def converttodate(inputdate):
    if isinstance(inputdate,str):
        inputdate=datetime.strptime(inputdate, '%Y-%m-%d').date()
    return inputdate

def getReturns(request,start_date,current_date,pnl_start_date=None):
    try:
        import pickle
        if pnl_start_date is None:
            pnl_start_date=start_date
        start_date=converttodate(start_date)
        current_date=converttodate(current_date)
        pnl_start_date=converttodate(pnl_start_date)
        customerName = request.user.username
        country = request.session['country']
        portfolioName = request.session['portfolio_name']
        universeKey = customerName+"_"+portfolioName+"_"+country+"_universe"
        marketKey = country+"_market"
        if cache.get(universeKey):
            pass
        else:
            getPortfolioWiseDataFromCacheBuilder(customerName)
        daily_returns = pickle.loads(cache.get(universeKey))
        if cache.get(marketKey):
            pass
        else:
            getMarketDataFromCacheBuilder()
        index_returns = pickle.loads(cache.get(marketKey))
        daily_returns = daily_returns[daily_returns['Date'].between(pnl_start_date,current_date)].reset_index()
        index_returns = index_returns[index_returns['Date'].between(start_date,current_date)].reset_index()
        daily_returns = daily_returns[['Company', 'Factset_Ticker', 'Date', 'Price', 'Return']]
        index_returns = index_returns[['Date' , 'Company' , 'Return']]  #Renaming the colmn names
        return daily_returns,index_returns
    except Exception as e:
        print(e)
@login_required
@csrf_exempt
@SubscriptionValidation
def portfolio_returns(request):   
    portfolio_name,portfolio_type = getPortfolio(request)
    symbols={'India':'₹','US':'$'}
    symbol=symbols[request.session['country']]    
    if request.method == 'POST':
        universe_model,security_model,market_model,risk_free_rate_model,daily_postions_object = getRequiredModelsObjects(request)
        current_date= universe_model.objects.all().aggregate(Max('date'))['date__max'] 
        country=request.session['country']    
        selectd_list = request.POST.get('required_value')
        selectd_list = json.loads(selectd_list) 
        required_value=selectd_list[0]                    
        Stock_Data = getStockData(request,universe_model,portfolio_name)
        Stock_Data = Stock_Data[['company_name','fs_ticker','price','quantity','sector_name']]
        Stock_Data.columns = ['Company','Factset_Ticker','Price','Quantity','GICS']
        stocks = set(Stock_Data["Factset_Ticker"])
        #Fetching prices data forr the companies in user selected portfolio
        start_date,pnl_start_date=None,None
        if selectd_list[3]!="":
            start_date=selectd_list[3]
        start_date,pnl_start_date = getDates(request,required_value,universe_model,daily_postions_object,portfolio_type,start_date)
        createdAt = date.today()
        try:
            createdAt = list(pd.DataFrame(list(CustomerPortfolioDetails.objects.filter(customer_name=request.user.username,portfolio_name=portfolio_name).values('created_at')))['created_at'])[0]
        except:
            createdAt = None
        if createdAt is None:      
            createdAt = date.today()    
        createdAt =  createdAt.strftime('%Y-%m-%d')
        gics= security_model.objects.filter(fs_ticker__in=stocks).values('fs_name','fs_ticker','gics','security_code') 
        gics_sec_data=pd.DataFrame(list(gics)) 
        gics_sec_data.columns=['Company','FS_Ticker','GICS','Security_Code'] 
        gics_data = gics_sec_data[['Company','FS_Ticker','GICS']]
        sector_data = gics_data[['GICS']] 
        daily_stocks_returns,index_returns = getReturns(request,start_date,current_date,pnl_start_date)
        risk_free_rates = risk_free_rate_model.objects.filter(date__range=(start_date, current_date)).order_by('date').values('date','risk_free_rate')  #retrieving risk free data
        risk_free_data = pd.DataFrame(list(risk_free_rates))                
        risk_free_data = risk_free_data[['date', 'risk_free_rate']]
        risk_free_data.columns = ['Date', 'Risk Free Rate']
        cumulative_return,daily_returns,df=CalcsMain(country,start_date,pnl_start_date,current_date, request.user.username,Stock_Data,daily_stocks_returns,index_returns,portfolio_name,risk_free_data,required_value)
        df.fillna(0,inplace=True)
        table_data = df.values.tolist()
        if(len(cumulative_return)>0):
            cumulative_return['Trade_Dt'] = cumulative_return['Trade_Dt'].dt.date.astype(str).str[0:11]
            cumulative_return.rename(columns = {'PNL':'Portfolio'}, inplace = True)
            output=pd.DataFrame(columns=['name','lineWidth','color','data'])
            final_dict = {'names':list(cumulative_return.columns)[1:],'lineWidth':[4,2,2,2,2],
                            'color':['#6610F2','#28a745','#02BCD4','#ffc107','#dc3545']}
            for i in range(0,5):
                output.loc[i]=[final_dict['names'][i],final_dict['lineWidth'][i],final_dict['color'][i],cumulative_return[['Trade_Dt',final_dict['names'][i]]].values.tolist()]
            output_cumulative_return=output.to_dict(orient='records')
            msg_df=cumulative_return.tail(1).reset_index(drop=True)
            msg_df=msg_df.drop(['Trade_Dt'],axis=1)
            msg_df_columns=msg_df.columns.tolist()
            msg_df_values=msg_df.values.tolist()
            required_value_texts = {'2019':' 2019 ','YTD':' Year To Date (YTD) ','MTD':' Month To Date (MTD) ','QTD':' Quarter To Date (QTD)','1Y':' One Year (1Y) '}
            if required_value=='Default':
                required_value_text = datetime.strptime(start_date, '%Y-%m-%d').strftime('%d-%b-%Y')
            else:
                required_value_text=required_value_texts[required_value]
            notes_equal="Since "+required_value_text+", Your Portolio has given a cumulative return of <b>"+str(round(msg_df_values[0][0]*100,2))+"%.</b> In the same period "+msg_df_columns[1]+" gave a return of <b>"+str(round(msg_df_values[0][1]*100,2))+"%, </b>"+msg_df_columns[3]+" gave a return of <b>"+str(round(msg_df_values[0][3]*100,2))+"% </b> and "+msg_df_columns[2]+" gave a return of <b>"+str(round(msg_df_values[0][2]*100,2))+"%.</b>" 
        else:
            notes_equal,table_data,output_cumulative_return='',[],[]

        stock_df,sector_wise = New_CurrentPositionsMain(start_date,current_date,daily_stocks_returns,Stock_Data,gics_data,sector_data,required_value) 
        sec_code_data = gics_sec_data[['FS_Ticker','Security_Code']]
        sec_code_data.columns = ['Factset_Ticker','Security_Code']
        stock_df = pd.merge(stock_df,sec_code_data,on='Factset_Ticker',how='inner')
        stock_df['Stock'] = stock_df['Stock'] + ' (' + stock_df['Security_Code'] + ')'
        filter_columns={'Stock':'Stock','Sector':'GICS'}
        data_columns={'Stock':stock_df,'Sector':sector_wise}
        filter_column = filter_columns[selectd_list[1]]
        df = data_columns[selectd_list[1]]
        if(len(df)>0):
            total_exposure=df['Current Exposure'].sum()
            df['Weight']=df['Current Exposure'].apply(lambda x:((x/total_exposure)*100))
            if(selectd_list[2]=='Investment'):
                df=df.sort_values(by=['Current Exposure'], ascending=False).reset_index(drop=True)
            elif(selectd_list[2]=='Actual Return(%)'):
                df=df.sort_values(by=[selectd_list[0]+'_Return'], ascending=False).reset_index(drop=True)
            else:
                df=df.sort_values(by=[selectd_list[0]+'_PNL'], ascending=False).reset_index(drop=True)
            categories=df[filter_column].values.tolist()
            exposure_list=[]
            pnl_list=[]
            pnl_return_list=[]
            PNL_df=df[[filter_column,selectd_list[0]+'_PNL']]
            PNL_return_df=df[[filter_column,selectd_list[0]+'_Return']]
            exposure_df=df[[filter_column,'Current Exposure','Weight']]
            exposure_df.columns=['name','y','exp']
            exposure_df['color'] = '#E49801'
            exposure_list=exposure_df.to_dict(orient='records')
            PNL_df['color'] = np.select([PNL_df[selectd_list[0]+'_PNL']>=0],['#5B47FB'],default='#B2B2B2')
            PNL_df.columns=['name','y','color']
            pnl_list=PNL_df.to_dict(orient='records')
            PNL_return_df['color'] = np.select([PNL_return_df[selectd_list[0]+'_Return']>=0],['#5B47FB'],default='#B2B2B2')
            PNL_return_df.columns=['name','y','color']
            PNL_return_df['y']= PNL_return_df['y'] * 100
            pnl_return_list=PNL_return_df.to_dict(orient='records')
            max_exp=df['Current Exposure'].idxmax()
            max_exposure=df.iloc[max_exp]['Current Exposure']
            max_exposure_return=df.iloc[max_exp][selectd_list[0]+'_Return']
            company=df.iloc[max_exp][filter_column]
            sql_conn=pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server}; SERVER='+SERVER+'; PORT='+PORT+'; DATABASE='+DATABASE+';UID='+UID+';PWD='+PWD+';')
            selected_list = request.POST.get('required_value2')
            selected_list = json.loads(selected_list) 
            
            if request.session['country']=="US":
                securityCode="Ticker"
                daily_stock_returns = pd.read_sql("select  * from US_Daily_Stock_Returns",sql_conn)
            else:
                daily_stock_returns = pd.read_sql("select  * from Daily_Stock_Returns",sql_conn)
                securityCode="NSE_Symbol"
            sql_conn.close()
            daily_stock_returns= (daily_stock_returns[daily_stock_returns["FS_Ticker"].isin(stocks)]).fillna(0) 
            daily_stock_returns  = daily_stock_returns[[securityCode,selected_list[0],selected_list[3],selected_list[4],selected_list[2]]]
            daily_stock_returns[selected_list[0]]=  daily_stock_returns[selected_list[0]]*100
          
            data = daily_stock_returns.groupby([selected_list[3],selected_list[4],securityCode])[selected_list[2],selected_list[0]].sum().reset_index()
            gics_sub_group = daily_stock_returns.groupby([selected_list[3],selected_list[4]])[selected_list[2],selected_list[0]].sum().reset_index()
            gics_main_group  = daily_stock_returns.groupby([selected_list[3]])[selected_list[2],selected_list[0]].sum().reset_index()
            Final_output = data.copy()
            
            gics_sub_group.columns=[selected_list[3], selected_list[4], 'SUB_GICS_size', 'SUB_GICS_FrequencyPercentChange']
            gics_main_group.columns=[selected_list[3], 'GICS_size', 'GICS_FrequencyPercentChange']
            Final_output = Final_output.merge(gics_sub_group,on=[selected_list[3],selected_list[4]],how='inner')
            Final_output = Final_output.merge(gics_main_group,on=[selected_list[3]],how='inner')
            final_dict_list=[]
            gics = Final_output[selected_list[3]].unique().tolist()
            gics_list = []
            mcap=0
            oneLayer=[]
            colorRange=selected_list[1]
            twoLayer=[]
            subIndustry=[]
            for main_gics in gics:
                i=0
                gics_wise = Final_output[Final_output[selected_list[3]]==main_gics]
                gics_wise.reset_index(drop=True,inplace=True)
                sub_gics = gics_wise[selected_list[4]].unique().tolist()
                sub_gics_list=[]
                twoLayerList=[]
                subIndustryList=[]
                for each_gics in sub_gics:
                    j=0
                    sub_gics_wise = gics_wise[gics_wise[selected_list[4]]==each_gics]
                    sub_gics_wise.reset_index(drop=True,inplace=True)
                    sub_gics_companies_list=[]

                    for k in range(0,len(sub_gics_wise)):
                        if sub_gics_wise.loc[k,selected_list[0]]>=colorRange[6]:
                            color='35764E'
                        elif sub_gics_wise.loc[k,selected_list[0]]>=colorRange[5]:
                            color='2F9E4F'
                        elif sub_gics_wise.loc[k,selected_list[0]]>=colorRange[4]:
                            color='30CC5A'
                        elif sub_gics_wise.loc[k,selected_list[0]]>=colorRange[3]:
                            color='414554'
                        elif sub_gics_wise.loc[k,selected_list[0]]>=colorRange[2]:
                            color='F63538'
                        elif sub_gics_wise.loc[k,selected_list[0]]>=colorRange[1]:
                            color='BF4045'
                        else:
                            color='8B444E'
                        if(sub_gics_wise.loc[k,selected_list[0]]>=0):
                            sub_gics_wise_dict={'label':sub_gics_wise.loc[k,securityCode]+"<br/> +"+str(round(sub_gics_wise.loc[k,selected_list[0]],2))+"%",'value':sub_gics_wise.loc[k,selected_list[2]],'svalue':round(sub_gics_wise.loc[k,selected_list[0]],2),'fillcolor':color}
                        else:
                            sub_gics_wise_dict={'label':sub_gics_wise.loc[k,securityCode]+"<br/>"+str(round(sub_gics_wise.loc[k,selected_list[0]],2))+"%",'value':sub_gics_wise.loc[k,selected_list[2]],'svalue':round(sub_gics_wise.loc[k,selected_list[0]],2),'fillcolor':color}
                    
                        oneLayer.append(sub_gics_wise_dict)
                        twoLayerList.append(sub_gics_wise_dict)
                        subIndustryList.append(sub_gics_wise_dict)
                        sub_gics_companies_list.append(sub_gics_wise_dict)
                        mcap=mcap+sub_gics_wise.loc[k,selected_list[2]]
            
                        if sub_gics_wise.loc[j,selected_list[0]]>=colorRange[6]:
                            color='35764E'
                        elif sub_gics_wise.loc[j,selected_list[0]]>=colorRange[5]:
                            color='2F9E4F'
                        elif sub_gics_wise.loc[j,selected_list[0]]>=colorRange[4]:
                            color='30CC5A'
                        elif sub_gics_wise.loc[j,selected_list[0]]>=colorRange[3]:
                            color='414554'
                        elif sub_gics_wise.loc[j,selected_list[0]]>=colorRange[2]:
                            color='F63538'
                        elif sub_gics_wise.loc[j,selected_list[0]]>=colorRange[1]:
                            color='BF4045'
                        else:
                            color='8B444E'
                
                    sub_gics_dict = { "fillcolor": color,'label':sub_gics_wise.loc[j,selected_list[4]],'value':sub_gics_wise.loc[j,'SUB_GICS_size'],'svalue':sub_gics_wise.loc[j,'SUB_GICS_FrequencyPercentChange']/len(sub_gics_wise),'data':sub_gics_companies_list}
                    sub_gics_list.append(sub_gics_dict)
                    subIndustry.append(sub_gics_dict)
                
                    j=j+1

                gics_dict = { "fillcolor": "20261b",'label':gics_wise.loc[i,selected_list[3]],'value':gics_wise.loc[i,'GICS_size'],'svalue':gics_wise.loc[i,'GICS_FrequencyPercentChange'],'data':sub_gics_list}
            
                gics_list.append(gics_dict)
                gics_dict_twolayer = { "fillcolor": "20261b",'label':gics_wise.loc[i,selected_list[3]],'value':gics_wise.loc[i,'GICS_size'],'svalue':gics_wise.loc[i,'GICS_FrequencyPercentChange'],'data':twoLayerList}
                twoLayer.append(gics_dict_twolayer)
        
                i=i+1
            if(selected_list[5]=="0"):
                gics_list=oneLayer
            elif(selected_list[5]=="1" or selected_list[5]=="2" or selected_list[5]=="3"):
                gics_list=twoLayer
            elif  selected_list[5]=="4":
                gics_list=subIndustry

        
            context={'createdAt':createdAt,'notes_equal':notes_equal,'table_data':table_data,'output_cumulative_return':output_cumulative_return,'symbol':symbol ,
            'categories':categories,'exposure':exposure_list,'Pnl':pnl_list,'Return':pnl_return_list,'max_exp':max_exposure,'max_exposure':max_exposure_return,'company':company,'gics_list':gics_list,'mcap':mcap}
            return HttpResponse(json.dumps(context), content_type='application/json')
        else:
            context={'createdAt':"",'notes_equal':notes_equal,'table_data':table_data,'output_cumulative_return':output_cumulative_return,'symbol':symbol ,
            'categories':[],'exposure':[],'Pnl':[],'Return':[],'max_exp':0,'max_exposure':0,'company':'','gics_list':[],'mcap':1}
            return HttpResponse(json.dumps(context), content_type='application/json')
    portfolio_names=setPortfolio(request)
    context={'symbol':symbol,'portfolio_names':request.session['portfoliolist'],'current_portfolio':request.session['portfolio_name']}
    return render(request, 'Portfolio_Returns.html',context)



@login_required
@csrf_exempt
@SubscriptionValidation
def monthlyPerformance(request):#monthly performance dashboard
    portfolio_name,portfolio_type = getPortfolio(request)
    universe_model,security_model,market_model,risk_free_rate_model,daily_postions_object = getRequiredModelsObjects(request)
    current_date= universe_model.objects.all().aggregate(Max('date'))['date__max'] 
    country=request.session['country']    
    Stock_Data = getStockData(request,universe_model,portfolio_name)
    Stock_Data=Stock_Data[['company_name','fs_ticker','price','quantity']]
    Stock_Data.columns=['Company','Factset_Ticker','Price','Quantity']
    stocks = list(Stock_Data['Factset_Ticker'])
    country=request.session['country']
    benchmarks={'India':['PNL','Nifty 50 Index','Nifty Smallcap 100 Index','Nifty Midcap 100 Index'],
                'US':['PNL','S&P 500','Russell 2000','Dow Jones Industrial Average']}
    benchmark_indices = benchmarks[country]
    start_date, end_date = date(2019,1,1), current_date
    stock_daily_returns,index_returns=getReturns(request,start_date,end_date)    
    df, df_temp = MonthlyCumulativeReturnsMain(Stock_Data, portfolio_name,country,stock_daily_returns,index_returns)
    if(len(df)>0):
        df = df.sort_values(by=['Year'], ascending=False)
        df = df.fillna(0)
        df["Month"] = pd.to_datetime(df.Month, format='%b', errors='coerce').dt.month
        df = df.sort_values(['Year',"Month"])
        df['Month'] = df['Month'].apply(lambda x: calendar.month_abbr[x])
        listofvalues = []
        def monthlyPerformance(columname):
            output = []
            for i in range(0,len(df)):
                value = max(df['PNL'][i]*100,df[columname][i]*100)
                if(value == df['PNL'][i]*100):
                    up_dumbbell = '#5B47FB'
                    down_dumbbell = '#B2B2B2'
                else:
                    up_dumbbell = '#B2B2B2'
                    down_dumbbell = '#5B47FB'
                x = {'Portfolio':df['PNL'][i],'other':df[columname][i],"name":str(df["Month"][i])+'-'+(str(df['Year'][i]))[2:4],'low':min(df['PNL'] [i]*100,df[columname][i]*100),'high':max(df['PNL'][i]*100,df[columname][i]*100),'lowColor':down_dumbbell,'connectorColor':up_dumbbell,'connectorWidth':4,'marker':{'fillColor':up_dumbbell,'radius':5}}
                output.append(x)
            listofvalues.append(output)
        monthlyPerformance(benchmark_indices[1])
        monthlyPerformance(benchmark_indices[2])
        monthlyPerformance(benchmark_indices[3])
        table_data = []
        table_data.append(additional_calcs_monthly_returns(df,benchmark_indices[1]).values.tolist())
        table_data.append(additional_calcs_monthly_returns(df,benchmark_indices[2]).values.tolist())
        table_data.append(additional_calcs_monthly_returns(df,benchmark_indices[3]).values.tolist())
    else:
        listofvalues,table_data=[],[]
    #Stock Performance View
    performanceTables={'India':Performance,'US':UsPerformance}
    perfromanceObject = performanceTables[country]
    final_results=pd.DataFrame(list(perfromanceObject.objects.filter(factset_ticker__in=stocks).values()))
    final_results.fillna(0,inplace=True)
    final_results.columns=['Company','2019toDate','YTD','MTD','1Y','FS_Ticker','s_no']

    gics= security_model.objects.filter(fs_ticker__in=stocks).values('fs_ticker','security_code') 
    gics_sec_data=pd.DataFrame(list(gics)) 
    gics_sec_data.columns=['FS_Ticker','Security_Code'] 
    final_results = pd.merge(final_results,gics_sec_data,on='FS_Ticker',how='inner')
    final_results['Company'] = final_results['Company'] + ' (' + final_results['Security_Code'] + ')'
    final_results=final_results[['Company','MTD','YTD','1Y','2019toDate']]
    final_list=[]
    final_list=final_results.values.tolist()
    header_list=final_results.columns.tolist()
    portfolio_names=setPortfolio(request)
    context = { 'portfolio_name':request.session['portfolio_name'],'listofvalues':listofvalues,'table_data':table_data,
                'output':final_list,'header_list':header_list,
                'portfolio_names':request.session['portfoliolist'],'current_portfolio':request.session['portfolio_name'],'country':request.session['country']}
    return render(request, 'MonthlyPerformance.html',context)


            
@login_required
@csrf_exempt
@SubscriptionValidation
#investment amount by sector market cap
def investment_amount_sector_mktcap(request):  #live portfolio pending
    portfolio_name,portfolio_type=getPortfolio(request)
    country=request.session['country'] 
    universe_model,security_model,market_model,risk_free_rate_model,daily_postions_object = getRequiredModelsObjects(request)
    symbols={'India':'₹','US':'$'}
    symbol=symbols[country]
    stocksdata = getStockData(request,universe_model,portfolio_name)
    stocksdata=stocksdata[['company_name','fs_ticker','quantity']]
    stocksdata.columns =['Company','Factset_Ticker','Quantity']
    stocks = list(stocksdata['Factset_Ticker'])        
    universe_objs = universe_model.objects.filter(factset_ticker__in=stocks, date=(universe_model.objects.aggregate(Max('date'))['date__max'])).values('company','factset_ticker','price')
    prices_data = pd.DataFrame(list(universe_objs))
    prices_data.columns = ['Company','Factset_Ticker','Price']    
    security_objs = security_model.objects.filter(fs_ticker__in=stocks).values('fs_name','fs_ticker','security_code','gics','market_cap_category')
    gics_data = pd.DataFrame(list(security_objs))        
    top3_stocks_by_exposure,stock_wise_exposure,gics_wise_exposure,top_sector_by_exposure,market_cap_wise_exposure = exposureDashboard(request.user.username,request.session['country'],stocksdata,prices_data,gics_data)
    if(len(stock_wise_exposure)>0):
        data_stock_wise=stock_wise_exposure['Exposure%'].tolist()
        stocks=stock_wise_exposure['Company'].tolist()
        count = 0
        output,output2 = [],[]
        for i in range (0,len(gics_wise_exposure)):      
            x={"name":gics_wise_exposure['GICS'][i]+' ('+str(int(round(gics_wise_exposure['Exposure%'][i])))+'%)',"y":gics_wise_exposure['Exposure%'][i],"Exposure":gics_wise_exposure['Exposure'][i],'colorValue':count,"new_name":gics_wise_exposure['GICS'][i]}
            output.append(x)
        for i in range (0,len(market_cap_wise_exposure)):
            val={"name":market_cap_wise_exposure['Market Cap Category'][i]+' ('+str(int(round(market_cap_wise_exposure['Exposure%'][i])))+'%)',"y":market_cap_wise_exposure['Exposure%'][i],"Exposure":market_cap_wise_exposure['Exposure'][i],'colorValue':count,"new_name":market_cap_wise_exposure['Market Cap Category'][i]}        
            output2.append(val)
        top=top_sector_by_exposure[['GICS','Exposure%']].values.tolist()
        top3=top3_stocks_by_exposure.values.tolist()
        total_3=round(top3_stocks_by_exposure['Exposure%'].sum(),1)
    else:
        top=top3data_stock_wise=stocks=output=output2=[]
        total_3=0
    portfolio_names=setPortfolio(request)
    context={'portfolioName':portfolio_name,'top':top,'top3':top3,'data_stock_wise':data_stock_wise,'stocks':stocks,'output':output,'output2':output2,
    'total_3':total_3,'portfolio_names':request.session['portfoliolist'],'current_portfolio':request.session['portfolio_name'],'symbol':symbol}
    return render(request, 'Investment_amount_sector_mktcap.html',context)
            

@login_required
@SubscriptionValidation
def analystTargetPrice(request):#analyst target price
    s1 = timezone.now()
    portfolio_name,portfolio_type=getPortfolio(request)
    country=request.session['country']
    requiredTableObjects={'India':[UniversePricesMain,MarketIndex,TargetPrices,SecurityMaster],
                    'US':[UsUniversePricesMain,UsMarketIndex,UsTargetPrices,UsSecurityMaster]}
    symbols={'India':'₹','US':'$'}
    symbol=symbols[country]
    universe_model = requiredTableObjects[country][0]
    market_model = requiredTableObjects[country][1]
    target_prices_model = requiredTableObjects[country][2]
    security_model = requiredTableObjects[country][3]
    current_date= universe_model.objects.all().aggregate(Max('date'))['date__max'] 
    
    Stock_Data = getStockData(request,universe_model,portfolio_name)
    Stock_Data=Stock_Data[['company_name','fs_ticker','quantity']]
    Stock_Data.columns =['Company','Factset_Ticker','Quantity']

    stock = list(Stock_Data['Factset_Ticker'])
    global output
    output=[]
    universe_prices = universe_model.objects.filter(factset_ticker__in=stock, date=(universe_model.objects.filter(date__lte=str(current_date)).aggregate(Max('date'))['date__max'])).order_by('company').values('company','factset_ticker', 'price')
    targets = target_prices_model.objects.filter(ticker__in=stock).values('company','ticker','current_price','avg_upside','avg_downside','number_52_week_high','number_52_week_low')        
    target_data = pd.DataFrame(list(targets))
    sec_code_data = pd.DataFrame(list(security_model.objects.filter(fs_ticker__in = stock).values("fs_ticker",'security_code')))
    sec_code_data.columns=['Factset_Ticker','Security_Code']    
    df=analystTargetPricesData(current_date,request.user.username,portfolio_name,portfolio_type,request.session['country'],Stock_Data,universe_prices,target_data)
    if len(df)==0:
        chart_data,max_value,symbol=[],0,""
        context={'data':[],'output':[],'output2':[],'max_value':0,
        'max_up_return':0,'max_down_return':0,'categories':[],'max_value2':0,
        'min_value2':0,'plot_line':[],'symbol':symbol,'portfolio_names':request.session['portfoliolist'],'current_portfolio':request.session['portfolio_name']}
    else:
        df = pd.merge(df,sec_code_data,on='Factset_Ticker',how='inner')
        df['Company'] = df['Company'] + " (" + df['Security_Code'] + ")"
        del df['Security_Code']
        df.columns=['Ticker Und','Factset_Ticker',  'Current Price','Weight', 'Upside Px','Downside Px','Week High 52','Week Low 52','Up Return', 'Down Return', 'Up/Down Ratio']
        df=df[['Ticker Und',  'Current Price','Weight', 'Upside Px','Downside Px','Up Return', 'Down Return', 'Up/Down Ratio','Week High 52','Week Low 52']]
        df=df.sort_values(by=['Weight'],ascending=False).reset_index(drop=True)
        chart_data = df[['Ticker Und',  'Current Price','Weight', 'Upside Px','Downside Px','Up/Down Ratio']].to_dict(orient='records')
        chart_data = json.dumps(chart_data, indent=2)
        max_value=int(np.nanmax(df[['Up Return', 'Down Return']].values.tolist())*100)+10
        max_up_return=df.loc[df['Up Return']==np.nanmax(df[['Up Return']]),['Ticker Und','Up Return']].values.tolist()
        max_down_return=df.loc[df['Down Return']==np.nanmin(df[['Down Return']]),['Ticker Und','Down Return']].values.tolist()

        def appendToOutput(up,down):
            output.append([
                {'weight':df['Weight'][i]*100,'name':"Up Return","data":[up],'color':'#5B47FB',
                "dataLabels":  {"align": 'right',"padding":-40, "style": {"fontWeight": 'normal',"textOutline": "false" ,"color":'black'}}},
                
                {'weight':df['Weight'][i]*100,'name':"Down Return","data":[down],'color':'#CBCBCB',
                "dataLabels": {  "align": 'left',"padding":-40,  "style": {"fontWeight": 'normal',"textOutline": "false" ,"color":'black'
                }}}])
        for i in range(0,len(df)):
            if(pd.isnull(df['Up Return'][i]) and pd.isnull(df['Down Return'][i])):
                appendToOutput("","")               
            elif (pd.isnull(df['Up Return'][i]) or pd.isnull(df['Down Return'][i])):
                up_downs={}
                if pd.isnull(df['Up Return'][i]):
                    appendToOutput('',df['Down Return'][i]*100)               
                else:
                    appendToOutput(df['Up Return'][i]*100,"")               
            else:
                appendToOutput(df['Up Return'][i]*100,df['Down Return'][i]*100)
        output=sorted(output, key=lambda value: value[0]['weight'],reverse=True)
        graph_52wk = df[['Current Price','Weight','Week High 52','Week Low 52']]
        graph_52wk.columns =['Current Price','weight','Week High 52','Week Low 52']
        graph_52wk['weight'] = graph_52wk['weight'] * 100
        graph_52wk['Week High 52']= graph_52wk['Week High 52'].round(1)
        graph_52wk['Week Low 52']= graph_52wk['Week Low 52'].round(1)        
        graph_52wk=graph_52wk.sort_values(by='weight',ascending=False).reset_index(drop=True)
        categories=graph_52wk[['Week Low 52']].values.tolist()
        plot_line=graph_52wk[['Current Price']].values.tolist()
        graph_52wk = graph_52wk[['weight','Week High 52']]
        graph_52wk.columns=['weight','y']
        output2=list(map(lambda element:[element], graph_52wk.to_dict(orient="records"))) 
        max_value2=float(np.nanmax(df[['Week High 52']].values.tolist()))
        min_value2=float(np.nanmax(df[['Week Low 52']].values.tolist()))
        portfolio_names=setPortfolio(request)
        context={'data':chart_data,'output':output,'output2':output2,'max_value':max_value,
        'max_up_return':max_up_return,'max_down_return':max_down_return,'categories':categories,'max_value2':max_value2,
        'min_value2':min_value2,'plot_line':plot_line,'symbol':symbol,'portfolio_names':portfolio_names,'current_portfolio':request.session['portfolio_name'] }
    print(timezone.now() - s1)
    return render(request, 'Analyst_Target_Price.html',context)
            
@login_required
@csrf_exempt
@SubscriptionValidation
def risk_overview(request):   
    import datetime
    portfolio_name,portfolio_type=getPortfolio(request)
    country=request.session['country']
    symbols={'India':'₹','US':'$'}
    symbol=symbols[country]
    universe_model,security_model,market_model,risk_free_rate_model,daily_postions_object = getRequiredModelsObjects(request)
    betaObjects={'India':StocksDailyBeta,'US':UsStocksDailyBeta}
    requiredTableObjects={'India':[ExponentialMovingAverage,VolumeDataNew,MovingAverage,TargetPrices,'Universe_Prices_Main'],
                            'US':[UsExponentialMovingAverage,UsVolumeDataNew,UsMovingAverage,UsTargetPrices,'Us_Universe_Prices_Main']}
    exponentialModel=requiredTableObjects[country][0]
    volumeDataModel=requiredTableObjects[country][1]
    movingAverageModel=requiredTableObjects[country][2]
    targetPricesModel=requiredTableObjects[country][3]
    pricesTable= requiredTableObjects[country][4]
    benchmarks={'India':'Nifty 50 Index','US':'S&P 500'}
    index_ticker=benchmarks[country]
    current_date= universe_model.objects.all().aggregate(Max('date'))['date__max']
    Stock_Data = getStockData(request,universe_model,portfolio_name)
    Stock_Data=Stock_Data[['company_name','fs_ticker','quantity']]
    Stock_Data.columns =['Company','Factset_Ticker','Quantity']
    stocks = set(Stock_Data["Factset_Ticker"])    
    sec_code_data = pd.DataFrame(list(security_model.objects.filter(fs_ticker__in=stocks).values("fs_ticker","security_code")))
    sec_code_data.columns =['Factset_Ticker','Security_Code']
    if request.method == 'POST':
        period=request.POST['required_value']
        start_date,pnl_start_date=getDates(request,period,universe_model,daily_postions_object,portfolio_type)        
        daily_stocks_returns,index_returns = getReturns(request,start_date,current_date,pnl_start_date)
        stocksDailyBetaModel = betaObjects[country]
        beta_data=pd.DataFrame(list(stocksDailyBetaModel.objects.filter(factset_ticker__in=stocks,date=(stocksDailyBetaModel.objects.all().aggregate(Max('date'))['date__max'])).values('date','factset_ticker','company','beta_1y')))
        beta_data=beta_data[['company','factset_ticker','beta_1y']]
        portfolio_df,beta,Portfolio_VaR_Component,Individual_VaR=portfolio_CalcsMain(start_date, pnl_start_date,current_date, request.user.username ,portfolio_name,Stock_Data,daily_stocks_returns,index_returns,country,period)                        
        portfolio_df['beta']=beta
        portfolio_df['var']=Portfolio_VaR_Component['VaR$']
        portfolio_df['var%']=Portfolio_VaR_Component['VaR%']
        Individual_VaR.columns=["Date","Company",'Factset_Ticker','VaR$',"Net$","VaR%"]
        output_master = stock_beta(start_date, pnl_start_date, current_date, request.user.username,request.session['portfolio_name'] ,Stock_Data,daily_stocks_returns,index_returns,beta_data,country,period)
        output_master=pd.merge(output_master,Individual_VaR[['Company','Factset_Ticker','VaR$',"VaR%"]],on=["Company","Factset_Ticker"],how="inner")
        stockwise_df=output_master[["Company","Factset_Ticker",'Annualized Return', 'Annualized Volatility', 'Beta','VaR$',"VaR%"]]        
        stockwise_df = pd.merge(stockwise_df,sec_code_data,on='Factset_Ticker',how='inner')
        stockwise_df['Company'] = stockwise_df['Company'] + " (" + stockwise_df['Security_Code'] + ")"
        del stockwise_df['Security_Code']
        del stockwise_df['Factset_Ticker']
        beta_df=(stockwise_df.sort_values(by='Beta',ascending=False)).reset_index(drop=True)
        beta_df.dropna(inplace=True)
        vol_df=(stockwise_df.sort_values(by='Annualized Volatility',ascending=False)).reset_index(drop=True)
        vol_df.dropna(inplace=True)
        var_df=(stockwise_df.sort_values(by='VaR$',ascending=False)).reset_index(drop=True)
        var_df.dropna(inplace=True)
        max_beta_list=beta_df[['Company','Beta']].head(1).values.tolist()
        max_vol_list=vol_df[['Company','Annualized Volatility']].head(1).values.tolist()
        max_var_list=var_df[['Company','VaR$']].head(1).values.tolist()
        min_beta_list=beta_df[['Company','Beta']].tail(1).values.tolist()
        min_vol_list=vol_df[['Company','Annualized Volatility']].tail(1).values.tolist()
        min_var_list=var_df[['Company','VaR$']].tail(1).values.tolist()
        stockwise_df=stockwise_df.fillna(123456789)
        portfolio_df=portfolio_df.fillna(123456789)
        portfolio_tabledata=portfolio_df.values.tolist()
        stockwise_table=stockwise_df.values.tolist()
        context={'symbol':symbol ,'portfolio_table':portfolio_tabledata,'stockwise_table':stockwise_table,
        'max_beta_list':max_beta_list,'max_vol_list':max_vol_list,'max_var_list':max_var_list,'min_beta_list':min_beta_list,
        'min_vol_list':min_vol_list,'min_var_list':min_var_list}
        return  HttpResponse(json.dumps(context), content_type="application/json")
    else:
        Stock_Data = Stock_Data[['Company','Factset_Ticker']]
        Stock_Data.columns=['company_name','Factset_Ticker']
        stock=list(Stock_Data['Factset_Ticker'])
        start_date =  current_date + timedelta(-365)
        Daily_Returns,index_returns = getReturns(request,start_date,current_date)
        cfs_ticker = Daily_Returns[['Company','Factset_Ticker']].drop_duplicates(keep="last")
        fs_tickers=list(cfs_ticker['Factset_Ticker'])
        IndexData = pd.DataFrame(list(market_model.objects.filter(company=index_ticker).order_by('date').values('date','company','return_field')))
        IndexData.columns=['Date','Company','Return']
        EMA_Data = pd.DataFrame(list(exponentialModel.objects.filter(factset_ticker__in=fs_tickers,date=exponentialModel.objects.all().aggregate(Max('date'))['date__max']).order_by('company').values('company','factset_ticker','ema')))
        EMA_Data.columns = ['Company','Factset_Ticker','EMA']
        MA50_Data=pd.DataFrame(list(movingAverageModel.objects.filter(factset_ticker__in=fs_tickers,date=movingAverageModel.objects.all().aggregate(Max('date'))['date__max']).order_by('company').values('company','factset_ticker','ma50')))
        MA50_Data.columns=['Company','Factset_Ticker','MA50']
        RSI_Data = pd.DataFrame(list(volumeDataModel.objects.filter(ticker__in=fs_tickers,date=volumeDataModel.objects.all().aggregate(Max('date'))['date__max'], factor='RSI').values()))
        RSI_Data.rename(columns={'date':'Date','factor':'Factor','ticker':'Factset_Ticker'}, inplace=True)
        week_52_data = pd.DataFrame(list(targetPricesModel.objects.filter(ticker__in=stocks).values('company','ticker','number_52_week_low','number_52_week_high','number_1_month_low','number_1_month_high')))
        week_52_data.columns = ['Company','Factset_Ticker','52_Week_Low','52_Week_High','1 Month Low','1 Month High']
        risk_monitor_df=RiskMonitorMain(request.user.username,request.session['portfolio_name'],request.session['country'],current_date,Stock_Data,Daily_Returns,IndexData,EMA_Data,MA50_Data,RSI_Data,week_52_data)
        if(len(risk_monitor_df)!=0):
            s1= timezone.now()
            risk_monitor_df['Views'] = ''
            risk_monitor_df=risk_monitor_df[['Company','Factset_Ticker','Price','1M Cumulative Return','1Y Cumulative Return','1M Excess Return', '3M Excess Return','1Y Excess Return','Min_Price_1Y', 'Max_Price_1Y', 'Min_Price_1M', 'Max_Price_1M','RSI','MA50','EMA','VFX','Views']]
            risk_monitor_df.columns=['Company','Factset_Ticker','Price','1M Cumulative Return','1Y Cumulative Return','1M Excess Return', '3M Excess Return','1Y Excess Return','Min_Price_1Y', 'Max_Price_1Y', 'Min_Price_1M', 'Max_Price_1M','RSI','50 DMA','20 EMA','VFX','Views']
            risk_monitor_df['Views'] = np.select([(risk_monitor_df['1M Excess Return']<0),(risk_monitor_df['1M Excess Return']>0)],
                                                    ['Stock has underperformed the market in the last 1 month','Stock has outperformed the market in the last 1 month'],
                                                    default ='')
            risk_monitor_df['Views'] = np.select([(risk_monitor_df['Price']<=(1.05*risk_monitor_df['Min_Price_1Y'])),
                                                    (risk_monitor_df['Price']>=(0.95*risk_monitor_df['Max_Price_1Y']))],                                             
                                                    [risk_monitor_df['Views'] + " and is trading near its 1 year low.",
                                                    risk_monitor_df['Views'] + " and is trading near its 1 year high." ],
                                                    default = risk_monitor_df['Views']+"." )
            risk_monitor_df['Views'] = np.select([(risk_monitor_df['RSI']>70),(risk_monitor_df['RSI']<30)],
                                                    [risk_monitor_df['Views'] + '<br/>RSI is more than 70, which implies that stock is overbought.',
                                                    risk_monitor_df['Views'] + '<br/>RSI is less than 30, which implies that stock is oversold.'],
                                                    default = risk_monitor_df['Views']+'')
            risk_monitor_df['Views'] = np.select(   [
                                                        ((risk_monitor_df['Price']>risk_monitor_df['50 DMA']) & (risk_monitor_df['Price']>risk_monitor_df['20 EMA'])),
                                                        ((risk_monitor_df['Price']<risk_monitor_df['50 DMA']) & (risk_monitor_df['Price']<risk_monitor_df['20 EMA'])),
                                                        ((risk_monitor_df['Price']>risk_monitor_df['50 DMA']) & (risk_monitor_df['Price']<risk_monitor_df['20 EMA'])),
                                                        ((risk_monitor_df['Price']<risk_monitor_df['50 DMA']) & (risk_monitor_df['Price']>risk_monitor_df['20 EMA'])),

                                                    ],
                                                    [
                                                        risk_monitor_df['Views'] + ' <br/><b class="text-success">It is trading above its 20EMA and 50DMA</b>',
                                                        risk_monitor_df['Views'] + ' <br/><b class="text-success">It is trading below its 20EMA and 50DMA</b>',
                                                        risk_monitor_df['Views'] + ' <br/><span class="text-success">It is trading below its 20EMA and above its 50DMA</span>',
                                                        risk_monitor_df['Views'] + ' <br/><span class="text-success">It is trading above its 20EMA and above its 50DMA</span>'                                                 
                                                    ],
                                                default = risk_monitor_df['Views']+''
                                                    )
            risk_monitor_df['Views'] = np.select([((risk_monitor_df['3M Excess Return']>risk_monitor_df['1Y Excess Return']) & (risk_monitor_df['1M Excess Return']>0.2) & (risk_monitor_df['Price']>=(0.95*risk_monitor_df['Max_Price_1Y'])) &(risk_monitor_df['RSI']>70) & (risk_monitor_df['VFX']<0.03)&(risk_monitor_df['Price']>risk_monitor_df['20 EMA']) &( risk_monitor_df['Price']> risk_monitor_df['50 DMA'])),
                                                ((risk_monitor_df['3M Excess Return']<risk_monitor_df['1Y Excess Return']) &(risk_monitor_df['Price']<=(1.05*risk_monitor_df['Min_Price_1Y'])) & (risk_monitor_df['RSI']<30) & (risk_monitor_df['Price']<risk_monitor_df['20 EMA']) & ( risk_monitor_df['Price']<risk_monitor_df['50 DMA']))],
                                                [risk_monitor_df['Views'] + ' and suggest profit-booking for a conservative investor' ,
                                                risk_monitor_df['Views'] + ' and suggest trimming allocation in this stock for a conservative investor' ],            
                                                default = risk_monitor_df['Views']+''
                                                )
            risk_monitor_df=risk_monitor_df.fillna(123456789)
            plot_line2 = list(map(lambda e1:[e1],list(risk_monitor_df['Price'])))
            categories2 =list(map(lambda e1:[e1],list(risk_monitor_df['Min_Price_1Y'])))
            output2 = list(risk_monitor_df['Max_Price_1Y'])
            output2=[[{'y':ele}] for ele in output2]
            max_value2=float(np.nanmax(risk_monitor_df[['Max_Price_1Y']].values.tolist()))
            min_value2=float(np.nanmax(risk_monitor_df[['Min_Price_1Y']].values.tolist()))
            plot_line1 = list(map(lambda e1:[e1],list(risk_monitor_df['Price'])))
            categories1 =list(map(lambda e1:[e1],list(risk_monitor_df['Min_Price_1M'])))
            output1 = list(risk_monitor_df['Max_Price_1M'])
            output1=[[{'y':ele}] for ele in output1]
            max_value1=float(np.nanmax(risk_monitor_df[['Max_Price_1M']].values.tolist()))
            min_value1=float(np.nanmax(risk_monitor_df[['Min_Price_1M']].values.tolist()))
            risk_monitor_df['1Y Price']=''
            risk_monitor_df['1M Price']=''

            risk_monitor_df=risk_monitor_df[['Company','Factset_Ticker','Price','Views','1M Cumulative Return','1Y Cumulative Return','1M Excess Return', '3M Excess Return','1Y Excess Return','1Y Price','1M Price','RSI','50 DMA','20 EMA','VFX']]
            risk_monitor_df = pd.merge(risk_monitor_df,sec_code_data,on='Factset_Ticker',how='inner')
            risk_monitor_df['Company'] = risk_monitor_df['Company'] + "(" + risk_monitor_df['Security_Code'] + ")"
            del risk_monitor_df['Factset_Ticker']
            del risk_monitor_df['Security_Code']
            riskmonitor_headerlist=risk_monitor_df.columns.tolist()
            riskmonitor_tabledata=risk_monitor_df.values.tolist()
            portfolio_names=setPortfolio(request)    
            context={'symbol':symbol,'riskmonitor_headerlist':riskmonitor_headerlist,'riskmonitor_tabledata':riskmonitor_tabledata,
            'portfolio_names':request.session['portfoliolist'],'current_portfolio':request.session['portfolio_name'],'categories2':categories2,'max_value2':max_value2,
                'min_value2':min_value2,'plot_line2':plot_line2,'output2':output2,'categories1':categories1,'max_value1':max_value1,
                'min_value1':min_value1,'plot_line1':plot_line1,'output1':output1}
            print(context)
            return render(request, 'Risk_Overview.html',context)
        else:
            print("Here")
            context={'symbol':symbol,'riskmonitor_headerlist':[],'riskmonitor_tabledata':[],
            'portfolio_names':request.session['portfoliolist'],'current_portfolio':request.session['portfolio_name'],'categories2':[],'max_value2':[],
                'min_value2':0,'plot_line2':[],'output2':[],'categories1':[],'max_value1':0,
                'min_value1':0,'plot_line1':[],'output1':[]}
            return render(request, 'Risk_Overview.html',context)


@login_required
@SubscriptionValidation
def factorInfo(request):
    portfolio_name,portfolio_type=getPortfolio(request)
    country=request.session['country']
    symbols_formats={'India':['₹','in Cr'],'US':['$','M']}
    symbol=symbols_formats[country][0]
    format=symbols_formats[country][1]
    requiredTableObjects = {'India':[UniversePricesMain,SecurityMaster],
                            'US':[UsUniversePricesMain,UsSecurityMaster]}
    universe_model=  requiredTableObjects[country][0]
    security_model = requiredTableObjects[country][1]  
    Stock_Data =  getStockData(request,universe_model,portfolio_name)
    Stock_Data = Stock_Data[['company_name','fs_ticker','price','quantity']]    
    Stock_Data.columns = ['Company','Factset_Ticker','Price','Quantity']
    stocks = set(Stock_Data["Factset_Ticker"])
    stock_full_details = security_model.objects.filter(fs_ticker__in=stocks).order_by('fs_name').all().values()
    stock_full_details=pd.DataFrame(list(stock_full_details))
    stock_full_details['fs_name'] = stock_full_details['fs_name'] + " (" + stock_full_details['security_code'] + ")"
    stock_full_details=stock_full_details.to_dict(orient='records')
    portfolio_names=setPortfolio(request)
    context = {'format':format,'stocks' : stock_full_details,'symbol':symbol,
    'portfolio_names':request.session['portfoliolist'],'current_portfolio':request.session['portfolio_name'] ,'portfolio_names':request.session['portfoliolist'],'current_portfolio':request.session['portfolio_name']}
    return render(request, 'FactorInfo.html', context)


@login_required
@csrf_exempt
@SubscriptionValidation
def investment_amount_factors(request):

    portfolio_name,portfolio_type=getPortfolio(request)
    universe_model,security_model,market_model,risk_free_rate_model,daily_postions_object = getRequiredModelsObjects(request)
    symbols={'India':'₹','US':'$'}

    current_date= universe_model.objects.all().aggregate(Max('date'))['date__max']
    constraint_df=pd.DataFrame(list(FactorsConstraints.objects.values()))
    constraint_df.dropna(inplace=True)       
    constraint_table_data=constraint_df.values.tolist()        
    country=request.session['country']

    portfolio=getStockData(request,universe_model,portfolio_name)
    portfolio = portfolio[['company_name','fs_ticker','quantity']]
    portfolio.columns = ['Company','Factset_Ticker','Quantity']
    stocks = set(portfolio['Factset_Ticker'])

    factors = security_model.objects.filter(fs_ticker__in = stocks).values('fs_name','fs_ticker','beta_1y','volatility_1y_field','idiosyncratic_vol_field', 'pe', 'pb', 'div_yield', 'div_payout', 'sales_growth','eps_growth', 'roe', 'roce', 'net_debt_ebitda', 'debt_equity',  'piotroski_score')
    factors_data = pd.DataFrame(list(factors))
    factors_data = factors_data[['fs_name','fs_ticker','beta_1y','volatility_1y_field','idiosyncratic_vol_field', 'pe', 'pb', 'div_yield', 'div_payout', 'sales_growth','eps_growth', 'roe', 'roce', 'net_debt_ebitda', 'debt_equity',  'piotroski_score']]
    factors_data.columns = ['Company','Factset_Ticker','Beta_1Y','Volatility_1Y','Idiosyncratic_Vol','PE','PB','Div_Yield','Div_Payout','Sales_Growth','EPS_Growth','ROE' ,'ROCE','Net_Debt/EBITDA','Debt/Equity','Piotroski_Score']
    if request.method == 'POST':
        required_value=request.POST['required_value']        
        start_date,pnl_start_date=getDates(request,required_value,universe_model,daily_postions_object,portfolio_type)
        Daily_Returns,index_returns=getReturns(request,start_date,current_date)
        bucket_output = factorsDashboard(Daily_Returns,factors_data,start_date,current_date,portfolio,'view')
        if(len(bucket_output)>0):
            bucket_output['Factor'] = bucket_output.Factor.str.replace('_Bucket?', '')
            factors=(bucket_output[['Factor']].drop_duplicates('Factor')).values.tolist()
            output=[]
            for i in range(0,len(factors)):
                temp_df=bucket_output[bucket_output['Factor']==factors[i][0]]
                output.append({'Factor':factors[i][0],'data':[temp_df['Exposure'].values.tolist(),temp_df['Return'].values.tolist(),temp_df['count'].values.tolist()]})
        else:
            output=[]
        portfolio_names=setPortfolio(request)
        context={'output':output,'portfolio_names':request.session['portfoliolist'],'current_portfolio':request.session['portfolio_name']}
        return  HttpResponse(json.dumps(context), content_type="application/json")
    portfolio_names=setPortfolio(request)
    context={'factors':['Beta_1Y','Volatility_1Y','PE','PB','Div_Yield','Div_Payout','Sales_Growth','EPS_Growth',
    'ROE','ROCE','Net_Debt/EBITDA','Debt/Equity','Piotroski_Score'],'constraint_table_data':constraint_table_data,'symbol':symbol,'portfolio_names':request.session['portfoliolist'],'current_portfolio':request.session['portfolio_name']}
    return render(request, 'Investment_amount_factors.html',context)


@login_required
@csrf_exempt
@SubscriptionValidation
def historical_multiples(request):
    portfolio_name,portfolio_type=getPortfolio(request)
    country=request.session['country']
    requiredTableObjects = {'India':[UniversePricesMain,SecurityMaster,HistoricalMultiplesNew],
                            'US':[UsUniversePricesMain,UsSecurityMaster,UsHistoricalMultiplesNew]}

    universe_model= requiredTableObjects[country][0]
    security_model = requiredTableObjects[country][1]
    historical_model = requiredTableObjects[country][2]
    Stock_Data = getStockData(request,universe_model,portfolio_name)
    Stock_Data = Stock_Data[['company_name','fs_ticker','price','quantity']]
    Stock_Data.columns = ['Company','Factset_Ticker','Price','Quantity']
    stock_list = set(Stock_Data['Factset_Ticker'])
    securites = security_model.objects.filter(fs_ticker__in=stock_list).values('fs_name','fs_ticker','security_code')
    stocks = pd.DataFrame(list(securites))
    stocks = stocks[['fs_name','fs_ticker','security_code']]
    stocks.columns = ['FS Name', 'FS Ticker','Security_Code']
    if request.method == 'POST':
        required_value1 = request.POST['required_value']
        required_value = ""
        required_value = required_value1.lower().replace("-","_")
        historical_objs = historical_model.objects.filter(ticker=required_value1).values('date', 'factor','ticker', 'value')
        required_df = pd.DataFrame(list(historical_objs))
        required_df.columns = ['Date', 'Factor','Ticker', 'Value']
        required_df.dropna(subset=['Date', 'Factor','Ticker', 'Value'], inplace=True)
        required_df['Date'] = required_df['Date'].astype(str)
        #line1
        PE_df=required_df[required_df['Factor']=='PE']
        PE_df=PE_df.reset_index(drop=True).sort_values(by='Date')
        my_list=PE_df[['Date','Value']].values.tolist()
        PE_line={"name":"Price-to-Earnings (PE)","data":my_list,'color':'orange','lineWidth':2}
        #line2
        PB_df=required_df[required_df['Factor']=='PB']
        PB_df=PB_df.reset_index(drop=True).sort_values(by='Date')
        my_list=PB_df[['Date','Value']].values.tolist()
        PB_line={"name":"Price-to-Book (PB)","data":my_list,'color':'steelblue','lineWidth':2}
        #line3
        Db_EQ_df=required_df[required_df['Factor']=='DE']
        Db_EQ_df=Db_EQ_df.reset_index(drop=True).sort_values(by='Date')                    
        my_list=Db_EQ_df[['Date','Value']].values.tolist()
        Db_EQ_line={"name":"Debt-to-Equity (DE)","data":my_list,'color':'blue','lineWidth':2}

        #line4
        LTM_df=required_df[required_df['Factor']=='ROE']
        LTM_df=LTM_df.reset_index(drop=True).sort_values(by='Date')
        my_list=LTM_df[['Date','Value']].values.tolist()
        LTM_line={"name":"Return-on-Equity (ROE)","data":my_list,'color':'green','lineWidth':2}
        db_avg=0
        if len(Db_EQ_df)>0:
            db_avg = Db_EQ_df['Value'].mean()
        context={'PE_line':PE_line,'PB_line':PB_line,'Db_EQ_line':Db_EQ_line,'LTM_line':LTM_line,'PE_AVG':PE_df['Value'].mean(),'PB_AVG':PB_df['Value'].mean(),
                'DB_AVG':db_avg,'LTM_AVG':LTM_df['Value'].mean()}
        return  HttpResponse(json.dumps(context), content_type="application/json")
    portfolio_names=setPortfolio(request)
    context={'company':(stocks['FS Name'] + ' (' + stocks['Security_Code'] + ")").values.tolist(),'company_ticker':stocks['FS Ticker'].values.tolist(),
            'portfolio_names':request.session['portfoliolist'],'current_portfolio':request.session['portfolio_name']}
    return render(request, 'historical_multiples.html',context)
   

@login_required
@csrf_exempt
@SubscriptionValidation
def macroimpact(request):        
    portfolio_name,portfolio_type=getPortfolio(request)
    country=request.session['country']
    symbols = {'India':'₹' ,  'US':'$'}
    global symbol
    symbol=symbols[country]
    requiredTableObjects={'India':[UniversePricesMain,MacroData,MarketIndex,SecurityMaster],
                            'US':[UsUniversePricesMain,UsMacroData,UsMarketIndex,UsSecurityMaster]}
    universe_model = requiredTableObjects[country][0]
    macrodata_model = requiredTableObjects[country][1]
    market_model = requiredTableObjects[country][2]
    security_model =  requiredTableObjects[country][3]
    current_date= universe_model.objects.all().aggregate(Max('date'))['date__max']                
    Stock_Data=getStockData(request,universe_model,portfolio_name)
    Stock_Data = Stock_Data[['company_name','fs_ticker','price','quantity']]
    Stock_Data.columns = ['Company','Factset_Ticker','Price','Quantity']
    tickers = list(set(Stock_Data["Factset_Ticker"]))

    sec_code_data = security_model.objects.filter(fs_ticker__in=tickers).values("fs_ticker","fs_name","security_code")    
    sec_code_data = pd.DataFrame(list(sec_code_data))
    sec_code_data['fs_name'] = sec_code_data['fs_name'] + " (" + sec_code_data['security_code'] + ")"
    company_names = list(sec_code_data['fs_name'])
    tickers = list(sec_code_data['fs_ticker'])
    df2=pd.DataFrame(list(macrodata_model.objects.all().values()))
    if len(df2.columns)==8:
        df2.columns=["Date","Oil (Dollars per Barrel)","FX Rate","Interest Rate","Vix","Inflation","Gold","Id"]
    else:
        df2.columns=["Date","Oil (Dollars per Barrel)","FX Rate","Interest Rate","Vix","Inflation","Id"]
    df2=df2.drop('Id',axis=1)
    start1= time.time()
    df2['Interest Rate']=(df2['Interest Rate']*100).round(2)
    df2['Inflation']=(df2['Inflation']*100).round(2)
    fixed_Values=df2.columns.tolist()
    df2['Date'] = df2['Date'].astype(str)
    df2['Date'] = df2['Date'].str[0:11]
    df=pd.DataFrame()
    if request.method == 'POST':
        company_value = request.POST['company_value']
        required_value = request.POST['required_value']
        if company_value=='Portfolio':
            start_date = universe_model.objects.filter(date__gte='2019-01-01').all().aggregate(Min('date'))['date__min']
            Daily_Returns,index_returns=getReturns(request,start_date,current_date)
            Daily_Returns.columns = ['Company', 'Factset_Ticker', 'Date', 'Price', 'Return']
            Stocks_Data_with_Exposure = ExposureCalculation(Stock_Data,start_date,current_date,Daily_Returns)    #calculating exposure by calling ExposureCalculation() function
            cumulative_return=MacroImpactCumulativeReturns(Stocks_Data_with_Exposure,start_date,current_date,Daily_Returns)
            cumulative_return=cumulative_return[['Trade_Dt','PNL']]
            cumulative_return.columns=['Date','Price']
            cumulative_return['Date'] = cumulative_return['Date'].dt.date
            cumulative_return['Date'] = cumulative_return['Date'].astype(str)
            cumulative_return['Date'] = cumulative_return['Date'].str[0:11]
            cumulative_return['Price']=cumulative_return['Price'].apply(lambda x: round(x*100,2))
            cumulative_return['Company']='Portfolio'
            df=df.append(cumulative_return).reset_index(drop=True)
            stock_df=df[df['Company']==company_value].reset_index(drop=True)
        else:
            df=pd.DataFrame(list(universe_model.objects.filter(factset_ticker=company_value).values('date','factset_ticker','company', 'price')))
            df.columns=['Date','Factset_Ticker','Company','Price']
            df['Date'] = df['Date'].astype(str)
            df['Date'] = df['Date'].str[0:11]
            stock_df=df[df['Factset_Ticker']==company_value].reset_index(drop=True)
        stock_df_output=[]
        fixed_df_output=[]
        temp_list=[]
        stock_df= pd.merge(stock_df,df2,on="Date",how="inner").reset_index(drop=True)
        stock_df=stock_df.sort_values(by=['Date'])
        stock_list=stock_df[['Date','Price']].values.tolist()
        x={"name":company_value,"lineWidth": 2,'color':'red',"data":stock_list}
        stock_df_output.append(x)
        fixed_df=stock_df[['Date',required_value]].dropna().reset_index(drop=True)
        fixed_df=fixed_df.sort_values(by=['Date'])
        fixed_list=fixed_df.values.tolist()
        y={"name":required_value,"lineWidth": 2,'color':'blue',"data":fixed_list}
        fixed_df_output.append(y)
        portfolio_names = setPortfolio(request)        
        context={'stock_output':stock_df_output,'fixed_output':fixed_df_output,
        'portfolio_names':request.session['portfoliolist'],'current_portfolio':request.session['portfolio_name']}
        return  HttpResponse(json.dumps(context), content_type="application/json")

    portfolio_names = setPortfolio(request)        
    context={'company_names':company_names,'fs_tickers':tickers,'fixed_Values':fixed_Values,'symbol':symbol,'portfolio_names':request.session['portfoliolist'],'current_portfolio':request.session['portfolio_name']}
    return render(request, 'MacroImpact.html',context)

@login_required
@csrf_exempt
@SubscriptionValidation
def volume_volatility(request):
    portfolio_name,portfolio_type=getPortfolio(request)
    country=request.session['country']
    global symbol
    symbols={'India':'₹','US':'$'}
    symbol=symbols[country]

    requiredTableObjects={'India':[UniversePricesMain,SecurityMaster,VolumeDataNew,MovingAverage],
                            'US':[UsUniversePricesMain,UsSecurityMaster,UsVolumeDataNew,UsMovingAverage]}

    universe_model=requiredTableObjects[country][0]
    security_model = requiredTableObjects[country][1]
    volume_model = requiredTableObjects[country][2]
    movingavg_model = requiredTableObjects[country][3]
    current_date= universe_model.objects.all().aggregate(Max('date'))['date__max']
    Stock_Data=getStockData(request,universe_model,portfolio_name)    
    Stock_Data = Stock_Data[['company_name','fs_ticker','price','quantity']]
    Stock_Data.columns = ['Company','Factset_Ticker','Price','Quantity']
    stock_list = set(Stock_Data['Factset_Ticker'])
    securites = security_model.objects.filter(fs_ticker__in=stock_list).values('fs_name','fs_ticker','security_code')
    stocks = pd.DataFrame(list(securites))
    stocks = stocks[['fs_name','fs_ticker','security_code']]
    stocks.columns = ['FS Name', 'FS Ticker','Security_Code']
    stocks['FS Name'] = stocks['FS Name'] + " (" + stocks['Security_Code'] +")"
    if request.method == 'POST':
        pre_one_year_date = current_date+timedelta(-365)
        required_value1 = request.POST['required_value']
        universe_prices = pd.DataFrame(list(universe_model.objects.filter(factset_ticker=required_value1,date__gte=pre_one_year_date).values('company', 'factset_ticker', 'date', 'price', 'return_field')))
        required_value = ""
        volumeData = VolumeData
        sql_conn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server}; SERVER='+SERVER+'; PORT='+PORT+'; DATABASE='+DATABASE+';UID='+UID+';PWD='+PWD+';') 
        if request.session['country']=='US':
            required_value = required_value1.lower().replace("-","_")
            required_df=pd.read_sql("select [date],factor,value from US_Volume_Data_New where Ticker='"+required_value1+"' order by date desc",sql_conn)
        else:
            required_value = required_value1.replace("-IN", "_in")
            required_value = "number_"+required_value
            required_df=pd.read_sql("select [date],factor,value from volume_Data_new where Ticker='"+required_value1+"' order by date desc",sql_conn)        
        sql_conn.close()
        required_df.columns = ['Date', 'Factor',required_value1]
        required_df.dropna(subset=['Date', 'Factor',required_value1], inplace=True)
        required_df['Date'] = required_df['Date'].astype(str)
        required_df.sort_values(by=['Date'],ascending=[True],inplace=True)
        universe_prices['date'] = universe_prices['date'].astype(str)
        universe_prices.sort_values(by=['date'],ascending=[True],inplace=True)
        universe_prices=universe_prices[['date','price']]
        universe_prices.columns=['Date','Price']
        volumedf=(required_df[required_df['Factor']=='Volume'])[['Date',required_value1]]
        volumedf.columns=['Date','Volume']
        volumedf['Date'] = volumedf['Date'].astype(str)
        sql_conn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server}; SERVER='+SERVER+'; PORT='+PORT+'; DATABASE='+DATABASE+';UID='+UID+';PWD='+PWD+';')        
        if request.session['country']=='US':
            volume_20=(pd.read_sql("select * from US_Volume_20_Data where date>='" +str(pre_one_year_date)+"' and Ticker='" +required_value1+"'",sql_conn ))
        else:
            volume_20=(pd.read_sql("select * from Volume_20_Data where date>='" +str(pre_one_year_date)+"' and Ticker='" +required_value1+"'",sql_conn ))
        sql_conn.close()
        volume_20['Date'] = volume_20['Date'].astype(str)
        volume_20.sort_values(by='Date',inplace=True)
        volume_20.dropna(inplace=True)
        volumedf.sort_values(by='Date',inplace=True)
        volumedf.dropna(inplace=True)                           
        df = pd.merge(universe_prices,volumedf,on='Date',how='inner')
        df.sort_values(inplace=True,by='Date')
        price_data=[]
        my_list=df[['Date','Price']].values.tolist()    
        price_data.append(my_list)        
        volume_data=[]        
        my_list=df[['Date','Volume']].values.tolist()  
        volume_data.append(my_list)
        max_vol=df[['Volume']].dropna()
        max_vol=int(max_vol['Volume'].max())
        rsi_df=(required_df[required_df['Factor']=='RSI'])[['Date',required_value1]]        
        rsi_df.columns=['Date','RSI']
        rsi_df.dropna(inplace=True)
        df_rsi = pd.merge(universe_prices,rsi_df,on='Date',how='inner')
        volatility_df=(required_df[required_df['Factor']=='Volatility'])[['Date',required_value1]]
        volatility_df=volatility_df.reset_index(drop=True)
        volatility_df.columns=['Date','Volatility']
        volatility_df.dropna(inplace=True)
        df_volatility = pd.merge(universe_prices,volatility_df,on='Date',how='inner')
        #Price_Vs_moving_Avg
        required_value2 = request.POST['required_value2']
        price_df=pd.DataFrame(list(movingavg_model.objects.filter(factset_ticker=required_value1,date__gte=pre_one_year_date).values('date','price','ma9','ma20','ma26','ma50','ma100','ma200')))
        price_df.columns=['Date','Price','MA9','MA20','MA26','MA50','MA100','MA200']
        price_df=price_df.reset_index(drop=True).sort_values(by='Date')
        price_df=price_df[['Date','Price','MA9','MA20','MA26','MA50','MA100','MA200']]
        price_df.sort_values(by='Date',inplace=True)                   
        # price_df.dropna(axis=1, inplace=True)
        price_df['Date'] = price_df['Date'].astype(str).str[0:11]
        output=pd.DataFrame(columns=['name','lineWidth','color','data'])
        final_dict = {'names':list(price_df.columns)[1:],'lineWidth':[3,2,2,2,2,2,2],
                        'color':['#6610F2','#28a745','#02BCD4','#ffc107','#dc3545','#5A6268','#23272B']}
        for i in range(0,len(list(price_df.columns)[1:])):
            output.loc[i]=[final_dict['names'][i],final_dict['lineWidth'][i],final_dict['color'][i],price_df[['Date',final_dict['names'][i]]].dropna().values.tolist()]
        output_pricevsDMA=output.to_dict(orient='records')
        volume_20_list=[]
        my_list=volume_20[['Date','Volume_20']].values.tolist()
        volume_20_list.append(my_list)
        context={'max_vol':max_vol,'price_list':volume_20_list,'volume_list':volume_data,
        'rsi':[(df_rsi[['Date','RSI']].dropna()).values.tolist()],
        'volatility':[(df_volatility[['Date','Volatility']].dropna()).values.tolist()],
        'output':output_pricevsDMA
        }
        return  HttpResponse(json.dumps(context), content_type="application/json")
    portfolio_names=setPortfolio(request)
    context={'company':stocks['FS Name'].values.tolist(),'company_ticker':stocks['FS Ticker'].values.tolist(),'symbol':symbol
            ,'portfolio_names':request.session['portfoliolist'],'current_portfolio':request.session['portfolio_name']}
    return render(request, 'Volume_Volatility.html',context)

@login_required
@csrf_exempt
@SubscriptionValidation
def dashboards_summary(request):
    portfolio_name,portfolio_type=getPortfolio(request)
    country=request.session['country']
    universe_model,security_model,market_model,risk_free_rate_model,daily_postions_object = getRequiredModelsObjects(request)
    symbols={'India':'₹','US':'$'}
    current_date= universe_model.objects.all().aggregate(Max('date'))['date__max']
    symbol=symbols[country]
    today = date.today()
    Stock_Data =  getStockData(request,universe_model,portfolio_name)
    Stock_Data = Stock_Data[['company_name','fs_ticker','price','quantity','sector_name']]
    Stock_Data.columns = ['Company','Factset_Ticker','Price','Quantity','GICS']
    stock = set(Stock_Data["Factset_Ticker"])
    if request.method == 'POST':
        import datetime
        cum_returns_list,investment_amt_list,risk_output_list=[],[],[]
        required_value=request.POST['required_value']
        pnl_start_date = universe_model.objects.filter(date__lt=str(current_date.year-1)+'-'+str(current_date.month)+'-'+str(current_date.day)).all().aggregate(Max('date'))['date__max']
        start_date= str(current_date.year-1)+'-'+str(current_date.month)+'-'+str(current_date.day)        
        Daily_Returns,index_returns = getReturns(request,start_date,current_date,pnl_start_date)
        risk_free_rates = risk_free_rate_model.objects.filter(date__range=(start_date, current_date)).order_by('date').values('date','risk_free_rate')  #retrieving risk free data
        Risk_Free_Data = pd.DataFrame(list(risk_free_rates))                
        Risk_Free_Data = Risk_Free_Data[['date', 'risk_free_rate']]
        Risk_Free_Data.columns = ['Date', 'Risk Free Rate']
        cumulative_return,Daily_Returns_values,df=CalcsMain(request.session['country'],start_date,pnl_start_date,current_date, request.user.username,Stock_Data,Daily_Returns,index_returns,portfolio_name,Risk_Free_Data,pnl_start_date,required_value)
        table_data = df.values.tolist()
        
        cumulative_return['Trade_Dt'] = cumulative_return['Trade_Dt'].dt.date.astype(str).str[0:11]
        cumulative_return.rename(columns = {'PNL':'Portfolio'}, inplace = True)
        output=pd.DataFrame(columns=['name','lineWidth','color','data'])
        final_dict = {'names':list(cumulative_return.columns)[1:],'lineWidth':[4,2,2,2,2],
                        'color':['#6610F2','#28a745','#02BCD4','#ffc107','#dc3545']}
        for i in range(0,5):
            output.loc[i]=[final_dict['names'][i],final_dict['lineWidth'][i],final_dict['color'][i],cumulative_return[['Trade_Dt',final_dict['names'][i]]].values.tolist()]
        output_cumulative_return=output.to_dict(orient='records')
        cumulative_return.rename(columns = {'Portfolio':'PNL'}, inplace = True)
        port_index_returns=[]
        port_index_returns.append(cumulative_return.loc[len(cumulative_return)-1,'PNL'])
        import math
        risk_output_list.append([Daily_Returns_values['Portfolio Return'].std()*math.sqrt(252)])
        col_identifiers={'India':'Nifty 50 Index','US':'S&P 500'}
        port_index_returns.append(cumulative_return.loc[len(cumulative_return)-1,col_identifiers[country]])
        risk_output_list.append([Daily_Returns_values[col_identifiers[country]+' Return'].std()*math.sqrt(252)])               
        beta = stats.linregress(Daily_Returns_values[col_identifiers[country]+' Return'].values,Daily_Returns_values["Portfolio Return"].values)[0:1][0]
        risk_output_list.append([round(beta,2)])
        cum_returns_list.append(port_index_returns)
        returns_data=Daily_Returns.pivot(index='Date',columns='Factset_Ticker',values='Return').reset_index()

        benchmarks={'India':'Nifty 50 Index','US':'S&P 500'}
        index_data=index_returns[index_returns['Company']==benchmarks[country]]
        returns_data=pd.merge(returns_data,index_data[['Date','Return']],on='Date',how='inner')
        returns_data.fillna(0,inplace=True)
        res,cols={},list(returns_data.columns)
        for i in range (1,len(cols)-1):
            beta = stats.linregress(returns_data['Return'].values,returns_data[cols[i]].values)[0:1][0]
            res[cols[i]]=beta
        risk_output_list.append([max(res, key=res.get),round(res[max(res, key=res.get)],2)])

        ## Returns_by_Stock_Sector

        gics= security_model.objects.filter(fs_ticker__in=stock).values('fs_name','fs_ticker','gics','market_cap_category','beta_1y','pe','eps_growth','security_code')
        gics_mcap_data=pd.DataFrame(list(gics))
        gics_mcap_data.columns=['Company','Factset_Ticker','GICS','Market_Cap_Category','Beta_1Y','PE','EPS_Growth','Security_Code']
        gics_data=gics_mcap_data[['Company','Factset_Ticker','GICS']]
        sector_data = gics_data[['GICS']]
        sec_code_data=gics_mcap_data[['Factset_Ticker','Company','Security_Code']]

        universe_objs = universe_model.objects.filter(factset_ticker__in=stock, date=(universe_model.objects.aggregate(Max('date'))['date__max'])).values('company','factset_ticker','price')
        prices_data = pd.DataFrame(list(universe_objs))
        prices_data = prices_data[['company','factset_ticker','price']]
        prices_data.columns = ['Company', 'Factset_Ticker','Price']
        security_objs = security_model.objects.filter(fs_ticker__in=stock).values('fs_name','fs_ticker','security_code','gics','market_cap_category')
        gics_data = pd.DataFrame(list(security_objs))


        required_value='LTD'
        df,sector_wise = New_CurrentPositionsMain(start_date,current_date,Daily_Returns,Stock_Data,gics_data,sector_data,required_value)                        
        df = pd.merge(df,sec_code_data[['Factset_Ticker','Security_Code']],on='Factset_Ticker',how='inner')
        df['Stock'] = df['Stock'] + " (" + df['Security_Code'] + ")"
        del df['Security_Code']
        sector_wise=sector_wise.dropna()
        total_exposure=df['Current Exposure'].sum()
        df=df.sort_values(by=required_value+'_Return',ascending=False).reset_index(drop=True)                        
        sector_wise=sector_wise.sort_values(by=required_value+'_Return',ascending=False).reset_index(drop=True)                        
        cum_returns_list.append([df['Stock'][0],str(round(df['LTD_PNL'][0],2)),str(round(df['LTD_Return'][0]*100,2))])
        cum_returns_list.append([df['Stock'][len(df)-1],str(round(df['LTD_PNL'][len(df)-1],2)),str(round(df['LTD_Return'][len(df)-1]*100,2))])
        cum_returns_list.append([sector_wise['GICS'][0],str(round(sector_wise['LTD_PNL'][0],2)),str(round(sector_wise['LTD_Return'][0]*100,2))])
        cum_returns_list.append([sector_wise['GICS'][len(sector_wise)-1],str(round(sector_wise['LTD_PNL'][len(sector_wise)-1],2)),str(round(sector_wise['LTD_Return'][len(sector_wise)-1]*100,2))])
        df=df.sort_values(by=['Current Exposure'],ascending=False).reset_index(drop=True)
        df=df.head(10)
        df=df[['Stock',"Current Exposure",required_value+'_PNL',required_value+'_Return']]
        sector_categories=sector_wise['GICS'].values.tolist()
        returns_by_sector=df.values.tolist()

        ##Investment amount by sector and marketcap
        stocksdata=Stock_Data[['Company','Factset_Ticker','Quantity']]
        top3_stocks_by_exposure,stock_wise_exposure,gics_wise_exposure,top_sector_by_exposure,market_cap_wise_exposure = exposureDashboard(request.user.username,request.session['country'],stocksdata,prices_data,gics_data)

        investment_amt_list.append([top_sector_by_exposure['GICS'][0],str(round(top_sector_by_exposure['Exposure'][0],2)),str(round(top_sector_by_exposure['Exposure%'][0],2))])
        if len(top3_stocks_by_exposure)>=3:
            investment_amt_list.append([[top3_stocks_by_exposure['Company'][0],str(round(top3_stocks_by_exposure['Exposure'][0],2)),str(round(top3_stocks_by_exposure['Exposure%'][0],2))],[top3_stocks_by_exposure['Company'][1],str(round(top3_stocks_by_exposure['Exposure'][1],2)),str(round(top3_stocks_by_exposure['Exposure%'][1],2))],[top3_stocks_by_exposure['Company'][2],str(round(top3_stocks_by_exposure['Exposure'][2],2)),str(round(top3_stocks_by_exposure['Exposure%'][2],2))]])
        else:
            output_list=[]
            for i in range(0,len(top3_stocks_by_exposure)):
                output_list.append([top3_stocks_by_exposure['Company'][i],str(round(top3_stocks_by_exposure['Exposure'][i],2)),str(round(top3_stocks_by_exposure['Exposure%'][i],2))])
            investment_amt_list.append(output_list)
        investment_amt_list.append([str(market_cap_wise_exposure['Market Cap Category'][0]),str(round(market_cap_wise_exposure['Exposure'][0],2)),str(round(market_cap_wise_exposure['Exposure%'][0],2))])

        data_stock_wise=stock_wise_exposure['Exposure%'].tolist()
        stocks=stock_wise_exposure['Company'].tolist()
        count = 0
        investment_output = []
        investment_output2 = []
        for i in range (0,len(gics_wise_exposure)):      
            x={"name":gics_wise_exposure['GICS'][i]+' ('+str(int(round(gics_wise_exposure['Exposure%'][i])))+'%)',"y":gics_wise_exposure['Exposure%'][i],"Exposure":gics_wise_exposure['Exposure'][i],'colorValue':count,"new_name":gics_wise_exposure['GICS'][i]}
            investment_output.append(x)
        for i in range (0,len(market_cap_wise_exposure)):
            val={"name":market_cap_wise_exposure['Market Cap Category'][i]+' ('+str(int(round(market_cap_wise_exposure['Exposure%'][i])))+'%)',"y":market_cap_wise_exposure['Exposure%'][i],"Exposure":market_cap_wise_exposure['Exposure'][i],'colorValue':count,"new_name":market_cap_wise_exposure['Market Cap Category'][i]}        
            investment_output2.append(val)
        top=top_sector_by_exposure[['GICS','Exposure%']].values.tolist()
        top3=top3_stocks_by_exposure.values.tolist()
        total_3=round(top3_stocks_by_exposure['Exposure%'].sum(),1)

        # recomondations
        gics_data = gics_mcap_data[['Company' ,'Factset_Ticker','GICS', 'Market_Cap_Category']]
        factors_data =gics_mcap_data [['Company','Factset_Ticker','Beta_1Y','PE','EPS_Growth']]
        Stock_Data=Stock_Data[['Company','Factset_Ticker','Quantity']]
        df1=cum_returns_list
        df2=investment_amt_list
        df3=risk_output_list
        portfolio_data=Stock_Data[['Company','Factset_Ticker','Quantity']]
        df=idealConstraints(request.user.username,portfolio_name,request.session['country'],portfolio_data)        
        table_data1 = df.values.tolist()
        ## Style Measures

        customer_portfolio_details=Stock_Data[['Company','Factset_Ticker','Quantity']]

        prices=pd.DataFrame(list(universe_model.objects.filter(factset_ticker__in=stock,date=current_date).values('date','company','price','factset_ticker')))
        market_data=pd.DataFrame(list(market_model.objects.filter(date=current_date).values('date','company','price')))

        cfs_ticker = prices[['company','factset_ticker']].drop_duplicates(keep="last")
        fs_tickers=list(cfs_ticker['factset_ticker'])

        prices=prices[['date','company','price','factset_ticker']]
        prices.columns=['Date','Company','Price','Factset_Ticker']
        market_data.columns=['Date','Company','Price']
        last_year = current_date+timedelta(-365)

        upm=pd.DataFrame(list(universe_model.objects.filter(factset_ticker__in=stock,date__lte=str(current_date),date__gte=str(last_year)).values('date','company','factset_ticker','return_field','price')))
        mi=pd.DataFrame(list(market_model.objects.filter(date__lte=str(current_date),date__gte=str(last_year)).values('date','company','return_field')))
        upm.columns=['Date','Company','Factset_Ticker','Return','Price']
        mi.columns=['Date','Company','Return']

        if request.session['country']=='US':
            sec_master=pd.DataFrame(list(security_model.objects.all().values('pe','pb','div_yield','sales_growth','eps_growth','roe','fs_name','fs_ticker','market_cap','s_p_500','dow_jones_30')))
            sec_master.columns=['PE','PB','Div_Yield','Sales_Growth','EPS_Growth','ROE','FS Name','Factset_Ticker','Market Cap','S&P 500','Dow Jones 30']
            style_metrics_df=style_measures_us(current_date,customer_portfolio_details,prices,market_data,sec_master,upm,mi)
        else:
            sec_master=pd.DataFrame(list(security_model.objects.all().values('pe','pb','div_yield','sales_growth','eps_growth','roe','fs_name','fs_ticker','market_cap','nifty_50','nifty_small_cap_100','nifty_mid_cap_100')))
            sec_master.columns=['PE','PB','Div_Yield','Sales_Growth','EPS_Growth','ROE','FS Name','Factset_Ticker','Market Cap','Nifty 50','Nifty Small Cap 100','Nifty Mid Cap 100']
            style_metrics_df=style_measures_india(last_year,current_date,request.user.username,portfolio_name,portfolio_type,customer_portfolio_details,prices,market_data,sec_master,upm,mi)


        style_metrics=style_metrics_df.values.tolist()
        Stock_Data1 = Stock_Data[['Factset_Ticker']]
        Stock_Data1.columns=['Factset_Ticker']

        requiredTableObjects={'India':[ExponentialMovingAverage,VolumeDataNew,MovingAverage,TargetPrices],
                                'US':[UsExponentialMovingAverage,UsVolumeDataNew,UsMovingAverage,UsTargetPrices]}
        exponentialModel=requiredTableObjects[country][0]
        volumeDataModel=requiredTableObjects[country][1]
        movingAverageModel=requiredTableObjects[country][2]
        targetPricesModel=requiredTableObjects[country][3]

        stocks = list(Stock_Data1['Factset_Ticker'])
        benchmarks={'India':'Nifty 50 Index','US':'S&P 500'}
        index_ticker=benchmarks[country] 

        IndexData = pd.DataFrame(list(market_model.objects.filter(company=index_ticker).order_by('date').values('date','company','return_field')))
        IndexData.columns=['Date','Company','Return']

        EMA_Data = pd.DataFrame(list(exponentialModel.objects.filter(factset_ticker__in=stocks,date=exponentialModel.objects.all().aggregate(Max('date'))['date__max']).order_by('company').values('company','factset_ticker','ema')))
        EMA_Data.columns = ['Company','Factset_Ticker','EMA']

        MA50_Data=pd.DataFrame(list(movingAverageModel.objects.filter(factset_ticker__in=stocks,date=movingAverageModel.objects.all().aggregate(Max('date'))['date__max']).order_by('company').values('company','factset_ticker','ma50')))
        MA50_Data.columns=['Company','Factset_Ticker','MA50']

        RSI_Data = pd.DataFrame(list(volumeDataModel.objects.filter(ticker__in=fs_tickers,date=volumeDataModel.objects.all().aggregate(Max('date'))['date__max'], factor='RSI').values()))
        RSI_Data.rename(columns={'date':'Date','factor':'Factor','ticker':'Factset_Ticker'}, inplace=True)

        week_52_data = pd.DataFrame(list(targetPricesModel.objects.filter(ticker__in=stocks).values('company','ticker','number_52_week_low','number_52_week_high','number_1_month_low','number_1_month_high')))
        week_52_data.columns = ['Company','Factset_Ticker','52_Week_Low','52_Week_High','1 Month Low','1 Month High']

        risk_monitor_df=RiskMonitorMain(request.user.username,request.session['portfolio_name'],request.session['country'],current_date,Stock_Data1,Daily_Returns,IndexData,EMA_Data,MA50_Data,RSI_Data,week_52_data)
        risk_monitor_df['Views'] = ''
        risk_monitor_df=risk_monitor_df[['Company','Factset_Ticker','Price','1M Cumulative Return','1Y Cumulative Return','1M Excess Return', '3M Excess Return','1Y Excess Return','Min_Price_1Y', 'Max_Price_1Y', 'Min_Price_1M', 'Max_Price_1M','RSI','MA50','EMA','VFX','Views']]
        risk_monitor_df.columns=['Company','Factset_Ticker','Price','1M Cumulative Return','1Y Cumulative Return','1M Excess Return', '3M Excess Return','1Y Excess Return','Min_Price_1Y', 'Max_Price_1Y', 'Min_Price_1M', 'Max_Price_1M','RSI','50 DMA','20 EMA','VFX','Views']
        risk_monitor_df['Views'] = np.select([(risk_monitor_df['1M Excess Return']<0),(risk_monitor_df['1M Excess Return']>0)],
                                                ['Stock has underperformed the market in the last 1 month','Stock has outperformed the market in the last 1 month'],
                                                default ='')
        risk_monitor_df['Views'] = np.select([(risk_monitor_df['Price']<=(1.05*risk_monitor_df['Min_Price_1Y'])),
                                                (risk_monitor_df['Price']>=(0.95*risk_monitor_df['Max_Price_1Y']))],                                             
                                                [risk_monitor_df['Views'] + " and is trading near its 1 year low.",
                                                   risk_monitor_df['Views'] + " and is trading near its 1 year high." ],
                                                default = risk_monitor_df['Views']+"." )
        risk_monitor_df['Views'] = np.select([(risk_monitor_df['RSI']>70),(risk_monitor_df['RSI']<30)],
                                                [risk_monitor_df['Views'] + '<br/>RSI is more than 70, which implies that stock is overbought.',
                                                 risk_monitor_df['Views'] + '<br/>RSI is less than 30, which implies that stock is oversold.'],
                                                default = risk_monitor_df['Views']+'')
        risk_monitor_df['Views'] = np.select(   [
                                                    ((risk_monitor_df['Price']>risk_monitor_df['50 DMA']) & (risk_monitor_df['Price']>risk_monitor_df['20 EMA'])),
                                                    ((risk_monitor_df['Price']<risk_monitor_df['50 DMA']) & (risk_monitor_df['Price']<risk_monitor_df['20 EMA'])),
                                                    ((risk_monitor_df['Price']>risk_monitor_df['50 DMA']) & (risk_monitor_df['Price']<risk_monitor_df['20 EMA'])),
                                                    ((risk_monitor_df['Price']<risk_monitor_df['50 DMA']) & (risk_monitor_df['Price']>risk_monitor_df['20 EMA'])),
                                                ],
                                                 [
                                                    risk_monitor_df['Views'] + ' <br/><b class="text-success">It is trading above its 20EMA and 50DMA</b>',
                                                    risk_monitor_df['Views'] + ' <br/><b class="text-success">It is trading below its 20EMA and 50DMA</b>',
                                                    risk_monitor_df['Views'] + ' <br/><span class="text-success">It is trading below its 20EMA and above its 50DMA</span>',
                                                    risk_monitor_df['Views'] + ' <br/><span class="text-success">It is trading above its 20EMA and above its 50DMA</span>'                                                 
                                                 ],
                                             default = risk_monitor_df['Views']+''
                                                )
        risk_monitor_df['Views'] = np.select([((risk_monitor_df['3M Excess Return']>risk_monitor_df['1Y Excess Return']) & (risk_monitor_df['1M Excess Return']>0.2) & (risk_monitor_df['Price']>=(0.95*risk_monitor_df['Max_Price_1Y'])) &(risk_monitor_df['RSI']>70) & (risk_monitor_df['VFX']<0.03)&(risk_monitor_df['Price']>risk_monitor_df['20 EMA']) &( risk_monitor_df['Price']> risk_monitor_df['50 DMA'])),
                                              ((risk_monitor_df['3M Excess Return']<risk_monitor_df['1Y Excess Return']) &(risk_monitor_df['Price']<=(1.05*risk_monitor_df['Min_Price_1Y'])) & (risk_monitor_df['RSI']<30) & (risk_monitor_df['Price']<risk_monitor_df['20 EMA']) & ( risk_monitor_df['Price']<risk_monitor_df['50 DMA']))],
                                             [risk_monitor_df['Views'] + ' and suggest profit-booking for a conservative investor' ,
                                              risk_monitor_df['Views'] + ' and suggest trimming allocation in this stock for a conservative investor' ],            
                                             default = risk_monitor_df['Views']+''
                                            )
        risk_monitor_df=risk_monitor_df[['Company','Factset_Ticker','Views']]
        risk_monitor_df=pd.merge(risk_monitor_df,sec_code_data,on=['Company','Factset_Ticker'],how="inner")
        risk_monitor_df['Company'] = risk_monitor_df['Company'] + " (" + risk_monitor_df['Security_Code'] + ")"
        del risk_monitor_df['Security_Code']
        del risk_monitor_df['Factset_Ticker']
        riskmonitor_tabledata=risk_monitor_df.values.tolist()
        context={'table_data1':table_data1,'df1':df1,'df2':df2,'df3':df3,'sector_count':len(sector_categories),'output_cumulative_return':output_cumulative_return,'table_data':table_data,##equal weighted
                'top':top,'top3':top3,'data_stock_wise':data_stock_wise,'stocks':stocks,'investment_output':investment_output,'investment_output2':investment_output2,'total_3':total_3, ##Investment amount sector mktcap
                'returns_by_sector':returns_by_sector,'user':request.user.username,'portfolio_name':portfolio_name,'stock_count':len(stock),'symbol':symbol,
                'style_metrics':style_metrics,'riskmonitor_tabledata':riskmonitor_tabledata
        }
        return HttpResponse(json.dumps(context), content_type='application/json')
    portfolio_names=setPortfolio(request)
    context={'date':(today),'portfolio_names':request.session['portfoliolist'],'current_portfolio':request.session['portfolio_name']}
    return render(request, 'dashboards_summary.html',context)

def optimized_portfolio(request):
    try:
        request.session['country'] = request.session['country'] 
    except:
        request.session['country'] = 'India'
    portfolio_name=request.session['portfolio_name']
    country=request.session['country']
    tableobjects={'India':[UniversePricesMain,MarketIndex,SecurityMaster],'US':[UsUniversePricesMain,UsMarketIndex,UsSecurityMaster]}
    universe_model = tableobjects[country][0]
    market_model = tableobjects[country][1]
    security_model = tableobjects[country][2]
    current_date= universe_model.objects.all().aggregate(Max('date'))['date__max']    
    Stock_Data = getStockData(request,universe_model,portfolio_name)
    Stock_Data = Stock_Data[['company_name','fs_ticker','quantity','price']]
    Stock_Data.columns = ['Company','Factset_Ticker','Quantity','Price']
    Stock_Data = Stock_Data[['Company','Factset_Ticker','Quantity']]
    stocks = set(Stock_Data["Factset_Ticker"])
    start_date=date(2019,1,1)
    stock_daily_returns,index_returns=getReturns(request,start_date,current_date)
    stock_daily_returns.columns = ['Company', 'Factset_Ticker',  'Date', 'Price', 'Return']
    stock_daily_returns=pd.merge(stock_daily_returns,Stock_Data,on=['Company','Factset_Ticker'],how="inner")
    stock_daily_returns['InvestmentAmount'] = stock_daily_returns ['Price'] * stock_daily_returns['Quantity']
    df= stock_daily_returns.sort_values('Date', ascending=True).groupby(['Company','Factset_Ticker'], as_index=False).first()
    df=df[['Company','Factset_Ticker','InvestmentAmount']]    
    df.columns=['Stock','Factset_Ticker','Current Exposure']
    df=df.sort_values(by=['Current Exposure'], ascending=False).reset_index(drop=True)
    df=df[['Stock','Factset_Ticker','Current Exposure']]
    portfolio_df=df
    total_exposure=df['Current Exposure'].sum()
    portfolio_df['Weight']=portfolio_df['Current Exposure'].apply(lambda x:((x/total_exposure)*100))
    portfolio_df=portfolio_df[['Stock','Factset_Ticker','Weight']]
    portfolio_df.columns=['Company','Factset_Ticker','Weight']
    portfolio_df['Weight']=portfolio_df['Weight']/100
    portfolio_df=portfolio_df.sort_values(['Company'])
    portfolio_df=portfolio_df.reset_index(drop = True)
    df['Weight']=df['Current Exposure'].apply(lambda x:str(round((x/total_exposure)*100,1))+"%")

    sec_code_data = pd.DataFrame(list(security_model.objects.filter(fs_ticker__in=stocks).values("fs_ticker","security_code")))
    sec_code_data.columns = ['Factset_Ticker','Security_Code']
    df = pd.merge(df,sec_code_data,on='Factset_Ticker') 
    df['Stock'] = df['Stock'] + " (" + df['Security_Code'] + ")"
    df=df[['Stock','Weight']]
    stock_weight = df.values.tolist()   
    return stock_weight,portfolio_df

@login_required
@csrf_exempt
@SubscriptionValidation
def efficient_frontier(request): 
    portfolio_name,portfolio_type=getPortfolio(request)
    country=request.session['country']
    tableobjects={'India':[UniversePricesMain,MarketIndex,SecurityMaster],'US':[UsUniversePricesMain,UsMarketIndex,UsSecurityMaster]}
    universe_model = tableobjects[country][0]
    market_model = tableobjects[country][1]
    security_model = tableobjects[country][2]
    Stock_Data = getStockData(request,universe_model,portfolio_name)
    Stock_Data = Stock_Data[['company_name','fs_ticker','price','quantity']]
    Stock_Data.columns = ['Company','Factset_Ticker','Price','Quantity']
    Stock_Data = Stock_Data[['Company','Factset_Ticker','Quantity']]
    stocks = set(Stock_Data["Factset_Ticker"])
    sec_code_data = pd.DataFrame(list(security_model.objects.filter(fs_ticker__in=stocks).values("fs_ticker","security_code")))
    sec_code_data.columns = ['Factset_Ticker','Security_Code']
    start_date=date(2019,1,1)
    current_date= universe_model.objects.all().aggregate(Max('date'))['date__max']    
    stock_daily_returns,index_returns=getReturns(request,start_date,current_date)
    stock_daily_returns.columns = ['Company', 'Factset_Ticker',  'Date', 'Price', 'Return']
    stock_weight,stock_weight_df=optimized_portfolio(request)
    df=efficientFrontier(stock_weight_df,stock_daily_returns,portfolio_name,country)    
    if(len(df)>0):
        df = pd.merge(df,sec_code_data,on='Factset_Ticker',how='inner')
        df['Company'] = df['Company'] + " (" + df['Security_Code'] + ")"
        del df['Security_Code']
        del df['Factset_Ticker']
        portfolio=df[['Order','Expected_Return','Std_Dev','Sharpe_Ratio','Iter']]
        portfolio=portfolio.drop_duplicates().reset_index(drop=True)
        portfolio['Iter']=portfolio['Iter'].astype(str)
        actual_return_df=portfolio.loc[portfolio['Iter']=='0']
        portfolio=portfolio[portfolio["Expected_Return"]>=0.001].reset_index(drop=True)
        portfolio=portfolio.append(actual_return_df,ignore_index=True)
        portfolio=portfolio.drop_duplicates().reset_index(drop=True)
        actual_index=(portfolio.loc[portfolio['Iter']=='0'].index.tolist())
        min_variance_index=portfolio.Std_Dev.idxmin()
        max_return_index=portfolio.Expected_Return.idxmax()
        max_sharpe_index=portfolio.Sharpe_Ratio.idxmax()
        x=portfolio['Std_Dev'][actual_index[0]]*100
        y=portfolio['Expected_Return'][actual_index[0]]*100        
        graph_data=[]
        for i in range(0,len(portfolio)):
            if(i==actual_index[0]):
                plotline=portfolio['Std_Dev'][i]*100
                graph_data.append({'name':portfolio['Order'][i],"zIndex": 2,'sharp':portfolio['Sharpe_Ratio'][i],'showInLegend':1,'data':[[portfolio['Std_Dev'][i]*100,portfolio['Expected_Return'][i]*100]], 'marker': {'symbol': 'triangle','fillColor':'red','radius':8}})
            else:
                graph_data.append({'name':portfolio['Order'][i],"zIndex": 1,'sharp':portfolio['Sharpe_Ratio'][i],'showInLegend':0,'data':[[portfolio['Std_Dev'][i]*100,portfolio['Expected_Return'][i]*100]],'marker': {'symbol': 'circle', "lineColor": '#7460FF',"lineWidth": 2,'fillColor':'white'}})
        c=0.1
        while(True):
            matched_stocks=(portfolio[(portfolio['Std_Dev']>=((plotline-c)/100)) & (portfolio['Std_Dev']<=((plotline+c)/100))]).reset_index(drop=True)
            if(len(matched_stocks)>0):
                break
            else:
                c=c+0.1
        match=matched_stocks['Expected_Return'].max()
        matched_row=portfolio[portfolio['Expected_Return']==match].reset_index(drop=True)
        case=df[['Order','Company','Weight']].reset_index(drop=True)
        case['Weight']=case['Weight'].apply(lambda x:str(round((x)*100,1))+"%")
        graph_table_data=[]
        group_case=case.groupby('Order')['Company','Weight'].apply(lambda x: x.values.tolist()).reset_index()
        group_case.columns=['Case','Data']
        for i in range(0,len(group_case)):
            graph_table_data.append({'case':group_case['Case'][i],'data':group_case['Data'][i]})
        actual_index=portfolio.loc[actual_index[0],'Order']
        x_default=matched_row['Std_Dev'][0]
        y_default=matched_row['Expected_Return'][0]
        default=matched_row['Order'][0]
    else:
        plotline=0
        x=0
        y=0
        x_default=0
        y_default=0
        stock_weight=[]
        actual_index=0
        default=0
        graph_data=[]
        graph_table_data=[]
    portfolio_names=setPortfolio(request)    
    context={'plotline':plotline,'x':x,'y':y,'x_default':x_default,'y_default':y_default,'stock_weight':stock_weight,
            'default':default,'actual_index':actual_index,'graph_data':graph_data,"graph_table_data":graph_table_data,
            'portfolio_names':request.session['portfoliolist'],'current_portfolio':request.session['portfolio_name'] 
            }
    return render(request, 'Efficient_Frontier.html',context)

@login_required
@csrf_exempt
@SubscriptionValidation
def target_return_model(request):
    portfolio_name,portfolio_type=getPortfolio(request)
    country=request.session['country']
    tableobjects={'India':[UniversePricesMain,MarketIndex,SecurityMaster],'US':[UsUniversePricesMain,UsMarketIndex,UsSecurityMaster]}
    universe_model = tableobjects[country][0]
    market_model = tableobjects[country][1]
    security_model = tableobjects[country][2]
    Stock_Data = getStockData(request,universe_model,portfolio_name)
    Stock_Data = Stock_Data[['company_name','fs_ticker','quantity','price']]
    Stock_Data.columns = ['Company','Factset_Ticker','Quantity','price']
    Stock_Data = Stock_Data[['Company','Factset_Ticker','Quantity']]
    stocks = set(Stock_Data["Factset_Ticker"])
    start_date=date(2019,1,1)
    current_date= universe_model.objects.all().aggregate(Max('date'))['date__max']    
    stock_daily_returns,index_returns=getReturns(request,start_date,current_date)
    stock_daily_returns.columns = ['Company', 'Factset_Ticker',  'Date', 'Price', 'Return']
    stock_daily_returns=stock_daily_returns[['Date','Company',"Factset_Ticker",'Return']]
    stock_weight,stock_weight_df=optimized_portfolio(request)                
    if request.method == 'POST':
        target_return=request.POST['target_return']
        df1,case=Optimal_Portfolio_Volatility(stock_weight_df,float(target_return)/100,request.session['country'],stock_daily_returns)        
        sec_code_data = pd.DataFrame(list(security_model.objects.filter(fs_ticker__in=stocks).values("fs_ticker","security_code")))
        sec_code_data.columns=['Factset_Ticker','Security_Code']
        if(len(df1)>0):
            temp_df=df1
            temp_df=temp_df.drop(['Actual_Return'],axis=1)
            max_values_list=temp_df.values.tolist()
            case=pd.merge(case,stock_weight_df[['Company','Factset_Ticker','Weight']],on=['Company','Factset_Ticker'],how='left')
            case=case.sort_values(by=['Weight_y'], ascending=False).reset_index(drop=True)
            maximum=max(case["Weight_x"].max(),case["Weight_y"].max())
            case = pd.merge(case,sec_code_data,on='Factset_Ticker',how='inner')
            case['Company'] =  case['Company'] + " (" + case['Security_Code'] + ")"
            del case['Factset_Ticker']
            del case['Security_Code']
            graph_data=[]
            save_data=case.values.tolist()
            for i in range(0,len(case)):
                graph_data.append([{'type':'bar','data':[case['Weight_y'][i]*100,case['Weight_x'][i]*100]}])
            case['Weight_x']=case['Weight_x'].apply(lambda x:str(round((x)*100,1))+"%")
            case['Weight_y']=case['Weight_y'].apply(lambda x:str(round((x)*100,1))+"%")
            case['graph']=''
            case.rename(columns={'Company':'Company','Weight_x':'Model Weight','Weight_y':'Actula Weight','graph':'graph'}, inplace=True)
            company=case[['Company','graph']]
            context={'save_data':save_data,'stock_weight':stock_weight,'maximum':maximum*100,'graph_data':graph_data,'graph_table_data':company.values.tolist(),
            'std':df1['Standard Deviation'][0],'expected_return':df1['Expected Return'][0],'actual_return':df1['Actual_Return'][0],'max_values_list':max_values_list}
            return  HttpResponse(json.dumps(context), content_type="application/json")
        else:
            context={'save_data':[],'stock_weight':[],'maximum':0,'graph_data':[],'graph_table_data':[],'std':0,'expected_return':0,'actual_return':0,'max_values_list':[]}
            return  HttpResponse(json.dumps(context), content_type="application/json")
    portfolio_names=setPortfolio(request)
    return render(request, 'target_return_model.html',context={'stock_weight':stock_weight,'portfolio_names':request.session['portfoliolist'],'current_portfolio':request.session['portfolio_name'] })
   

class EfficientFrontierOptimizedPortfolioSaveView(LoginRequiredMixin,View):
    context = dict() #Empty global dictionary for saving the context
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(EfficientFrontierOptimizedPortfolioSaveView, self).dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        self.context['msg'] = "Something went wrong, please try again!"
        return HttpResponse(json.dumps(self.context), content_type='application/json')

    
    def post(self,request,*args,**kwargs):
        data = request.POST.get('save_data')
        datalist = pd.DataFrame(json.loads(data))
        datalist['user'] = request.user.username
        datalist['date_time'] = datetime.now()
        basePortfolio = request.session['portfolio_name']
        datalist['Base_Portfolio'] = basePortfolio
        universeModel = UniversePricesMain
        securityModel = SecurityMaster
        if request.session['country']=='US':
            universeModel = UsUniversePricesMain
            securityModel = UsSecurityMaster
        datalist.columns=['Company','Model','Actual','Risk','Return','Portfolio Name','Actual_Risk','Actual_Return','User','Date Time','Base_Portfolio']
        datalist=datalist[['User','Date Time','Portfolio Name','Company','Model','Actual','Risk','Return','Base_Portfolio','Actual_Risk','Actual_Return']]
        if datalist['Portfolio Name'][0] == 'Efficient_Frontier_Better_Return_Portfolio':
            OptSavedPortfolioEff.objects.filter(user=request.user.username,Base_Portfolio=request.session['portfolio_name'],portfolio_name='Efficient_Frontier_Better_Return_Portfolio').delete()
            for i in range(0,len(datalist)):
                OptSavedPortfolioEff(user=datalist['User'][i],date_time=datalist['Date Time'][i],portfolio_name=datalist['Portfolio Name'][i],company= re.sub(r' \(.*?\)', '', datalist['Company'][i]),model=datalist['Model'][i],
                actual=datalist['Actual'][i],risk=datalist['Risk'][i],return_field=datalist['Return'][i],Base_Portfolio=datalist['Base_Portfolio'][i],actual_risk=datalist['Actual_Risk'][i], actual_return=datalist['Actual_Return'][i]).save()
        else:
            try:
                count = OptSavedPortfolioEff.objects.filter(user=request.user.username,Base_Portfolio=request.session['portfolio_name'],portfolio_name=datalist['Portfolio Name'][0]).count()
            except:
                count=0
            if count == 0:
                stocks = CustomerPortfolioDetails.objects.filter(customer_name=request.user.username,country=request.session['country'], portfolio_name=request.session['portfolio_name']).values('company_name','quantity')
                stockslist = set()
                if stocks.count() == 0:
                    portfolios = {'India':'72PI India Portfolio','US':'72PI US Portfolio'}
                    stocks = ModelPortfolioDetails.objects.filter(country=request.session['country'], model_portfolio_name=portfolios[request.session['country']]).values('company_name','quantity')
                for stock in stocks:
                    if stock['company_name'] not in stockslist:
                        stockslist.add(stock['company_name'])
                stockData = pd.DataFrame(list(stocks))
                stocks=list(stockData['company_name'])
                country=request.session['country']
                tableobjects={'India':[UniversePricesMain,MarketIndex],'US':[UsUniversePricesMain,UsMarketIndex]}
                universe_model = tableobjects[country][0]
                market_model = tableobjects[country][1]
                current_date= universe_model.objects.all().aggregate(Max('date'))['date__max']    
                start_date=date(2019,1,1)
                stock_daily_returns,index_returns=getReturns(request,start_date,current_date)
                pricesData= stock_daily_returns.sort_values('Date', ascending=True).groupby('Company', as_index=False).first()
                pricesData = pricesData[['Company','Price']]
                sectors = securityModel.objects.filter(fs_name__in=stockslist).values('fs_name','gics','fs_ticker')               
                sectorData = pd.DataFrame(list(sectors))
                sectorData.columns = ['Company','Sector','Ticker']
                stockData.columns=['Company','Quantity']
                pricesData.columns=['Company','Price']
                stockData = pd.merge(stockData,sectorData,how="inner", on="Company")
                stockData = pd.merge(stockData,pricesData,how="inner", on="Company")
                stockData['Investment'] = stockData['Price']*stockData['Quantity']
                totalInvestment = stockData['Investment'].sum()
                portfolio = datalist[['User','Portfolio Name','Company','Model','Actual']]
                for i in range(0,len(stockData)):
                    portfolio['Company'][i] = re.sub(r' \(.*?\)', '', portfolio['Company'][i])
                stockData = pd.merge(stockData,portfolio,how="inner", on="Company")
                stockData['IndvidualInvst'] = stockData['Model']*totalInvestment
                stockData['NewQuantity'] = (stockData['IndvidualInvst']/stockData['Price']).astype(int)
                try:
                    ccount = CustomerPortfolioDetails.objects.filter(customer_name=request.user.username,country=request.session['country'],portfolio_name=stockData['Portfolio Name'][0]).count()
                except:
                    ccount=0
                if ccount == 0:
                    for i in range(0,len(stockData)):
                        if stockData['NewQuantity'][i]:
                            if not CustomerPortfolioDetails.objects.filter(country=request.session['country'],customer_name=stockData['User'][i],portfolio_name=stockData['Portfolio Name'][i],company_name=stockData['Company'][i]).exists():
                                CustomerPortfolioDetails(customer_name=stockData['User'][i],portfolio_name=stockData['Portfolio Name'][i],company_name=stockData['Company'][i],sector_name=stockData['Sector'][i],
                                price=stockData['Price'][i],quantity=stockData['NewQuantity'][i],model_portfolio='no', portfolio_type='General',country=request.session['country'],fs_ticker=stockData['Ticker'][i]).save()
                    CustomerPortfolioLog(customer_name=request.user.username,portfolio_name=stockData['Portfolio Name'][0],last_action='portfolio_created',created_at=timezone.now(),status='active').save()
                for i in range(0,len(datalist)):
                    OptSavedPortfolioEff(user=datalist['User'][i],date_time=datalist['Date Time'][i],portfolio_name=datalist['Portfolio Name'][i],company=datalist['Company'][i],model=datalist['Model'][i],
                    actual=datalist['Actual'][i],risk=datalist['Risk'][i],return_field=datalist['Return'][i],Base_Portfolio=datalist['Base_Portfolio'][i],actual_risk=datalist['Actual_Risk'][i], actual_return=datalist['Actual_Return'][i]).save()
                self.context['msg'] = 'Your selected portfolio saved successfully!'
                createCacheForNewPortfolio(request.user.username, request.session['country'], stockData['Portfolio Name'][0])
            else:
                self.context['msg'] = 'Portfolio name '+datalist['Portfolio Name'][0] +' is already taken.'
        return HttpResponse(json.dumps(self.context), content_type='application/json')

    
class TargetReturnModelOptimizedPortfolioSaveView(LoginRequiredMixin,View):

    context = dict() #Empty global dictionary for saving the context
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(TargetReturnModelOptimizedPortfolioSaveView, self).dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        self.context['msg'] = "Something went wrong, please try again!"
        return HttpResponse(json.dumps(self.context), content_type='application/json')

    def post(self,request,*args,**kwargs):
        data = request.POST.get('save_data')
        datalist = pd.DataFrame(json.loads(data))
        datalist['user'] = request.user.username
        datalist['date_time'] = datetime.now()
        basePortfolio = request.session['portfolio_name']
        datalist['Base_Portfolio'] = basePortfolio
        universeModel = UniversePricesMain
        securityModel = SecurityMaster
        if request.session['country']=='US':
            universeModel = UsUniversePricesMain
            securityModel = UsSecurityMaster
        datalist.columns = ['Company','Model','Actual','Risk','Return','Portfolio Name','User','Date Time','Base_Portfolio']
        datalist = datalist[['User','Date Time','Portfolio Name','Company','Model','Actual','Risk','Return','Base_Portfolio']]
        if datalist['Portfolio Name'][0] == 'Target_Return_20_Portfolio':
            OptSavedPortfolioVol.objects.filter(user=request.user.username,Base_Portfolio=request.session['portfolio_name'],portfolio_name='Target_Return_20_Portfolio').delete()
            for i in range(0,len(datalist)):
                    OptSavedPortfolioVol(user=datalist['User'][i],date_time=datalist['Date Time'][i],portfolio_name=datalist['Portfolio Name'][i],company=re.sub(r' \(.*?\)', '', datalist['Company'][i]),model=datalist['Model'][i],
                    actual=datalist['Actual'][i],risk=datalist['Risk'][i],return_field=datalist['Return'][i],Base_Portfolio=datalist['Base_Portfolio'][i]).save()
    
        else:
            count = OptSavedPortfolioVol.objects.filter(user=request.user.username,Base_Portfolio=request.session['portfolio_name'],portfolio_name=datalist['Portfolio Name'][0]).count()
            if count == 0:
                stocks = CustomerPortfolioDetails.objects.filter(customer_name=request.user.username,country=request.session['country'], portfolio_name=request.session['portfolio_name']).values('company_name','quantity')
                stockslist = set()
                if stocks.count() == 0:
                    portfolios = {'India':'72PI India Portfolio','US':'72PI US Portfolio'}
                    stocks = ModelPortfolioDetails.objects.filter(country=request.session['country'], model_portfolio_name=portfolios[request.session['country']]).values('company_name','quantity')
                for stock in stocks:
                    if stock['company_name'] not in stockslist:
                        stockslist.add(stock['company_name'])
                stockData = pd.DataFrame(list(stocks))
                stocks=list(stockData['company_name'])
                country=request.session['country']
                tableobjects={'India':[UniversePricesMain,MarketIndex],'US':[UsUniversePricesMain,UsMarketIndex]}
                universe_model = tableobjects[country][0]
                market_model = tableobjects[country][1]
                current_date= universe_model.objects.all().aggregate(Max('date'))['date__max']    
                start_date=date(2019,1,1)
                stock_daily_returns,index_returns=getReturns(request,start_date,current_date)
                pricesData= stock_daily_returns.sort_values('Date', ascending=True).groupby('Company', as_index=False).first()
                pricesData = pricesData[['Company','Price']]
                sectors = securityModel.objects.filter(fs_name__in=stockslist).values('fs_name','gics','fs_ticker')
                sectorData = pd.DataFrame(list(sectors))
                sectorData.columns = ['Company','Sector','Ticker']
                stockData.columns=['Company','Quantity']
                pricesData.columns=['Company','Price']
                stockData = pd.merge(stockData,sectorData,how="inner", on="Company")
                stockData = pd.merge(stockData,pricesData,how="inner", on="Company")
                stockData['Investment'] = stockData['Price']*stockData['Quantity']
                totalInvestment = stockData['Investment'].sum()
                portfolio = datalist[['User','Portfolio Name','Company','Model','Actual']]
                for i in range(0,len(stockData)):
                    portfolio['Company'][i] = re.sub(r' \(.*?\)', '', portfolio['Company'][i])
                stockData = pd.merge(stockData,portfolio,how="inner", on="Company")
                stockData['IndvidualInvst'] = stockData['Model']*totalInvestment
                stockData['NewQuantity'] = (stockData['IndvidualInvst']/stockData['Price']).astype(int)
                ccount = CustomerPortfolioDetails.objects.filter(customer_name=request.user.username,country=request.session['country'],portfolio_name=stockData['Portfolio Name'][0]).count()
                if ccount == 0:
                    for i in range(0,len(stockData)):
                        if stockData['NewQuantity'][i]:
                            if not CustomerPortfolioDetails.objects.filter(customer_name=stockData['User'][i],portfolio_name=stockData['Portfolio Name'][i],company_name=stockData['Company'][i]).exists():
                                CustomerPortfolioDetails(customer_name=stockData['User'][i],portfolio_name=stockData['Portfolio Name'][i],company_name=stockData['Company'][i],sector_name=stockData['Sector'][i],
                                price=stockData['Price'][i],quantity=stockData['NewQuantity'][i],model_portfolio='no', portfolio_type='General',country=request.session['country'],fs_ticker=stockData['Ticker'][i]).save()
                    CustomerPortfolioLog(customer_name=request.user.username,portfolio_name=stockData['Portfolio Name'][0],last_action='portfolio_created',created_at=timezone.now(),status='active').save()
                for i in range(0,len(datalist)):
                    OptSavedPortfolioVol(user=datalist['User'][i],date_time=datalist['Date Time'][i],portfolio_name=datalist['Portfolio Name'][i],company=datalist['Company'][i],model=datalist['Model'][i],
                    actual=datalist['Actual'][i],risk=datalist['Risk'][i],return_field=datalist['Return'][i],Base_Portfolio=datalist['Base_Portfolio'][i]).save()
                self.context['msg'] = 'Your selected portfolio saved successfully!'
                createCacheForNewPortfolio(request.user.username, request.session['country'], stockData['Portfolio Name'][0])
            else:
                self.context['msg'] = 'Portfolio name '+datalist['Portfolio Name'][0] +' is already taken.'
        return HttpResponse(json.dumps(self.context), content_type='application/json')


@login_required
@csrf_exempt
@SubscriptionValidation
def resultsComparision(request):
    portfolio_name,portfolio_type=getPortfolio(request)
    table_df=opt_summary_data(request.user.username,request.session['portfolio_name'])
    if len(table_df)==0:
        context={'table_data':[],'header_list':[],'Risk':[],'Return':[],'portfolio_names':request.session['portfoliolist'],'current_portfolio':request.session['portfolio_name'] }
        return render(request, 'ResultsComparision.html',context)
    else:
        df1=(pd.DataFrame(list(OptSavedPortfolioEff.objects.filter(user=request.user.username,Base_Portfolio=request.session['portfolio_name']).values('portfolio_name','actual_risk','actual_return')))).drop_duplicates( subset=None, keep="first", inplace=False)
        df1=df1.head(1)
        df1["portfolio_name"]="Actual_Portfolio"
        df1.columns=['portfolio_name','risk','return_field']
        df2=(pd.DataFrame(list(OptSavedPortfolioEff.objects.filter(user=request.user.username,Base_Portfolio=request.session['portfolio_name']).values('portfolio_name','risk','return_field')))).drop_duplicates( subset=None, keep="first", inplace=False)
        df3=(pd.DataFrame(list(OptSavedPortfolioVol.objects.filter(user=request.user.username,Base_Portfolio=request.session['portfolio_name']).values('portfolio_name','risk','return_field')))).drop_duplicates( subset=None, keep="first", inplace=False)
        df1=df1.append(df2)
        df1=df1.append(df3)    
        df1.reset_index(drop=True)
        table_df=table_df.sort_values(['Actual_Portfolio'],ascending=False).reset_index(drop=True)
        header_list=table_df.columns.tolist()
        temp=pd.DataFrame(table_df.columns[1:len(table_df.columns)])
        temp.columns=["portfolio_name"]
        temp['order'] = range(0, len(temp))
        temp = temp.merge(df1,on="portfolio_name",how="inner")
        table_data=table_df.values.tolist()
        risk_list=temp['risk'].values.tolist()
        return_list=temp['return_field'].values.tolist()
        portfolio_names=setPortfolio(request)
        context={'table_data':table_data,'header_list':header_list,'Risk':risk_list,'Return':return_list,'portfolio_names':request.session['portfoliolist'],'current_portfolio':request.session['portfolio_name'] }
        return render(request, 'ResultsComparision.html',context)

class WelcomePageView(LoginRequiredMixin, TemplateView):
    template_name = 'WelcomePage.html'


@login_required
def homepage_new(request):
    portfolio_names = list(CustomerPortfolioDetails.objects.filter(customer_name=request.user.username,country=request.session['country']).values('portfolio_name').distinct())
    portfolio_names= ([y['portfolio_name'] for y in portfolio_names])
    context={'portfolio':portfolio_names[0]}
    return render(request, 'Homepage_new.html',context)



def getstocklist(request):
    stockStartsWith = request.GET.get('startswith', None)
    country = request.GET.get('country', None)
    context = {}
    if country == 'India':
        if stockStartsWith != 'others':
            indiastocks = pd.DataFrame(list(SecurityMaster.objects.filter(flag='yes', fs_name__istartswith=stockStartsWith).values('fs_name').order_by('fs_name').distinct()))
            context = {'stocklist':indiastocks.values.tolist(),'country':country}
        else:
            number_prefixes = [i for i in range(0,10)]
            query_filters = Q()
            for number_prefix in number_prefixes:
                query_filters |= Q(fs_name__startswith=number_prefix)
            indiastocks =pd.DataFrame(list( SecurityMaster.objects.filter(query_filters,flag='yes' ).values('fs_name').order_by('fs_name').distinct()))
        context = {'stocklist':indiastocks.values.tolist(),'country':country}
    else:
        if stockStartsWith != 'others':
            usstocks = pd.DataFrame(list( UsSecurityMaster.objects.filter(fs_name__istartswith=stockStartsWith).values('fs_name').order_by('fs_name').distinct()))
            context = {'stocklist':usstocks.values.tolist(),'country':country}
        else:
            number_prefixes = [i for i in range(0,10)]
            query_filters = Q()
            for number_prefix in number_prefixes:
                query_filters |= Q(fs_name__startswith=number_prefix)
            usstocks = pd.DataFrame(list(UsSecurityMaster.objects.filter(query_filters).values('fs_name').order_by('fs_name').distinct()))
        context = {'stocklist':usstocks.values.tolist(),'country':country}

    return render(request,'stockList.html', context)    
        
@login_required
@csrf_exempt
def import_portfolio(request):
    try:
        request.session['country'] = request.session['country'] 
    except:
        request.session['country'] = 'India'
    portfolio_names = list(CustomerPortfolioDetails.objects.filter(customer_name=request.user.username,country=request.session['country']).values('portfolio_name').distinct())
    portfoliocount=len(portfolio_names)
    if request.method == 'POST':
        selected_option = request.POST.get('radiovalue')
        selected_option=selected_option.replace('"','')
        datastr = request.POST.get('inputfiledata')
        importdata = json.loads(datastr)
        key=list(importdata.keys())[0]
        datalist=[]
        for i in range(1,len(importdata[key])):
            datalist.append(importdata[key][i])
        df=pd.DataFrame(datalist)
        df_columnlist=importdata[key][0]
        if 'Broker' and 'Notes' in df_columnlist:
            df_columnlist.remove('Broker')
            df_columnlist.remove('Notes')

        if(len(df.columns)>16):
            df.drop(df.iloc[:,-2:], axis=1, inplace=True)
            df.columns=df_columnlist
        else:
            df.columns=df_columnlist
        df=df.rename({df_columnlist[0]:'Stock Name'}, axis='columns')
        df=df.dropna().reset_index(drop=True)
        final_df=importportfolio(selected_option,df)
        companies = SecurityMaster.objects.filter(fs_name__in=final_df['Company'].tolist(),flag='yes').values('fs_name', 'market_cap','gics','market_cap_category').distinct().order_by('fs_name')
        companies=pd.DataFrame(list(companies))
        universe_prices = UniversePricesMain.objects.filter(date= UniversePricesMain.objects.values('date').aggregate(Max('date'))['date__max']).values('company','price').distinct().order_by('company') # distinct opt

        universe_prices = pd.DataFrame(list(universe_prices))
        universe_prices = universe_prices[['company', 'price']]
        universe_prices.columns = ['fs_name', 'price']
        result = pd.merge(companies,universe_prices,on="fs_name",how="inner")
        result=result.rename({'fs_name':'Company'}, axis='columns')
        result = pd.merge(result,final_df,on="Company",how="inner")
        result['market_cap']=round(result['market_cap']/10000000,2)
        result.drop_duplicates(keep='first', inplace=True)

        table_data=result.values.tolist()
        context = {'table_data' : table_data, 'username' : request.user.username}

        return  HttpResponse(json.dumps(context), content_type="application/json")

    return render(request, 'ImportPortfolio.html',{'portfoliocount':portfoliocount})

def FundManagement(request):
    x_data=['Mar','Apr','May','Jun']
    number_of_accounts=[20,30,25,40]
    color_list=['#4473C5', '#4473C5', '#4473C5','#ED7D31']
    total_cash_remaining=[10000,20000,15000,30000]
    invested_amt=[10000,20000,15000,30000]
    total_capital=[10,10.2,11,12]
    context={'x_data':x_data,'number_of_accounts':number_of_accounts,'color_list':color_list,'total_cash_remaining':total_cash_remaining,
    'invested_amt':invested_amt,'total_capital':total_capital,'symbol':'₹'}
    return render(request,'FundManager.html',context)
       

def FundManagerLogin(request):
    if request.method == 'POST':
        form=FundManagerLoginForm(request.POST or None)
        if  form.is_valid():
            try:
                username = form.cleaned_data.get('username')
                password = form.cleaned_data.get('password')
                user = User.objects.get(username=username)
                if user.is_superuser:
                    authuser = authenticate(username=username, password=password)
                    user_login(request,authuser)
                    return render(request,'FundManager.html')
                else:
                    Form = FundManagerLoginForm()
                    error = 'Sorry, you are not authorized to access this page'
                    return render(request,'FundManagerLogin.html',{'form':Form,'error':error})
            except User.DoesNotExist:
                Form = FundManagerLoginForm()
                error = 'Sorry, user does not exist!'
                return render(request,'FundManagerLogin.html',{'form':Form,'error':error})
        else:
            Form = FundManagerLoginForm()
            error = 'Invalid login,try again!'
            return render(request,'FundManagerLogin.html',{'form':Form,'error':error})
    else:
        if request.user.is_superuser:
           return render(request,'FundManager.html')
        if request.user.is_anonymous or not request.user.is_superuser:
            user_logout(request)
            Form = FundManagerLoginForm()
            return render(request,'FundManagerLogin.html',{'form':Form})

@login_required
def fund_dashboard(request):
    x_data=['Mar','Apr','May','Jun']
    number_of_accounts=[20,30,25,40]
    color_list=['#4473C5', '#4473C5', '#4473C5','#ED7D31']
    total_cash_remaining=[10000,20000,15000,30000]
    invested_amt=[10000,20000,15000,30000]
    total_capital=[10,10.2,11,12]
    context={'x_data':x_data,'number_of_accounts':number_of_accounts,'color_list':color_list,'total_cash_remaining':total_cash_remaining,
    'invested_amt':invested_amt,'total_capital':total_capital,'symbol':'₹'}
    return render(request,'dashboard.html',context)
           

@login_required
def fundTradeNotifications(request):
    finalcompanies = SecurityMaster.objects.filter(flag='yes').values('fs_name','fs_name').distinct()
    return render(request,'FundTradeNotifications.html',{'companies':finalcompanies})


@login_required
@csrf_exempt
def hedgeAccountsDisplay(request):
    responseMsg = {}
    customers = []
    if request.method == 'POST':
        tradesignals = request.POST.get('tradesignals')
        tradeSignals = json.loads(tradesignals)
        for signal in tradeSignals:
           buy_sell = signal['Buy_Sell']
           company_name = signal['CompanyName']
        if buy_sell == 'BUY':
            v5Objects = PortfolioV5.objects.values('customer_name','portfolio_name').annotate(invst_amount=Sum('value_cost'))
            capitaldict = {'ghctest':5000000,'Client2':3000000,'Client3':10000000}
            for item in v5Objects:
                if item['customer_name'] in capitaldict:
                   item['capital_amt'] =  capitaldict.get(item['customer_name'])
                   item['buy_sell'] =  buy_sell
                   item['company_name'] =  company_name
        else:
            # buy_sell == 'SELL'
            v5Objects = PortfolioV5.objects.filter(company=company_name).values('customer_name','portfolio_name','curr_qty').annotate(invst_amount=Sum('value_cost'))
            capitaldict = {'ghctest':5000000,'Client2':3000000,'Client3':10000000}
            for item in v5Objects:
                if item['customer_name'] in capitaldict:
                   item['capital_amt'] =  capitaldict.get(item['customer_name'])
                   item['buy_sell'] =  buy_sell
                   item['company_name'] =  company_name
        customers = [item for item in v5Objects]
        responseMsg['resMsg1'] =  customers
        responseMsg['resMsg2'] = str(len(customers))+" found."
    else:
        responseMsg['resMsg1'] =  customers
        responseMsg['resMsg2'] = str(len(customers))+" found."
    return HttpResponse(json.dumps(responseMsg), content_type='application/json')

    
@login_required
@csrf_exempt
def fundClientAccount(request):
    cust=PortfolioV5.objects.all().values('customer_name').distinct()
    userlist=[d['customer_name'] for d in cust]
    symbol ='₹'
    username=request.user.username
    if request.method=='POST':
        user = request.POST.get('user')
        df=pd.DataFrame(list(PortfolioV5.objects.filter(customer_name=user).values()))
        df['date'] = pd.to_datetime(df.date)
        date= df['date'][0].date()
        equity_val=df['value_mkt_price'].sum()
        equity_invstmnt=df['value_cost'].sum()
        pnl=df['pnl_unrealized'].sum()
        daily_pnl=df['pnl_unrealized_prev_day'].sum() 
        gainer=df.loc[df['pct_chg_day'] == df['pct_chg_day'].max(),'company'].values.tolist()
        loser=df.loc[df['pct_chg_day'] == df['pct_chg_day'].min(),'company'].values.tolist()
        if(len(gainer)==0):
            gainer='No Stock gaining today'
        else:
            gainer=gainer[0]
            
        if(len(loser)==0):
            loser='No Stock gaining today'
        else:
            loser=loser[0]
        profit=len(df[df['profit_loss']=='PROFIT'])
        loss=len(df[df['profit_loss']=='LOSS'])
        total_allocation=df['value_mkt_price'].sum()
        allocation_df=df.groupby('sector', as_index=False)['value_mkt_price'].sum()
        allocation_df['percent']=allocation_df['value_mkt_price'].apply(lambda x:round((x/total_allocation)*100,2))
        allocation_list=[]
        for i in range (0,len(allocation_df)):      
            x={"name":allocation_df['sector'][i]+' ('+str(int(round(allocation_df['percent'][i])))+'%)',"y":allocation_df['percent'][i]}
            allocation_list.append(x)
        highlights_list=[]
        portfolio_highlights_df=df[['company','sector','pnl_realized','value_mkt_price','pct_chg']]
        highlights_list.append(portfolio_highlights_df['pnl_realized'].sum())
        top_exposure=allocation_df.loc[allocation_df['percent'] == allocation_df['percent'].max(),['sector','percent']].values.tolist()
        highlights_list.append(top_exposure[0])
        most_profit=(portfolio_highlights_df.loc[portfolio_highlights_df['pct_chg'] == portfolio_highlights_df['pct_chg'].max(),['company','pct_chg']].values.tolist())
        highlights_list.append(most_profit[0])
        least_profit=(portfolio_highlights_df.loc[portfolio_highlights_df['pct_chg'] == portfolio_highlights_df['pct_chg'].min(),['company','pct_chg']].values.tolist())
        highlights_list.append(least_profit[0])
        table_df=df[['company','curr_qty','curr_price','value_mkt_price','pnl_realized','long_term_qty','avg_buy_price','value_cost','pnl_unrealized']]
        table_data=table_df.values.tolist()
        sql_conn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server}; SERVER='+SERVER+'; PORT='+PORT+'; DATABASE='+DATABASE+';UID='+UID+';PWD='+PWD+';')
        equity_cashflow=pd.read_sql("select eomonth(DATE) Month_End ,sum([P&L]) PNL from Daily_Positions where DATE>='2020-01-01' group by eomonth(DATE)",sql_conn)
        sql_conn.close()
        equity_cashflow['Month_End']=equity_cashflow['Month_End'].astype(str)
        header_list=['Company Name','Date','Action','Qty','Transaction Price','Value Cost','Additional charges','Remarks']
        trans_df=pd.DataFrame(list(Transactions.objects.filter(customer=user).values()))
        trans_df['tradedate']=trans_df['tradedate'].astype(str)
        sub_df=trans_df[['secdescription','tradedate','action','quantity','price','grossmoney','totalcomm','totalfees']]
        sub_df['ADDITIONAL_CHARGES']=sub_df['totalcomm']+sub_df['totalfees']
        sub_df['remarks']=''
        sub_df=sub_df.drop(['totalcomm', 'totalfees'], axis = 1) 
        table_data2=sub_df.values.tolist()
        context={'cashflow_data':equity_cashflow.values.tolist(),
        'username':username,'symbol':symbol,
        'equity_val':equity_val,'equity_invstmnt':equity_invstmnt,'pnl':pnl,'daily_pnl':daily_pnl,'gainer':gainer,'loser':loser,
        'profit':profit,'loss':loss,'allocation_list':allocation_list,'highlights_list':highlights_list,'table_data':table_data,
        'username':request.user.username,'header_list':header_list,'table_data2':table_data2
        }
        return  HttpResponse(json.dumps(context), content_type="application/json")
    return render(request,'FundClientAccount.html',{'userlist':userlist})




@login_required
def CustomerVerification(request):
    loginuser = request.user
    if loginuser.groups.filter(name='subscriptionadmins').exists():
        context = {}
        freesubscribers = Group.objects.get(name='freesubscribers')
        goldsubscribers = Group.objects.get(name='goldsubscribers')
        diamondsubscribers = Group.objects.get(name='diamondsubscribers')
        allusers = freesubscribers.user_set.all().union(goldsubscribers.user_set.all()).union(diamondsubscribers.user_set.all())
        subscriptions = CustomerSubscription.objects.all()
        for user in allusers:
            for item in subscriptions:
                if item.customer_name_id == user.id:
                    user.valid_from = item.subscription_starts
                    user.valid_till = item.subscription_ends
        context['users'] = allusers
        context['alluserscount'] = allusers.count()
        context['freeuserscount'] = freesubscribers.user_set.all().count()
        context['golduserscount'] = goldsubscribers.user_set.all().count()
        context['diamonduserscount'] = diamondsubscribers.user_set.all().count()
        return render(request,'SubscriptionAdmin.html',context) 
    elif loginuser.groups.filter(name='fundmanagementadmins').exists():
        x_data=['Mar','Apr','May','Jun']
        number_of_accounts=[20,30,25,40]
        color_list=['#4473C5', '#4473C5', '#4473C5','#ED7D31']
        total_cash_remaining=[10000,20000,15000,30000]
        invested_amt=[10000,20000,15000,30000]
        total_capital=[10,10.2,11,12]
        context={'x_data':x_data,'number_of_accounts':number_of_accounts,'color_list':color_list,'total_cash_remaining':total_cash_remaining,
        'invested_amt':invested_amt,'total_capital':total_capital,'symbol':'₹'}
        return render(request,'FundAdmin.html',context)
    else:
        if CustomerSubscription.objects.filter(customer_name=loginuser).exists():
            try:
                user = get_object_or_404(InvestorRiskProfile, customer_name=request.user)
            except Http404:
                form = InvestorsRiskProfileForm(request.GET)
                context = {'form' : form}
                template_name='InvestorRiskProfile.html'
                return render(request, template_name, context)
            return redirect('welcomepage')
        else:
            myprofile = MyProfile.objects.filter(user=request.user).all()
            first_name, last_name, email , contact_number = None,None,None,None
            for item in myprofile:
                first_name = item.first_name
                last_name = item.last_name
                email = item.email
                contact_number = item.contact_number
            if first_name:
                pass
            else:
                first_name = request.user.first_name
            if last_name:
                pass
            else:
                last_name = request.user.last_name
            if email:
                pass
            else:
                email = request.user.email
            from datetime import date
            endsubscription = date.today() + timedelta(days=30)
            startsubscription = date.today()
            CustomerSubscription(customer_name=request.user, first_name=first_name,last_name=last_name, email=email,contact_no=contact_number,
            subscription_type='FREE',subscription_starts=startsubscription,subscription_ends=endsubscription,
            subscription_status='ACTIVE', subscription_plan='30 DAY TRIAL', paid='NO',currency='NA').save()
            return render(request, 'freesubscriptionactivated.html',{'endtime':endsubscription})


class PreLoginSignalsView(TemplateView):
    template_name = 'Prelogin_Signal.html'


class PreLoginProducts(TemplateView):
    template_name = 'Prelogin_Products.html'


class CustomerContactUSView(View):
    def get(self, request, *args, **kwargs):
        template_name = 'contact.html'
        contactform = CustomerContactForm()
        context = {'form': contactform}
        return render(request,template_name, context)

    def post(self, request, *args, **kwargs):
        contactform = CustomerContactForm(request.POST)
        template_name = 'contact.html'
        if contactform.is_valid():
            first_name = contactform.cleaned_data['first_name']
            last_name = contactform.cleaned_data['last_name']
            email = contactform.cleaned_data['email']
            contact = contactform.cleaned_data['contact_no']
            subject = contactform.cleaned_data['subject']
            message = contactform.cleaned_data['message']
            CustomerContactUs(first_name=first_name,last_name=last_name,email=email,contact_no=contact,subject=subject,message=message).save()
            mail_subject = '72PI Customer Support Team'
            mail_body = render_to_string('CustomerContact.html', {'customer': first_name,'subject': subject,'message':message})
            send_mail(mail_subject, message, 'support <support@72pi.ai>', [email], fail_silently = True,html_message=mail_body)
            send_mail(mail_subject, message, 'support <support@72pi.ai>', ['support@72pi.ai'], fail_silently = True,html_message=mail_body)
            contactform = CustomerContactForm(request.GET or None)
            context = {'form': contactform, 'msg2':'Thank you for contacting us, will get back to you soon!'}
        else:
            context = {'form': contactform, 'msg1':'Please check all the fields and try again!'}
        return render(request, template_name, context)


class ListOfStockNotificationsView(LoginRequiredMixin,View):
    notifications = list()
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(ListOfStockNotificationsView, self).dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        return HttpResponse(json.dumps(self.notifications), content_type='application/json')

    def post(self, request, *args, **kwargs):
        companyName = request.POST['companyName']
        portfolioName = request.POST['portfolioName']
        self.notifications=[]
        if companyName and portfolioName:
            notifications1 = CustomerPriceMovementsAlerts.objects.filter(Q(customer_name=request.user) & Q(company_name=companyName) & Q(portfolio_name=portfolioName)  &
                            (Q(price_status='Notified') | Q(percent_status='Notified') | Q(price52wh_status='Notified'))).all()
            notifications2 = CustomerTechnicalIndicationAlerts.objects.filter(Q(customer_name=request.user) & Q(company_name=companyName) & Q(portfolio_name=portfolioName) & 
                            (Q(rsi_status='Notified') | Q(dma_status='Notified'))).all()
            # Prive Movement Notifications
            for item in notifications1:
                # Conditions for Price in Rs.
                if item.price_value and item.price_value_condition and item.price_status == 'Notified':
                    if item.price_value_condition == 'above':
                        self.notifications.append('Last Price >= '+str(item.price_value)+' | Price Movement ')
                    if item.price_value_condition == 'below':
                        self.notifications.append('Last Price <= '+str(item.price_value)+' | Price Movement')
                # Conditions for Price in %
                if item.price_percent and item.price_percent_condition and item.percent_status == 'Notified':
                    if item.price_percent_condition == 'above':
                        self.notifications.append('Last Price >= '+str(item.price_percent)+'% | Price Movement')
                    if item.price_percent_condition == 'below':
                        self.notifications.append('Last Price <= '+str(item.price_percent)+'% | Price Movement')
                # Conditions for 52-week high
                if item.price_52wh_percent and item.price_52wh_condition and item.price52wh_status == 'Notified':
                    if item.price_52wh_condition == 'above':
                        self.notifications.append('Last Price >= '+str(item.price_52wh_percent)+'% of 52-WH | Price Movement')
                    if item.price_52wh_condition == 'below':
                        self.notifications.append('Last Price <= '+str(item.price_52wh_percent)+'% of 52-WH | Price Movement')
            # Technical Indication Notifications
            for item in notifications2:
                # Conditions for 14 Day RSI
                if item.rsi_operand1 and item.rsi_condition and item.rsi_metric and item.rsi_status == 'Notified':
                    if item.rsi_condition == 'greaterthan':
                        self.notifications.append('14 Day RSI >= '+str(item.rsi_operand1)+' | Technical Indication')
                    if item.rsi_condition == 'lessthan':
                        self.notifications.append('14 Day RSI <= '+str(item.rsi_operand1)+' | Technical Indication')
                # Conditions for DMA
                if item.dma_operand1 and item.dma_operand2 and item.dma_condition and item.dma_status == 'Notified':
                    if item.dma_condition == 'greaterthan':
                        self.notifications.append(str(re.search(r'\d+', item.dma_operand1).group())+' Day MA >= '+str(re.search(r'\d+', item.dma_operand2).group())+' Day MA | Technical Indication')
                    if item.dma_condition == 'lessthan':
                        self.notifications.append(str(re.search(r'\d+', item.dma_operand1).group())+' Day MA <= '+str(re.search(r'\d+', item.dma_operand2).group())+' Day MA | Technical Indication')
        return HttpResponse(json.dumps(self.notifications), content_type='application/json')


class SaveCustomerPricemovementsView(LoginRequiredMixin,View):
    status = dict() #Defining empty dictionary 
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(SaveCustomerPricemovementsView, self).dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        self.status['result'] ='Something went wrong, try again'
        self.status['status'] = False
        return HttpResponse(json.dumps(self.status), content_type='application/json')
    
    def post(self, request, *args, **kwargs):
        try:
            portfolioName = request.POST['portfolioName']
            companyName = request.POST['companyName']
            priceValue = request.POST['priceValue']
            priceCondition = request.POST['priceCondition']
            price_select = request.POST['price_select']
            percent_select = request.POST['percent_select']
            _52wh_select = request.POST['_52wh_select']
            universeModel = UniversePricesMain
            securityModel = SecurityMaster
            if request.session['country'] == 'US':
                universeModel = UsUniversePricesMain
                securityModel = UsSecurityMaster
            sectors = securityModel.objects.filter(fs_name=companyName).first()
            fsTicker = sectors.fs_ticker
            priceStatus,percentStatus,_52Status = 'Waiting', 'Waiting', 'Waiting'
            priceCreated,percentCreated,price52whCreated = timezone.now(),timezone.now(),timezone.now()
            if priceValue == 'NaN':
                priceValue, priceCondition, priceStatus, priceCreated,price_select = None, None, 'NA', None,None
            pricePercent = request.POST['pricePercent']
            percentCondition = request.POST['percentCondition']
            if pricePercent == 'NaN':
                pricePercent, percentCondition, percentStatus, percentCreated ,percent_select= None, None, 'NA', None,None
            _52whPercent = request.POST['52whPercent']
            _52whCondition = request.POST['52whCondition']
            if _52whPercent == 'NaN':
                _52whPercent, _52whCondition, _52Status, price52whCreated,_52wh_select = None, None, 'NA', None,None
            lastPrice = 0.0
            status = {}
            prices = universeModel.objects.filter(date= 
                        universeModel.objects.all().aggregate(Max('date'))['date__max'], company=companyName).values('price')[0]
            try:
                lastPrice = prices['price']
            except KeyError:
                pass
            if priceValue and priceCondition and pricePercent and percentCondition and _52whPercent and _52whCondition:
                if CustomerPriceMovementsAlerts.objects.filter(customer_name=request.user, portfolio_name=portfolioName,company_name=companyName,
                price_value=priceValue,price_value_condition=priceCondition,price_percent=pricePercent,price_percent_condition=percentCondition,
                price_52wh_percent=_52whPercent,price_52wh_condition=_52whCondition,price_status='Waiting',percent_status='Waiting',price52wh_status='Waiting',
                country=request.session['country'],pricealert_for=price_select,percentalert_for=percent_select,price52alert_for=_52wh_select,fs_ticker=fsTicker).exists():
                    self.status['portfolio'] = portfolioName
                    self.status['result'] ='Same alerts already created, and are in waiting state too!'
                    self.status['status'] = False
                else:
                    alert1 = CustomerPriceMovementsAlerts.objects.filter(customer_name=request.user, portfolio_name=portfolioName,company_name=companyName,
                    price_value=priceValue,price_value_condition=priceCondition,price_status='Waiting',pricealert_for=price_select).all()
                    if alert1:
                        priceValue, priceCondition, priceStatus, priceCreated = None, None, 'NA', None
                    alert2 = CustomerPriceMovementsAlerts.objects.filter(customer_name=request.user,portfolio_name=portfolioName,company_name=companyName,
                    price_percent=pricePercent,price_percent_condition=percentCondition,percent_status='Waiting',percentalert_for=percent_select).all()
                    if alert2:
                        pricePercent, percentCondition, percentStatus, percentCreated = None, None, 'NA', None
                    alert3 =CustomerPriceMovementsAlerts.objects.filter(customer_name=request.user,portfolio_name=portfolioName,company_name=companyName,
                    price_52wh_percent=_52whPercent,price_52wh_condition=_52whCondition,price52wh_status='Waiting',price52alert_for=_52wh_select).all()
                    if alert3:
                        _52whPercent, _52whCondition, _52Status, price52whCreated = None, None, 'NA', None
                    CustomerPriceMovementsAlerts(customer_name=request.user, customer_email=request.user.email,portfolio_name=portfolioName,company_name=companyName,
                    alert_type='Price Movement', last_price=lastPrice,price_value_condition=priceCondition,price_value=priceValue,price_percent_condition=percentCondition,
                    price_percent=pricePercent,price_52wh_condition=_52whCondition,price_52wh_percent=_52whPercent,percent_status=percentStatus, price_status=priceStatus,
                    price52wh_status=_52Status, price_createdat=priceCreated, pricepercent_createdat=percentCreated, price52wh_createdat=price52whCreated,country=request.session['country'],
                    pricealert_for=price_select,percentalert_for=percent_select,price52alert_for=_52wh_select,fs_ticker=fsTicker).save()
                    self.status['portfolio'] = portfolioName
                    self.status['result'] = 'Price Movement alerts created successfully'
                    self.status['status'] = True
            elif priceValue and priceCondition:
                if CustomerPriceMovementsAlerts.objects.filter(customer_name=request.user, portfolio_name=portfolioName,company_name=companyName,
                price_value=priceValue,price_value_condition=priceCondition,price_status='Waiting',pricealert_for=price_select,fs_ticker=fsTicker).exists():
                    self.status['portfolio'] = portfolioName
                    self.status['result'] ='Same alerts already created, and are in waiting state too!'
                    self.status['status'] = False
                else:
                    CustomerPriceMovementsAlerts(customer_name=request.user, customer_email=request.user.email,portfolio_name=portfolioName,company_name=companyName,
                    alert_type='Price Movement', last_price=lastPrice,price_value_condition=priceCondition,price_value=priceValue,price_percent_condition=percentCondition,
                    price_percent=pricePercent,price_52wh_condition=_52whCondition,price_52wh_percent=_52whPercent,percent_status=percentStatus, price_status=priceStatus,
                    price52wh_status=_52Status, price_createdat=priceCreated, pricepercent_createdat=percentCreated, price52wh_createdat=price52whCreated,country=request.session['country'],
                    pricealert_for=price_select,percentalert_for=percent_select,price52alert_for=_52wh_select,fs_ticker=fsTicker).save()
                    self.status['portfolio'] = portfolioName
                    self.status['result'] = 'Price Movement alerts created successfully'
                    self.status['status'] = True
            elif pricePercent and percentCondition:
                if CustomerPriceMovementsAlerts.objects.filter(customer_name=request.user, portfolio_name=portfolioName,company_name=companyName,
                price_percent=pricePercent,price_percent_condition=percentCondition,percent_status='Waiting',percentalert_for=percent_select,fs_ticker=fsTicker).exists():
                    self.status['portfolio'] = portfolioName
                    self.status['result'] ='Same alerts already created, and are in waiting state too!'
                    self.status['status'] = False
                else:
                    CustomerPriceMovementsAlerts(customer_name=request.user, customer_email=request.user.email,portfolio_name=portfolioName,company_name=companyName,
                    alert_type='Price Movement', last_price=lastPrice,price_value_condition=priceCondition,price_value=priceValue,price_percent_condition=percentCondition,
                    price_percent=pricePercent,price_52wh_condition=_52whCondition,price_52wh_percent=_52whPercent,percent_status=percentStatus, price_status=priceStatus,
                    price52wh_status=_52Status, price_createdat=priceCreated, pricepercent_createdat=percentCreated, price52wh_createdat=price52whCreated,country=request.session['country'],
                    pricealert_for=price_select,percentalert_for=percent_select,price52alert_for=_52wh_select,fs_ticker=fsTicker).save()
                    self.status['portfolio'] = portfolioName
                    self.status['result'] = 'Price Movement alerts created successfully'
                    self.status['status'] = True
            elif  _52whPercent and _52whCondition:
                if CustomerPriceMovementsAlerts.objects.filter(customer_name=request.user, portfolio_name=portfolioName,company_name=companyName,
                price_52wh_percent=_52whPercent,price_52wh_condition=_52whCondition,price52wh_status='Waiting',price52alert_for=_52wh_select,fs_ticker=fsTicker).exists():
                    self.status['portfolio'] = portfolioName
                    self.status['result'] ='Same alerts already created, and are in waiting state too!'
                    self.status['status'] = False
                else:
                    CustomerPriceMovementsAlerts(customer_name=request.user, customer_email=request.user.email,portfolio_name=portfolioName,company_name=companyName,
                    alert_type='Price Movement', last_price=lastPrice,price_value_condition=priceCondition,price_value=priceValue,price_percent_condition=percentCondition,
                    price_percent=pricePercent,price_52wh_condition=_52whCondition,price_52wh_percent=_52whPercent,percent_status=percentStatus, price_status=priceStatus,
                    price52wh_status=_52Status, price_createdat=priceCreated, pricepercent_createdat=percentCreated, price52wh_createdat=price52whCreated,country=request.session['country'],
                    pricealert_for=price_select,percentalert_for=percent_select,price52alert_for=_52wh_select,fs_ticker=fsTicker).save()
                    self.status['portfolio'] = portfolioName
                    self.status['result'] = 'Price Movement alerts created successfully'
                    self.status['status'] = True
            else:
                pass
        except Exception as e:  #Any Exception
            print(e)
            self.status['portfolio'] = portfolioName
            self.status['result'] ='Something went wrong, please try again'
            self.status['status'] = False
        return HttpResponse(json.dumps(self.status), content_type='application/json')

class SaveCustomerTechnicalIndicationsView(LoginRequiredMixin,View):
    status = dict() #Defining empty dictionary 
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(SaveCustomerTechnicalIndicationsView, self).dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        self.status['result'] ='Something went wrong, please try again'
        self.status['status'] = False
        return HttpResponse(json.dumps(self.status), content_type='application/json')

    def post(self, request, *args, **kwargs):
        try:
            portfolioName = request.POST['portfolioName']
            companyName = request.POST['companyName']
            rsiValue = request.POST['rsiValue']
            rsiCondition = request.POST['rsiCondition']
            dmaLeft = request.POST['dmaLeft']
            dmaRight = request.POST['dmaRight']
            dmaCondition = request.POST['dmaCondition']
            rsi_select = request.POST['rsi_select']
            dma_select = request.POST['dma_select']
            securityModel = SecurityMaster
            if request.session['country'] == 'US':
                securityModel = UsSecurityMaster
            sectors = securityModel.objects.filter(fs_name=companyName).first()
            fsTicker = sectors.fs_ticker
            rsiStatus,dmaStatus = 'Waiting', 'Waiting'
            rsiCreated, dmaCreated = timezone.now(), timezone.now()
            if rsiValue == 'NaN':
                rsiValue, rsiCondition, rsiStatus, rsiCreated,rsi_select = None, None, 'NA', None,None
            if dmaLeft == 'select' or dmaRight == 'select':
                dmaLeft, dmaRight, dmaCondition, dmaStatus, dmaCreated,dma_select = None, None, None, 'NA', None,None
            if rsiValue and rsiCondition and rsiStatus and dmaLeft and dmaRight and dmaCondition:
                if CustomerTechnicalIndicationAlerts.objects.filter(customer_name=request.user, portfolio_name=portfolioName,company_name=companyName,
                rsi_operand1=rsiValue,rsi_condition=rsiCondition,dma_operand1=dmaLeft,dma_condition=dmaCondition,dma_operand2=dmaRight,
                rsi_status='Waiting',dma_status='Waiting', rsialert_for=rsi_select, dmaalert_for=dma_select,fs_ticker=fsTicker).exists():
                    self.status['portfolio'] = portfolioName
                    self.status['result'] ='Same alerts already created, and are in waiting state too!'
                    self.status['status'] = False
                else:
                    alert1 = CustomerTechnicalIndicationAlerts.objects.filter(customer_name=request.user, portfolio_name=portfolioName,company_name=companyName,
                    rsi_operand1=rsiValue,rsi_condition=rsiCondition).all()
                    if alert1:
                        rsiValue, rsiCondition, rsiStatus, rsiCreated = None, None, 'NA', None
                    alert2 = CustomerTechnicalIndicationAlerts.objects.filter(customer_name=request.user,portfolio_name=portfolioName,company_name=companyName,
                    dma_operand1=dmaLeft,dma_condition=dmaCondition,dma_operand2=dmaRight).all()
                    if alert2:
                        dmaLeft, dmaRight, dmaCondition, dmaStatus, dmaCreated = None, None, None, 'NA', None
                    CustomerTechnicalIndicationAlerts(customer_name=request.user, customer_email=request.user.email,portfolio_name=portfolioName,company_name=companyName,
                    alert_type='Technical Indicator',rsi_operand1=rsiValue,rsi_condition=rsiCondition,dma_operand1=dmaLeft,dma_condition=dmaCondition,dma_operand2=dmaRight,
                    rsi_status=rsiStatus,dma_status=dmaStatus, rsi_createdat=rsiCreated, dma_createdat=dmaCreated,country=request.session['country'],rsialert_for=rsi_select,
                    dmaalert_for=dma_select,fs_ticker=fsTicker).save()
                    self.status['portfolio'] = portfolioName
                    self.status['result'] = 'Technical Indication alerts created successfully'
                    self.status['status'] = True
            elif rsiValue and rsiCondition and rsiStatus:
                if CustomerTechnicalIndicationAlerts.objects.filter(customer_name=request.user, portfolio_name=portfolioName,company_name=companyName,
                rsi_operand1=rsiValue,rsi_condition=rsiCondition,rsi_status='Waiting',rsialert_for=rsi_select,fs_ticker=fsTicker).exists():
                    self.status['portfolio'] = portfolioName
                    self.status['result'] ='Same alerts already created, and are in waiting state too!'
                    self.status['status'] = False
                else:
                    CustomerTechnicalIndicationAlerts(customer_name=request.user, customer_email=request.user.email,portfolio_name=portfolioName,company_name=companyName,
                    alert_type='Technical Indicator',rsi_operand1=rsiValue,rsi_condition=rsiCondition,dma_operand1=dmaLeft,dma_condition=dmaCondition,dma_operand2=dmaRight,
                    rsi_status=rsiStatus,dma_status=dmaStatus, rsi_createdat=rsiCreated, dma_createdat=dmaCreated,country=request.session['country'],rsialert_for=rsi_select,
                    dmaalert_for=dma_select,fs_ticker=fsTicker).save()
                    self.status['portfolio'] = portfolioName
                    self.status['result'] = 'Technical Indication alerts created successfully'
                    self.status['status'] = True
            elif dmaLeft and dmaRight and dmaCondition:
                if CustomerTechnicalIndicationAlerts.objects.filter(customer_name=request.user,portfolio_name=portfolioName,company_name=companyName,
                dma_operand1=dmaLeft,dma_condition=dmaCondition,dma_operand2=dmaRight,dma_status='Waiting',dmaalert_for=dma_select,fs_ticker=fsTicker).exists():
                    self.status['portfolio'] = portfolioName
                    self.status['result'] ='Same alerts already created, and are in waiting state too!'
                    self.status['status'] = False
                else:
                    CustomerTechnicalIndicationAlerts(customer_name=request.user, customer_email=request.user.email,portfolio_name=portfolioName,company_name=companyName,
                    alert_type='Technical Indicator',rsi_operand1=rsiValue,rsi_condition=rsiCondition,dma_operand1=dmaLeft,dma_condition=dmaCondition,dma_operand2=dmaRight,
                    rsi_status=rsiStatus,dma_status=dmaStatus, rsi_createdat=rsiCreated, dma_createdat=dmaCreated,country=request.session['country'],rsialert_for=rsi_select,
                    dmaalert_for=dma_select,fs_ticker=fsTicker).save()
                    self.status['portfolio'] = portfolioName
                    self.status['result'] = 'Technical Indication alerts created successfully'
                    self.status['status'] = True
            else:
                pass
        except Exception as e:
            self.status['portfolio'] = portfolioName
            self.status['result'] ='Something went wrong, please try again'
            self.status['status'] = False
        return HttpResponse(json.dumps(self.status), content_type='application/json')

class InvestorRiskPreferences(LoginRequiredMixin,CreateView):
    def get(self, request,*args, **kwargs):
        form = InvestorsRiskProfileForm(request.GET)
        last_update = timezone.now()
        try:
            user = get_object_or_404(InvestorRiskProfile, customer_name=self.request.user)
            if user:
                form = InvestorsRiskProfileForm(instance=user)
                last_update = user.lastupdatedat
        except Http404:
            pass
        context = {'form' : form, 'lastupdate':last_update}
        template_name='InvestorRiskProfile.html'
        return render(request, template_name, context)
        
    def post(self, request, *args, **kwargs):
        form = InvestorsRiskProfileForm(data = request.POST)
        msg = 'Invalid input, try again!'
        noerror = True
        last_update = timezone.now()
        if form.is_valid():
            name = form.cleaned_data['name']
            age = form.cleaned_data['age']
            risk_type = form.cleaned_data['risk_type']
            approach = form.cleaned_data['investor_approch']
            current_stage = form.cleaned_data['current_stage']
            income_source = form.cleaned_data['income_source']
            portfolio_investment = form.cleaned_data['portfolio_investment']
            financial_status = form.cleaned_data['financial_status']
            investment_experience = form.cleaned_data['investment_experience']
            asset_classes = form.cleaned_data['asset_classes']
            risk_understanding = form.cleaned_data['risk_understanding']
            risk_awareness = form.cleaned_data['risk_awareness']
            portfolio_corrects = form.cleaned_data['portfolio_corrects']
            investment_objective = form.cleaned_data['trade_objective']
            liquidate_period = form.cleaned_data['liquidate_period']
            annual_return_investment = form.cleaned_data['annual_return_investment']
            if risk_type == 'Risk Averse':
                objective = 'Protect Capital [0% to 4% returns per year]'
                appetite  = 'No losses'
            elif risk_type == 'Conservative':
                objective = 'Gain atleast fixed deposit returns [5% to 8% returns per year]'
                appetite = 'Mild losses'
            elif risk_type == 'Aggressive':
                objective = 'Moderate capital growth [9% to 15% returns per year]'
                appetite = 'Moderate losses'
            elif risk_type == 'Very Aggressive':
                objective = 'High capital growth [15% and above returns per year]'
                appetite = 'Heavy losses'
            if InvestorRiskProfile.objects.filter(customer_name=request.user).count() == 0:
               InvestorRiskProfile(customer_name=request.user,name=name,age=age,risk_type=risk_type,investment_objective=objective,risk_appetite=appetite,
               investor_approch=approach,current_stage=current_stage,income_source=income_source,portfolio_investment=portfolio_investment,
               financial_status=financial_status,investment_experience=investment_experience,asset_classes=asset_classes,
               risk_understanding=risk_understanding,risk_awareness=risk_awareness,portfolio_corrects=portfolio_corrects,
               trade_objective=investment_objective,liquidate_period=liquidate_period,
               annual_return_investment=annual_return_investment).save()
               msg = 'Your Risk Profile has been saved successfully!'
               noerror = True
            else:
                InvestorRiskProfile.objects.filter(customer_name=request.user).update(risk_type=risk_type,investment_objective=objective,risk_appetite=appetite,
                investor_approch=approach,name=name,age=age,current_stage=current_stage,income_source=income_source,portfolio_investment=portfolio_investment,
                financial_status=financial_status,investment_experience=investment_experience,asset_classes=asset_classes,risk_understanding=risk_understanding,
                risk_awareness=risk_awareness,portfolio_corrects=portfolio_corrects, trade_objective=investment_objective,liquidate_period=liquidate_period,
                annual_return_investment=annual_return_investment,lastupdatedat=timezone.now())
                msg = 'Your Risk Profile has been updated successfully!'
                noerror = True
        elif form.errors:
            msg = 'Please fill the fileds and try again!'
            noerror = False
        else:
            msg = 'Please fill the fileds and try again!'
            noerror = False
        template_name='InvestorRiskProfile.html'
        context = {'form' : form, 'msg' : msg, 'noerror' : noerror, 'lastupdate' : last_update}
        return render(request, template_name, context)

class MyInformationFormView(LoginRequiredMixin,CreateView):
    def get(self, request,*args, **kwargs):
        form = MyProfileForm(request.GET)
        last_update = timezone.now()
        try:
            user = get_object_or_404(MyProfile, user=self.request.user)
            if user:
                form = MyProfileForm(instance=user)
        except Http404:
            pass
        context = {'form' : form, 'lastupdate':last_update}
        template_name='MyInformation.html'
        return render(request, template_name, context)
        
    def post(self, request, *args, **kwargs):
        form = MyProfileForm(data = request.POST)
        msg = 'Unknown Internal Server Error'
        last_update = timezone.now()
        if form.is_valid():
            first_name = form.cleaned_data['first_name']
            last_name = form.cleaned_data['last_name']
            email = form.cleaned_data['email']
            contact_number = form.cleaned_data['contact_number']
            if MyProfile.objects.filter(user=request.user).exists():
                MyProfile.objects.filter(user=request.user).update(first_name=first_name,last_name=last_name,email=email,contact_number=contact_number)
                msg = 'Your profile updated successfully!'
            else:
                MyProfile(user=request.user,first_name=first_name,last_name=last_name,email=email,contact_number=contact_number).save()
                msg = 'Your profile saved successfully!'
        else:
            msg = 'Please fill the fileds and try again!'
        template_name='MyInformation.html'
        context = {'form' : form, 'msg':msg, 'lastupdate':last_update}
        return render(request, template_name, context)



@csrf_exempt
@login_required
def create_portfolio_visually(request):
    #bubble chart
        try:
            request.session['country'] = request.session['country'] 
        except:
            request.session['country'] = 'India'
        context={}
        country=request.session['country']
        screenerQuantObject = ScreenerQuantitative
        screenerFundObject = ScreenerFundamental
        screenerTechObject = ScreenerTechnical
        screenerRiskObject = ScreenerRisk
        if country=='India':
            input_df=pd.DataFrame(list(Screening.objects.values('fs_name','market_cap','sales','beta_1y','volatility_1y','pe','pb','roe','week_high_52','debt_equity','ev_sales','sales_growth','eps_growth','security_code','price')))
            nse_data=pd.DataFrame(list(Indexconstituents.objects.filter(flag='yes').values('lname','index_name').distinct()))
            bse_data=pd.DataFrame(list(IndexconstituentsBse.objects.filter(flag='yes').values('lname','index_name').distinct()))
            data=pd.concat([nse_data,bse_data])
            symbol = '₹'
        else:
            input_df=pd.DataFrame(list(UsScreening.objects.values('fs_name','market_cap','sales','beta_1y','volatility_1y','pe','pb','roe','week_high_52','debt_equity','ev_sales','sales_growth','eps_growth','security_code','price')))
            data=pd.DataFrame(list(UsIndexconstituents.objects.filter(flag='yes').values('lname','index_name').distinct()))
            screenerQuantObject = UsScreenerQuantitative
            screenerFundObject = UsScreenerFundamental
            screenerTechObject = UsScreenerTechnical
            screenerRiskObject = UsScreenerRisk
            symbol = '$'
        input_df.fillna(0,inplace=True)
     
        # input_df['52_week_high']=input_df['52_week_high'].apply(lambda x: round(x*100,2))

        input_df['week_high_52']=input_df['week_high_52'].round(0)

        input_df['price']=input_df['price'].round(0)

        input_df['sales_growth']=input_df['sales_growth'].apply(lambda x: round(x*100,2))
        input_df['eps_growth']=input_df['eps_growth'].apply(lambda x: round(x*100,2))

        input_df=input_df.to_json(orient='records')



        # data=pd.concat([nse_data,bse_data])
        data=pd.pivot(data,columns='index_name',values='lname')
        data.columns=[col.replace("&","") for col in data.columns]
        output={}
        for column in data.columns:
            output[column]=data[column].dropna().unique().tolist()
        

        quant_market_cap=screenerQuantObject.objects.values('quant_market_cap').exclude(quant_market_cap=None)
        quant_market_cap= ([y['quant_market_cap'] for y in quant_market_cap if y['quant_market_cap'] not in ['Large Cap','Mid Cap','Small Cap'] ] )

        quant_sales=screenerQuantObject.objects.values('quant_sales').exclude(quant_sales=None)
        quant_sales= ([y['quant_sales'] for y in quant_sales])
        quant_index=[]

        quant_index.extend(list(output.keys()))

        
        fund_pe=screenerFundObject.objects.values('pe').exclude(pe=None)
        fund_pe= ([y['pe'] for y in fund_pe])

        fund_pb=screenerFundObject.objects.values('pb').exclude(pb=None)
        fund_pb= ([y['pb'] for y in fund_pb])

        fund_ev_sales=screenerFundObject.objects.values('ev_sales').exclude(ev_sales=None)
        fund_ev_sales= ([y['ev_sales'] for y in fund_ev_sales])

        fund_debt_equity=screenerFundObject.objects.values('debt_equity').exclude(debt_equity=None)
        fund_debt_equity= ([y['debt_equity'] for y in fund_debt_equity])
        
        
        fund_sales_growth=screenerFundObject.objects.values('sales_growth').exclude(sales_growth=None)
        fund_sales_growth= ([y['sales_growth'] for y in fund_sales_growth])

        fund_eps_growth=screenerFundObject.objects.values('eps_growth').exclude(eps_growth=None)
        fund_eps_growth= ([y['eps_growth'] for y in fund_eps_growth])
        
        fund_roe=screenerFundObject.objects.values('roe').exclude(roe=None)
        fund_roe= ([y['roe'] for y in fund_roe])
    
        risk_beta=screenerRiskObject.objects.values('beta').exclude(beta=None)
        risk_beta= ([y['beta'] for y in risk_beta])

        risk_volatility=screenerRiskObject.objects.values('volatility').exclude(volatility=None)
        risk_volatility= ([y['volatility'] for y in risk_volatility])

        tech_price_52_week=screenerTechObject.objects.values('week_high_52').exclude(week_high_52=None)
        tech_price_52_week= ([y['week_high_52'] for y in tech_price_52_week])
    
        context['fund_eps_growth']=fund_eps_growth
        context['fund_sales_growth']=fund_sales_growth
        context['fund_debt_equity']=fund_debt_equity
        context['fund_ev_sales']=fund_ev_sales
        context['quant_market_cap']=quant_market_cap
        context['quant_sales']=quant_sales
        context['fund_pe']=fund_pe
        context['fund_pb']=fund_pb
        context['fund_roe']=fund_roe
        context['risk_beta']=risk_beta
        context['risk_volatility']=risk_volatility
        context['tech_price_52_week']=tech_price_52_week
        context['input_df']=input_df
        context['output']=output
        context['quant_index']=quant_index
        return render(request,'createPortfolioVisually.html',context)


@login_required
@csrf_exempt
@SubscriptionValidation
def saveNewVisualPortfolio(request):
    context = {}
    if request.method == 'POST': # Checking whether the request is POST or not.
        datastr = request.POST.get('stock_list')  # taking the newly added stock data as string from the client POST request
        datalist = json.loads(datastr) # De-Serializing the stock data
        portfolio_name = request.POST.get('portfolio_name')  # Taking the portfolio name from the client POST request
        try:
            request.session['country'] = request.session['country']
        except:
            request.session['country'] = 'India'
        security_model = SecurityMaster
        if request.session['country']=='US':
            security_model = UsSecurityMaster
        securities = security_model.objects.filter(fs_name__in=datalist).values('fs_name', 'gics')
        securityDF = pd.DataFrame(list(securities))
        filterstocks =', '.join('"{0}"'.format(w) for w in [datalist[i].replace("'","''") for i in range (0,len(datalist))] ).replace('"',"'")
        if request.session['country']=='US':
            defaultInvestment=5000
            query = '''select b.Company,a.factset_ticker,a.Price from US_Universe_Prices_Main a join (select Company,min(date) Date from US_Universe_Prices_Main 
                                            where date>='2019-01-01' and company in ('''+filterstocks+''') group by Factset_Ticker,Company) b on a.Company=b.Company and a.date=b.Date'''
        else:
            defaultInvestment=100000
            query = '''select b.Company,a.factset_ticker,a.Price from Universe_Prices_Main a join (select Company,min(date) Date from Universe_Prices_Main 
                    where date>='2019-01-01' and company in ('''+filterstocks+''') group by Factset_Ticker,Company) b on a.Company=b.Company and a.date=b.Date'''
        conn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server}; SERVER='+SERVER+'; PORT='+PORT+'; DATABASE='+DATABASE+';UID='+UID+';PWD='+PWD+';') 
        pricesDF = pd.read_sql(query,conn)
        pricesDF.columns = ['company','ticker','price']
        stocksDF = pd.merge(securityDF, pricesDF, how='inner', left_on='fs_name', right_on='company')
        stocksDF['quantity'] = (defaultInvestment/stocksDF['price']).astype(int)
        conn.close()
        if not stocksDF.empty:  # Checking if newly added stock data is exists or not
            try:
                if CustomerPortfolioDetails.objects.filter(customer_name=request.user.username,country=request.session['country'], portfolio_name=portfolio_name).count() == 0: # Checking if the portfolio name already exists or not
                    for index, row in stocksDF.iterrows():     # Iterating the stocks (if more than one stock is adding at a time)
                        if row['quantity']:
                            if CustomerPortfolioDetails.objects.filter(customer_name=request.user.username,country=request.session['country'], portfolio_name=portfolio_name,company_name=row['fs_name']).count() == 0: # To avoid the duplicates in portfolio database
                                CustomerPortfolioDetails(customer_name=request.user.username, portfolio_name=portfolio_name, company_name=row['fs_name'],
                                quantity=int(row['quantity']), price=row['price'],sector_name=row['gics'],model_portfolio='no', portfolio_type='General',
                                country=request.session['country'], fs_ticker=row['ticker']).save()  # Saving the stock
                        else:
                            print("Invalid quantity!!!")
                    CustomerPortfolioLog(customer_name=request.user.username,portfolio_name=portfolio_name,last_action='portfolio_created',created_at=timezone.now(),status='active').save()
                    context['res_Status'] = True
                    context['portfolio'] = portfolio_name
                    context['username'] = request.user.username
                    createCacheForNewPortfolio(request.user.username, request.session['country'], portfolio_name)
                    context['res_Msg'] = 'Your Portfolio has been saved successfully!'
                    return HttpResponse(json.dumps(context), content_type='application/json')
                else:  # If the portfolio name already exists, raise exception
                    raise PortfolioExistsError('Portfolio name is already taken!')
            # Handling the exceptions
            except PortfolioExistsError as p_error:  
                context['res_Status'] = False
                context['portfolio'] = portfolio_name
                context['res_Msg'] = p_error.msg
                return HttpResponse(json.dumps(context), content_type='application/json')
            except:
                context['res_Status'] = False
                context['portfolio'] = portfolio_name
                context['res_Msg'] = 'Something went wrong, try again!'
                return HttpResponse(json.dumps(context), content_type='application/json')
        else:
            context['res_Status'] = False
            context['portfolio'] = portfolio_name
            context['res_Msg'] = 'Something went wrong, try again!'
            return HttpResponse(json.dumps(context), content_type='application/json')
    else:
        context['res_Status'] = False
        context['res_Msg'] = 'Invalid input, try again!'
    return HttpResponse(json.dumps(context), content_type='application/json')



            
def SiteMap(request):
    return render (request, 'sitemap.xml', content_type='text/xml')


def screener_dummy(request):
    country=request.session['country']
    screenerQuantObject = ScreenerQuantitative
    screenerFundObject = ScreenerFundamental
    screenerTechObject = ScreenerTechnical
    screenerRiskObject = ScreenerRisk


    if country=='India':
        input_df=Screening.objects.all()
        nse_data=pd.DataFrame(list(Indexconstituents.objects.filter(flag='yes').values('lname','index_name').distinct()))
        bse_data=pd.DataFrame(list(IndexconstituentsBse.objects.filter(flag='yes').values('lname','index_name').distinct()))
        data=pd.concat([nse_data,bse_data])
    else:
        input_df=Screening.objects.all()
        data=pd.DataFrame(list(UsIndexconstituents.objects.filter(flag='yes').values('lname','index_name').distinct()))
        screenerQuantObject = UsScreenerQuantitative
        screenerFundObject = UsScreenerFundamental
        screenerTechObject = UsScreenerTechnical
        screenerRiskObject = ScreenerRisk

    
    data=pd.pivot(data,columns='index_name',values='lname')
    data.columns=[col.replace("&","") for col in data.columns]
    output={}
    for column in data.columns:
        output[column]=data[column].dropna().unique().tolist()
    
    quant_market_cap=ScreenerQuantitative.objects.values('quant_market_cap').exclude(quant_market_cap=None)
    quant_market_cap= ([y['quant_market_cap'] for y in quant_market_cap])

    quant_sales=ScreenerQuantitative.objects.values('quant_sales').exclude(quant_sales=None)
    quant_sales= ([y['quant_sales'] for y in quant_sales])

    quant_sector=ScreenerQuantitative.objects.values('quant_sector').exclude(quant_sector=None)
    quant_sector= ([y['quant_sector'] for y in quant_sector])

    quant_fo_stocks=ScreenerQuantitative.objects.values('quant_fo_stocks').exclude(quant_fo_stocks=None)
    quant_fo_stocks= ([y['quant_fo_stocks'] for y in quant_fo_stocks])

    quant_price=ScreenerQuantitative.objects.values('quant_price').exclude(quant_price=None)
    quant_price= ([y['quant_price'] for y in quant_price])

    quant_target_price=ScreenerQuantitative.objects.values('quant_target_price').exclude(quant_target_price=None)
    quant_target_price= ([y['quant_target_price'] for y in quant_target_price])

    quant_volume=ScreenerQuantitative.objects.values('quant_volume').exclude(quant_volume=None)
    quant_volume= ([y['quant_volume'] for y in quant_volume])
    # quant_index=quant_conditions['Quant_Index'].dropna().unique().tolist()
    quant_index=['Any']
    quant_index.extend(list(output.keys()))


    fund_pe=ScreenerFundamental.objects.values('pe').exclude(pe=None)
    fund_pe= ([y['pe'] for y in fund_pe])

    fund_pb=ScreenerFundamental.objects.values('pb').exclude(pb=None)
    fund_pb= ([y['pb'] for y in fund_pb])

    fund_ev_sales=ScreenerFundamental.objects.values('ev_sales').exclude(ev_sales=None)
    fund_ev_sales= ([y['ev_sales'] for y in fund_ev_sales])

    fund_ev_ebitda=ScreenerFundamental.objects.values('ev_ebitda').exclude(ev_ebitda=None)
    fund_ev_ebitda= ([y['ev_ebitda'] for y in fund_ev_ebitda])

    fund_div_yield=ScreenerFundamental.objects.values('div_yield').exclude(div_yield=None)
    fund_div_yield= ([y['div_yield'] for y in fund_div_yield])

    fund_sales_growth=ScreenerFundamental.objects.values('sales_growth_qoq').exclude(sales_growth_qoq=None)
    fund_sales_growth= ([y['sales_growth_qoq'] for y in fund_sales_growth])

    fund_sales_growth_3y=ScreenerFundamental.objects.values('sales_growth_3y_cagr').exclude(sales_growth_3y_cagr=None)
    fund_sales_growth_3y= ([y['sales_growth_3y_cagr'] for y in fund_sales_growth_3y])

    fund_eps_growth=ScreenerFundamental.objects.values('eps_growth_qoq').exclude(eps_growth_qoq=None)
    fund_eps_growth= ([y['eps_growth_qoq'] for y in fund_eps_growth])
    
    fund_eps_growth_3y=ScreenerFundamental.objects.values('eps_growth_3y_cagr').exclude(eps_growth_3y_cagr=None)
    fund_eps_growth_3y= ([y['eps_growth_3y_cagr'] for y in fund_eps_growth_3y])

    fund_roe=ScreenerFundamental.objects.values('roe').exclude(roe=None)
    fund_roe= ([y['roe'] for y in fund_roe])

    fund_roce=ScreenerFundamental.objects.values('roce').exclude(roce=None)
    fund_roce= ([y['roce'] for y in fund_roce])

    fund_fcf=ScreenerFundamental.objects.values('fcf_margin').exclude(fcf_margin=None)
    fund_fcf= ([y['fcf_margin'] for y in fund_fcf])

    fund_sales=ScreenerFundamental.objects.values('sales_total_assets').exclude(sales_total_assets=None)
    fund_sales= ([y['sales_total_assets'] for y in fund_sales])

    fund_net_debt_fcf=ScreenerFundamental.objects.values('net_debt_fcf').exclude(net_debt_fcf=None)
    fund_net_debt_fcf= ([y['net_debt_fcf'] for y in fund_net_debt_fcf])

    fund_net_debt_ebitda=ScreenerFundamental.objects.values('net_debt_ebitda').exclude(net_debt_ebitda=None)
    fund_net_debt_ebitda= ([y['net_debt_ebitda'] for y in fund_net_debt_ebitda])

    fund_debt_equity=ScreenerFundamental.objects.values('debt_equity').exclude(debt_equity=None)
    fund_debt_equity= ([y['debt_equity'] for y in fund_debt_equity])

    risk_beta=ScreenerRisk.objects.values('beta').exclude(beta=None)
    risk_beta= ([y['beta'] for y in risk_beta])

    risk_volatility=ScreenerRisk.objects.values('volatility').exclude(volatility=None)
    risk_volatility= ([y['volatility'] for y in risk_volatility])

    risk_score=ScreenerRisk.objects.values('piotroski_score').exclude(piotroski_score=None)
    risk_score= ([y['piotroski_score'] for y in risk_score])

    risk_avg_daily_volume=ScreenerRisk.objects.values('average_daily_traded_volume').exclude(average_daily_traded_volume=None)
    risk_avg_daily_volume= ([y['average_daily_traded_volume'] for y in risk_avg_daily_volume])

    risk_avg_shares_turnover=ScreenerRisk.objects.values('share_turnover').exclude(share_turnover=None)
    risk_avg_shares_turnover= ([y['share_turnover'] for y in risk_avg_shares_turnover])
    
    tech_rsi_30=ScreenerTechnical.objects.values('relative_strength_30d').exclude(relative_strength_30d=None)
    tech_rsi_30= ([y['relative_strength_30d'] for y in tech_rsi_30])

    tech_rsi_90=ScreenerTechnical.objects.values('relative_strength_90d').exclude(relative_strength_90d=None)
    tech_rsi_90= ([y['relative_strength_90d'] for y in tech_rsi_90])

    tech_volume_chg=ScreenerTechnical.objects.values('volume_change').exclude(volume_change=None)
    tech_volume_chg= ([y['volume_change'] for y in tech_volume_chg])

    tech_price_52_week=ScreenerTechnical.objects.values('price_vs_52_week_high').exclude(price_vs_52_week_high=None)
    tech_price_52_week= ([y['price_vs_52_week_high'] for y in tech_price_52_week])

    tech_price_50dma=ScreenerTechnical.objects.values('price_vs_50dma').exclude(price_vs_50dma=None)
    tech_price_50dma= ([y['price_vs_50dma'] for y in tech_price_50dma])

    tech_price_200dma=ScreenerTechnical.objects.values('price_vs_200dma').exclude(price_vs_200dma=None)
    tech_price_200dma= ([y['price_vs_200dma'] for y in tech_price_200dma])

    symbol = '₹'
    portfolioSet = set()
    stocks = CustomerPortfolioDetails.objects.filter(customer_name=request.user.username,country=request.session['country']).values('portfolio_name').order_by('portfolio_name').values()
    for stock in stocks:
        if not stock['portfolio_name'].endswith('-US'):
            portfolioSet.add(stock['portfolio_name'])
    portfoliocount=len(portfolioSet)
    context={'quant_market_cap':quant_market_cap,'quant_sales':quant_sales,'quant_sector':quant_sector,
            'quant_fo_stocks':quant_fo_stocks,'quant_price':quant_price,'quant_target_price':quant_target_price,
            'quant_volume':quant_volume,'quant_index':quant_index,    'fund_pe':fund_pe,'fund_pb':fund_pb,'fund_ev_sales':fund_ev_sales,'fund_ev_ebitda':fund_ev_ebitda,          
            'fund_div_yield':fund_div_yield,'fund_sales_growth':fund_sales_growth,'fund_sales_growth_3y':fund_sales_growth_3y,
            'fund_eps_growth':fund_eps_growth,'fund_eps_growth_3y':fund_eps_growth_3y,'fund_roe':fund_roe,
            'fund_roce':fund_roce,'fund_fcf':fund_fcf,'fund_sales':fund_sales,'fund_net_debt_fcf':fund_net_debt_fcf,
            'fund_net_debt_ebitda':fund_net_debt_ebitda,'fund_debt_equity':fund_debt_equity,'risk_beta':risk_beta,
            'risk_volatility':risk_volatility,'risk_piotroski_score':risk_score,'risk_avg_daily_volume':risk_avg_daily_volume,
            'risk_avg_shares_turnover':risk_avg_shares_turnover,'tech_rsi_30d':tech_rsi_30,'tech_rsi_90d':tech_rsi_90,
            'tech_volume_chg':tech_volume_chg,'tech_price_52_week':tech_price_52_week,
            'tech_price_50dma':tech_price_50dma,'tech_price_200dma':tech_price_200dma,
            'companies':input_df,'index_companies':output,'defaultInvestmt':100000,'symbol':symbol,
            'portfoliocount':portfoliocount,'portfolios':portfolioSet,'flag':1 }
    return render(request,'screener_dummy.html',context)


def getGicsValues(filterCondition):
    output=None
    gics_values = {'Cyclical':['Materials','Finance','Consumer Discretionary','Realty'],
                    'Defensive':['Health Care','Utilities','Consumer Staples'],
                    'Sensitive':['Telecom','Information Technology','Industrials','Energy']}
    if 'Cyclical' in filterCondition:
        output=gics_values['Cyclical']
    elif 'Defensive' in filterCondition:
        output=gics_values['Defensive']
    elif 'Sensitive' in filterCondition:
        output=gics_values['Sensitive']
    else:
        output=[filterCondition]
    return output

def getIndexCompanies(condition,country):
    if country=='India':
        nse_data=pd.DataFrame(list(Indexconstituents.objects.filter(flag='yes').values('lname','index_name').distinct()))
        bse_data=pd.DataFrame(list(IndexconstituentsBse.objects.filter(flag='yes').values('lname','index_name').distinct()))
        data=pd.concat([nse_data,bse_data])
    else:
        data = pd.DataFrame(list(UsIndexconstituents.objects.filter(flag='yes').values('lname','index_name').distinct()))
    data=data[data['index_name'].str.replace("&","")==condition]
    return list(data['lname'].unique())

def FormatScreenerData(inputdata):

    def multiply_by_100(number):
        return round(number*100,2)
    def round_zero(number): 
        return round(number,0)
    def round_two(number): 
        return round(number,2)
    def addper_sym(number):
        return round(number,2)

    inputdata['piotroski_score']=inputdata['piotroski_score'].fillna(0).astype(int)
    inputdata['f_o'].fillna("No",inplace=True)          
    inputdata[['market_cap','price','sales','piotroski_score','volume','volume_20']] = inputdata[['market_cap','price','sales','piotroski_score','volume','volume_20']].apply(round_zero)
    inputdata[['beta_1y','pe','pb','sales_total_assets','net_debt_ebitda','debt_equity','ev_sales','ev_ebitda','net_debt_fcf']] = inputdata[['beta_1y','pe','pb','sales_total_assets','net_debt_ebitda','debt_equity','ev_sales','ev_ebitda','net_debt_fcf']].apply(round_two)
    inputdata[['div_yield','sales_growth','eps_growth','fcf_margin','rsi_30','rsi_90']] =inputdata[['div_yield','sales_growth','eps_growth','fcf_margin','rsi_30','rsi_90']].apply(multiply_by_100) 
    inputdata[['volatility_1y','sales_growth_3y_5r_cagr','eps_growth_3y_5r_cagr','roe','roce']]= inputdata[['volatility_1y','sales_growth_3y_5r_cagr','eps_growth_3y_5r_cagr','roe','roce']].apply(addper_sym)
    inputdata['target_price']=inputdata['target_price'].round(1)
    inputdata['volume_change']=inputdata['volume_change'].round(1)
    inputdata['share_turnover']=inputdata['share_turnover'].round(1)
    inputdata['rsi']=inputdata['rsi'].round(2)
    inputdata['ma50']=inputdata['ma50'].round(0)
    inputdata['ma200']=inputdata['ma200'].round(0)
    inputdata['week_high_52']=inputdata['week_high_52'].round(0)
    return inputdata

import pandas as pd
import locale as lc

@login_required
@csrf_exempt
@SubscriptionValidation
def screener(request):
    if request.method == 'POST':
        import re
        country = request.session['country']
        filterList = request.POST.get('filters_list')
        filterList=json.loads(filterList)
        screeningObjects={'India':Screening,'US':UsScreening}
        screener_data=pd.DataFrame(list(screeningObjects[country].objects.values()))
        screener_data = FormatScreenerData(screener_data)
        if len(filterList)>0:            
            min_value,max_value,category_value=None,None,None
            greaterthanequal_strings=['Above','above','Over','over','>']
            lessthanequal_strings=['Below','below','Under','under','<','Negative']                                         
            columns_check={ 'quant_market_cap':'market_cap','quant_sales':'sales','quant_sector':'gics','quant_fo_stocks':'f_o',
                            'quant_price':'price','quant_target_price':'target_price','quant_index':'fs_name','quant_volume':'volume',

                            'fund_pe':'pe','fund_pb':'pb','fund_ev_sales':'ev_sales','fund_ev_ebitda':'ev_ebitda','fund_div_yield':'div_yield','fund_sales_growth':'sales_growth',
                            'fund_sales_growth_3y':'sales_growth_3y_5r_cagr','fund_eps_growth':'eps_growth','fund_eps_growth_3y':'eps_growth_3y_5r_cagr',
                            'fund_roe':'roe','fund_roce':'roce','fund_fcf':'fcf_margin','fund_sales':'sales_total_assets','fund_net_debt_fcf':'net_debt_fcf',
                            'fund_net_debt_ebitda':'net_debt_ebitda','fund_debt_equity':'debt_equity',
                            
                            'tech_rsi_30d':'rsi_30','tech_rsi_90d':'rsi_90','tech_volume_chg':'volume_change','tech_price_52_week':'week_high_52',
                            'tech_ma50':'ma50','tech_ma200':'ma200', 'tech_rsi':'rsi',
                            
                            'risk_beta':'beta_1y','risk_volatility':'volatility_1y','risk_piotroski_score':'piotroski_score','risk_avg_daily_volume':'volume_20',
                            'risk_avg_shares_turnover':'share_turnover'
                            }
            screener_data['f_o'].fillna("No",inplace=True)          
            # screener_data['ma50'].fillna(0,inplace=True)          
            # screener_data['ma200'].fillna(0,inplace=True)          

            filterList = [elem for elem in filterList if len(elem)!=0]
            for i in range(0,len(filterList)):
                key = filterList[i]['factor']
                condition  = filterList[i]['condition'].replace(",","").replace("50DMA","").replace("200DMA","")
                dynamic_column = columns_check[key]
                if (key in ['quant_volume','risk_avg_daily_volume']):
                    condition=condition.replace("lakh",'00000').replace("cr","0000000").replace("10k","10000").replace("100k","100000").replace("500k","500000").replace("1M","1000000").replace("2.5M","2500000").replace("5M","5000000")
                if key == 'quant_sector':
                    gicsvalues = getGicsValues(condition)
                    screener_data=screener_data[screener_data[dynamic_column].isin(gicsvalues)]
                elif key == 'quant_index':                
                    companies = getIndexCompanies(condition,country)
                    screener_data=screener_data[screener_data[dynamic_column].isin(companies)]
                elif key == 'quant_fo_stocks':
                    companies = getIndexCompanies(condition,country)
                    screener_data=screener_data[screener_data[dynamic_column]==condition]
                elif key == 'quant_market_cap'  and any(elem in condition for elem in ['Large Cap','Small Cap','Mid Cap']) and country=='India':
                    screener_data=screener_data[screener_data['market_cap_category'].str.contains(condition,na=False)]                
                else:
                    values = re.findall(r'-?\b\d+(?:\.\d+)?(?!\d)', condition)
                    if len(values)>0:
                        values =[float(val) for val in values]                                
                    if len(values)>1:
                        if key in ('quant_target_price','tech_ma50','tech_ma200'):
                            screener_data=screener_data[(screener_data[dynamic_column]>=screener_data['price'] + (screener_data['price'] * (values[0]/100))) & (screener_data[dynamic_column]<=(screener_data['price'] +screener_data['price'] * (values[1]/100)))]
                        else:   
                            screener_data=screener_data[(screener_data[dynamic_column]>=values[0]) & (screener_data[dynamic_column]<=values[1])]
                    else:
                        print(dynamic_column)
                        if any(elem in condition for elem in greaterthanequal_strings):
                            if len(values)>=0 and key in ('quant_target_price','tech_ma50','tech_ma200','tech_price_52_week'):
                                try:
                                    val=values[0]
                                except:
                                    val = 0                                
                                screener_data=screener_data[(screener_data[dynamic_column]>=screener_data['price'] + (screener_data['price'] * (val/100)))]                           
                            elif len(values)>0:
                                screener_data=screener_data[screener_data[dynamic_column]>=values[0]]
                        elif any(elem in condition for elem in lessthanequal_strings):
                            if len(values)>=0 and key in ('quant_target_price','tech_ma50','tech_ma200','tech_price_52_week'):
                                try:
                                    val=values[0]
                                except:
                                    val = 0
                                screener_data=screener_data[(screener_data[dynamic_column]<screener_data['price'] - (screener_data['price'] * (val/100)))]                            
                            elif len(values)>0:
                                screener_data=screener_data[screener_data[dynamic_column]<values[0]]                        
                        elif any(elem in ['None','Zero'] for elem in condition.split(" ")):
                            screener_data=screener_data[screener_data[dynamic_column]==0]
                        else:
                            if key in ('quant_target_price','tech_ma50','tech_ma200','tech_price_52_week'):
                                screener_data=screener_data[(screener_data[dynamic_column]==screener_data['price'] )]                           
                            else:
                                screener_data=screener_data[screener_data[dynamic_column]==values[0]]


        defaultInvestment =  {'India':100000,'US':5000}
        screener_data['quantity']  = np.ceil((defaultInvestment[country]/screener_data['price'])) 
        screener_data['quantity'] = "<input type='text' name='quantity' value='" + screener_data['quantity'].replace(np.inf,np.nan).replace(-np.inf,np.nan).fillna(0).astype(int).astype(str) +"'>"
        screener_data['check'] = "<input type='checkbox' class='stock'>"

        if country=="India":
            screener_data['market_cap']=screener_data['market_cap'].apply(lambda x:x if math.isnan(float(x)) else(format_currency(x,'',locale='en_IN')))
            screener_data['price']=screener_data['price'].apply(lambda x:x if math.isnan(float(x)) else (format_currency(x,'',locale='en_IN')))
            screener_data['target_price']=screener_data['target_price'].apply(lambda x:x if math.isnan(float(x)) else (format_currency(x,'',locale='en_IN')))
            screener_data['volume_20']=screener_data['volume_20'].apply(lambda x:x if math.isnan(float(x)) else (format_currency(x,'',locale='en_IN')))
            screener_data['sales']=screener_data['sales'].apply(lambda x:x if math.isnan(float(x)) else (format_currency(x,'',locale='en_IN')))
            screener_data['volume']=screener_data['volume'].apply(lambda x:x if math.isnan(float(x)) else (format_currency(x,'',locale='en_IN')))
        else:
            screener_data['market_cap']=screener_data['market_cap'].apply(lambda x:x if math.isnan(float(x)) else (format_currency(x,'',locale='en_US')))
            screener_data['price']=screener_data['price'].apply(lambda x:x if math.isnan(float(x)) else (format_currency(x,'',locale='en_US')))
            screener_data['target_price']=screener_data['target_price'].apply(lambda x:x if math.isnan(float(x)) else (format_currency(x,'',locale='en_US')))
            screener_data['volume_20']=screener_data['volume_20'].apply(lambda x:x if math.isnan(float(x)) else (format_currency(x,'',locale='en_US')))
            screener_data['sales']=screener_data['sales'].apply(lambda x:x if math.isnan(float(x)) else (format_currency(x,'',locale='en_US')))
            screener_data['volume']=screener_data['volume'].apply(lambda x:x if math.isnan(float(x)) else (format_currency(x,'',locale='en_US')))

        screener_data = screener_data.applymap(str)
        screener_data[['div_yield','sales_growth','eps_growth','fcf_margin','rsi_30','rsi_90','rsi']] =screener_data[['div_yield','sales_growth','eps_growth','fcf_margin','rsi_30','rsi_90','rsi']]+'%'
        screener_data[['volatility_1y','sales_growth_3y_5r_cagr','eps_growth_3y_5r_cagr','roe','roce']]= screener_data[['volatility_1y','sales_growth_3y_5r_cagr','eps_growth_3y_5r_cagr','roe','roce']]+'%'
        screener_data['volume_change']=screener_data['volume_change']+'%'
        screener_data['share_turnover']=screener_data['share_turnover']+'%'
        screener_data['rsi']=screener_data['rsi']+'%'
        screener_data['beta_1y']=screener_data['beta_1y'].replace("nan","0")
        screener_data['volatlity_1y']=screener_data['volatility_1y'].replace("nan%","0%")
        screener_data['price']=screener_data['price'].replace("nan","")
        screener_data['week_high_52']=screener_data['week_high_52'].replace("nan","")
        screener_data['fs_name'] = screener_data['fs_name']+" ("+screener_data['security_code']+")"
        screener_data=screener_data[['check','fs_name', 'security_code', 'gics', 
       'market_cap_category','market_cap','price','quantity', 'beta_1y', 'volatility_1y','sales',
       'pe', 'pb', 'div_yield','sales_growth','sales_growth_3y_5r_cagr','eps_growth',
       'eps_growth_3y_5r_cagr', 'roe', 'roce', 'fcf_margin',
       'sales_total_assets', 'net_debt_fcf', 'net_debt_ebitda', 'debt_equity',
        'ev_sales', 'ev_ebitda', 'piotroski_score',
        'rsi_30','rsi_90','volume_change','target_price','week_high_52', 'ma50', 
        'ma200','volume','rsi','volume_20',
        'share_turnover',  'f_o','fs_ticker']]
        users = screener_data.values.tolist()
        context={'users':users}
        return HttpResponse(json.dumps(context), content_type='application/json')
    else:
        screeningObjects={'India':Screening,'US':UsScreening}
        s1 = timezone.now()
        try:
            country=request.session['country']
        except:
            country='India'
            request.session['country'] = country
        symbols={'India':['₹',100000],'US':['$',5000]}
        symbol = symbols[country][0]
        default_investment = symbols[country][1]
        requiredTableObjects={'India':[ScreenerQuantitative,ScreenerFundamental,ScreenerTechnical,ScreenerRisk],
                                'US':[UsScreenerQuantitative,UsScreenerFundamental,UsScreenerTechnical,UsScreenerRisk]}
        screenerQuantObject = requiredTableObjects[country][0]
        screenerFundObject = requiredTableObjects[country][1]
        screenerTechObject = requiredTableObjects[country][2]
        screenerRiskObject = requiredTableObjects[country][3]
        if country=='India':
            nse_data=pd.DataFrame(list(Indexconstituents.objects.filter(flag='yes').values('lname','index_name').distinct()))
            bse_data=pd.DataFrame(list(IndexconstituentsBse.objects.filter(flag='yes').values('lname','index_name').distinct()))
            data=pd.concat([nse_data,bse_data])
        else:
            data=pd.DataFrame(list(UsIndexconstituents.objects.filter(flag='yes').values('lname','index_name').distinct()))
        data=pd.pivot(data,columns='index_name',values='lname')
        data.columns=[col.replace("&","") for col in data.columns]
        output={}
        for column in data.columns:
            output[column]=data[column].dropna().unique().tolist()
        quantOptions = screenerQuantObject.objects.all().exclude(quant_market_cap=None)
        quant_market_cap=screenerQuantObject.objects.values('quant_market_cap').exclude(quant_market_cap=None)
        quant_market_cap= ([y['quant_market_cap'] for y in quant_market_cap])
        quant_sales=screenerQuantObject.objects.values('quant_sales').exclude(quant_sales=None)
        quant_sales= ([y['quant_sales'] for y in quant_sales])
        quant_sector=screenerQuantObject.objects.values('quant_sector').exclude(quant_sector=None)
        quant_sector= ([y['quant_sector'] for y in quant_sector])
        quant_fo_stocks=screenerQuantObject.objects.values('quant_fo_stocks').exclude(quant_fo_stocks=None)
        quant_fo_stocks= ([y['quant_fo_stocks'] for y in quant_fo_stocks])
        quant_price=screenerQuantObject.objects.values('quant_price').exclude(quant_price=None)
        quant_price= ([y['quant_price'] for y in quant_price])
        quant_target_price=screenerQuantObject.objects.values('quant_target_price').exclude(quant_target_price=None)
        quant_target_price= ([y['quant_target_price'] for y in quant_target_price])
        quant_volume=screenerQuantObject.objects.values('quant_volume').exclude(quant_volume=None)
        quant_volume= ([y['quant_volume'] for y in quant_volume])
        quant_index=['Any']
        quant_index.extend(list(output.keys()))
        fund_pe=screenerFundObject.objects.values('pe').exclude(pe=None)
        fund_pe= ([y['pe'] for y in fund_pe])
        fund_pb=screenerFundObject.objects.values('pb').exclude(pb=None)
        fund_pb= ([y['pb'] for y in fund_pb])
        fund_ev_sales=screenerFundObject.objects.values('ev_sales').exclude(ev_sales=None)
        fund_ev_sales= ([y['ev_sales'] for y in fund_ev_sales])
        fund_ev_ebitda=screenerFundObject.objects.values('ev_ebitda').exclude(ev_ebitda=None)
        fund_ev_ebitda= ([y['ev_ebitda'] for y in fund_ev_ebitda])
        fund_div_yield=screenerFundObject.objects.values('div_yield').exclude(div_yield=None)
        fund_div_yield= ([y['div_yield'] for y in fund_div_yield])
        fund_sales_growth=screenerFundObject.objects.values('sales_growth').exclude(sales_growth=None)
        fund_sales_growth= ([y['sales_growth'] for y in fund_sales_growth])
        fund_sales_growth_3y=screenerFundObject.objects.values('sales_growth_3y_cagr').exclude(sales_growth_3y_cagr=None)
        fund_sales_growth_3y= ([y['sales_growth_3y_cagr'] for y in fund_sales_growth_3y])
        fund_eps_growth=screenerFundObject.objects.values('eps_growth').exclude(eps_growth=None)
        fund_eps_growth= ([y['eps_growth'] for y in fund_eps_growth])
        fund_eps_growth_3y=screenerFundObject.objects.values('eps_growth_3y_cagr').exclude(eps_growth_3y_cagr=None)
        fund_eps_growth_3y= ([y['eps_growth_3y_cagr'] for y in fund_eps_growth_3y])
        fund_roe=screenerFundObject.objects.values('roe').exclude(roe=None)
        fund_roe= ([y['roe'] for y in fund_roe])
        fund_roce=screenerFundObject.objects.values('roce').exclude(roce=None)
        fund_roce= ([y['roce'] for y in fund_roce])
        fund_fcf=screenerFundObject.objects.values('fcf_margin').exclude(fcf_margin=None)
        fund_fcf= ([y['fcf_margin'] for y in fund_fcf])
        fund_sales=screenerFundObject.objects.values('sales_total_assets').exclude(sales_total_assets=None)
        fund_sales= ([y['sales_total_assets'] for y in fund_sales])
        fund_net_debt_fcf=screenerFundObject.objects.values('net_debt_fcf').exclude(net_debt_fcf=None)
        fund_net_debt_fcf= ([y['net_debt_fcf'] for y in fund_net_debt_fcf])
        fund_net_debt_ebitda=screenerFundObject.objects.values('net_debt_ebitda').exclude(net_debt_ebitda=None)
        fund_net_debt_ebitda= ([y['net_debt_ebitda'] for y in fund_net_debt_ebitda])
        fund_debt_equity=screenerFundObject.objects.values('debt_equity').exclude(debt_equity=None)
        fund_debt_equity= ([y['debt_equity'] for y in fund_debt_equity])
        risk_beta=screenerRiskObject.objects.values('beta').exclude(beta=None)
        risk_beta= ([y['beta'] for y in risk_beta])
        risk_volatility=screenerRiskObject.objects.values('volatility').exclude(volatility=None)
        risk_volatility= ([y['volatility'] for y in risk_volatility])
        risk_score=screenerRiskObject.objects.values('piotroski_score').exclude(piotroski_score=None)
        risk_score= ([y['piotroski_score'] for y in risk_score])
        risk_avg_daily_volume=screenerRiskObject.objects.values('average_daily_traded_volume').exclude(average_daily_traded_volume=None)
        risk_avg_daily_volume= ([y['average_daily_traded_volume'] for y in risk_avg_daily_volume])
        risk_avg_shares_turnover=screenerRiskObject.objects.values('share_turnover').exclude(share_turnover=None)
        risk_avg_shares_turnover= ([y['share_turnover'] for y in risk_avg_shares_turnover])
        tech_rsi_30=screenerTechObject.objects.values('relative_strength_30d').exclude(relative_strength_30d=None)
        tech_rsi_30= ([y['relative_strength_30d'] for y in tech_rsi_30])
        tech_rsi_90=screenerTechObject.objects.values('relative_strength_90d').exclude(relative_strength_90d=None)
        tech_rsi_90= ([y['relative_strength_90d'] for y in tech_rsi_90])
        tech_volume_chg=screenerTechObject.objects.values('volume_change').exclude(volume_change=None)
        tech_volume_chg= ([y['volume_change'] for y in tech_volume_chg])
        tech_price_52_week=screenerTechObject.objects.values('week_high_52').exclude(week_high_52=None)
        tech_price_52_week= ([y['week_high_52'] for y in tech_price_52_week])
        tech_50dma=screenerTechObject.objects.values('ma50').exclude(ma50=None)
        tech_50dma= ([y['ma50'] for y in tech_50dma])
        tech_200dma=screenerTechObject.objects.values('ma200').exclude(ma200=None)
        tech_200dma= ([y['ma200'] for y in tech_200dma])
        tech_rsi=screenerTechObject.objects.values('rsi_14d_field').exclude(rsi_14d_field=None)
        tech_rsi  =  ([y['rsi_14d_field'] for y in tech_rsi])

        portfolioSet = set()
        stocks = CustomerPortfolioDetails.objects.filter(customer_name=request.user.username,country=request.session['country']).values('portfolio_name').order_by('portfolio_name').values()
        for stock in stocks:
            portfolioSet.add(stock['portfolio_name'])
        from django.core.paginator import Paginator
        mcap_default={'India':500,'US':2000}
        user_list = screeningObjects[country].objects.filter(market_cap__gte=mcap_default[country]).values()
  
        screener_data = FormatScreenerData(pd.DataFrame(list(user_list)))

        # defaultInvestment =  {'India':100000,'US':5000}
        # screener_data['quantity']  = np.ceil((defaultInvestment[country]/screener_data['price'])) 
        # screener_data['quantity'] = "<input type='text' name='quantity' value='" + screener_data['quantity'].replace(np.inf,np.nan).replace(-np.inf,np.nan).fillna(0).astype(int).astype(str) +"'>"

        screener_data['price_quant'] = screener_data['price']
        screener_data = screener_data.applymap(str)
        if country=="India":
            screener_data['market_cap']=screener_data['market_cap'].apply(lambda x:x if math.isnan(float(x)) else(format_currency(x,'',locale='en_IN')))
            screener_data['price']=screener_data['price'].apply(lambda x:x if math.isnan(float(x)) else (format_currency(x,'',locale='en_IN')))
            screener_data['target_price']=screener_data['target_price'].apply(lambda x:x if math.isnan(float(x)) else (format_currency(x,'',locale='en_IN')))
            screener_data['volume_20']=screener_data['volume_20'].apply(lambda x:x if math.isnan(float(x)) else (format_currency(x,'',locale='en_IN')))
            screener_data['sales']=screener_data['sales'].apply(lambda x:x if math.isnan(float(x)) else (format_currency(x,'',locale='en_IN')))
            screener_data['volume']=screener_data['volume'].apply(lambda x:x if math.isnan(float(x)) else (format_currency(x,'',locale='en_IN')))
        else:
            screener_data['market_cap']=screener_data['market_cap'].apply(lambda x:x if math.isnan(float(x)) else (format_currency(x,'',locale='en_US')))
            screener_data['price']=screener_data['price'].apply(lambda x:x if math.isnan(float(x)) else (format_currency(x,'',locale='en_US')))
            screener_data['target_price']=screener_data['target_price'].apply(lambda x:x if math.isnan(float(x)) else (format_currency(x,'',locale='en_US')))
            screener_data['volume_20']=screener_data['volume_20'].apply(lambda x:x if math.isnan(float(x)) else (format_currency(x,'',locale='en_US')))
            screener_data['sales']=screener_data['sales'].apply(lambda x:x if math.isnan(float(x)) else (format_currency(x,'',locale='en_US')))
            screener_data['volume']=screener_data['volume'].apply(lambda x:x if math.isnan(float(x)) else (format_currency(x,'',locale='en_US')))

        # lambda x: 2 if np.isnan(x['Col1']) else 1, axis=1
        screener_data[['div_yield','sales_growth','eps_growth','fcf_margin','rsi_30','rsi_90','rsi']] =screener_data[['div_yield','sales_growth','eps_growth','fcf_margin','rsi_30','rsi_90','rsi']]+'%'
        screener_data[['volatility_1y','sales_growth_3y_5r_cagr','eps_growth_3y_5r_cagr','roe','roce']]= screener_data[['volatility_1y','sales_growth_3y_5r_cagr','eps_growth_3y_5r_cagr','roe','roce']]+'%'
        screener_data['volume_change']=screener_data['volume_change']+'%'
        screener_data['share_turnover']=screener_data['share_turnover']+'%'
        screener_data['beta_1y']=screener_data['beta_1y'].replace("nan","0")
        screener_data['volatlity_1y']=screener_data['volatility_1y'].replace("nan%","0%")
        screener_data['price']=screener_data['price'].replace("nan","")
 
        screener_data['fs_name'] = screener_data['fs_name']+" ("+screener_data['security_code']+")"
        screener_data=screener_data[['fs_name', 'security_code', 'gics', 
       'market_cap_category','market_cap','price','price_quant','beta_1y', 'volatility_1y','sales',
       'pe', 'pb', 'div_yield','sales_growth','sales_growth_3y_5r_cagr','eps_growth',
       'eps_growth_3y_5r_cagr', 'roe', 'roce', 'fcf_margin',
       'sales_total_assets', 'net_debt_fcf', 'net_debt_ebitda', 'debt_equity',
        'ev_sales', 'ev_ebitda', 'piotroski_score',
        'rsi_30','rsi_90','volume_change','target_price','week_high_52', 'ma50', 
        'ma200','volume','rsi','volume_20',
        'share_turnover',  'f_o','fs_ticker']]

        user_list = screener_data.to_dict(orient='records') 
        page = request.GET.get('page', 1)
        paginator = Paginator(user_list, len(user_list))
        try:
            users = paginator.page(page)
        except PageNotAnInteger:
            users = paginator.page(1)
        except EmptyPage:
            users = paginator.page(paginator.num_pages)
        invested_amt={'India':[100000,' In cr '],'US':[5000,'M']}
        context={'quant_market_cap':quant_market_cap,'quant_sales':quant_sales,'quant_sector':quant_sector,
                'quant_fo_stocks':quant_fo_stocks,'quant_price':quant_price,'quant_target_price':quant_target_price,
                'quant_volume':quant_volume,'quant_index':quant_index,    'fund_pe':fund_pe,'fund_pb':fund_pb,'fund_ev_sales':fund_ev_sales,'fund_ev_ebitda':fund_ev_ebitda,          
                'fund_div_yield':fund_div_yield,'fund_sales_growth':fund_sales_growth,'fund_sales_growth_3y':fund_sales_growth_3y,
                'fund_eps_growth':fund_eps_growth,'fund_eps_growth_3y':fund_eps_growth_3y,'fund_roe':fund_roe,
                'fund_roce':fund_roce,'fund_fcf':fund_fcf,'fund_sales':fund_sales,'fund_net_debt_fcf':fund_net_debt_fcf,
                'fund_net_debt_ebitda':fund_net_debt_ebitda,'fund_debt_equity':fund_debt_equity,'risk_beta':risk_beta,
                'risk_volatility':risk_volatility,'risk_piotroski_score':risk_score,'risk_avg_daily_volume':risk_avg_daily_volume,
                'risk_avg_shares_turnover':risk_avg_shares_turnover,'tech_rsi_30d':tech_rsi_30,'tech_rsi_90d':tech_rsi_90,
                'tech_volume_chg':tech_volume_chg,'tech_price_52_week':tech_price_52_week,
                'tech_ma200':tech_200dma,'tech_ma50':tech_50dma,'tech_rsi':tech_rsi,'index_companies':output,'defaultInvestmt':invested_amt[country][0],
                'symbol':symbol,'users':users,'mcap_in':invested_amt[country][1],'portfolios':portfolioSet}
        s2=timezone.now()
        return render(request,'screener.html',context)

@login_required
@csrf_exempt
@SubscriptionValidation
def technicalIndicators(request):
    lineColor=['#6610F2','#28a745','#02BCD4','#ffc107','#dc3545','#5A6268','#23272B']
    portfolio_name,portfolio_type=getPortfolio(request)
    country=request.session['country']
    global symbol
    symbols={'India':'₹','US':'$'}
    symbol=symbols[country]
    requiredTableObjects={'India':[UniversePricesMain,SecurityMaster,VolumeDataNew,MovingAverage],
                            'US':[UsUniversePricesMain,UsSecurityMaster,UsVolumeDataNew,UsMovingAverage]}
    universe_model=requiredTableObjects[country][0]
    security_model = requiredTableObjects[country][1]
    volume_model = requiredTableObjects[country][2]
    movingavg_model = requiredTableObjects[country][3]
    current_date= universe_model.objects.all().aggregate(Max('date'))['date__max']
    if request.method == 'POST':
        pre_one_year_date = current_date+timedelta(-365)
        required_value1 = request.POST['required_value']
        universe_prices = pd.DataFrame(list(universe_model.objects.filter(factset_ticker=required_value1,date__gte=pre_one_year_date).values('company', 'factset_ticker', 'date', 'price', 'return_field')))
        required_value = ""
        volumeData = VolumeData
        sql_conn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server}; SERVER='+SERVER+'; PORT='+PORT+'; DATABASE='+DATABASE+';UID='+UID+';PWD='+PWD+';') 
        if request.session['country']=='US':
            required_value = required_value1.lower().replace("-","_")
            required_df=pd.read_sql("select [date],factor,value from US_Volume_Data_New where Ticker='"+required_value1+"' order by date desc",sql_conn)
            candle_df=pd.read_sql("select [Date],[High],[Low],[Close],[Open] from [US_Average_True_Range] where FS_Ticker='"+required_value1+"'and Date >='"+str(pre_one_year_date)+"'  order by date asc",sql_conn)        
            macd_df=pd.read_sql("select [Date],[MACD Line],[Signal Line],[MACD Histogram] from [US_Moving_Average_Convergence_Divergence] where [FS_Ticker]='"+required_value1+"'and Date >='"+str(pre_one_year_date)+"'  order by date asc",sql_conn)        
            technical_indicators_df=pd.read_sql("select [Date],[Symbol],[Price],[Return],[Net_Change],[MA9],[MA9_Actions],[MA20],[MA20_Actions],[MA50],[MA50_Actions],[MA200],[MA200_Actions],[MA9_MA20_Value],[MA9_MA20_Actions],[MA20_MA50_Value],[MA20_MA50_Actions],[MA50_MA200_Value],[MA50_MA200_Actions],[RSI_Value],[RSI_Actions],[CCI_Value],[CCI_Actions],[Williams_Value],[Williams_Actions],[StochRSI_Value],[StochRSI_Actions],[Stochastics_Value],[Stochastics_Actions],[MACD_Value],[MACD_Actions],[ATR_Value],[ATR_Actions],[ADX_Value],[ADX_Actions],[Super_Trend_Value],[Super_Trend_Actions] ,[MFI_Value],[MFI_Actions],[PVO_Value],[PVO_Actions],[CMF_Value],[CMF_Actions],[Moving_Average_Rating],[Momentum_Rating],[Trend_Rating],[Volume_Rating],[Final_Rating]  from US_Technical_Indicators_Daily where FS_Ticker='"+required_value1+"'",sql_conn) 
        else:
            required_value = required_value1.replace("-IN", "_in")
            required_value = "number_"+required_value
            required_df=pd.read_sql("select [date],factor,value from volume_Data_new where Ticker='"+required_value1+"' order by date desc",sql_conn) 
            candle_df=pd.read_sql("select [Date],[High],[Low],[Close],[Open] from [Average_True_Range] where FS_Ticker='"+required_value1+"'and Date >='"+str(pre_one_year_date)+"'  order by date asc",sql_conn)        
            macd_df=pd.read_sql("select [Date],[MACD Line],[Signal Line],[MACD Histogram] from [Moving_Average_Convergence_Divergence] where [FS_Ticker]='"+required_value1+"'and Date >='"+str(pre_one_year_date)+"'  order by date asc",sql_conn)        
            technical_indicators_df=pd.read_sql("select [Date],[Symbol],[Price],[Return],[Net_Change],[MA9],[MA9_Actions],[MA20],[MA20_Actions],[MA50],[MA50_Actions],[MA200],[MA200_Actions],[MA9_MA20_Value],[MA9_MA20_Actions],[MA20_MA50_Value],[MA20_MA50_Actions],[MA50_MA200_Value],[MA50_MA200_Actions],[RSI_Value],[RSI_Actions],[CCI_Value],[CCI_Actions],[Williams_Value],[Williams_Actions],[StochRSI_Value],[StochRSI_Actions],[Stochastics_Value],[Stochastics_Actions],[MACD_Value],[MACD_Actions],[ATR_Value],[ATR_Actions],[ADX_Value],[ADX_Actions],[Super_Trend_Value],[Super_Trend_Actions] ,[MFI_Value],[MFI_Actions],[PVO_Value],[PVO_Actions],[CMF_Value],[CMF_Actions],[Moving_Average_Rating],[Momentum_Rating],[Trend_Rating],[Volume_Rating],[Final_Rating]  from Technical_Indicators_Daily where FS_Ticker='"+required_value1+"'",sql_conn) 
        sql_conn.close()
        candle_df['Date'] = candle_df['Date'].astype(str)
        macd_df['Date'] = macd_df['Date'].astype(str)
        technical_indicators_df['Date']=technical_indicators_df['Date'].astype(str)
        required_df.columns = ['Date', 'Factor',required_value1]
        required_df.dropna(subset=['Date', 'Factor',required_value1], inplace=True)
        required_df['Date'] = required_df['Date'].astype(str)
        required_df.sort_values(by=['Date'],ascending=[True],inplace=True)
        universe_prices['date'] = universe_prices['date'].astype(str)
        universe_prices.sort_values(by=['date'],ascending=[True],inplace=True)
        universe_prices=universe_prices[['date','price']]
        universe_prices.columns=['Date','Price']
        volumedf=(required_df[required_df['Factor']=='Volume'])[['Date',required_value1]]
        volumedf.columns=['Date','Volume']
        volumedf['Date'] = volumedf['Date'].astype(str)
        sql_conn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server}; SERVER='+SERVER+'; PORT='+PORT+'; DATABASE='+DATABASE+';UID='+UID+';PWD='+PWD+';')        
        if request.session['country']=='US':
            volume_20=(pd.read_sql("select * from US_Volume_20_Data where date>='" +str(pre_one_year_date)+"' and Ticker='" +required_value1+"'",sql_conn ))
        else:
            volume_20=(pd.read_sql("select * from Volume_20_Data where date>='" +str(pre_one_year_date)+"' and Ticker='" +required_value1+"'",sql_conn ))
        sql_conn.close()
        volume_20['Date'] = volume_20['Date'].astype(str)
        volume_20.sort_values(by='Date',inplace=True)
        volumedf.sort_values(by='Date',inplace=True)                              
        df = pd.merge(universe_prices,volumedf,on='Date',how='inner')
        df.sort_values(inplace=True,by='Date')
        output_volume=[{"type":"column","name":"Volume","data":df[['Date',"Volume"]].dropna().values.tolist()},{"type":"spline","name":"Volume_20Day","data":volume_20[['Date','Volume_20']].dropna().values.tolist(),"lineWidth": 2,'color':lineColor[0]}]
        #rsi
        rsi_df=(required_df[required_df['Factor']=='RSI'])[['Date',required_value1]]        
        rsi_df.columns=['Date','RSI']
        df_rsi = pd.merge(universe_prices,rsi_df,on='Date',how='inner')
        output_rsi=[{"type":"spline","name":"RSI","data":df_rsi[['Date',"RSI"]].dropna().values.tolist(),'color':"#972F97",'zIndex':9}]
        #volatility
        volatility_df=(required_df[required_df['Factor']=='Volatility'])[['Date',required_value1]]
        volatility_df=volatility_df.reset_index(drop=True)
        volatility_df.columns=['Date','Volatility']        
        df_volatility = pd.merge(universe_prices,volatility_df,on='Date',how='inner')
        output_volatility=[{"type":"spline","name":"Volatility","data":df_volatility[['Date',"Volatility"]].dropna().values.tolist(),"lineWidth":1.5,'color':"#23272B"}]
        #Price_Vs_moving_Avg
        price_df=pd.DataFrame(list(movingavg_model.objects.filter(factset_ticker=required_value1,date__gte=pre_one_year_date).values('date','ma9','ma20','ma26','ma50','ma100','ma200')))
        price_df.columns=['Date','MA9','MA20','MA26','MA50','MA100','MA200']
        price_df=price_df.reset_index(drop=True).sort_values(by='Date')
        price_df=price_df[['Date','MA9','MA20','MA26','MA50','MA100','MA200']]
        price_df.sort_values(by='Date',inplace=True)                   
        price_df.dropna(inplace=True)
        price_df['Date'] = price_df['Date'].astype(str).str[0:11]
        output_candle=[
            {"type":"candlestick","name":"Price","data":candle_df[["Date","Open","High","Low","Close"]].dropna().values.tolist()},
            {"type":"spline","name":"MA9","lineWidth": 2,'color':"#3366FF","data":price_df[['Date',"MA9"]].dropna().values.tolist()},
            {"type":"spline","name":"MA20","lineWidth": 2,'color':"#3B82AF","data":price_df[['Date',"MA20"]].dropna().values.tolist()},
            {"type":"spline","name":"MA26","lineWidth": 2,'color':"#FFC107","data":price_df[['Date',"MA26"]].dropna().values.tolist()},
            {"type":"spline","name":"MA50","lineWidth": 2,'color':"#B11817","data":price_df[['Date',"MA50"]].dropna().values.tolist()},
            {"type":"spline","name":"MA100","lineWidth": 2,'color':"#5A6268","data":price_df[['Date',"MA100"]].dropna().values.tolist()},
            {"type":"spline","name":"MA200","lineWidth": 2,'color':"#23272B","data":price_df[['Date',"MA200"]].dropna().values.tolist()}]
        output_macd=[{"type":"spline","name":"MACD Line","data":macd_df[['Date',"MACD Line"]].dropna().values.tolist(),"lineWidth": 1.5,'color':lineColor[0]},
    {"type":"spline","name":"Signal Line","data":macd_df[['Date',"Signal Line"]].dropna().values.tolist(),"lineWidth": 1.5,'color':"#046099"},
    {"type":"column","name":"MACD Histogram","data":macd_df[['Date',"MACD Histogram"]].dropna().values.tolist()}]
        technical_indicators_df.fillna("N/A",inplace=True)
        context={'technical_indicators_df':technical_indicators_df.values.tolist(),'output_volume':output_volume,'output_rsi':output_rsi,
        'output_candle':output_candle,'output_volatility':output_volatility,'output_macd':output_macd,'max_price':candle_df['High'].max(),'min_price':candle_df['Low'].min(),
        'max_volume':df['Volume'].max(),'min_volume':df['Volume'].min(),'max_rsi':rsi_df['RSI'].max(),'min_rsi':rsi_df['RSI'].min(),
        'max_volatility':df_volatility['Volatility'].max(),'min_volatility':df_volatility['Volatility'].min(),
        'max_macd':max(macd_df['MACD Line'].max(),macd_df['Signal Line'].max()),'min_macd':min(macd_df['MACD Line'].min(),macd_df['Signal Line'].min())}
        return  HttpResponse(json.dumps(context), content_type="application/json")
    context={}
    dist_companies=pd.DataFrame(list(UniversePricesMain.objects.filter(price__isnull=True).values('company').distinct()))
    filters=list(dist_companies['company'])
    us_filters=[]
    us_dist_companies=pd.DataFrame(list(UsUniversePricesMain.objects.filter(price__isnull=True).values('company').distinct()))
    if len(us_dist_companies)>0:
        us_filters = list(us_dist_companies['company'])
    if(request.session['country']=='US'):
        us_companies = UsSecurityMaster.objects.exclude(fs_name__in=us_filters).values('fs_name','security_code','fs_ticker').distinct()
        us_companies = us_companies.order_by('fs_name')
        context['companies'] = us_companies
    else:
        companies = SecurityMaster.objects.exclude(fs_name__in=filters).filter(flag='yes').values('fs_name','security_code','fs_ticker').distinct()
        companies = companies.order_by('fs_name')
        context['companies'] = companies
    return render(request,'technicalIndicators.html',context)



class SendDailyMarketUpdateEmail(LoginRequiredMixin, View):

    def get(self, request, *args, **kwargs):
        objects = DailyMarketUpdateEmail.objects.filter(to_be_sent='YES').first()
        newobjects = DailyMarketUpdateEmail.objects.filter(to_be_sent='YES').values()[0]
        EmailData, temp = dict(), dict()
        for field in objects._meta.fields:
            temp[field.db_column] = newobjects[field.name]
        indices = ['Nifty 50','Nifty Midcap 100','Nifty Smallcap 100','Nifty 500','India VIX','DOW 30','S&P 500','Nasdaq','S&P 500 VIX','DAX','FTSE 100','CAC 40']
        sectors = ['Nifty Energy','Nifty Metal','Nifty PSU Bank','Nifty FMCG','Nifty Realty','Nifty Media','Nifty Infrastructure','Nifty Financial Services','Nifty Private Bank','Nifty Bank','Nifty Auto','Nifty IT', 'Nifty Pharma']
        template_name = 'notifications/72PIDailyMarketUpdate.html'
        return render(request, template_name)

    def post(self, request, *args, **kwargs):
        pass


@csrf_exempt
def treemap(request):
    sql_conn=pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server}; SERVER='+SERVER+'; PORT='+PORT+'; DATABASE='+DATABASE+';UID='+UID+';PWD='+PWD+';')
    if request.method == 'POST':
        selected_list = request.POST.get('required_value')
        selected_list = json.loads(selected_list)
        print(selected_list) 
        if request.session['country']=="US":
            col="Ticker"
            daily_stock_returns = pd.read_sql("select  * from US_Daily_Stock_Returns where "+selected_list[6]+"='Yes'",sql_conn)
        else:
            col="NSE_Symbol"
            daily_stock_returns = pd.read_sql("select  * from Daily_Stock_Returns where "+selected_list[6]+"='Yes'",sql_conn)
        sql_conn.close()
        daily_stock_returns[selected_list[0]] =  daily_stock_returns[selected_list[0]]*100
        daily_stock_returns=daily_stock_returns.fillna(0) 
        daily_stock_returns  = daily_stock_returns[[col,selected_list[0],selected_list[3],selected_list[4],selected_list[2]]]
        data = daily_stock_returns.groupby([selected_list[3],selected_list[4],col])[selected_list[2],selected_list[0]].sum().reset_index()
        gics_sub_group = daily_stock_returns.groupby([selected_list[3],selected_list[4]])[selected_list[2],selected_list[0]].sum().reset_index()
        gics_main_group  = daily_stock_returns.groupby([selected_list[3]])[selected_list[2],selected_list[0]].sum().reset_index()
        Final_output = data.copy()
        gics_sub_group.columns=[selected_list[3], selected_list[4], 'SUB_GICS_size', 'SUB_GICS_FrequencyPercentChange']
        gics_main_group.columns=[selected_list[3], 'GICS_size', 'GICS_FrequencyPercentChange']
        Final_output = Final_output.merge(gics_sub_group,on=[selected_list[3],selected_list[4]],how='inner')
        Final_output = Final_output.merge(gics_main_group,on=[selected_list[3]],how='inner')
        final_dict_list=[]
        gics = Final_output[selected_list[3]].unique().tolist()
        gics_list = []
        mcap=0
        oneLayer=[]
        colorRange=selected_list[1]
        twoLayer=[]
        subIndustry=[]
        for main_gics in gics:
            i=0
            gics_wise = Final_output[Final_output[selected_list[3]]==main_gics]
            gics_wise.reset_index(drop=True,inplace=True)
            sub_gics = gics_wise[selected_list[4]].unique().tolist()
            sub_gics_list=[]
            twoLayerList=[]
            subIndustryList=[]
            for each_gics in sub_gics:
                j=0
                sub_gics_wise = gics_wise[gics_wise[selected_list[4]]==each_gics]
                sub_gics_wise.reset_index(drop=True,inplace=True)
                sub_gics_companies_list=[]
                for k in range(0,len(sub_gics_wise)):
                    if sub_gics_wise.loc[k,selected_list[0]]>=colorRange[6]:
                        color='35764E'
                    elif sub_gics_wise.loc[k,selected_list[0]]>=colorRange[5]:
                        color='2F9E4F'
                    elif sub_gics_wise.loc[k,selected_list[0]]>=colorRange[4]:
                        color='30CC5A'
                    elif sub_gics_wise.loc[k,selected_list[0]]>=colorRange[3]:
                        color='414554'
                    elif sub_gics_wise.loc[k,selected_list[0]]>=colorRange[2]:
                        color='F63538'
                    elif sub_gics_wise.loc[k,selected_list[0]]>=colorRange[1]:
                        color='BF4045'
                    else:
                        color='8B444E'
                    if(sub_gics_wise.loc[k,selected_list[0]]>=0):
                        sub_gics_wise_dict={'label':sub_gics_wise.loc[k,col]+"<br/> +"+str(round(sub_gics_wise.loc[k,selected_list[0]],2))+"%",'value':sub_gics_wise.loc[k,selected_list[2]],'svalue':round(sub_gics_wise.loc[k,selected_list[0]],2),'fillcolor':color}
                    else:
                        sub_gics_wise_dict={'label':sub_gics_wise.loc[k,col]+"<br/>"+str(round(sub_gics_wise.loc[k,selected_list[0]],2))+"%",'value':sub_gics_wise.loc[k,selected_list[2]],'svalue':round(sub_gics_wise.loc[k,selected_list[0]],2),'fillcolor':color}
                    oneLayer.append(sub_gics_wise_dict)
                    twoLayerList.append(sub_gics_wise_dict)
                    subIndustryList.append(sub_gics_wise_dict)
                    sub_gics_companies_list.append(sub_gics_wise_dict)
                    mcap=mcap+sub_gics_wise.loc[k,selected_list[2]]
                    if sub_gics_wise.loc[j,selected_list[0]]>=colorRange[6]:
                        color='35764E'
                    elif sub_gics_wise.loc[j,selected_list[0]]>=colorRange[5]:
                        color='2F9E4F'
                    elif sub_gics_wise.loc[j,selected_list[0]]>=colorRange[4]:
                        color='30CC5A'
                    elif sub_gics_wise.loc[j,selected_list[0]]>=colorRange[3]:
                        color='414554'
                    elif sub_gics_wise.loc[j,selected_list[0]]>=colorRange[2]:
                        color='F63538'
                    elif sub_gics_wise.loc[j,selected_list[0]]>=colorRange[1]:
                        color='BF4045'
                    else:
                        color='8B444E'
                sub_gics_dict = { "fillcolor": color,'label':sub_gics_wise.loc[j,selected_list[4]],'value':sub_gics_wise.loc[j,'SUB_GICS_size'],'svalue':sub_gics_wise.loc[j,'SUB_GICS_FrequencyPercentChange']/len(sub_gics_wise),'data':sub_gics_companies_list}
                sub_gics_list.append(sub_gics_dict)
                subIndustry.append(sub_gics_dict)
                j=j+1
            gics_dict = { "fillcolor": "20261b",'label':gics_wise.loc[i,selected_list[3]],'value':gics_wise.loc[i,'GICS_size'],'svalue':gics_wise.loc[i,'GICS_FrequencyPercentChange'],'data':sub_gics_list}
            gics_list.append(gics_dict)
            gics_dict_twolayer = { "fillcolor": "20261b",'label':gics_wise.loc[i,selected_list[3]],'value':gics_wise.loc[i,'GICS_size'],'svalue':gics_wise.loc[i,'GICS_FrequencyPercentChange'],'data':twoLayerList}
            twoLayer.append(gics_dict_twolayer)
            i=i+1
        if(selected_list[5]=="0"):
            gics_list=oneLayer
        elif(selected_list[5]=="1" or selected_list[5]=="2" or selected_list[5]=="3"):
            gics_list=twoLayer
        elif  selected_list[5]=="4":
            gics_list=subIndustry
        context = {'gics_list':gics_list,'mcap':mcap}
        return HttpResponse(json.dumps(context), content_type='application/json')
    return render(request,'treemap.html')


@csrf_exempt
def getFiiDiiDaily(request):
    sql_conn=pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server}; SERVER='+SERVER+'; PORT='+PORT+'; DATABASE='+DATABASE+';UID='+UID+';PWD='+PWD+';')
    if request.method == 'POST':
        selected_list = request.POST.get('required_value')
        selected_list = json.loads(selected_list)
        daily_df = pd.read_sql("select Date,FII,DII,Nifty_500_Return from FII_DII_Data_Daily where  month([Date]) = "+selected_list[0]+" and year([Date]) = "+selected_list[1]+"order by Date Desc",sql_conn)
        sql_conn.close()
        daily_df['Date'] = pd.to_datetime(daily_df['Date'], format='%Y-%m-%d').dt.strftime('%d-%b-%Y')
        daily_df['Date']=daily_df['Date'].astype(str)
        daily_df['FII'] = daily_df['FII'].apply(lambda x:x if math.isnan(float(x)) else (format_currency(x,'',locale='en_IN')))
        daily_df['DII'] = daily_df['DII'].apply(lambda x:x if math.isnan(float(x)) else (format_currency(x,'',locale='en_IN')))
        daily_df['Nifty_500_Return'] = daily_df['Nifty_500_Return'].apply(lambda x:str(round(x*100,2))+'%')
        context={'daily_table':daily_df.values.tolist()}
    return HttpResponse(json.dumps(context), content_type='application/json')


def DailyMarketUpdate(request):
    return render(request,'DailyMarketUpdate.html')