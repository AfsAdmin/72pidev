from django.core.exceptions import ValidationError
from mysite.core.models import *
import re

def emailValidation(inputEmail):
    if inputEmail is None:
        raise  ValidationError('Enter your email id')
    else:
        regex = r'(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)'
        check = re.search(regex, inputEmail)
        if check is None:
            raise ValidationError('')
    return inputEmail


def contactValidation(inputContact):
    if inputContact == '':
        raise ValidationError('Enter your mobile number')
    else:
        regex = r'(?:(?:\+|0{0,2})91(\s*[\-]\s*)?|[0]?)?[6789]\d{9}$'
        check = re.search(regex, inputContact)
        if check is None:
            raise ValidationError('Enter valid 10 digit mobile number!')
    return inputContact

def panValidation(inputPan):
    if inputPan == '':
        raise ValidationError('Enter your mobile number')
    else:
        regex = r'[A-Za-z]{5}\d{4}[A-Za-z]{1}'
        check = re.search(regex, inputPan)
        if check is None:
            raise ValidationError('Enter valid pan number!')
    return inputPan

def gstValidation(inputGst):
    if inputGst == '':
        raise ValidationError('Enter your GST number')
    else:
        regex = r'^([0][1-9]|[1-2][0-9]|[3][0-7])([a-zA-Z]{5}[0-9]{4}[a-zA-Z]{1}[1-9a-zA-Z]{1}[zZ]{1}[0-9a-zA-Z]{1})+$'
        check = re.search(regex, inputGst)
        if check is None:
            raise ValidationError('Enter valid GST number!')
    return inputGst