import pandas as pd
import calendar
import pyodbc
# from .models import CustomerPortfolioDetails, UniversePricesMain, MarketIndex, RiskFreeRate, UsUniversePricesMain, UsMarketIndex
from datetime import date, datetime, timedelta
# from django.db.models import Avg, Max, Min, Sum

# maxUniverseDate = UniversePricesMain.objects.all().aggregate(Max('date'))['date__max']
def MonthlyCumulativeReturnsMain(Stock_Data, portfolio_name,country,daily_returns,index_data):
    try:
        Exposure_Data = Stock_Data.copy()
        Daily_Returns = daily_returns.copy()
        Exposure_Data = Exposure_Data[['Company','Factset_Ticker','Quantity']]
        Stocks_Data_with_Exposure = ExposureCalculation(Daily_Returns, Exposure_Data)
        Index_Ticker1="Nifty 50 Index" ; Index_Ticker2="Nifty Smallcap 100 Index" ; Index_Ticker3="Nifty Midcap 100 Index" 
        if country=='US':
            Index_Ticker1="S&P 500" ; Index_Ticker2="Russell 2000"; Index_Ticker3="Dow Jones Industrial Average"
        Exposure_Data = Stocks_Data_with_Exposure[["Company",'Factset_Ticker',"Date","Net"]]
        Fund_Daily_Returns = StockReturns(Daily_Returns, Exposure_Data)
        Fund_Daily_Returns.columns=["Date","Return"]
        Index1_Daily_Returns=IndexDailyReturns (Index_Ticker1, portfolio_name,country,index_data)
        Index2_Daily_Returns=IndexDailyReturns (Index_Ticker2, portfolio_name,country,index_data)
        Index3_Daily_Returns=IndexDailyReturns (Index_Ticker3, portfolio_name,country,index_data)
        Fund_Monthly_Cumulative_Returns = MonthlyCumulativeReturns(Fund_Daily_Returns)
        Index1_Monthly_Cumulative_Returns = MonthlyCumulativeReturns(Index1_Daily_Returns)
        Index2_Monthly_Cumulative_Returns = MonthlyCumulativeReturns(Index2_Daily_Returns)
        Index3_Monthly_Cumulative_Returns = MonthlyCumulativeReturns(Index3_Daily_Returns)
        Fund_Monthly_Cumulative_Returns.columns=["MonthDate_BOM","MonthDate_EOM",'Year','Month',"PNL"]
        Index1_Monthly_Cumulative_Returns.columns=["MonthDate_BOM","MonthDate_EOM",'Year','Month',Index_Ticker1]
        Index2_Monthly_Cumulative_Returns.columns=["MonthDate_BOM","MonthDate_EOM",'Year','Month',Index_Ticker2]
        Index3_Monthly_Cumulative_Returns.columns=["MonthDate_BOM","MonthDate_EOM",'Year','Month',Index_Ticker3]
        Monthly_Cumulative_Returns = pd.merge(Fund_Monthly_Cumulative_Returns,Index1_Monthly_Cumulative_Returns,on=["Year","Month"],how="inner")
        Monthly_Cumulative_Returns = pd.merge(Monthly_Cumulative_Returns,Index2_Monthly_Cumulative_Returns,on=["Year","Month"],how="inner")
        Monthly_Cumulative_Returns = pd.merge(Monthly_Cumulative_Returns,Index3_Monthly_Cumulative_Returns,on=["Year","Month"],how="inner")
        Monthly_Cumulative_Returns =  Monthly_Cumulative_Returns[["Year","Month","PNL",Index_Ticker1,Index_Ticker2,Index_Ticker3]]
        Monthly_Cumulative_Returns['Month'] = Monthly_Cumulative_Returns['Month'].apply(lambda x: calendar.month_abbr[x])
        Monthly_Cumulative_Returns.sort_values(by='Year',inplace=True)
        return Monthly_Cumulative_Returns,Fund_Monthly_Cumulative_Returns
    except Exception as e:
        return pd.DataFrame(),pd.DataFrame()
def ExposureCalculation(Daily_Returns, Exposure_Data):
    Daily_Returns = pd.merge(Daily_Returns,Exposure_Data,on = ['Company','Factset_Ticker'],how = 'inner')
    Daily_Returns["Net"] = Daily_Returns["Price"] * Daily_Returns["Quantity"]
    Daily_Returns = Daily_Returns[['Company','Factset_Ticker','Date','Net']]
    return Daily_Returns

def StockReturns(Daily_Returns, Exposure_Data):
    Exposure_Data = pd.merge(Exposure_Data,Daily_Returns, on=['Factset_Ticker','Company','Date'])                           
    Exposure_Data.fillna(0, inplace = True)
    Net_Data=Exposure_Data.groupby(['Date'])["Net"].agg('sum').reset_index()   
    Net_Data.columns = ['Date','Denom']
    Exposure_Data=pd.merge(Exposure_Data,Net_Data,on='Date',how='inner')
    Exposure_Data["Net%"] = Exposure_Data["Net"]/Exposure_Data['Denom']
    Exposure_Data['Date'] = pd.to_datetime(Exposure_Data['Date'])
    Exposure_Data['PNL%'] = Exposure_Data['Return'] *  Exposure_Data.groupby(['Company','Factset_Ticker'])['Net%'].shift(1)    
    PNL_data=Exposure_Data.groupby(['Date'])['PNL%'].agg('sum').reset_index()                         
    PNL_data.columns = ["Date","Daily Ret"]   
    Cumulative_Returns = PNL_data[["Date","Daily Ret"]] 
    return Cumulative_Returns 

def IndexDailyReturns (index_ticker, portfolio_name,country,Index_data):
    index_data = Index_data.copy()
    index_data = index_data[index_data['Company']==index_ticker]
    index_data.reset_index(drop=True,inplace=True)
    index_data.fillna(0, inplace = True)
    IndexDailyReturn = index_data[["Date","Company","Return"]]
    return IndexDailyReturn

def MonthlyCumulativeReturns(DailyReturns):
    Input_Data = DailyReturns.copy()
    Input_Data['Year'] = pd.DatetimeIndex(Input_Data['Date']).year
    Input_Data['Month'] = pd.DatetimeIndex(Input_Data['Date']).month
    Input_Data["MonthDate_BOM"] = Input_Data.groupby(["Year","Month"])['Date'].transform(min)
    Input_Data["MonthDate_EOM"] = Input_Data.groupby(["Year","Month"])['Date'].transform(max)      #retrieving min and max dates of month
    Input_Data["Return"] = Input_Data["Return"]+1
    Input_Data["Monthly Cumulative Return"] = Input_Data.groupby(['MonthDate_EOM','MonthDate_BOM'])["Return"].cumprod()-1
    Monthly_Cumulative_Returns = Input_Data.groupby(['MonthDate_BOM','MonthDate_EOM']).tail(1)                              #taking last day cumulative
    Monthly_Cumulative_Returns = Monthly_Cumulative_Returns[['MonthDate_BOM','MonthDate_EOM','Year','Month','Monthly Cumulative Return']]
    return Monthly_Cumulative_Returns

    
