from django.db import models
from mysite.core.models import *
import pandas as pd
from modelcluster.fields import ParentalKey
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required

from wagtail.core.models import Page, Orderable
from wagtail.core.fields import StreamField
from wagtail.core import blocks

from wagtail.admin.edit_handlers import FieldPanel, MultiFieldPanel, InlinePanel, StreamFieldPanel

from wagtail.images.edit_handlers import ImageChooserPanel
from wagtail.documents.models import Document
from wagtail.documents.edit_handlers import DocumentChooserPanel
from wagtail.documents.models import get_document_model

from wagtail.contrib.settings.models import BaseSetting, register_setting
from wagtail.contrib.routable_page.models import RoutablePageMixin, route

from wagtail.core import hooks
from django.shortcuts import redirect
from django.http import HttpResponse
from django.core.validators import MaxValueValidator, MinValueValidator


class HomePage(Page):
    '''def get_context(self, request):
        context = super().get_context(request)
        indexes=['Nifty 50 Index','Nifty Midcap 100 Index','Nifty Smallcap 100 Index']
        df=pd.DataFrame(list(MarketIndex.objects.filter(factset_ticker__in=indexes,date__gte=str('2019-01-01')).values().order_by('factset_ticker','date')))
        df['date']=df['date'].astype(str)
        df=df.dropna().reset_index(drop=True)
        nifty_df=df.loc[df['factset_ticker'] == indexes[0],['date','price','return_field','factset_ticker']].reset_index(drop=True) 
        nifty_mid_df=df.loc[df['factset_ticker'] == indexes[1],['date','price','return_field','factset_ticker']].reset_index(drop=True) 
        nifty_small_df=df.loc[df['factset_ticker'] == indexes[2],['date','price','return_field','factset_ticker']].reset_index(drop=True)  
        lineColor=['#02BCD4','#28a745','#6610F2','#ffc107','#dc3545']
        output1_df=[{"name":indexes[0],'lineWidth': 2,'color':lineColor[1],'data':nifty_df[['date','price']].values.tolist()}]
        nifty_df['IndexTicker'] = (nifty_df['return_field']+1).cumprod()-1
        nifty_mid_df['IndexTicker'] = (nifty_mid_df['return_field']+1).cumprod()-1
        nifty_small_df['IndexTicker'] = (nifty_small_df['return_field']+1).cumprod()-1
        output2_df=[{"name":indexes[0],'lineWidth': 2,'color':lineColor[1],'data':nifty_df[['date','IndexTicker']].values.tolist()},
                    {"name":indexes[1],"lineWidth": 2,'color':lineColor[2],'data':nifty_mid_df[['date','IndexTicker']].values.tolist()},
                    {"name":indexes[2],"lineWidth": 2,'color':lineColor[3],'data':nifty_small_df[['date','IndexTicker']].values.tolist()}]
        df2=pd.DataFrame(list(MacroData.objects.filter(date__gte=str('2019-01-01')).values()))
        df2.columns=["Date","Oil (Dollars per Barrel)","FX Rate","Interest Rate","Vix","Inflation","Gold","Id"]
        df2=df2.drop('Id',axis=1)
        df2['Date'] = df2['Date'].astype(str)
        gold_df=[{"name":'Gold','lineWidth': 2,'color':lineColor[0],'data':df2[['Date','Gold']].values.tolist()}]
        oil_df=[{"name":'Oil','lineWidth': 2,'color':lineColor[2],'data':df2[['Date','Oil (Dollars per Barrel)']].values.tolist()}]
        fxrate_df=[{"name":'Fx Rate','lineWidth': 2,'color':lineColor[3],'data':df2[['Date','FX Rate']].values.tolist()}]
        inflation_df=[{"name":'Interest Rate','lineWidth': 2,'color':lineColor[4],'data':df2[['Date','Interest Rate']].values.tolist()}]
        nifty_data = pd.DataFrame(list(NiftyOutput.objects.values('ticker','jan','feb','mar','apr','may','ytd','number_2019')))
        nifty_data.columns = ["Ticker", "Jan-20", "Feb-20","Mar-20","Apr-20","May-20","YTD","2019"]
        # nifty_data = nifty_data.reindex([0,13,8,1,2,3,4,5,6,7,9,10,11,12])
        # nifty_data.reset_index(drop=True)
        # print(nifty_data)
        market_indices=nifty_data.loc[nifty_data['Ticker'].isin(['NIFTY 50','NIFTY Midcap 100','NIFTY SmallCap 100'])].reset_index(drop=True)
        sector_indices=nifty_data.loc[~nifty_data['Ticker'].isin(['NIFTY 50','NIFTY Midcap 100','NIFTY SmallCap 100'])].reset_index(drop=True)
        
        sector_factor_data = pd.DataFrame(list(FactorsNew.objects.values('gics','factor','date','value')))
        sector_factor_data.columns = ["Sector", "Factor", "Date","Value"]
        table2_header_list=sector_factor_data['Date'].unique()
        sub_sector_factor=(sector_factor_data.loc[sector_factor_data['Factor'].isin(['PE','PB'])]).reset_index(drop=True)
        sub_sector_factor['Value']=sub_sector_factor['Value'].apply(lambda x: round(x,2))
        table2_df=sub_sector_factor.groupby(['Sector','Date']).aggregate(lambda tdf: tdf.unique().tolist())
        
        dates=sub_sector_factor['Date'].unique().tolist()
        sector_list=sub_sector_factor['Sector'].unique()
        output_table_list=[]
        header_list=['']
        header=['Sector']

        for i in range(0,len(dates)):
            header.append(dates[i])
            header.append('')
            header_list.append('PE')
            header_list.append('PB')
        output_table_list.append(header_list)
        for i in range(0,len(sector_list)):
            x=[sector_list[i]]
            for j in range(0,len(dates)):
                pe_df=sub_sector_factor[(sub_sector_factor['Factor']=='PE')&(sub_sector_factor['Date']==dates[j])&(sub_sector_factor['Sector']==sector_list[i])].reset_index(drop=True)
                pb_df=sub_sector_factor[(sub_sector_factor['Factor']=='PB')&(sub_sector_factor['Date']==dates[j])&(sub_sector_factor['Sector']==sector_list[i])].reset_index(drop=True)
                if(len(pe_df)>0):
                    x.append(pe_df['Value'][0])
                else:
                    x.append('')
                if(len(pb_df)>0):
                    x.append(pb_df['Value'][0])
                else:
                    x.append('')
            output_table_list.append(x)

        nity_header_list = nifty_data.columns.tolist()
        market_indices = market_indices.values.tolist()
        sector_indices = sector_indices.values.tolist()

        dates.insert(0,'Sector')
        context={'header':dates,'output_table_list':output_table_list,'output1_df':output1_df,'output2_df':output2_df,'gold_values':gold_df,'oil_values':oil_df,'fxrate_values':fxrate_df,'inflation_values':inflation_df,
                'market_indices':market_indices,'sector_indices':sector_indices,'nity_header_list':nity_header_list}
        context['news'] = News.objects.child_of(self).live()
        context['blogs'] = Blogs.objects.child_of(self).live()
        context['market'] = Market.objects.child_of(self).live()
        context['documents'] = DocumentsBlog.objects.child_of(self).live()
        ideas = InvestmentIdeas.objects.child_of(self).live()
        longideas=[]
        shortideas=[]
        for idea in ideas:
            if idea.buy_sell=='BUY':
                templist=[idea.title,'BUY',idea.purchased_price,idea.target_price,idea.time_period]
                longideas.append(templist)
            else:
                templist=[idea.title,'SELL',idea.purchased_price,idea.target_price,idea.time_period]
                shortideas.append(templist)
        context['longideas']=longideas
        context['shortideas']=shortideas
        return context'''


class InvestmentIdeas(Page):
    choices=(('BUY','BUY'),('SELL','SELL'))
    buy_sell=models.CharField("BUY/SELL",max_length=4,choices=choices)
    purchased_price=models.CharField("Purchased Price(₹)",max_length=255)
    target_price=models.CharField("Target Price(₹)",max_length=255)
    time_period=models.CharField("Time Period",max_length=255)
    content_panels = Page.content_panels + [
        FieldPanel('buy_sell'),
        FieldPanel('purchased_price'),
        FieldPanel('target_price'),
        FieldPanel('time_period')
    ]    

class News(Page):
    short_description = models.TextField(blank=True, null=True)
    posted_at = models.DateTimeField("Posted At",auto_now_add=True,null=True)
    source = models.CharField(blank=True, null=True, max_length=255)
    body = StreamField([
        ('description', blocks.RichTextBlock(null=True)),
    ],null=True)
    image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )

    content_panels = Page.content_panels + [
        FieldPanel('short_description'),
        FieldPanel('source'),
        ImageChooserPanel('image'),
        StreamFieldPanel('body'),
    ]

class Blogs(Page):
    short_description = models.TextField(blank=True, null=True)
    posted_at = models.DateTimeField("Posted At",auto_now_add=True,null=True)
    source = models.CharField(blank=True, null=True, max_length=255)
    body = StreamField([
        ('blog_description', blocks.RichTextBlock(null=True)),
    ],null=True)
    content_panels = Page.content_panels + [
        FieldPanel('short_description'),
        FieldPanel('source'),
        StreamFieldPanel('body'),
            ]

    

class USInsights(Page):
    short_description = models.TextField(blank=True, null=True)
    posted_at = models.DateTimeField("Posted At",auto_now_add=True,null=True)
    source = models.CharField(blank=True, null=True, max_length=255)
    body = StreamField([
        ('blog_description', blocks.RichTextBlock(null=True)),
    ],null=True)
    content_panels = Page.content_panels + [
        FieldPanel('short_description'),
        FieldPanel('source'),
        StreamFieldPanel('body'),
        ]


class IndiaPortfolioPerformance(Page):
    action = models.CharField(max_length=5, choices=(('BUY','BUY'),('SELL','SELL'),('TOTAL', 'TOTAL')))
    signals_issued = models.PositiveIntegerField("No of signals issued", default=1, validators=[MinValueValidator(1)])
    win_percent = models.CharField("Win (%)", null=True, blank=True, max_length=5) #made this char field to allow -'s
    avg_gain_percent = models.CharField("Average Gain (%) Per Signal", null=True, blank=True, max_length=5) #made this char field to allow -'s
    avg_loss_percent = models.CharField("Average Loss (%) Per Signal", null=True, blank=True, max_length=5) #made this char field to allow -'s
    gainvsloss_percent = models.CharField("Overall Gain/Loss (%) Per Signal", null=True, blank=True, max_length=5) #made this char field to allow -'s

    content_panels = Page.content_panels + [
        FieldPanel('action'),
        FieldPanel('signals_issued'),
        FieldPanel('win_percent'),
        FieldPanel('avg_gain_percent'),
        FieldPanel('avg_loss_percent'),
        FieldPanel('gainvsloss_percent'),
        ]


class USPortfolioPerformance(Page):
    action = models.CharField(max_length=5, choices=(('BUY','BUY'),('SELL','SELL'),('TOTAL', 'TOTAL')))
    signals_issued = models.PositiveIntegerField("No of signals issued", default=1, validators=[MinValueValidator(1)])
    win_percent = models.CharField("Win (%)", null=True, blank=True, max_length=5) #made this char field to allow -'s
    avg_gain_percent = models.CharField("Average Gain (%) Per Signal", null=True, blank=True, max_length=5) #made this char field to allow -'s
    avg_loss_percent = models.CharField("Average Loss (%) Per Signal", null=True, blank=True, max_length=5) #made this char field to allow -'s
    gainvsloss_percent = models.CharField("Overall Gain/Loss (%) Per Signal", null=True, blank=True, max_length=5) #made this char field to allow -'s

    content_panels = Page.content_panels + [
        FieldPanel('action'),
        FieldPanel('signals_issued'),
        FieldPanel('win_percent'),
        FieldPanel('avg_gain_percent'),
        FieldPanel('avg_loss_percent'),
        FieldPanel('gainvsloss_percent'),
        ]


class ResearchReports(Page):
    gics = SecurityMaster.objects.filter(flag='yes').values_list('gics','gics').distinct()
    sectors = (gics)
    sector = models.CharField("Sector Name", max_length=255, choices=sectors,default='Industrials')
    diamond = models.CharField("diamond", max_length=5, choices=(('yes','Yes'),('no','No')),default='Yes')
    posted_at = models.DateTimeField("Posted At",auto_now=True,null=True)
    image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )
    book_file = models.ForeignKey(
        'wagtaildocs.Document',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )
    content_panels = Page.content_panels + [
        FieldPanel('sector'),
        FieldPanel('diamond'),
        ImageChooserPanel('image'),
        DocumentChooserPanel('book_file'),
    ]

    @hooks.register('before_serve_document')
    def serve_document(document, request):
        if document.file_extension != 'pdf':
            return  # Empty return results in the existing response
        response = HttpResponse(document.file.read(), content_type='application/pdf')
        response['Content-Disposition'] = 'filename="' + document.file.name.split('/')[-1] + '"'
        if request.GET.get('download', False) in [True, 'True', 'true']:
            response['Content-Disposition'] = 'attachment; ' + response['Content-Disposition']
        return response
   
class OffBeat(Page):
    short_description = models.TextField(blank=True, null=True)
    posted_at = models.DateTimeField("Posted At",auto_now_add=True,null=True)
    image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )
    reference = models.URLField("Reference Link", blank=True, null=True, unique=True)
    body = StreamField([
        ('description', blocks.RichTextBlock(null=True, balnk=True)),
    ],null=True, blank=True)
    content_panels = Page.content_panels + [
        FieldPanel('short_description'),
        FieldPanel('reference'),
        ImageChooserPanel('image'),
        StreamFieldPanel('body'),
    ]

    def get_reference_url(self):
        return self.reference

class MarketPoints(Page):
    posted_at = models.DateTimeField("Date",auto_now_add=True,null=True)
    body = StreamField([
        ('description', blocks.RichTextBlock(null=True)),
    ],null=True)
    content_panels = Page.content_panels + [
        StreamFieldPanel('body'),
    ]

class USMarketPoints(Page):
    posted_at = models.DateTimeField("Date",auto_now_add=True,null=True)
    body = StreamField([
        ('description', blocks.RichTextBlock(null=True)),
    ],null=True)
    content_panels = Page.content_panels + [
        StreamFieldPanel('body'),
    ]

    class Meta:
        db_table = 'USMarketPoints'

class TastyByte(Page):
    quote = models.TextField("Quote of the Day",blank=True, null=True)
    short_description = models.TextField("Short Description",blank=True, null=True)
    posted_at = models.DateTimeField("Posted At",auto_now_add=True,null=True)
    source = models.CharField("Source",blank=True, null=True, max_length=255)
    reference = models.URLField("Reference Link", unique=True, blank=True)
    content_panels = Page.content_panels + [
        FieldPanel('quote'),
        FieldPanel('short_description'),
        FieldPanel('source'),
        FieldPanel('reference'),
            ]
    
    def get_url(self):
        return self.reference


class NewBlogs(Page):
    short_description = models.TextField(blank=True, null=True)
    posted_at = models.DateTimeField("Posted At",auto_now_add=True,null=True)
    body = StreamField([
        ('description', blocks.RichTextBlock(null=True)),
    ],null=True)
    image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )
    content_panels = Page.content_panels + [
        FieldPanel('short_description'),
        ImageChooserPanel('image'),
        StreamFieldPanel('body'),
    ]

