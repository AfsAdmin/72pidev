from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from django.db.models import Avg, Max, Min, Sum
from django.db.models import Count, QuerySet
from django.contrib.auth.mixins import LoginRequiredMixin
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page
from django.views.generic import TemplateView, ListView
from django.views import View
import pandas as pd
import json
from mysite.core.models import *
from mysite.home.models import *
from django.utils import timezone
# from mysite.core.PortfolioReturns1 import *                      
from mysite.core.PortfolioReturns2 import * 
from mysite.core.Returns_by_stock_sector import *  
from mysite.core.Investment_amount_sector_mktcap import *          
from mysite.core.Live_Cumulative_Returns_Dashboard import *
# Userdefined exceptions 
from mysite.core.Exceptions import *
from django.db import connection
import pyodbc
from  more_itertools import unique_everseen
from datetime import datetime,date,timedelta
from mysite.core.Appconfigproperties import *
from mysite.core.Mixins import SubscriptionValidationMixin
from mysite.core.CacheBuilder import *
from babel.numbers import format_currency
# CMS views here
current_date= UniversePricesMain.objects.all().aggregate(Max('date'))['date__max']



class OurMarketView(LoginRequiredMixin,SubscriptionValidationMixin,TemplateView):
	template_name = 'ourmarketview.html'
	def get_context_data(self, **kwargs):
			context = super().get_context_data(**kwargs)
			months_list = list()
			marketPointsModel = MarketPoints
			try:
				if self.request.session['country']=='US':
					marketPointsModel = USMarketPoints
			except:
				self.request.session['country'] = 'India'
			marketPoints = marketPointsModel.objects.all().order_by('-posted_at')
			for market in marketPoints:
				if market.title not in months_list:
					months_list.append(market.title)
			context['points']=marketPoints
			context['months']=months_list
			return context


class TradeSignalsView(LoginRequiredMixin,SubscriptionValidationMixin,TemplateView):
	template_name = 'Portfolio72PI.html'

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		tradeStocks,output = list(),list()
		transactionsModel = PositionsInput
		universeModel = UniversePricesMain
		tradeIdeasModel = TradeIdeasPerformance
		tradeSignalsModel = TradeSignals
		securityModel = SecurityMaster
		symbol='₹'
		try:
			if self.request.session['country']=='US':
				transactionsModel = UsPositionsInput
				universeModel=UsUniversePricesMain
				tradeSignalsModel = USTradeSignals
				tradeIdeasModel = USTradeIdeasPerformance
				securityModel = UsSecurityMaster
				symbol='$'
		except:
			self.request.session['country']='India'
		tradesignals = tradeSignalsModel.objects.values()
		tradeSignalsDF = pd.DataFrame(list(tradesignals))
		tradeideas = tradeIdeasModel.objects.values().order_by('-issue_date')
		for idea in tradeideas:
			idea['new_company'] = str(idea['company_name_id'])
		tradeIdeasDF = pd.DataFrame(list(tradeideas))
		tradeIdeasDF2 =tradeIdeasDF['issue_date'].groupby(tradeIdeasDF['company_name_id']).min().reset_index()
		transactions = transactionsModel.objects.filter(customer='suresh').values('tradedate','action','price','secdescription').order_by('secdescription')
		transactionsDF = pd.DataFrame(list(transactions))
		transactionsDF.columns = ['tradedate','action','purchase_price','secdescription']
		tradeStocks = transactionsDF['secdescription'].unique()
		buyDF = transactionsDF[transactionsDF['action'].isin(['BUY','Buy','buy'])]
		sellDF = transactionsDF[transactionsDF['action'].isin(['SELL','Sell','sell'])]
		buyDF.columns =['tradedate','action','buy_price','secdescription']
		sellDF.columns =['tradedate','action','sell_price','secdescription']
		buyDF2 =pd.merge(tradeIdeasDF2,buyDF,how='left',left_on='company_name_id',right_on='secdescription')
		sellDF2 =pd.merge(tradeIdeasDF2,sellDF,how='left',left_on='company_name_id',right_on='secdescription')
		currentUniversePrices = universeModel.objects.filter(date= 
		universeModel.objects.all().aggregate(Max('date'))['date__max'],company__in=tradeStocks).values('company','price')
		gics = securityModel.objects.filter(fs_name__in=tradeStocks).values('fs_name','fs_ticker','gics','security_code')
		pricesDF = pd.DataFrame(list(currentUniversePrices))
		gicsDF = pd.DataFrame(list(gics))
		transactionsDF = pd.merge(transactionsDF,gicsDF, left_on='secdescription',right_on='fs_name',how='inner')
		tradeSignalsDF = pd.merge(tradeSignalsDF, gicsDF, how='inner', left_on='fs_ticker',right_on='fs_ticker')
		buyDF = pd.merge(buyDF, pricesDF, how='left', left_on='secdescription',right_on='company')
		additionDF = pd.merge(buyDF, gicsDF, left_on='secdescription',right_on='fs_name' )
		sellDF = pd.merge(sellDF, pricesDF, how='left', left_on='secdescription',right_on='company')
		exitDF = pd.merge(sellDF, gicsDF, left_on='secdescription',right_on='fs_name' )
		exitDF = pd.merge(exitDF,additionDF,how='inner', on='secdescription')
		exitDF = exitDF[['tradedate_x','action_x','buy_price','sell_price','secdescription','company_x','price_x','fs_name_x','gics_x','security_code_y']]
		exitDF.columns = ['tradedate','action','buy_price','sell_price','secdescription','company','price','fs_name','gics','security_code_y']
		exitCompanies = list(exitDF['secdescription'].unique())
		activeDF = additionDF[~additionDF['secdescription'].isin(exitCompanies)]
		context['tradesignals'] = tradeSignalsDF.T.to_dict()
		context['tradeideas'] = tradeideas
		context['activeIdeas'] = activeDF.T.to_dict()
		context['additionsIdeas'] = additionDF.T.to_dict()
		context['exitIdeas'] = exitDF.T.to_dict()
		context['symbol']=symbol
		return context


class MarketIndexPerformanceView(TemplateView):
	template_name = "IndexPerformance.html"
	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		main_df=getEODMarketDataFromCacheBuilder()
		main_df['date'] = main_df['date'].astype(str).str[0:11]
		bse_df=main_df.loc[main_df['ticker']=='BSE Sensex']
		nifty_df=main_df.loc[main_df['ticker']=='Nifty 50']
		sp_df=main_df.loc[main_df['ticker']=='S&P 500']
		russell_df=main_df.loc[main_df['ticker']=='Russell 2000']
		oil_df=main_df.loc[main_df['ticker']=='Oil']
		gold_df=main_df.loc[main_df['ticker']=='Gold']
		usd_df=main_df.loc[main_df['ticker']=='USD/INR']
		dow_df=main_df.loc[main_df['ticker']=='Dow Jones 30']
		gold_us_df=main_df.loc[main_df['ticker']=='US Gold']
		bse_data = bse_df[['date','adjusted_close']].values.tolist()
		nifty_data = nifty_df[['date','adjusted_close']].values.tolist()
		sp_data = sp_df[['date','adjusted_close']].values.tolist()
		russell_data = russell_df[['date','adjusted_close']].values.tolist()
		oil_data = oil_df[['date','adjusted_close']].values.tolist()
		gold_data = gold_df[['date','adjusted_close']].values.tolist()
		usd_data = usd_df[['date','adjusted_close']].values.tolist()
		dow_data = dow_df[['date','adjusted_close']].values.tolist()
		gold_us_data = gold_us_df[['date','adjusted_close']].values.tolist()
		context={'bse_data':bse_data,'nifty_data':nifty_data,'sp_data':sp_data,'russell_data':russell_data,'oil_data':oil_data,
		'gold_data':gold_data,'usd_data':usd_data,'dow_data':dow_data,'gold_us_data':gold_us_data}
		return context

class MarketView(TemplateView):
	template_name = 'Market.html'
	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		try:
			country = self.request.session['country']
		except:
			self.request.session['country'] = 'India'
		if(self.request.session['country']=='US'):
			indexes=['S&P 500','Russell 2000','Dow Jones Industrial Average']
			df=pd.DataFrame(list(UsMarketIndex.objects.filter(company__in=indexes,date__gte=str('2019-01-01')).values().order_by('date')))
			df['date']=df['date'].astype(str)
			df=df.dropna().reset_index(drop=True)
			sp_df=df.loc[df['company'] == indexes[0],['date','price','return_field','company']].reset_index(drop=True) 
			russell_df=df.loc[df['company'] == indexes[1],['date','price','return_field','company']].reset_index(drop=True) 
			dow_jones_df=df.loc[df['company'] == indexes[2],['date','price','return_field','company']].reset_index(drop=True)  
			lineColor=['#02BCD4','#28a745','#6610F2','#ffc107','#dc3545']
			output1_df=[{"name":indexes[0],'lineWidth': 2,'color':lineColor[1],'data':sp_df[['date','price']].values.tolist()}]
			sp_df['IndexTicker'] = (sp_df['return_field']+1).cumprod()-1
			russell_df['IndexTicker'] = (russell_df['return_field']+1).cumprod()-1
			dow_jones_df['IndexTicker'] = (dow_jones_df['return_field']+1).cumprod()-1
			output2_df=[{"name":indexes[0],'lineWidth': 2,'color':lineColor[1],'data':sp_df[['date','IndexTicker']].values.tolist()},
						{"name":indexes[1],"lineWidth": 2,'color':lineColor[2],'data':russell_df[['date','IndexTicker']].values.tolist()},
						{"name":indexes[2],"lineWidth": 2,'color':lineColor[3],'data':dow_jones_df[['date','IndexTicker']].values.tolist()}]
			df2=pd.DataFrame(list(UsMacroData.objects.filter(date__gte=str('2019-01-01')).values().order_by('date')))
			df2.columns=["Date","Oil (Dollars per Barrel)","FX Rate","Interest Rate","Vix","Inflation","Gold","Id"]
			df2=df2.drop('Id',axis=1)
			df2=df2.dropna().reset_index(drop=True)
			df2['Date'] = df2['Date'].astype(str)
			gold_df=[{"name":'Gold','lineWidth': 2,'color':lineColor[0],'data':df2[['Date','Gold']].sort_values(by=['Date']).values.tolist()}]
			oil_df=[{"name":'Oil','lineWidth': 2,'color':lineColor[2],'data':df2[['Date','Oil (Dollars per Barrel)']].sort_values(by=['Date']).values.tolist()}]
			fxrate_df=[{"name":'Fx Rate','lineWidth': 2,'color':lineColor[3],'data':df2[['Date','FX Rate']].sort_values(by=['Date']).values.tolist()}]
			inflation_df=[{"name":'Interest Rate','lineWidth': 2,'color':lineColor[4],'data':df2[['Date','Interest Rate']].sort_values(by=['Date']).values.tolist()}]
			us_index_data = pd.DataFrame(list(UsIndexPerformanceOutput.objects.values('ticker','jan','feb','mar','apr','may','jun','jul','aug','sep','oct','nov','dec','ytd','number_2019')))
			us_index_data.columns = ["Ticker", "Jan-20", "Feb-20","Mar-20","Apr-20","May-20","Jun-20","Jul-20","Aug-20","Sep-20","Oct-20","Nov-20","Dec-20","YTD","2019"]
			market_indices=us_index_data.loc[us_index_data['Ticker'].isin(['S&P 500','Russell 2000','Dow Jones 30'])].reset_index(drop=True).sort_values(by=['Ticker'],ascending=False)
			sector_indices=us_index_data.loc[~us_index_data['Ticker'].isin(['S&P 500','Russell 2000','Dow Jones 30'])].reset_index(drop=True)
			sector_indices = sector_indices.replace(np.nan, '', regex=True)
			nifty_header_list = us_index_data.columns.tolist()
			market_indices = market_indices.values.tolist()
			sector_indices = sector_indices.values.tolist()
			sp_schiller_df1 = pd.DataFrame(list(Snp500SchillerPriceToEarnings.objects.all().values())).sort_values(by=['date'], ascending=True)
			sp_schiller_df1['date'] = sp_schiller_df1['date'].astype(str)
			sp_schiller_df=sp_schiller_df1[['date','p_e']].dropna() 
			my_list=sp_schiller_df.values.tolist()
			sp_schiller_line={"name":"Schiller PE of S&P 500 ","data":my_list,'color':'orange','lineWidth':2,'zIndex':9}
			sp_pe_df1 = pd.DataFrame(list(Snp500TtmPriceToEarnings.objects.all().values())).sort_values(by=['date'], ascending=True)
			sp_pe_df1['date'] = sp_pe_df1['date'].astype(str)
			sp_pe_df=sp_pe_df1[['date','p_e']].dropna() 
			my_list1=sp_pe_df.values.tolist()
			sp_pe_line={"name":"Trailing 12M PE of S&P 500","data":my_list1,'color':'orange','lineWidth':2,'zIndex':9}
			cboe_df1 = pd.DataFrame(list(CboeVix.objects.all().values())).sort_values(by=['date'], ascending=True)
			cboe_df1['date'] = cboe_df1['date'].astype(str)
			cboe_df=cboe_df1[['date','vix']].dropna() 
			my_list2=cboe_df.values.tolist()
			cboe_line={"name":"CBOE Volatility Index","data":my_list2,'color':'orange','lineWidth':2,'zIndex':9}
			sp_schiller_df_avg=sp_schiller_df1['average'].unique()
			sp_schiller_df_1sd=sp_schiller_df1['number_1sd'].unique()
			sp_schiller_df_2sd=sp_schiller_df1['number_2sd'].unique()
			sp_schiller_df_3sd=sp_schiller_df1['number_3sd'].unique()
			sp_schiller_df_1sd_neg=sp_schiller_df1['field_1sd'].unique()
			sp_schiller_df_2sd_neg=sp_schiller_df1['field_2sd'].unique()
			sp_schiller_df_3sd_neg=sp_schiller_df1['field_3sd'].unique()
			sp_pe_df_avg=sp_pe_df1['average'].unique()
			sp_pe_df_1sd=sp_pe_df1['number_1sd'].unique()
			sp_pe_df_2sd=sp_pe_df1['number_2sd'].unique()
			sp_pe_df_3sd=sp_pe_df1['number_3sd'].unique()
			sp_pe_df_1sd_neg=sp_pe_df1['field_1sd'].unique()
			sp_pe_df_2sd_neg=sp_pe_df1['field_2sd'].unique()
			sp_pe_df_3sd_neg=sp_pe_df1['field_3sd'].unique()
			cboe_df_avg=cboe_df1['average'].unique()
			cboe_df_1sd=cboe_df1['number_1sd'].unique()
			cboe_df_2sd=cboe_df1['number_2sd'].unique()
			cboe_df_3sd=cboe_df1['number_3sd'].unique()
			cboe_df_1sd_neg=cboe_df1['field_1sd'].unique()
			cboe_df_2sd_neg=cboe_df1['field_2sd'].unique()
			cboe_df_3sd_neg=cboe_df1['field_3sd'].unique()
			table_df=pd.DataFrame(list(MarketUs.objects.all().values('returns','s_p_500','russell_2000','dow_jones_30','bse_sensex','nifty_50','oil','gold','usd_inr')))
			table_df.columns=['','S&P 500','Russell 2000','Dow Jones 30','BSE Sensex','Nifty 50','Oil ($/barrel)','Gold ($/oz)','USD/INR']
			market_header_list = table_df.columns.values.tolist()
			marketDate=list(set((pd.DataFrame(list(MarketUs.objects.all().values('max_date'))))['max_date'].astype(str)))
			context={'output1_df':output1_df,'output2_df':output2_df,'oil_values':oil_df,'gold_values':gold_df,'fxrate_values':fxrate_df,'inflation_values':inflation_df,
					'marketindices':market_indices,'sector_indices':sector_indices,'nifty_header_list':nifty_header_list,'marketDate':marketDate,'market_header_list':market_header_list,
					'cboe_line':cboe_line,'cboe_average_line':cboe_df_avg,'cboe_1st':cboe_df_1sd,'cboe_2nd':cboe_df_2sd,'cboe_3rd':cboe_df_3sd,'cboe_N1st':cboe_df_1sd_neg,'cboe_N2st':cboe_df_2sd_neg,'cboe_N3st':cboe_df_3sd_neg,
					'sp_schiller_line':sp_schiller_line,'sp_schiller_average_line':sp_schiller_df_avg,'sp_schiller_1st':sp_schiller_df_1sd,'sp_schiller_2nd':sp_schiller_df_2sd,'sp_schiller_3rd':sp_schiller_df_3sd,
					'sp_schiller_N1st':sp_schiller_df_1sd_neg,'sp_schiller_N2nd':sp_schiller_df_2sd_neg,'sp_schiller_N3rd':sp_schiller_df_3sd_neg,
					'sp_pe_line':sp_pe_line,'sp_pe_average_line':sp_pe_df_avg,'sp_pe_1st':sp_pe_df_1sd,'sp_pe_2nd':sp_pe_df_2sd,'sp_pe_3rd':sp_pe_df_3sd,'sp_pe_N1st':sp_pe_df_1sd_neg,
					'sp_pe_N2nd':sp_pe_df_2sd_neg,'sp_pe_N3rd':sp_pe_df_3sd_neg,
					'beer_line':[],'beer_average_line':[],'beer_1st':[],'beer_2nd':[],'beer_3rd':[],'beer_N1st':[],'beer_N2nd':[],'beer_N3rd':[],
					'pe_line':[],'pe_average_line':[],'pe_1st':[],'pe_2nd':[],'pe_3rd':[],'pe_N1st':[],'pe_N2nd':[],'pe_N3rd':[],
					'vix_line':[],'vix_average_line':[],'vix_1st':[],'vix_2nd':[],'vix_3rd':[],'vix_N1st':[],'vix_N2nd':[],
					'nifty_ma_line':[],'nifty_average_line':[],'nifty_1st':[],'nifty_2nd':[],'nifty_N1st':[],'nifty_N2nd':[],'output_table_list':[],'table_df':table_df.values.tolist()}
		else:
			indexes=['Nifty 50 Index','Nifty Midcap 100 Index','Nifty Smallcap 100 Index']
			df=pd.DataFrame(list(MarketIndex.objects.filter(factset_ticker__in=indexes,date__gte=str('2019-01-01')).values().order_by('date')))
			df['date']=df['date'].astype(str)
			df=df.dropna().reset_index(drop=True)
			nifty_df=df.loc[df['factset_ticker'] == indexes[0],['date','price','return_field','factset_ticker']].reset_index(drop=True) 
			nifty_mid_df=df.loc[df['factset_ticker'] == indexes[1],['date','price','return_field','factset_ticker']].reset_index(drop=True) 
			nifty_small_df=df.loc[df['factset_ticker'] == indexes[2],['date','price','return_field','factset_ticker']].reset_index(drop=True)  
			lineColor=['#02BCD4','#28a745','#6610F2','#ffc107','#dc3545']
			output1_df=[{"name":indexes[0],'lineWidth': 2,'color':lineColor[1],'data':nifty_df[['date','price']].values.tolist()}]
			nifty_df['IndexTicker'] = (nifty_df['return_field']+1).cumprod()-1
			nifty_mid_df['IndexTicker'] = (nifty_mid_df['return_field']+1).cumprod()-1
			nifty_small_df['IndexTicker'] = (nifty_small_df['return_field']+1).cumprod()-1
			output2_df=[{"name":indexes[0],'lineWidth': 2,'color':lineColor[1],'data':nifty_df[['date','IndexTicker']].values.tolist()},
						{"name":indexes[1],"lineWidth": 2,'color':lineColor[2],'data':nifty_mid_df[['date','IndexTicker']].values.tolist()},
						{"name":indexes[2],"lineWidth": 2,'color':lineColor[3],'data':nifty_small_df[['date','IndexTicker']].values.tolist()}]
			df2=pd.DataFrame(list(MacroData.objects.filter(date__gte=str('2019-01-01')).values()))
			df2.columns=["Date","Oil (Dollars per Barrel)","FX Rate","Interest Rate","Vix","Inflation","Gold","Id"]
			df2=df2.drop('Id',axis=1)
			df2=df2.dropna().reset_index(drop=True)
			df2['Date'] = df2['Date'].astype(str)
			gold_df=[{"name":'Gold','lineWidth': 2,'color':lineColor[0],'data':df2[['Date','Gold']].sort_values(by=['Date']).values.tolist()}]
			oil_df=[{"name":'Oil','lineWidth': 2,'color':lineColor[2],'data':df2[['Date','Oil (Dollars per Barrel)']].sort_values(by=['Date']).values.tolist()}]
			fxrate_df=[{"name":'Fx Rate','lineWidth': 2,'color':lineColor[3],'data':df2[['Date','FX Rate']].sort_values(by=['Date']).values.tolist()}]
			inflation_df=[{"name":'Interest Rate','lineWidth': 2,'color':lineColor[4],'data':df2[['Date','Interest Rate']].sort_values(by=['Date']).values.tolist()}]
			nifty_data = pd.DataFrame(list(NiftyOutput.objects.values('ticker','jan','feb','mar','apr','may','jun','jul','aug','sep','oct','nov','dec','ytd','number_2019')))
			nifty_data.columns = ["Ticker", "Jan-20", "Feb-20","Mar-20","Apr-20","May-20","Jun-20","Jul-20","Aug-20","Sep-20","Oct-20","Nov-20","Dec-20","YTD","2019"]
			market_indices=nifty_data.loc[nifty_data['Ticker'].isin(['NIFTY 50','NIFTY Midcap 100','NIFTY SmallCap 100'])].reset_index(drop=True)
			sector_indices=nifty_data.loc[~nifty_data['Ticker'].isin(['NIFTY 50','NIFTY Midcap 100','NIFTY SmallCap 100'])].reset_index(drop=True)
			sector_indices = sector_indices.replace(np.nan, '', regex=True)
			sector_factor_data = pd.DataFrame(list(FactorsNew.objects.values('gics','factor','date','value','date_new')))
			sector_factor_data.columns = ["Sector", "Factor", "Date","Value",'date_new']
			sector_factor_data= (sector_factor_data.sort_values(by=['date_new'], ascending=True)).reset_index(drop=True)
			sector_factor_data=sector_factor_data[ ["Sector", "Factor", "Date","Value"]]
			table2_header_list=list(unique_everseen(sector_factor_data["Date"].tolist()))
			sub_sector_factor=(sector_factor_data.loc[sector_factor_data['Factor'].isin(['PE','PB'])]).reset_index(drop=True)
			sub_sector_factor['Value']=sub_sector_factor['Value'].apply(lambda x: round(x,2))
			table2_df=sub_sector_factor.groupby(['Sector','Date']).aggregate(lambda tdf: tdf.unique().tolist())
			sub_sector_factor.sort_values(by=['Date','Sector'],inplace=True)
			sub_sector_factor=sub_sector_factor.reset_index(drop=True)
			dates=list(unique_everseen(sector_factor_data["Date"].tolist()))
			sector_list=sub_sector_factor['Sector'].unique()
			output_table_list=[]
			header_list=['']
			header=['Sector']
			for i in range(0,len(dates)):
				header.append(dates[i])
				header.append('')
				header_list.append('PE')
				header_list.append('PB')
			output_table_list.append(header_list)
			for i in range(0,len(sector_list)):
				x=[sector_list[i]]
				for j in range(0,len(dates)):
					pe_df=sub_sector_factor[(sub_sector_factor['Factor']=='PE')&(sub_sector_factor['Date']==dates[j])&(sub_sector_factor['Sector']==sector_list[i])].reset_index(drop=True)
					pb_df=sub_sector_factor[(sub_sector_factor['Factor']=='PB')&(sub_sector_factor['Date']==dates[j])&(sub_sector_factor['Sector']==sector_list[i])].reset_index(drop=True)
					pb_df = pb_df.replace(np.nan, '', regex=True)
					pe_df = pe_df.replace(np.nan, '', regex=True)
					if(len(pe_df)>0):
						x.append(pe_df['Value'][0])
					else:
						x.append('')
					if(len(pb_df)>0):
						x.append(pb_df['Value'][0])
					else:
						x.append('')
				output_table_list.append(x)
			nifty_header_list = nifty_data.columns.tolist()
			market_indices = market_indices.values.tolist()
			sector_indices = sector_indices.values.tolist()
			dates.insert(0,'Sector')
			beer_df1 = pd.DataFrame(list(BondEquityEarnings.objects.all().values())).sort_values(by=['date'], ascending=True)
			beer_df1['date'] = beer_df1['date'].astype(str)
			beer_df=beer_df1[['date','beer']]
			my_list=beer_df.values.tolist()
			beer_line={"name":"BEER","data":my_list,'color':'orange','lineWidth':2,'zIndex':9}
			pe_df1 = pd.DataFrame(list(PriceToEarnings.objects.all().values())).sort_values(by=['date'], ascending=True)
			pe_df1['date'] = pe_df1['date'].astype(str)
			pe_df=pe_df1[['date','p_e']]
			my_list1=pe_df.values.tolist()
			pe_line={"name":"P/E","data":my_list1,'color':'orange','lineWidth':2,'zIndex':9}
			vix_df1 = pd.DataFrame(list(IndiaVix.objects.all().values())).sort_values(by=['date'], ascending=True)
			vix_df1['date'] = vix_df1['date'].astype(str)
			vix_df=vix_df1[['date','vix']]
			my_list2=vix_df.values.tolist()
			vix_line={"name":"India Vix","data":my_list2,'color':'orange','lineWidth':2,'zIndex':9}
			nifty_ma_df1 = pd.DataFrame(list(Nifty500200Ma.objects.all().values()))
			nifty_ma_df1['date'] = nifty_ma_df1['date'].astype(str)
			nifty_ma_df1['field_of_nifty500_stocks_above_200ma'] = (nifty_ma_df1['field_of_nifty500_stocks_above_200ma']*100).round(2)
			nifty_ma_df1['average'] = (nifty_ma_df1['average']*100).round(2)
			nifty_ma_df1['number_1sd'] = (nifty_ma_df1['number_1sd']*100).round(2)
			nifty_ma_df1['number_2sd'] = (nifty_ma_df1['number_2sd']*100).round(2)
			nifty_ma_df1['field_1sd'] = (nifty_ma_df1['field_1sd']*100).round(2)
			nifty_ma_df1['field_2sd'] = (nifty_ma_df1['field_2sd']*100).round(2)
			nifty_ma_df=nifty_ma_df1[['date','field_of_nifty500_stocks_above_200ma']].sort_values(by=['date'], ascending=True)
			my_list3=nifty_ma_df.values.tolist()
			nifty_ma_line={"name":"Nifty500200Ma","data":my_list3,'color':'orange','lineWidth':2,'zIndex':9}
			beer_df_avg=beer_df1['average'].unique()
			beer_df_1sd=beer_df1['number_1sd'].unique()
			beer_df_2sd=beer_df1['number_2sd'].unique()
			beer_df_3sd=beer_df1['number_3sd'].unique()
			beer_df_1sd_neg=beer_df1['field_1sd'].unique()
			beer_df_2sd_neg=beer_df1['field_2sd'].unique()
			beer_df_3sd_neg=beer_df1['field_3sd'].unique()
			pe_df_avg=pe_df1['average'].unique()
			pe_df_1sd=pe_df1['number_1sd'].unique()
			pe_df_2sd=pe_df1['number_2sd'].unique()
			pe_df_3sd=pe_df1['number_3sd'].unique()
			pe_df_1sd_neg=pe_df1['field_1sd'].unique()
			pe_df_2sd_neg=pe_df1['field_2sd'].unique()
			pe_df_3sd_neg=pe_df1['field_3sd'].unique()
			vix_df_avg=vix_df1['average'].unique()
			vix_df_1sd=vix_df1['number_1sd'].unique()
			vix_df_2sd=vix_df1['number_2sd'].unique()
			vix_df_3sd=vix_df1['number_3sd'].unique()
			vix_df_1sd_neg=vix_df1['field_1sd'].unique()
			vix_df_2sd_neg=vix_df1['field_2sd'].unique()
			nifty_ma_df_avg=nifty_ma_df1['average'].unique()
			nifty_ma_df_1sd=nifty_ma_df1['number_1sd'].unique()
			nifty_ma_df_2sd=nifty_ma_df1['number_2sd'].unique()
			nifty_ma_df_1sd_neg=nifty_ma_df1['field_1sd'].unique()
			nifty_ma_df_2sd_neg=nifty_ma_df1['field_2sd'].unique()
			table_df=pd.DataFrame(list(MarketIndia.objects.all().values('returns','bse_sensex','nifty_50','s_p_500','russell_2000','oil','gold','usd_inr')))
			table_df.columns=['','BSE Sensex','Nifty 50','S&P 500','Russell 2000','Oil ($/barrel)','Gold (₹/gm)','USD/INR']
			market_header_list = table_df.columns.values.tolist()
			marketDate=list(set((pd.DataFrame(list(MarketIndia.objects.all().values('max_date'))))['max_date'].astype(str)))
			context={'header':dates,'output_table_list':output_table_list,'output1_df':output1_df,'output2_df':output2_df,'gold_values':gold_df,'oil_values':oil_df,'fxrate_values':fxrate_df,'inflation_values':inflation_df,
					'marketindices':market_indices,'sector_indices':sector_indices,'nifty_header_list':nifty_header_list,'marketDate':marketDate,'market_header_list':market_header_list,
					'beer_line':beer_line,'beer_average_line':beer_df_avg,'beer_1st':beer_df_1sd,'beer_2nd':beer_df_2sd,'beer_3rd':beer_df_3sd,'beer_N1st':beer_df_1sd_neg,'beer_N2nd':beer_df_2sd_neg,'beer_N3rd':beer_df_3sd_neg,
					'pe_line':pe_line,'pe_average_line':pe_df_avg,'pe_1st':pe_df_1sd,'pe_2nd':pe_df_2sd,'pe_3rd':pe_df_3sd,'pe_N1st':pe_df_1sd_neg,'pe_N2nd':pe_df_2sd_neg,'pe_N3rd':pe_df_3sd_neg,
					'vix_line':vix_line,'vix_average_line':vix_df_avg,'vix_1st':vix_df_1sd,'vix_2nd':vix_df_2sd,'vix_3rd':vix_df_3sd,'vix_N1st':vix_df_1sd_neg,'vix_N2nd':vix_df_2sd_neg,
					'nifty_ma_line':nifty_ma_line,'nifty_average_line':nifty_ma_df_avg,'nifty_1st':nifty_ma_df_1sd,'nifty_2nd':nifty_ma_df_2sd,'nifty_N1st':nifty_ma_df_1sd_neg,'nifty_N2nd':nifty_ma_df_2sd_neg,
					'cboe_line':[],'cboe_average_line':[],'cboe_1st':[],'cboe_2nd':[],'cboe_3rd':[],'cboe_N1st':[],'cboe_N2st':[],'cboe_N3st':[],
					'sp_schiller_line':[],'sp_schiller_average_line':[],'sp_schiller_1st':[],'sp_schiller_2nd':[],'sp_schiller_3rd':[],
					'sp_schiller_N1st':[],'sp_schiller_N2nd':[],'sp_schiller_N3rd':[],
					'sp_pe_line':[],'sp_pe_average_line':[],'sp_pe_1st':[],'sp_pe_2nd':[],'sp_pe_3rd':[],'sp_pe_N1st':[],
					'sp_pe_N2nd':[],'sp_pe_N3rd':[],'table_df':table_df.values.tolist()
					}
		return context


class CompanyWiseResearchView(LoginRequiredMixin,SubscriptionValidationMixin,TemplateView):
	template_name = 'ResearchPage.html'
	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		try:
			country = self.request.session['country']
		except:
			self.request.session['country'] = 'India'
		if(self.request.session['country']=='US'):
			context['documents']= ResearchReports.objects.none()
		else:
			context['documents'] = ResearchReports.objects.live().order_by('-posted_at')
		return context


class MarketNewsBlogsView(LoginRequiredMixin,SubscriptionValidationMixin,TemplateView):
	template_name = 'MarketNews.html'
	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		try:
			country = self.request.session['country']
		except:
			self.request.session['country'] = 'India'
		if self.request.session['country'] == 'US':
			IndexSectorBreakdown_df=pd.DataFrame(list(UsIndexSectorBreakdown.objects.all().values('sector','breakdown','date')))
			IndexSectorBreakdown_df['breakdown'] = (IndexSectorBreakdown_df['breakdown']*100).round(2)
			date=list(set(IndexSectorBreakdown_df['date'].astype(str)))
			indexsector_values=IndexSectorBreakdown_df[['sector','breakdown']].values.tolist()
			current_date = datetime.now()
			df=pd.DataFrame(list(UsMarketInsights.objects.filter(date__gte=str(current_date.year)+'-01-01').values()))
			df.columns=['Date','Company','Price','Return','s_no']
			indexes= ['S&P 500','Russell 2000','Dow Jones Industrial Average','Bitcoin','Gold','NASDAQ Composite']
			df['Date']=df['Date'].astype(str)
			df=df.dropna().reset_index(drop=True)
			sp_df=df.loc[df['Company'] == indexes[0],['Date','Price','Return','Company']].reset_index(drop=True)
			russell_df=df.loc[df['Company'] == indexes[1],['Date','Price','Return','Company']].reset_index(drop=True)
			dow_jones_df=df.loc[df['Company'] == indexes[2],['Date','Price','Return','Company']].reset_index(drop=True)
			bitcoin_df = df.loc[df['Company'] == indexes[3],['Date','Price','Return','Company']].reset_index(drop=True)
			gold_df = df.loc[df['Company'] == indexes[4],['Date','Price','Return','Company']].reset_index(drop=True)
			nasdaq_df = df.loc[df['Company'] == indexes[5],['Date','Price','Return','Company']].reset_index(drop=True)
			lineColor=['#02BCD4','#28A745','#6610F2','#FFC107','#DC3545','#A9A9A9']
			sp_df['IndexTicker'] = (sp_df['Return']+1).cumprod()-1
			russell_df['IndexTicker'] = (russell_df['Return']+1).cumprod()-1
			dow_jones_df['IndexTicker'] = (dow_jones_df['Return']+1).cumprod()-1
			bitcoin_df['IndexTicker'] = (bitcoin_df['Return']+1).cumprod()-1
			gold_df['IndexTicker'] = (gold_df['Return']+1).cumprod()-1
			nasdaq_df['IndexTicker'] = (nasdaq_df['Return']+1).cumprod()-1
			output2_df=[{"name":indexes[0],'lineWidth': 2,'color':lineColor[1],'data':(sp_df[['Date','IndexTicker']]).sort_values(by=['Date'], ascending=True).values.tolist()},
						{"name":indexes[1],"lineWidth": 2,'color':lineColor[2],'data':(russell_df[['Date','IndexTicker']]).sort_values(by=['Date'], ascending=True).values.tolist()},
						{"name":indexes[2],"lineWidth": 2,'color':lineColor[3],'data':(dow_jones_df[['Date','IndexTicker']]).sort_values(by=['Date'], ascending=True).values.tolist()},
						{"name":indexes[3],"lineWidth": 2,'color':lineColor[4],'data':(bitcoin_df[['Date','IndexTicker']]).sort_values(by=['Date'], ascending=True).values.tolist()},
						{"name":indexes[4],"lineWidth": 2,'color':lineColor[5],'data':(gold_df[['Date','IndexTicker']]).sort_values(by=['Date'], ascending=True).values.tolist()},
						{"name":indexes[5],"lineWidth": 2,'color':lineColor[0],'data':(nasdaq_df[['Date','IndexTicker']]).sort_values(by=['Date'], ascending=True).values.tolist()}
					]
			context['indexsector_values'] = indexsector_values
			context['date'] = date
			context['blogs'] = USInsights.objects.live().order_by('-posted_at')
			context['symbol']='$'
			context['market_performance']=output2_df
			context['daily_table']=[]
			context['monthly_table']=[]
		else:
			sql_conn=pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server}; SERVER='+SERVER+'; PORT='+PORT+'; DATABASE='+DATABASE+';UID='+UID+';PWD='+PWD+';')
			daily_df = pd.read_sql("select Date,FII,DII,Nifty_500_Return from FII_DII_Data_Daily where  month(date)=month(getdate()) order by Date Desc",sql_conn)
			daily_df=daily_df.fillna(0)
			daily_df['Date'] = pd.to_datetime(daily_df['Date'], format='%Y-%m-%d').dt.strftime('%d-%b-%Y')
			daily_df['Date']=daily_df['Date'].astype(str)
			daily_df['FII'] = daily_df['FII'].apply(lambda x:x if math.isnan(float(x)) else (format_currency(x,'',locale='en_IN')))
			daily_df['DII'] = daily_df['DII'].apply(lambda x:x if math.isnan(float(x)) else (format_currency(x,'',locale='en_IN')))
			daily_df['Nifty_500_Return'] = daily_df['Nifty_500_Return'].apply(lambda x:str(round(x*100,2))+'%')
			monthly_df = pd.read_sql("select Date,FII,DII,Nifty_500_Monthly_Return from FII_DII_Data_Monthly order by Year,Month Desc",sql_conn)
			monthly_df['FII'] = monthly_df['FII'].apply(lambda x:x if math.isnan(float(x)) else (format_currency(x,'',locale='en_IN')))
			monthly_df['DII'] = monthly_df['DII'].apply(lambda x:x if math.isnan(float(x)) else (format_currency(x,'',locale='en_IN')))
			monthly_df['Nifty_500_Monthly_Return'] = monthly_df['Nifty_500_Monthly_Return'].apply(lambda x:str(round(x*100,2))+'%')
			sql_conn.close()
			context['blogs'] = Blogs.objects.live().order_by('-posted_at')
			context['tastybytes'] = TastyByte.objects.live().order_by('-posted_at')
			IndexSectorBreakdown_df=pd.DataFrame(list(IndexSectorBreakdown.objects.all().values('sector','breakdown','date')))
			IndexSectorBreakdown_df['breakdown'] = (IndexSectorBreakdown_df['breakdown']*100).round(2)
			date=list(set(IndexSectorBreakdown_df['date'].astype(str)))
			indexsector_values=IndexSectorBreakdown_df[['sector','breakdown']].values.tolist()
			context['indexsector_values'] = indexsector_values
			context['date'] = date
			context['symbol']='₹'
			context['market_performance']=[]
			context['daily_table']=daily_df.values.tolist()
			context['monthly_table']=monthly_df.values.tolist()
			context['years']=set((pd.DatetimeIndex(daily_df['Date']).year))
		return context

class MarketFutureOptionsView(LoginRequiredMixin,SubscriptionValidationMixin,TemplateView):
	template_name = 'MarketFutureOptions.html'
	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		fostocks = set()
		try:
			country = self.request.session['country']
		except:
			self.request.session['country'] = 'India'
		if(self.request.session['country']=='US'):
			context['months'] = []
			context['options'] = []
			context['nifty_table'] = []
			context['display']='NO'
		else:
			options = FuturesAndOptions.objects.values().order_by('month')
			optionsDF = pd.DataFrame(list(options))
			tickers = list(optionsDF['fs_ticker'].unique())
			securities = SecurityMaster.objects.filter(fs_ticker__in=tickers).values('fs_ticker','security_code')
			secDF = pd.DataFrame(list(securities))
			optionsDF =pd.merge(optionsDF, secDF, how='inner',on='fs_ticker')
			optionsDF.fillna('', inplace=True)
			# if optionsDF['security_code']:
			# 	optionsDF['company_name'] = optionsDF['company_name']+" ("+optionsDF['security_code']+")"
			months = list(optionsDF['month'].unique())
			## To sort the months_list  by latest month and year
			unsorted_dates = [datetime.strptime(value, '%b-%Y') for value in months]
			sorted_dates = sorted(unsorted_dates)
			sorted_strings = [value.strftime('%b-%Y') for value in sorted_dates]
			sorted_strings.reverse()
			sorted_strings=[x.upper() for x in sorted_strings]
			nifty_table = NiftyFuturesandoptions.objects.all()
			context['months'] = sorted_strings
			context['options'] = optionsDF.T.to_dict()
			context['nifty_table'] = nifty_table
			context['display']='YES'
			return context

class AIMLSignalsView(LoginRequiredMixin,SubscriptionValidationMixin,View):
	context = dict()
	@method_decorator(csrf_exempt)
	def dispatch(self, *args, **kwargs):
		return super(AIMLSignalsView, self).dispatch(*args, **kwargs)

	def get(self, request, *args, **kwargs):
		try:
			country = self.request.session['country']
		except:
			self.request.session['country'] = 'India'
		predictionsModel = BsePredictions
		gicsclassificationModel = GicsClassification
		securityModel = SecurityMaster
		self.context['sectordata']	= 'YES'	
		if(self.request.session['country']=='US'):
			predictionsModel = UsPredictions
			gicsclassificationModel = UsGicsClassification
			securityModel = UsSecurityMaster
			self.context['sectordata']	= None
		df1  = pd.DataFrame(list(predictionsModel.objects.all().values()))
		## To sort the months_list  by latest month and year
		months_list=list(df1['period'].unique())
		unsorted_dates = [datetime.strptime(value, '%b-%Y') for value in months_list]
		sorted_dates = sorted(unsorted_dates)
		sorted_strings = [value.strftime('%b-%Y') for value in sorted_dates]
		sorted_strings.reverse()
		sorted_strings=[x.upper() for x in sorted_strings]
		gicscomp = gicsclassificationModel.objects.values('company','fs_ticker','tier','gics','description','rating')
		gicscomps = pd.DataFrame(list(gicscomp))
		fsTickers = list(gicscomps['fs_ticker'].unique())
		secmasterDataDF = pd.DataFrame(list(securityModel.objects.filter(fs_ticker__in=fsTickers).values('fs_ticker','security_code')))
		gicscomps = pd.merge(gicscomps,secmasterDataDF, on='fs_ticker', how='inner')
		gicscomps['company'] = gicscomps['company']+' ('+gicscomps['security_code']+')'
		gicscomps['count']=gicscomps.groupby(['gics','description','rating','tier']).cumcount()
		gicscomps.reset_index(inplace=True,drop=True)
		gicscomps = (gicscomps.set_index(['gics','rating','description','count'])
        .pivot(columns="tier")['company']
        .reset_index()
        .rename_axis(None, axis=1)
     	)
		gicscomps.fillna('', inplace=True)
		del gicscomps['count']
		gics = gicsclassificationModel.objects.values('gics','description','rating').order_by('gics')
		gicsdata = pd.DataFrame(list(gics))
		gicsdata = gicsdata.drop_duplicates().reset_index(drop=True).values.tolist()
		gicscomps = gicscomps.values.tolist()
		finalgicsdata = list()
		for gic in gicsdata:
			temp1 = [gic[0],gic[1],gic[2]]
			temp3 = list()
			for gics in gicscomps:
				if gics[0] == gic[0]:
					temp2 =  [gics[3],gics[4],gics[5]]
					temp3.append(temp2)
			temp1.append(temp3)
			finalgicsdata.append(temp1)
		self.context['months'] = sorted_strings
		self.context['gicsdata'] = finalgicsdata
		return render(request,'AIMLSignals.html',self.context)
				
	def post(self, request, *args, **kwargs):
		securityModel = SecurityMaster
		predictionsModel = BsePredictions
		self.context['sectordata']	= 'YES'	
		if request.session['country'] == 'US':
			securityModel = UsSecurityMaster
			predictionsModel = UsPredictions
			self.context['sectordata']	= None
		filtermonth = request.POST.get('portfolioname')
		filtertype = request.POST.get('filtertype')
		df1  = pd.DataFrame(list(predictionsModel.objects.all().values()))
		df2  = pd.DataFrame(list(predictionsModel.objects.filter(confidence_level='LONG', return_prediction__gte=0.02).all().values()))
		df3  = pd.DataFrame(list(predictionsModel.objects.filter(confidence_level='SHORT', return_prediction__lte=-0.02).all().values()))
		df4  = pd.DataFrame(list(predictionsModel.objects.filter(confidence_level='Neutral', return_prediction__range=(-0.02,0.02)).all().values()))
		df1=df1.append(df2)
		df1=df1.append(df3)
		df1=df1.append(df4)
		if(filtertype=='all'):
			df1=df1.loc[df1['period']==filtermonth].reset_index(drop=True)
		else:
			df1=df1.loc[df1['period']==filtermonth].reset_index(drop=True)
			df1=df1.loc[df1['flag']=='Yes'].reset_index(drop=True)
		sectors=pd.DataFrame(list(securityModel.objects.values('fs_ticker','gics')))
		sectors.columns = ['ticker', 'Sector']
		df=df1.merge(sectors,left_on="ticker",right_on='ticker',how='left')
		Sector=(list(df['Sector'].dropna().reset_index(drop=True).unique()))
		Sector=sorted(Sector)
		output_data_total=[]
		output_data=[]
		company1=[]
		company2=[]
		for j in range (0,len(Sector)):
			temp_df=df[df['Sector']==Sector[j]].reset_index(drop=True)
			for i in range(0,len(temp_df)):
					output_data_total.append([j,temp_df['return_prediction'][i]])
					company1.append([Sector[j],temp_df['return_prediction'][i],temp_df['company_name'][i]])
		self.context={'output_data_total':output_data_total,'sector':Sector,'company':company1}
		return HttpResponse(json.dumps(self.context), content_type='application/json')

class SignalsPerformanceView(LoginRequiredMixin,TemplateView):
	template_name = 'OurPerformance.html'
	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		try:
			country = self.request.session['country']
		except:
			self.request.session['country'] = 'India'
		country = self.request.session['country']
		if country=='India':
			Index_Ticker1,Index_Ticker2,Index_Ticker3="Nifty 50 Index","Nifty Smallcap 100 Index","Nifty Midcap 100 Index" 
			dailyPositionsObject=DailyPositions
			transactionsObject=Transactions
			universe_model = UniversePricesMain
			market_model = MarketIndex
			total_cash=5000000
		else:
			Index_Ticker1,Index_Ticker2,Index_Ticker3='S&P 500','Russell 2000','Dow Jones Industrial Average'
			dailyPositionsObject=UsDailyPositions
			transactionsObject=UsTransactions
			universe_model = UsUniversePricesMain
			market_model = UsMarketIndex
			total_cash=1000000
		sql_conn=pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server}; SERVER='+SERVER+'; PORT='+PORT+'; DATABASE='+DATABASE+';UID='+UID+';PWD='+PWD+';')
		username=self.request.user.username
		secTables = {'India':SecurityMaster,'US':UsSecurityMaster}		
		security_model = secTables[country]
		if country=='India':
			sec_code_data = pd.DataFrame(list(security_model.objects.filter(flag='yes').values("fs_ticker","security_code")))
		else:
			sec_code_data = pd.DataFrame(list(security_model.objects.values("fs_ticker","security_code")))
		sec_code_data.columns=['FS Ticker','Security_Code']						
		if country=='India':
			self.request.session['symbol'] ='₹'
			df=pd.read_sql("select * from Portfolio_vw_V5 where customer_name='suresh' and portfolio_name='datax10'",sql_conn)
		else:
			self.request.session['symbol'] ='$'
			df=pd.read_sql("select * from Portfolio_vw_V5_US where customer_name='suresh' and portfolio_name='datax10'",sql_conn)
		df.fillna(0,inplace=True)
		df = pd.merge(df,sec_code_data,on='FS Ticker',how='inner')
		df['COMPANY'] = df['COMPANY'] + " (" + df['Security_Code'] + ")"
		del df['Security_Code']
		df.columns = [x.lower() for x in df.columns]
		df['pnl_realized']=df['pnl_realized']+df['divpnl']
		df['pnl']=df['pnl_realized']+df['pnl_unrealized']
		if country=='India':
			daily_pnl_data=pd.read_sql("select * from daily_positions where customer_name='suresh' and portfolio_name='datax10' and date=(select max(date) as maxdate from daily_positions where customer_name='suresh' and portfolio_name='datax10')",sql_conn)
		else:
			daily_pnl_data=pd.read_sql("select * from us_daily_positions where customer_name='suresh' and portfolio_name='datax10' and date=(select max(date) as maxdate from daily_positions where customer_name='suresh' and portfolio_name='datax10')",sql_conn)
				
		df['date'] = pd.to_datetime(df.date)
		date= df['date'][0].date()
		equity_val=df['value_mkt_price'].sum()
		equity_invstmnt=df['value_cost'].sum()
		pnl=df['pnl_unrealized'].sum()
		daily_pnl=daily_pnl_data['P&LUnrealized'].sum() 
		cash_remaining=total_cash-df['value_cost'].sum()+ df['pnl_realized'].sum()
		gainer=df.loc[df['pnl'] == df['pnl'].max(),['company','pnl_unrealized','pnl_realized','price_chg_day','pct_chg_day']].values.tolist()
		loser=df.loc[df['pnl'] == df['pnl'].min(),['company','pnl_unrealized','pnl_realized','price_chg_day','pct_chg_day']].values.tolist()
		if(len(gainer)==0):
			gainer='No Stock gaining today'
		else:
			gainer=gainer[0]
		if(len(loser)==0):
			loser='No Stock losing today'
		else:
			loser=loser[0]
		profit=len(df[(df['profit_loss']=='PROFIT') & (df['status']=='ACTIVE')])
		loss=len(df[(df['profit_loss']=='LOSS') & (df['status']=='ACTIVE')])
		total_allocation=df['value_mkt_price'].sum()
		allocation_df=df.groupby('sector', as_index=False)['value_mkt_price'].sum()
		allocation_df['percent']=allocation_df['value_mkt_price'].apply(lambda x:round((x/total_allocation)*100,2))
		allocation_list=[]
		for i in range (0,len(allocation_df)):      
			x={"name":allocation_df['sector'][i]+' ('+str(int(round(allocation_df['percent'][i])))+'%)',"y":allocation_df['percent'][i]}
			allocation_list.append(x)
		highlights_list=[]
		portfolio_highlights_df=df[['company','sector','pnl_realized','value_mkt_price','pct_chg']]
		highlights_list.append(portfolio_highlights_df['pnl_realized'].sum())
		top_exposure=allocation_df.loc[allocation_df['percent'] == allocation_df['percent'].max(),['sector','percent']].values.tolist()
		highlights_list.append(top_exposure[0])
		most_profit=(portfolio_highlights_df.loc[portfolio_highlights_df['pct_chg'] == portfolio_highlights_df['pct_chg'].max(),['company','pct_chg']].values.tolist())
		highlights_list.append(most_profit[0])
		least_profit=(portfolio_highlights_df.loc[portfolio_highlights_df['pct_chg'] == portfolio_highlights_df['pct_chg'].min(),['company','pct_chg']].values.tolist())
		highlights_list.append(least_profit[0])
		# table_df=df[['company','curr_qty','curr_price','value_mkt_price','pnl_realized','long_term_qty','avg_buy_price','value_cost','pnl_unrealized']]					
		table_df=df[df['status']=='ACTIVE']
		table_df=table_df[['company','curr_qty','value_cost','value_mkt_price','pct_chg','pnl']]
		table_df.sort_values(by=['company'],ascending=[True],inplace=True)
		table_df['pct_chg']=table_df['pct_chg'].apply(lambda x:round((x)*100,1))
		table_data=table_df.values.tolist()
		if country=='India':
			equity_cashflow=pd.read_sql("select eomonth(DATE) Month_End ,sum([P&L]) PNL from Daily_Positions where DATE>='2020-01-01' and customer_name='suresh'  group by eomonth(DATE)",sql_conn)
		else:
			equity_cashflow=pd.read_sql("select eomonth(DATE) Month_End ,sum([P&L]) PNL from US_Daily_Positions where DATE>='2020-01-01' and customer_name='suresh'  group by eomonth(DATE)",sql_conn)
		# equity_cashflow=pd.DataFrame(list(DailyPositions.objects.filter(customer_name=request.user.username,).values()))
		equity_cashflow['Month_End']=equity_cashflow['Month_End'].astype(str)
		header_list=['Company Name','Date','Action','Qty','Transaction Price','Value Cost']
		sub_df=pd.DataFrame(list(transactionsObject.objects.filter(customer='Suresh',).values('secdescription','security_id','tradedate','action','quantity','price','grossmoney','totalcomm','totalfees')))
		sub_df = pd.merge(sub_df,sec_code_data,left_on='security_id',right_on="FS Ticker",how='inner')
		sub_df['secdescription'] = sub_df['secdescription'] + " (" + sub_df['Security_Code'] + ")"		
		del sub_df['Security_Code']
		del sub_df['security_id']
		sub_df['tradedate']=sub_df['tradedate'].astype(str)
		# sub_df=df[['SECDESCRIPTION','TRADEDATE','ACTION','QUANTITY','PRICE','GROSSMONEY','TOTALCOMM','TOTALFEES']]
		sub_df['ADDITIONAL_CHARGES']=sub_df['totalcomm']+sub_df['totalfees']
		sub_df['ADDITIONAL_CHARGES'].fillna(0,inplace=True)
		sub_df['REMARKS']=''
		sub_df=sub_df.drop(['totalcomm', 'totalfees'], axis = 1) 
		transaction_table_data=sub_df.values.tolist()
		EndDate= dailyPositionsObject.objects.filter(customer_name='suresh',portfolio_name='datax10').aggregate(Max('date'))['date__max']
		StartDate = dailyPositionsObject.objects.filter(customer_name='suresh',portfolio_name='datax10').aggregate(Min('date'))['date__min']
		required_value='ITD'
		MTD_StartDate= universe_model.objects.all().aggregate(Max('date'))['date__max']
		MTD_StartDate=MTD_StartDate.replace(day=1)
		MTD_Portfolio_CumulativePNL,MTD_Portfolio_daily_pnl =  LivePortfolioCumulativeReturns(MTD_StartDate,EndDate,'suresh','datax10',country)
		MTD_Index1_CumulativeReturns,MTD_Index1_Daily_Return = IndexCumulativeReturns (Index_Ticker1,MTD_StartDate,EndDate,country)
		MTD_Index2_CumulativeReturns,MTD_Index2_Daily_Return = IndexCumulativeReturns (Index_Ticker2,MTD_StartDate,EndDate,country)
		MTD_Index3_CumulativeReturns,MTD_Index3_Daily_Return = IndexCumulativeReturns (Index_Ticker3,MTD_StartDate,EndDate,country)	
		MTD_Cumulative_Returns = pd.merge(MTD_Portfolio_CumulativePNL,MTD_Index1_CumulativeReturns,on="Date",how="inner") 
		MTD_Cumulative_Returns = pd.merge(MTD_Cumulative_Returns,MTD_Index2_CumulativeReturns,on="Date",how="inner")
		MTD_Cumulative_Returns = pd.merge(MTD_Cumulative_Returns,MTD_Index3_CumulativeReturns,on="Date",how="inner")
		MTD_Cumulative_Returns =  MTD_Cumulative_Returns[["Date","Portfolio",Index_Ticker1,Index_Ticker2,Index_Ticker3]]
		mtd_date= MTD_Cumulative_Returns['Date'].min()
		mtd_date=mtd_date.strftime('%Y-%m-%d')
		mtd_cumulative=[]
		mtd_cumulative.append(MTD_Cumulative_Returns.loc[len(MTD_Cumulative_Returns)-1,'Portfolio'])
		mtd_cumulative.append(MTD_Cumulative_Returns.loc[len(MTD_Cumulative_Returns)-1,Index_Ticker1])
		mtd_cumulative.append(MTD_Cumulative_Returns.loc[len(MTD_Cumulative_Returns)-1,Index_Ticker2])
		mtd_cumulative.append(MTD_Cumulative_Returns.loc[len(MTD_Cumulative_Returns)-1,Index_Ticker3])
		Portfolio_CumulativePNL,Portfolio_daily_pnl =  LivePortfolioCumulativeReturns(StartDate,EndDate,'suresh','datax10',country)
		Index1_CumulativeReturns,Index1_Daily_Return = IndexCumulativeReturns (Index_Ticker1,StartDate,EndDate,country)
		Index2_CumulativeReturns,Index2_Daily_Return = IndexCumulativeReturns (Index_Ticker2,StartDate,EndDate,country)
		Index3_CumulativeReturns,Index3_Daily_Return = IndexCumulativeReturns (Index_Ticker3,StartDate,EndDate,country)	
		Cumulative_Returns = pd.merge(Portfolio_CumulativePNL,Index1_CumulativeReturns,on="Date",how="inner") 
		Cumulative_Returns = pd.merge(Cumulative_Returns,Index2_CumulativeReturns,on="Date",how="inner")
		Cumulative_Returns = pd.merge(Cumulative_Returns,Index3_CumulativeReturns,on="Date",how="inner")
		Cumulative_Returns =  Cumulative_Returns[["Date","Portfolio",Index_Ticker1,Index_Ticker2,Index_Ticker3]]
		# print(Cumulative_Returns['Date'][0]+timedelta(-1))
		cumulative_return=Cumulative_Returns
		cumulative_return.columns=['Trade_Dt','Portfolio',Index_Ticker1,Index_Ticker2,Index_Ticker3]
		temp_df = pd.DataFrame({"Trade_Dt":[Cumulative_Returns['Trade_Dt'][0]+timedelta(-1)], 
		"Portfolio":[0],Index_Ticker1:[0],Index_Ticker2:[0],Index_Ticker3:[0]}) 
		cumulative_return=temp_df.append(cumulative_return)
		cumulative_return['Trade_Dt'] = cumulative_return['Trade_Dt'].dt.date
		cumulative_return['Trade_Dt'] = cumulative_return['Trade_Dt'].astype(str)
		cumulative_return['Trade_Dt'] = cumulative_return['Trade_Dt'].str[0:11]
		cumulative_return=cumulative_return.dropna().reset_index(drop=True)
		output_cumulative_return=[]		
		temp_list=[]
		lineColor=['#6610F2','#28a745','#02BCD4','#ffc107','#dc3545','#5A6268','#23272B']
		i=0
		for col in cumulative_return.columns: 
			if(col!='Trade_Dt'):
				cumulative_return_df=cumulative_return[['Trade_Dt',col]]
				my_list=cumulative_return_df.values.tolist()
				if(col!='PNL'):
					x={"name":col,"lineWidth": 2,'color':lineColor[i],"data":my_list}
				else:
					x={"name":"Portfolio","lineWidth": 4,'color':lineColor[i],"data":my_list}
				output_cumulative_return.append(x)
				i=i+1
		temp=output_cumulative_return[2]
		output_cumulative_return[2]=output_cumulative_return[3]
		output_cumulative_return[3]=temp
		context={'cashflow_data':equity_cashflow.values.tolist(),'cash_remaining':cash_remaining,
				'username':username,'symbol':self.request.session['symbol'],'date':date,
				'equity_val':equity_val,'equity_invstmnt':equity_invstmnt,'pnl':pnl,'daily_pnl':daily_pnl,'gainer':gainer,'loser':loser,
				'profit':profit,'loss':loss,'allocation_list':allocation_list,'highlights_list':highlights_list,'table_data': table_data,
				'header_list':header_list,'transaction_table_data':transaction_table_data,
				'output_cumulative_return':output_cumulative_return,'mtd_cumulative_return':mtd_cumulative,'mtd_date':mtd_date,
				'Index_Ticker1':Index_Ticker1,'Index_Ticker2':Index_Ticker2,'Index_Ticker3':Index_Ticker3}
		return context


class ListofModelPortfoliosView(LoginRequiredMixin,SubscriptionValidationMixin,TemplateView):
	template_name = 'ModelPortfolio.html'

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		security_model=SecurityMaster
		universeModel=UniversePricesMain
		symbol='₹'
		try:
			if self.request.session['country']=='US':
				security_model=UsSecurityMaster
				universeModel=UsUniversePricesMain
				symbol='$'
		except:
			pass
		modelportfolios = ModelPortfolioDetails.objects.filter(country=self.request.session['country']).values('company_name','fs_ticker','model_portfolio_name','quantity').order_by('created_at','company_name')
		ModelPortfolios = pd.DataFrame(list(modelportfolios))
		ModelPortfolios.columns = ['CompanyName','Factset_Ticker','ModelPortfolioName','Quantity']
		IndiamodelPortfolioStocks = list(ModelPortfolios['Factset_Ticker'].unique())
		portfolioList = list(ModelPortfolios['ModelPortfolioName'].unique())
		indiafactors = security_model.objects.filter(fs_ticker__in=IndiamodelPortfolioStocks).values('fs_name','fs_ticker','gics','market_cap_category').order_by('fs_name')
		Indiafactors = pd.DataFrame(list(indiafactors))
		Indiafactors.columns =['CompanyName','Factset_Ticker','Sector','MarketCapCategory']
		indiaprices = universeModel.objects.filter(date= 
		universeModel.objects.all().aggregate(Max('date'))['date__max'],factset_ticker__in=IndiamodelPortfolioStocks).values('company','factset_ticker','price').order_by('company')
		Indiaprices = pd.DataFrame(list(indiaprices))
		Indiaprices.columns = ['CompanyName','Factset_Ticker','Price']
		ModelPortfolios = pd.merge(ModelPortfolios,Indiafactors,on=["CompanyName",'Factset_Ticker'],how="inner")
		ModelPortfolios = pd.merge(ModelPortfolios,Indiaprices,on=["CompanyName",'Factset_Ticker'],how="inner")
		secTables = {'India':SecurityMaster,'US':UsSecurityMaster}
		sec_code_data = secTables[self.request.session['country']].objects.filter(fs_ticker__in=IndiamodelPortfolioStocks).values("fs_ticker",'fs_name',"security_code")
		sec_code_data=pd.DataFrame(list(sec_code_data))
		sec_code_data.columns=['Factset_Ticker','FS_Name','Security_Code']
		ModelPortfolios = pd.merge(ModelPortfolios,sec_code_data,right_on=['Factset_Ticker','FS_Name'],left_on=['Factset_Ticker','CompanyName'],how='inner')
		ModelPortfolios['Security_Code'] = ModelPortfolios['Security_Code'].fillna("")
		finalstocks = list()
		for ind in ModelPortfolios.index:
			temp = {'ModelPortfolioName': ModelPortfolios['ModelPortfolioName'][ind] , 'CompanyName': ModelPortfolios['CompanyName'][ind] 	+ " (" + ModelPortfolios['Security_Code'][ind] + ")",'Sector':ModelPortfolios['Sector'][ind],'MarketCapCategory' : ModelPortfolios['MarketCapCategory'][ind],
					'Quantity': ModelPortfolios['Quantity'][ind], 'Price' : ModelPortfolios['Price'][ind]}
			finalstocks.append(temp)
		context['modelportfolio'] = finalstocks
		context['portfolioList'] = portfolioList
		context['symbol']=symbol
		return context


class CalculateModelPortfolioReturnsView(LoginRequiredMixin,SubscriptionValidationMixin,View):
	context = dict()
	@method_decorator(csrf_exempt)
	def dispatch(self, *args, **kwargs):
		return super(CalculateModelPortfolioReturnsView, self).dispatch(*args, **kwargs)

	def get(self, request, *args, **kwargs):
		self.context['YTD_creturns'] = []
		self.context['ITD_creturns'] = []
		self.context['YTD_msg_df_values'] = []
		self.context['ITD_msg_df_values'] = []
		return HttpResponse(json.dumps(self.context), content_type='application/json')

	def post(self, request, *args, **kwargs):
		portfolio_name  = request.POST.get("portfolioname")
		try:
			country_name=self.request.session['country']
		except:
			self.request.session['country'] = 'India'
		country_name=self.request.session['country']
		universeModel = UniversePricesMain
		securityModel = SecurityMaster
		marketIndexModel = MarketIndex
		risk_free_rate_model = RiskFreeRate
		modelPortfolioName = portfolio_name
		# countryName = ModelPortfolioDetails.objects.filter(model_portfolio_name=portfolio_name).values('country')[0]
		table_headerlist=['PNL','Nifty 50 Index','Nifty Smallcap 100 Index','Nifty Midcap 100 Index'] 
		if country_name == 'US':
			universeModel = UsUniversePricesMain
			securityModel = UsSecurityMaster
			marketIndexModel = UsMarketIndex
			risk_free_rate_model = UsRiskFreeRate
			modelPortfolioName += "-US"
			table_headerlist=['PNL','S&P 500','Dow Jones Industrial Average','Russell 2000'] 
		try:	
			Stock_Data = CustomerPortfolioDetails.objects.filter(portfolio_name=portfolio_name,country=request.session['country'],model_portfolio='Yes').values('company_name','fs_ticker','quantity','price')	
			Stock_Data = pd.DataFrame(list(Stock_Data))
			Stock_Data = Stock_Data[['company_name','fs_ticker','quantity','price']]
		except:
			Stock_Data = ModelPortfolioDetails.objects.filter(model_portfolio_name=portfolio_name,country=request.session['country'],add_on='Yes').values('company_name','fs_ticker','quantity')
			Universe_Prices_Main = pd.DataFrame(list(universeModel.objects.filter(date = universeModel.objects.all().aggregate(Max('date'))['date__max']).values()))
			Universe_Prices_Main = Universe_Prices_Main[['company','factset_ticker','price']]
			Universe_Prices_Main.columns=['company_name','fs_ticker','price']
			Stock_Data = pd.DataFrame(list(Stock_Data))
			Stock_Data = pd.merge(Stock_Data,Universe_Prices_Main,on=['company_name','fs_ticker'],how='inner')
			Stock_Data = Stock_Data[['company_name','fs_ticker','quantity','price']]
		Stock_Data.columns = ['Company','Factset_Ticker','Quantity','Price']
		stocks = set(Stock_Data["Factset_Ticker"])
		current_date= universeModel.objects.all().aggregate(Max('date'))['date__max']
		pnl_start_date_YTD = universeModel.objects.filter(date__lt=str(current_date.year)+'-01-01').all().aggregate(Max('date'))['date__max']
		YTD_date = '0101'+str(current_date.year)
		format_exp = '%d%m%Y' # The format
		start_date_YTD = datetime.strptime(YTD_date, format_exp).date()
		pnl_start_date_ITD = universeModel.objects.filter(date__lt='2019-01-01').all().aggregate(Max('date'))['date__max']
		ITD_date = '01012019' # 2019 Jan 1st as ITD for model portfolio, will display
		start_date_ITD = datetime.strptime(ITD_date, format_exp).date() #current_date.replace(day=1)

		universe_prices = universeModel.objects.filter(factset_ticker__in=stocks, date__range=(pnl_start_date_ITD,current_date)).order_by('company','date').values('company', 'factset_ticker', 'date', 'price', 'return_field')
		Daily_Returns = pd.DataFrame(list(universe_prices))
		Daily_Returns_ITD = Daily_Returns[['company', 'factset_ticker', 'date', 'price', 'return_field']]
		Daily_Returns_ITD.columns = ['Company', 'Factset_Ticker', 'Date', 'Price', 'Return']
		Daily_Returns_YTD = Daily_Returns_ITD[Daily_Returns_ITD['Date'] >= pnl_start_date_YTD ]
		market_indexes = marketIndexModel.objects.filter(date__range=(start_date_ITD,current_date)).order_by('date').values('date', 'company', 'return_field')
		index_cumulative_data = pd.DataFrame(list(market_indexes))
		index_cumulative_data_ITD = index_cumulative_data[['date', 'company', 'return_field']]
		index_cumulative_data_ITD.columns = ['Date' , 'Company' , 'Return']
		index_cumulative_data_YTD = index_cumulative_data_ITD[index_cumulative_data_ITD['Date'] >= start_date_YTD]
		risk_free_rates = risk_free_rate_model.objects.filter(date__range=(start_date_ITD, current_date)).order_by('date').values('date','risk_free_rate')  #retrieving risk free data
		Risk_Free_Data_ITD = pd.DataFrame(list(risk_free_rates))
		Risk_Free_Data_ITD = Risk_Free_Data_ITD[['date','risk_free_rate']]
		Risk_Free_Data_ITD.columns = ['Date','Risk Free Rate']
		risk_free_rates = risk_free_rate_model.objects.filter(date__range=(start_date_YTD, current_date)).order_by('date').values('date','risk_free_rate')  #retrieving risk free data
		Risk_Free_Data_YTD = pd.DataFrame(list(risk_free_rates))
		Risk_Free_Data_YTD = Risk_Free_Data_YTD[['date', 'risk_free_rate']]
		Risk_Free_Data_YTD.columns = ['Date', 'Risk Free Rate']

		cumulative_return_YTD,daily_returnsytd,df_YTD=CalcsMain(country_name,start_date_YTD,pnl_start_date_YTD,current_date, request.user.username,Stock_Data,Daily_Returns_YTD,index_cumulative_data_YTD,modelPortfolioName,Risk_Free_Data_YTD,'YTD','CMS')
		cumulative_return_ITD,daily_returnsitd,df_ITD=CalcsMain(country_name,start_date_ITD,pnl_start_date_ITD,current_date, request.user.username,Stock_Data,Daily_Returns_ITD,index_cumulative_data_ITD,modelPortfolioName,Risk_Free_Data_ITD,'ITD','CMS')
		# print(cumulative_return_ITD.head(2))
		# print(cumulative_return_YTD.head(2))
		df_YTD=df_YTD[df_YTD['']!='Equal Weighted Portfolio']
		df_ITD=df_ITD[df_ITD['']!='Equal Weighted Portfolio']
		df_YTD=df_YTD.replace([np.inf, -np.inf],0)
		YTD_table_data = df_YTD.values.tolist()

		YTD_table_data[2], YTD_table_data[3] =  YTD_table_data[3], YTD_table_data[2]
		cumulative_return_YTD['Trade_Dt'] = cumulative_return_YTD['Trade_Dt'].dt.date
		cumulative_return_YTD['Trade_Dt'] = cumulative_return_YTD['Trade_Dt'].astype(str)
		cumulative_return_YTD['Trade_Dt'] = cumulative_return_YTD['Trade_Dt'].str[0:11]
		cumulative_return_YTD=cumulative_return_YTD.dropna().reset_index(drop=True)
		output_cumulative_return_YTD=[]
		# lineColor=['#6610F2','#F45B5B','#91E8E1','#90ED7D']
		lineColor=['#6610F2','#28a745','#02BCD4','#ffc107','#dc3545','#5A6268','#23272B']
		i=0
		del cumulative_return_YTD['Equalweighted pnl']
		for col in cumulative_return_YTD.columns: 
			if(col!='Trade_Dt'):
				cumulative_return_df_YTD=cumulative_return_YTD[['Trade_Dt',col]]
				my_list=cumulative_return_df_YTD.values.tolist()
				if(col!='PNL'):
					x={"name":col,"lineWidth": 2,'color':lineColor[i],"data":my_list}
				else:
					x={"name":"Portfolio","lineWidth": 4,'color':lineColor[i],"data":my_list}
				output_cumulative_return_YTD.append(x)
				i=i+1
		output_cumulative_return_YTD[2],output_cumulative_return_YTD[3]=output_cumulative_return_YTD[3],output_cumulative_return_YTD[2]
		df_ITD=df_ITD.replace([np.inf, -np.inf],0)
		ITD_table_data = df_ITD.values.tolist()
		ITD_table_data[2], ITD_table_data[3] =  ITD_table_data[3], ITD_table_data[2]
		cumulative_return_ITD['Trade_Dt'] = cumulative_return_ITD['Trade_Dt'].dt.date
		cumulative_return_ITD['Trade_Dt'] = cumulative_return_ITD['Trade_Dt'].astype(str)
		cumulative_return_ITD['Trade_Dt'] = cumulative_return_ITD['Trade_Dt'].str[0:11]
		cumulative_return_ITD=cumulative_return_ITD.dropna().reset_index(drop=True)
		del cumulative_return_ITD['Equalweighted pnl']
		port_itd_beta = stats.linregress(cumulative_return_ITD['PNL'].values,cumulative_return_ITD[table_headerlist[1]].values)[0:1][0]
		port_ytd_beta = stats.linregress(cumulative_return_YTD['PNL'].values,cumulative_return_YTD[table_headerlist[1]].values)[0:1][0]
		nifty50_itd_beta = stats.linregress(cumulative_return_ITD[table_headerlist[1]].values,cumulative_return_ITD[table_headerlist[1]].values)[0:1][0]
		nifty50_ytd_beta = stats.linregress(cumulative_return_YTD[table_headerlist[1]].values,cumulative_return_YTD[table_headerlist[1]].values)[0:1][0]
		niftysmall_itd_beta = stats.linregress(cumulative_return_ITD[table_headerlist[2]].values,cumulative_return_ITD[table_headerlist[1]].values)[0:1][0]
		niftysmall_ytd_beta = stats.linregress(cumulative_return_YTD[table_headerlist[2]].values,cumulative_return_YTD[table_headerlist[1]].values)[0:1][0]
		niftymid_itd_beta = stats.linregress(cumulative_return_ITD[table_headerlist[3]].values,cumulative_return_ITD[table_headerlist[1]].values)[0:1][0]
		niftymid_ytd_beta = stats.linregress(cumulative_return_YTD[table_headerlist[3]].values,cumulative_return_YTD[table_headerlist[1]].values)[0:1][0]
		output_cumulative_return_ITD=[]
		temp_list=[]
		# lineColor=['#6610F2','#F45B5B','#91E8E1','#90ED7D']
		lineColor=['#6610F2','#28a745','#02BCD4','#ffc107','#dc3545','#5A6268','#23272B']
		j=0
		for col in cumulative_return_ITD.columns: 
			if(col!='Trade_Dt'):
				cumulative_return_df_ITD=cumulative_return_ITD[['Trade_Dt',col]]
				my_list=cumulative_return_df_ITD.values.tolist()
				if(col!='PNL'):
					x={"name":col,"lineWidth": 2,'color':lineColor[j],"data":my_list}
				else:
					x={"name":"Portfolio","lineWidth": 4,'color':lineColor[j],"data":my_list}
				output_cumulative_return_ITD.append(x)
				j=j+1
		output_cumulative_return_ITD[2],output_cumulative_return_ITD[3]=output_cumulative_return_ITD[3],output_cumulative_return_ITD[2]
		YTD_msg_df=cumulative_return_YTD.tail(1).reset_index(drop=True)
		ITD_msg_df=cumulative_return_ITD.tail(1).reset_index(drop=True)
		YTD_msg_df=YTD_msg_df.drop(['Trade_Dt'],axis=1)
		ITD_msg_df=ITD_msg_df.drop(['Trade_Dt'],axis=1)
		# table_headerlist=['PNL','Nifty 50 Index','Nifty Smallcap 100 Index','Nifty Midcap 100 Index'] 
		YTD_msg_df_values=YTD_msg_df.values.tolist()
		ITD_msg_df_values=ITD_msg_df.values.tolist()
		YTD_msg_df_values=[round((i * 100),2) for i in YTD_msg_df_values[0]]
		ITD_msg_df_values=[round((i * 100),2) for i in ITD_msg_df_values[0]]
		itd_betas=[port_itd_beta,nifty50_itd_beta,niftysmall_itd_beta,niftymid_itd_beta]
		ytd_betas=[port_ytd_beta,nifty50_ytd_beta,niftysmall_ytd_beta,niftymid_ytd_beta]
		df_ITD['Cumulative Return']=ITD_msg_df_values
		df_YTD['Cumulative Return']=YTD_msg_df_values
		df_ITD['Beta']=itd_betas
		df_YTD['Beta']=ytd_betas					
		df_YTD=df_YTD.drop(['Annualized Return'],axis=1)
		df_ITD=df_ITD.drop(['Annualized Return'],axis=1)
		df_YTD=df_YTD.drop(['Maximum DrawDown'],axis=1)
		df_ITD=df_ITD.drop(['Maximum DrawDown'],axis=1)
		df_ITD= df_ITD[["","Cumulative Return",'Annualized Volatility','Sharpe','Beta']]
		df_YTD= df_YTD[["","Cumulative Return",'Annualized Volatility','Sharpe','Beta']]
		# YTD_main=[]
		# ITD_main=[]
		# for i in range(0,len(table_headerlist)):
		# 	temp=[table_headerlist[i],YTD_msg_df_values[i]]
		# 	temp1=[table_headerlist[i],ITD_msg_df_values[i]]
		# 	YTD_main.append(temp)
		# 	ITD_main.append(temp1)
		self.context['YTD_creturns'] = output_cumulative_return_YTD
		self.context['ITD_creturns'] = output_cumulative_return_ITD
		self.context['YTD_msg_df_values'] = df_YTD.values.tolist()
		self.context['ITD_msg_df_values'] = df_ITD.values.tolist()
		return HttpResponse(json.dumps(self.context), content_type='application/json')
				

class ModelPortfolioAddingView(LoginRequiredMixin, SubscriptionValidationMixin, View):
	context = dict()
	@method_decorator(csrf_exempt)
	def dispatch(self, *args, **kwargs):
		return super(ModelPortfolioAddingView, self).dispatch(*args, **kwargs)

	def get(self, request, *args, **kwargs):
		self.context['status'] = False
		self.context['msg'] = 'Something went wrong, please try again!'
		self.context['portfolioname'] = 'Invalid'
		return HttpResponse(json.dumps(self.context), content_type='application/json')

	def post(self, request, *args, **kwargs):
		try:
			newPortfolioName  = request.POST.get("newportfolioname")
			originalPortfolioName  = request.POST.get("originalportfolioname")
			modelPortfolios = ModelPortfolioDetails.objects.filter(model_portfolio_name=originalPortfolioName, country=request.session['country']).values('company_name','quantity','sector_name').order_by('company_name')
			stocksDF = pd.DataFrame(list(modelPortfolios))
			stocksDF.columns = ['Company','Quantity','Sector']
			portfolioStocks = list(stocksDF['Company'].unique())
			stocks =', '.join('"{0}"'.format(w) for w in [portfolioStocks[i].replace("'","''") for i in range(0,len(portfolioStocks))] ).replace('"',"'")
			query = '''SELECT B.COMPANY,A.FACTSET_TICKER, A.DATE, A.PRICE FROM UNIVERSE_PRICES_MAIN A JOIN (SELECT COMPANY, MIN(DATE) DATE FROM UNIVERSE_PRICES_MAIN WHERE DATE >= '2019-01-01' AND 
					   COMPANY IN ('''+stocks+''') GROUP BY FACTSET_TICKER, COMPANY) B ON A.COMPANY = B.COMPANY AND A.DATE = B.DATE '''
			if request.session['country'] == 'US':
				query = '''SELECT B.COMPANY,A.FACTSET_TICKER, A.DATE, A.PRICE FROM US_UNIVERSE_PRICES_MAIN A JOIN (SELECT COMPANY, MIN(DATE) DATE FROM US_UNIVERSE_PRICES_MAIN WHERE DATE >= '2019-01-01' AND 
				           COMPANY IN ('''+stocks+''') GROUP BY FACTSET_TICKER, COMPANY) B ON A.COMPANY = B.COMPANY AND A.DATE = B.DATE '''
			cursor = connection.cursor() #Create cursor from the DB connection
			try:
				resultSet = cursor.execute(query).fetchall() #Excecute the query and fetch all the records from the DB
			except Exception as e:
				cursor.close() #Close the connection, if any exception occurs
			results = [list(item) for item in resultSet] #Convert pyodbc row to list
			pricesDF = pd.DataFrame(results, columns=['Company','Ticker','Date','Price']) #Convert resultset to dataframe
			stocksDF = pd.merge(stocksDF, pricesDF, on='Company', how='inner')
			if not stocksDF.empty:
				if not CustomerPortfolioDetails.objects.filter(customer_name=request.user.username,country=request.session['country'], portfolio_name=newPortfolioName).exists(): #Check portfolio exists or not
					for index, row in stocksDF.iterrows():
						if not CustomerPortfolioDetails.objects.filter(customer_name=request.user.username,country=request.session['country'], portfolio_name=newPortfolioName,company_name=row['Company']).exists():
							CustomerPortfolioDetails(customer_name=request.user.username, portfolio_name=newPortfolioName, company_name=row['Company'],
							quantity=row['Quantity'], price=row['Price'], sector_name=row['Sector'],model_portfolio='yes', portfolio_type='General',
							country=request.session['country'], fs_ticker=row['Ticker']).save()  # Saving the stock
					CustomerPortfolioLog(customer_name=request.user.username,portfolio_name=newPortfolioName,last_action='portfolio_created',created_at=timezone.now(),status='active').save()
					self.context['status'] = True
					createCacheForNewPortfolio(request.user.username, request.session['country'], newPortfolioName)
					self.context['msg'] = 'Your Portfolio has been added successfully!'
					self.context['portfolioname'] = newPortfolioName
				else:  # If the portfolio name already exists, raise error as exception
					raise PortfolioExistsError('Portfolio name is already taken!')
			else:
				self.context['status'] = False
				self.context['msg'] = 'Invalid Input, try again!'
				self.context['portfolioname'] = 'Invalid'
        # Handling the exceptions
		except PortfolioExistsError as perror:  
			self.context['status'] = False
			self.context['msg'] = perror.msg
			self.context['portfolioname'] = 'Invalid'
		except Exception as e:
			print(e)
			self.context['status'] = False
			self.context['msg'] = 'Something went wrong, please try again!'
			self.context['portfolioname'] = 'Invalid'
		return HttpResponse(json.dumps(self.context), content_type='application/json')


class PortfolioCreationWizardView(LoginRequiredMixin,SubscriptionValidationMixin,TemplateView):
	template_name = 'PortfolioWizard.html'

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		selectedIndex = self.request.GET.get('index-name','Nifty 500')  # Taking the index name from the client request
		try:
			country=self.request.session['country']
		except:
			self.request.session['country'] = 'India'
		country = self.request.session['country']
		if country == 'US':
			selectedIndex = self.request.GET.get('index-name','S&P 500')  # Taking the index name from the client request
		if selectedIndex:  #Index exist check
			selectedIndex = selectedIndex.replace('pi72pi', '&')  # Expression framing
			self.request.session['wizardindex'] = selectedIndex
			indexWiseStocksDF = getWizardTableDataFromCacheBuilder(country, selectedIndex) #Taking the index matched stocks data
			indexWiseStocksDF=indexWiseStocksDF.fillna(0)
			if country == 'India':
				try:  # Removing the ETF
					indexWiseStocksDF.drop(indexWiseStocksDF[indexWiseStocksDF['gics'] == 'Other'].index , inplace=True)
				except:  #Handling the KeyError(and other)
					pass
		stockData = indexWiseStocksDF[['Company','Factset_Ticker']].drop_duplicates(keep="last")
		finalstocks = list()
		try:
			finalstocks = list(stockData['Factset_Ticker'])  #Taking the filtered stocks
		except:
			pass
		self.request.session['wizardIndexStocks_s0'] = finalstocks #Saving the filtered stocks to current active session
		stockcount = indexWiseStocksDF.groupby(['gics']).size().reset_index(name='count') # Sector wise count
		marketcapstocks = indexWiseStocksDF.groupby(['Market_Cap_Category']).size().reset_index(name='count') # Market cap category count
		indexes = getWizardIndicesDataFromCacheBuilder(country)
		indexWiseStocksDF.rename(columns={'Market Cap':'Market_Cap'},inplace=True)

		secTables = {'India':SecurityMaster,'US':UsSecurityMaster}
		sec_code_data = pd.DataFrame(list(secTables[country].objects.filter(fs_ticker__in=list(indexWiseStocksDF['Factset_Ticker'])).values("fs_ticker","security_code")))
		sec_code_data.columns=['Factset_Ticker','Security_Code']
		indexWiseStocksDF = pd.merge(indexWiseStocksDF,sec_code_data,on='Factset_Ticker',how='inner')
		sectorwisestockscount = [[row['gics'], row['count']] for index, row in stockcount.iterrows() if row['gics']] #Converting the sector count to list
		marketcapwisestockscount = [[row['Market_Cap_Category'], row['count']] for index, row in marketcapstocks.iterrows() if row['Market_Cap_Category']] #Converting the market cap
		context['stocksCount'] = len(indexWiseStocksDF) #Saving the count of stocks
		context['stocks'] = indexWiseStocksDF.T.to_dict() #Converting the transpose of o/p to dict
		context['sectorwisestockscount'] = sectorwisestockscount #Saving the sector wise list
		context['marketcapwisestockscount'] = marketcapwisestockscount #Saving the market cap list
		context['indexes'] = indexes #Saving the indexes
		context['selectedIndex'] = selectedIndex   #Displaying the selected index to user
		symbols_format ={'India':['₹','Cr'],'US':['$','M']}
		context['symbol'] = symbols_format[country][0]
		context['format'] = symbols_format[country][1]
		return context


class FundaMentalWizardFilteringView(LoginRequiredMixin,SubscriptionValidationMixin,View):

	@method_decorator(csrf_exempt)
	def dispatch(self, *args, **kwargs):
		return super(FundaMentalWizardFilteringView, self).dispatch(*args, **kwargs)

	def get(self, request, *args, **kwargs):
		result_companies = []
		context = dict()
		context['resMsg1'] =  result_companies
		context['resMsg2'] = str(len(result_companies))+" stocks matched to your preferences."
		return HttpResponse(json.dumps(context), content_type='application/json')

	def post(self, request, *args, **kwargs):
		country=self.request.session['country']
		filters = request.POST.get('fundamentalfilters') # Taking the filters from the client POST
		fundamentalFilters = json.loads(filters) # Deserializing the data 
		request.session['fundamentalfilters'] = fundamentalFilters
		stocks = list() # Declaring the empty list to store index wise tickers
		selectedIndex = ' '
		try:
			stocks = request.session['wizardIndexStocks_s0'] #Taking the stocks from session
			selectedIndex = request.session['wizardindex']
		except:
			pass
		if len(fundamentalFilters) == 0:  #select all condition(skip)
			fundamentalStocksDF = getWizardTableDataFromCacheBuilder(country,selectedIndex,stocks)
			try:
				stocks = list(fundamentalStocksDF['Factset_Ticker'].unique()) #Taking the unique stocks from the data
			except:
				pass
			request.session['wizardIndexStocks_s1'] = stocks # saving to session
			result_companies =[] #Empty list decalration
			context = dict() #Context dict declaration
			fundamentalStocksDF=fundamentalStocksDF.fillna(0)
			for ind in fundamentalStocksDF.index: #Saving the data to list 
				temp = {'fs_name': fundamentalStocksDF['Company'][ind],'gics':fundamentalStocksDF['gics'][ind],'market_cap_category' : fundamentalStocksDF['Market_Cap_Category'][ind],
						'market_cap': fundamentalStocksDF['Market Cap'][ind], 'price' : fundamentalStocksDF['Current_Price'][ind]}
				result_companies.append(temp)
			stockcount = fundamentalStocksDF.groupby(['gics']).size().reset_index(name='count') # Sector wise count
			marketcapstocks = fundamentalStocksDF.groupby(['Market_Cap_Category']).size().reset_index(name='count') # Market cap wise count
			sectorwisestockscount = [[row['gics'], row['count']] for index, row in stockcount.iterrows() if row['gics']] # Converting the o/p to list
			marketcapwisestockscount = [[row['Market_Cap_Category'], row['count']] for index, row in marketcapstocks.iterrows() if row['Market_Cap_Category']] #Converting th o/p to list
			context['resMsg1'] = result_companies # Saving the data to context
			context['resMsg2'] = str(len(result_companies))+" stocks matched to your preferences."
			context['resMsg3'] =  sectorwisestockscount
			context['resMsg4'] =  marketcapwisestockscount
			return HttpResponse(json.dumps(context), content_type='application/json') #Serializing the data 
		else: #Some of the filters applied could be default or customized
			try:
				pscore,divyield,roe=0,0,0 # Declaring the filters
				for ffilter in fundamentalFilters:
					if 'Piotroski_Score' in ffilter:
						pscore = ffilter['Piotroski_Score']
					if 'Div_Yield' in ffilter:
						divyield = ffilter['Div_Yield']
					if 'ROE' in ffilter:
						roe = ffilter['ROE']
				fundamentalStocksDF = getWizardTableDataFromCacheBuilder(country,selectedIndex,stocks)
				fundamentalStocksDF=fundamentalStocksDF[fundamentalStocksDF['Factset_Ticker'].isin(stocks)]
				if pscore:
					fundamentalStocksDF=fundamentalStocksDF[fundamentalStocksDF['Piotroski_Score']>pscore]
				if divyield:
					fundamentalStocksDF=fundamentalStocksDF[fundamentalStocksDF['Div_Yield']>divyield]
				if roe:
					fundamentalStocksDF=fundamentalStocksDF[fundamentalStocksDF['ROE']>roe]
				try:
					finalstocks = list(fundamentalStocksDF['Factset_Ticker'].unique()) #Taking the list of stocks from fundamental filtering
				except:
					pass
				request.session['wizardIndexStocks_s1'] = finalstocks  #Saving the selected stocks into current active session
				result_companies =[] # Declaring the empty list for o/p
				context = dict() # Declaring the empty dict for o/p
				fundamentalStocksDF=fundamentalStocksDF.fillna(0)
				secTables = {'India':SecurityMaster,'US':UsSecurityMaster}
				security_model=secTables[country]
				sec_code_data = pd.DataFrame(list(security_model.objects.filter(fs_ticker__in=finalstocks).values("fs_ticker","security_code")))
				sec_code_data.columns=['Factset_Ticker','Security_Code']
				fundamentalStocksDF = pd.merge(fundamentalStocksDF,sec_code_data,on='Factset_Ticker',how='inner')
				for ind in fundamentalStocksDF.index:
					temp = {'fs_name': fundamentalStocksDF['Company'][ind] + " (" + fundamentalStocksDF['Security_Code'][ind] + ")",'gics':fundamentalStocksDF['gics'][ind],'market_cap_category' : fundamentalStocksDF['Market_Cap_Category'][ind],
							'market_cap': fundamentalStocksDF['Market Cap'][ind], 'price' : fundamentalStocksDF['Current_Price'][ind]}
					result_companies.append(temp)
				stockcount = fundamentalStocksDF.groupby(['gics']).size().reset_index(name='count')
				marketcapstocks = fundamentalStocksDF.groupby(['Market_Cap_Category']).size().reset_index(name='count')
				sectorwisestockscount = [[row['gics'], row['count']] for index, row in stockcount.iterrows() if row['gics']]
				marketcapwisestockscount = [[row['Market_Cap_Category'], row['count']] for index, row in marketcapstocks.iterrows() if row['Market_Cap_Category']]
				context['resMsg1'] = result_companies
				context['resMsg2'] = str(len(result_companies))+" stocks matched to your preferences."
				context['resMsg3'] =  sectorwisestockscount
				context['resMsg4'] =  marketcapwisestockscount
				return HttpResponse(json.dumps(context), content_type='application/json')
			except Exception as e:
				print(e)

class TechnicalWizardFilteringView(LoginRequiredMixin,SubscriptionValidationMixin,View):
	@method_decorator(csrf_exempt)
	def dispatch(self, *args, **kwargs):
		return super(TechnicalWizardFilteringView, self).dispatch(*args, **kwargs)

	def get(self, request, *args, **kwargs):
		result_companies = []
		context = dict()
		context['resMsg1'] =  result_companies
		context['resMsg2'] = str(len(result_companies))+" stocks matched to your preferences."
		return HttpResponse(json.dumps(context), content_type='application/json')

	def post(self, request, *args, **kwargs):
		wizardTableObject=Wizardtable
		filters = request.POST.get('technicalfilters')
		technicalFilters = json.loads(filters)
		stocks = list()
		selectedIndex = ''
		country = self.request.session['country']
		try:
			stocks = request.session['wizardIndexStocks_s1']
			selectedIndex = request.session['wizardindex']
		except:
			pass
		if len(technicalFilters) == 0:
			technicalStocksDF = getWizardTableDataFromCacheBuilder(country,selectedIndex,stocks)
		else:
			rsi , _52wh =0,0
			for ffilter in technicalFilters:
				if 'RSI' in ffilter:
					rsi = float(ffilter['RSI'])
				if '52wh' in ffilter:
					_52wh = ffilter['52wh']
			technicalStocksDF = getWizardTableDataFromCacheBuilder(country,selectedIndex,stocks)
			technicalStocksDF=technicalStocksDF.fillna(0)

			if rsi:
				technicalStocksDF =technicalStocksDF[technicalStocksDF['RSI']>rsi]
			if _52wh:
				technicalStocksDF =technicalStocksDF[technicalStocksDF['Current_Price']> technicalStocksDF['52_Week_High'] * _52wh]
		try:
			techstocks = list(technicalStocksDF['Factset_Ticker'].unique())
		except:
			pass
		request.session['wizardIndexStocks_s2'] = techstocks
		result_companies = []
		context = dict()
		secTables = {'India':SecurityMaster,'US':UsSecurityMaster}
		sec_code_data = pd.DataFrame(list(secTables[country].objects.filter(fs_ticker__in=list(technicalStocksDF['Factset_Ticker'])).values("fs_ticker","security_code")))
		sec_code_data.columns=['Factset_Ticker','Security_Code']
		technicalStocksDF = pd.merge(technicalStocksDF,sec_code_data,on='Factset_Ticker',how='inner')

		for ind in technicalStocksDF.index:
			temp = {'fs_name': technicalStocksDF['Company'][ind] + " (" + technicalStocksDF['Security_Code'][ind] + ")",'gics':technicalStocksDF['gics'][ind],'market_cap_category' : technicalStocksDF['Market_Cap_Category'][ind],
					'market_cap': technicalStocksDF['Market Cap'][ind], 'price' : technicalStocksDF['Current_Price'][ind]}
			result_companies.append(temp)
		stockcount = technicalStocksDF.groupby(['gics']).size().reset_index(name='count')
		marketcapstocks = technicalStocksDF.groupby(['Market_Cap_Category']).size().reset_index(name='count')
		sectorwisestockscount = [[row['gics'], row['count']] for index, row in stockcount.iterrows() if row['gics']]
		marketcapwisestockscount = [[row['Market_Cap_Category'], row['count']] for index, row in marketcapstocks.iterrows() if row['Market_Cap_Category']]
		context['resMsg1'] =  result_companies
		context['resMsg2'] = str(len(result_companies))+" stocks matched to your preferences."
		context['resMsg3'] =  sectorwisestockscount
		context['resMsg4'] =  marketcapwisestockscount
		return HttpResponse(json.dumps(context), content_type='application/json')
		
class AIMLWizardFilteringView(LoginRequiredMixin,SubscriptionValidationMixin,View):
	
	@method_decorator(csrf_exempt)
	def dispatch(self, *args, **kwargs):
		return super(AIMLWizardFilteringView, self).dispatch(*args, **kwargs)

	def get(self, request, *args, **kwargs):
		result_companies = list()
		context = dict()
		context['resMsg1'] =  result_companies
		context['resMsg2'] = str(len(result_companies))+" stocks matched to your preferences."
		return HttpResponse(json.dumps(context), content_type='application/json')

	def post(self, request, *args, **kwargs):
		country=self.request.session['country']
		gicsTableObject = Gicswisecompaniesclassification
		gicsclassificationModel = GicsClassification
		if country=='US':
			gicsTableObject =  UsGicswisecompaniesclassification
			gicsclassificationModel = UsGicsClassification
		filters = request.POST.get('aimlfilters')
		aimlFilters = json.loads(filters)
		techstocks = list()
		selectedIndex = ''
		try:
			techstocks = set(request.session['wizardIndexStocks_s2'])
			selectedIndex = request.session['wizardindex'] 
		except:
			pass
		bottomStocks,midStocks,topStocks,aimlstocks = set(),set(),set(),set()
		if len(aimlFilters) == 0:
			aimlStocksDF=getWizardTableDataFromCacheBuilder(country,selectedIndex,techstocks)
		else:
			gicscomp = gicsclassificationModel.objects.values('company','fs_ticker','tier','gics','description','rating')
			gicscomps = pd.DataFrame(list(gicscomp))
			gicscomps['count']=gicscomps.groupby(['gics','description','rating','tier']).cumcount()
			gicscomps.reset_index(inplace=True,drop=True)
			gicscomps = (gicscomps.set_index(['gics','rating','description','count'])
			.pivot(columns="tier")['fs_ticker']
			.reset_index()
			.rename_axis(None, axis=1)
			)
			for ffilter in aimlFilters:
				if ffilter['bottomtier']:
					categoryStocks = gicscomps[gicscomps['Bottom Tier'].isin(techstocks)]['Bottom Tier'].values.tolist()
					for stock in categoryStocks:
							if stock not in bottomStocks:
								bottomStocks.add(stock)
				if ffilter['midtier']:
					categoryStocks1 = gicscomps[gicscomps['Bottom Tier'].isin(techstocks)]['Bottom Tier'].values.tolist()
					categoryStocks2 = gicscomps[gicscomps['Top Tier'].isin(techstocks)]['Top Tier'].values.tolist()
					for stock in categoryStocks1:
						if stock not in midStocks:
							midStocks.add(stock)
					for stock in categoryStocks2:
						if stock not in midStocks:
							midStocks.add(stock)
					midStocks = techstocks - midStocks
				if ffilter['toptier']:
					categoryStocks = gicscomps[gicscomps['Top Tier'].isin(techstocks)]['Top Tier'].values.tolist()
					for stock in categoryStocks:
							if stock not in topStocks:
								topStocks.add(stock)
			aimlstocks.update(bottomStocks)
			aimlstocks.update(midStocks)
			aimlstocks.update(topStocks)
			aimlstocks = list(aimlstocks)
			aimlStocksDF = getWizardTableDataFromCacheBuilder(country,selectedIndex,aimlstocks)	
		try:
			finalstocks = list(aimlStocksDF['Factset_Ticker'].unique())
		except:
			pass
		result_companies = [] # declaring empty list for output
		context = dict()
		request.session['wizardIndexStocks_s3'] = finalstocks
		aimlStocksDF=aimlStocksDF.fillna(0)
		secTables = {'India':SecurityMaster,'US':UsSecurityMaster}
		sec_code_data = pd.DataFrame(list(secTables[country].objects.filter(fs_ticker__in=list(aimlStocksDF['Factset_Ticker'])).values("fs_ticker","security_code")))
		sec_code_data.columns=['Factset_Ticker','Security_Code']
		aimlStocksDF = pd.merge(aimlStocksDF,sec_code_data,on='Factset_Ticker',how='inner')
		for ind in aimlStocksDF.index:
			temp = {'fs_name': aimlStocksDF['Company'][ind]+" ("+aimlStocksDF['Security_Code'][ind]+")",'gics':aimlStocksDF['gics'][ind],'market_cap_category' : aimlStocksDF['Market_Cap_Category'][ind],
					'market_cap': aimlStocksDF['Market Cap'][ind], 'price' : aimlStocksDF['Current_Price'][ind]}
			result_companies.append(temp)
		stockcount = aimlStocksDF.groupby(['gics']).size().reset_index(name='count')
		marketcapstocks = aimlStocksDF.groupby(['Market_Cap_Category']).size().reset_index(name='count')
		sectorwisestockscount = [[row['gics'], row['count']] for index, row in stockcount.iterrows() if row['gics']]
		marketcapwisestockscount = [[row['Market_Cap_Category'], row['count']] for index, row in marketcapstocks.iterrows() if row['Market_Cap_Category']]
		context['resMsg1'] =  result_companies
		context['resMsg2'] = str(len(result_companies))+" stocks matched to your preferences."
		context['resMsg3'] =  sectorwisestockscount
		context['resMsg4'] =  marketcapwisestockscount
		return HttpResponse(json.dumps(context), content_type='application/json')
				

class WizardPortfolioSummaryView(LoginRequiredMixin,SubscriptionValidationMixin,View):
	
	@method_decorator(csrf_exempt)
	def dispatch(self, *args, **kwargs):
		return super(WizardPortfolioSummaryView, self).dispatch(*args, **kwargs)

	def get(self, request, *args, **kwargs):
		result_companies = list()
		context = dict()
		context['resMsg1'] =  result_companies
		context['resMsg2'] = str(len(result_companies))+" stocks matched to your preferences."
		return HttpResponse(json.dumps(context), content_type='application/json')

	def post(self, request, *args, **kwargs):
		country=self.request.session['country']
		aimlstocks = list()
		try:
			aimlstocks = request.session['wizardIndexStocks_s3']
		except:
			pass
		start_date=None
		if country=='India':
			current_date= UniversePricesMain.objects.all().aggregate(Max('date'))['date__max']
			start_date = UniversePricesMain.objects.filter(date__lte=current_date,date__gte=str(current_date.year)+'-01-01').all().aggregate(Min('date'))['date__min']
			universePrices = UniversePricesMain.objects.filter(date=start_date,factset_ticker__in=aimlstocks).values('factset_ticker','company','price')
		else:
			current_date= UsUniversePricesMain.objects.all().aggregate(Max('date'))['date__max']
			start_date = UsUniversePricesMain.objects.filter(date__lte=current_date,date__gte=str(current_date.year)+'-01-01').all().aggregate(Min('date'))['date__min']
			universePrices = UsUniversePricesMain.objects.filter(date=start_date,factset_ticker__in=aimlstocks).values('factset_ticker','company','price')
			
		Stock_Prices = pd.DataFrame(list(universePrices))
		Stock_Prices.columns = ['Factset_Ticker','Company','Price']
		Stock_Data = pd.DataFrame(list(aimlstocks))
		Stock_Data.columns=['Factset_Ticker']
		Stock_Data = pd.merge(Stock_Data,Stock_Prices,how="inner", on="Factset_Ticker")
		country_quant={'India':200000,'US':5000}
		Stock_Data.dropna(inplace=True)
		Stock_Data['Quantity'] = (country_quant[country]/Stock_Data['Price']).astype(int)
		stock = list(Stock_Data["Factset_Ticker"])
		symbols={'India':'₹','US':'$'}
		symbol=symbols[country]
		portfolio_name = 'wizardportfolio'
		requiredTableObjects={'India':[UniversePricesMain,SecurityMaster,MarketIndex,RiskFreeRate,'Universe_Prices_Main','Market_Index'],
								'US':[UsUniversePricesMain,UsSecurityMaster,UsMarketIndex,UsRiskFreeRate,'US_Universe_Prices_Main','US_Market_Index']}

		universe_model,security_model,market_model,risk_free_rate_model =requiredTableObjects[country][0] ,requiredTableObjects[country][1] ,requiredTableObjects[country][2] ,requiredTableObjects[country][3]
		
		pricesTable=requiredTableObjects[country][4]
		markettable=requiredTableObjects[country][5]

		## Equal Weighted Portfolio
		risk_free_rates = risk_free_rate_model.objects.filter(date__range=(start_date, current_date)).order_by('date').values('date','risk_free_rate')  #retrieving risk free data
		Risk_Free_Data = pd.DataFrame(list(risk_free_rates))
		Risk_Free_Data = Risk_Free_Data[['date','risk_free_rate']]
		Risk_Free_Data.columns = ['Date','Risk Free Rate']
		companies_query=', '.join('"{0}"'.format(w) for w in [stock[i].replace("'","''") for i in range (0,len(stock))] ).replace('"',"'")		
		conn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server}; SERVER='+SERVER+'; PORT='+PORT+'; DATABASE='+DATABASE+';UID='+UID+';PWD='+PWD+';')
		Daily_Returns =  pd.read_sql("select Company,Factset_Ticker,Date,Price,[Return] from " +  pricesTable + " where date between  '" +str(start_date) +"'  and '" +str(current_date) + "' and factset_ticker in (" + companies_query +")" ,conn)
		index_cumulative_data = pd.read_sql("select Date,Company,[Return] from " +markettable + " where date between  '" + str(start_date) +"'  and '" + str(current_date) + "' order by date",conn)
		conn.close()
		pnl_start_date=start_date
		cumulative_return,daily_returns,df=CalcsMain(country,start_date,pnl_start_date,current_date, request.user.username,Stock_Data,Daily_Returns,index_cumulative_data,portfolio_name,Risk_Free_Data,'YTD')
		table_data = df.values.tolist()
		del table_data[1]
		cumulative_return=cumulative_return.dropna().reset_index(drop=True)
		cumulative_return['Trade_Dt'] = cumulative_return['Trade_Dt'].dt.date.astype(str).str[0:11]
		cumulative_return.rename(columns = {'PNL':'Portfolio'}, inplace = True)
		output=pd.DataFrame(columns=['name','lineWidth','color','data'])
		final_dict = {'names':list(cumulative_return.columns)[1:],'lineWidth':[4,2,2,2,2],'color':['#6610F2','#28a745','#02BCD4','#ffc107','#dc3545']}
		for i in range(0,5):
			output.loc[i]=[final_dict['names'][i],final_dict['lineWidth'][i],final_dict['color'][i],[[str(date.today().year-1)+'-12-31',0]]+cumulative_return[['Trade_Dt',final_dict['names'][i]]].values.tolist()]						
		output_cumulative_return=output.to_dict(orient='records')
		### Returns_by_Stock_Sector
		Daily_Returns = Daily_Returns[['Company','Factset_Ticker','Price' , 'Date']]
		gics_mcap= security_model.objects.filter(fs_ticker__in=stock).values('fs_name','fs_ticker','security_code','gics','market_cap_category')
		gics_mcap_data=pd.DataFrame(list(gics_mcap))
		gics_mcap_data.columns=['Company','Factset_Ticker','Security_Code','GICS','Market_Cap_Category']
		gics_data=gics_mcap_data
		sector_data = gics_data[['GICS']]
		Stock_Data=pd.merge(Stock_Data,gics_data,on=['Factset_Ticker','Company'],how='inner')

		df,sector_wise = New_CurrentPositionsMain(start_date,current_date,Daily_Returns,Stock_Data,gics_data,sector_data,'YTD')
		sector_wise=sector_wise.dropna()
		total_exposure=df['Current Exposure'].sum()
		df=df.sort_values(by=['Current Exposure'],ascending=False).reset_index(drop=True)
		df=df.head(10)
		df=df[['Stock','Factset_Ticker',"Current Exposure",'YTD_PNL','YTD_Return']]
		sec_code_data=gics_mcap_data[['Factset_Ticker','Security_Code']]
		df = pd.merge(df,sec_code_data,on='Factset_Ticker',how='inner')
		df['Stock'] = df['Stock'] + " (" + df['Security_Code'] + ")"
		del df['Security_Code']		
		del df['Factset_Ticker']
		sector_categories=sector_wise['GICS'].values.tolist()
		returns_by_sector=df.values.tolist()
		Stock_DataNew = Stock_Data[['Company','Factset_Ticker','Quantity']]

		prices_data= Daily_Returns[Daily_Returns['Date']==current_date].reset_index()
		prices_data = prices_data[['Company','Factset_Ticker','Price']]
		gics_mcap_data.columns=['fs_name','fs_ticker','security_code','gics','market_cap_category']
				###Investment amount by sector and marketcap
		top3_stocks_by_exposure,stock_wise_exposure,gics_wise_exposure,top_sector_by_exposure,market_cap_wise_exposure = exposureDashboard(str(request.user.username),country,Stock_DataNew,prices_data,gics_mcap_data)
		data_stock_wise=stock_wise_exposure['Exposure%'].tolist()
		stocks=stock_wise_exposure['Company'].tolist()
		count = 0
		investment_output = []
		investment_output2 = []
		for i in range (0,len(gics_wise_exposure)):      
			x={"name":gics_wise_exposure['GICS'][i]+' ('+str(int(round(gics_wise_exposure['Exposure%'][i])))+'%)',"y":gics_wise_exposure['Exposure%'][i],"Exposure":gics_wise_exposure['Exposure'][i],'colorValue':count,"new_name":gics_wise_exposure['GICS'][i]}
			investment_output.append(x)
		for i in range (0,len(market_cap_wise_exposure)):
			val={"name":market_cap_wise_exposure['Market Cap Category'][i]+' ('+str(int(round(market_cap_wise_exposure['Exposure%'][i])))+'%)',"y":market_cap_wise_exposure['Exposure%'][i],"Exposure":market_cap_wise_exposure['Exposure'][i],'colorValue':count,"new_name":market_cap_wise_exposure['Market Cap Category'][i]}        
			investment_output2.append(val)
		top=top_sector_by_exposure[['GICS','Exposure%']].values.tolist()
		top3=top3_stocks_by_exposure.values.tolist()
		total_3=round(top3_stocks_by_exposure['Exposure%'].sum(),1)
		context = dict()
		context['output_cumulative_return'] = output_cumulative_return
		context['table_data'] = table_data
		context['top'] = top
		context['top3'] = top3
		context['data_stock_wise'] = data_stock_wise
		context['stocks'] = stocks
		context['investment_output'] = investment_output
		context['investment_output2'] = investment_output2
		context['total_3'] = total_3
		context['returns_by_sector'] = returns_by_sector
		context['user'] = request.user.username
		return HttpResponse(json.dumps(context), content_type='application/json')

class SaveWizardPortfolioView(LoginRequiredMixin,SubscriptionValidationMixin,View):
	result_companies = list()
	context = dict()
	@method_decorator(csrf_exempt)
	def dispatch(self, *args, **kwargs):
		return super(SaveWizardPortfolioView, self).dispatch(*args, **kwargs)

	def get(self, request, *args, **kwargs):
		self.context['res_Status'] = False
		self.context['res_Msg'] = 'Invalid input, try again!'
		return HttpResponse(json.dumps(self.context), content_type='application/json')

	def post(self, request, *args, **kwargs):
		try:
			self.request.session['country']
		except:
			self.request.session['country'] = 'India'
		country=self.request.session['country']
		portfolio_name = request.POST.get('portfolioName')  # Taking the portfolio name from the client POST request
		aimlstocks = list()
		try:
			aimlstocks = request.session['wizardIndexStocks_s3']
		except:
			pass
		filterstocks =', '.join('"{0}"'.format(w) for w in [aimlstocks[i].replace("'","''") for i in range (0,len(aimlstocks))] ).replace('"',"'")
		if country=='India':
			query = '''select a.Factset_Ticker,a.Price from Universe_Prices_Main a join (select Factset_Ticker,min(date) Date 
				from Universe_Prices_Main where date>='2019-01-01' and Factset_Ticker in ('''+filterstocks+''') group by Factset_Ticker) b on a.Factset_Ticker=b.Factset_Ticker
				and a.date=b.Date'''
		else:
			query = '''select  a.Factset_Ticker,a.Price from US_Universe_Prices_Main a join (select Factset_Ticker,min(date) Date 
				from Us_Universe_Prices_Main where date>='2019-01-01' and Factset_Ticker in ('''+filterstocks+''') group by Factset_Ticker) b on a.Factset_Ticker=b.Factset_Ticker
				and a.date=b.Date'''
		conn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server}; SERVER='+SERVER+'; PORT='+PORT+'; DATABASE='+DATABASE+';UID='+UID+';PWD='+PWD+';') 
		Stock_Prices = pd.read_sql(query,conn)
		conn.close()
		Stock_Prices.columns = ['Ticker','Price']
		Stock_Data = pd.DataFrame(list(aimlstocks))
		Stock_Data.columns=['Ticker']
		Stock_Data = pd.merge(Stock_Data,Stock_Prices,how="inner", on="Ticker")
		if country=='India':
			Stock_Data['Quantity']= (200000/Stock_Data['Price']).astype(int)
			sectors = SecurityMaster.objects.filter(flag='yes',fs_ticker__in=aimlstocks).values('fs_name','gics','fs_ticker')
		else:
			Stock_Data['Quantity']= (5000/Stock_Data['Price']).astype(int)
			sectors = UsSecurityMaster.objects.filter(fs_ticker__in=aimlstocks).values('fs_name','gics','fs_ticker')
		Sector_Data = pd.DataFrame(list(sectors))
		Sector_Data.columns=['Company','Sector','Ticker']
		Stock_Data = pd.merge(Stock_Data,Sector_Data,how="inner", on="Ticker")
		if not Stock_Data.empty:  # Checking if newly added stock data is exists or not
			try:
				if CustomerPortfolioDetails.objects.filter(customer_name=request.user.username,country=request.session['country'], portfolio_name=portfolio_name).count() == 0: # Checking if the portfolio name already exists or not
					for index, row in Stock_Data.iterrows():     # Iterating the stocks (if more than one stock is adding at a time)
						if row['Quantity']:
							if CustomerPortfolioDetails.objects.filter(customer_name=request.user.username,country=request.session['country'], portfolio_name=portfolio_name,company_name=row['Company']).count() == 0: # To avoid the duplicates in portfolio database
								CustomerPortfolioDetails(customer_name=request.user.username, portfolio_name=portfolio_name, company_name=row['Company'],
								quantity=int(row['Quantity']), price=row['Price'],sector_name=row['Sector'],country=request.session['country'], fs_ticker=row['Ticker']).save()  # Saving the stock
						else:
							print("Invalid quantity!!!")
					CustomerPortfolioLog(customer_name=request.user.username,portfolio_name=portfolio_name,last_action='portfolio_created',created_at=timezone.now(),status='active').save()
					self.context['res_Status'] = True
					self.context['portfolio'] = portfolio_name
					self.context['username'] = request.user.username
					createCacheForNewPortfolio(request.user.username, request.session['country'], portfolio_name)
					self.context['res_Msg'] = 'Your Portfolio has been saved successfully!'
					return HttpResponse(json.dumps(self.context), content_type='application/json')
				else:  # If the portfolio name already exists, raise exception
					raise PortfolioExistsError('Portfolio name is already taken!')
			# Handling the exceptions
			except PortfolioExistsError as p_error:  
				self.context['res_Status'] = False
				self.context['portfolio'] = portfolio_name
				self.context['res_Msg'] = p_error.msg
				return HttpResponse(json.dumps(self.context), content_type='application/json')
			except Exception as e:
				print(e)
				self.context['res_Status'] = False
				self.context['portfolio'] = portfolio_name
				self.context['res_Msg'] = 'Something went wrong, try again!'
				return HttpResponse(json.dumps(self.context), content_type='application/json')
		else:
			self.context['res_Status'] = False
			self.context['portfolio'] = portfolio_name
			self.context['res_Msg'] = 'Something went wrong, try again!'
			return HttpResponse(json.dumps(self.context), content_type='application/json')
			

class OffStreetBeatView(TemplateView):
	template_name = 'OffStreetBeat.html'
	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['beats'] = OffBeat.objects.live().order_by("-posted_at")
		return context

class RelativeStrengthSectorsView(TemplateView):
	def get_template_names(self):
		templates = {'India':'RelativeStrength_INDIA.html','US':'RelativeStrength_USA.html'}
		template_name = templates[self.request.session['country']]
		return template_name
		
	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		try:
			country = self.request.session['country']
		except:
			self.request.session['country'] = 'India'
		country = self.request.session['country']
		companies_india = ['NIFTY Midcap 100', 'NIFTY SmallCap 100','NIFTY Auto', 'NIFTY Bank', 'NIFTY Energy',  'NIFTY Financial Services','NIFTY FMCG','NIFTY Infra', 'NIFTY IT','NIFTY Media','NIFTY Metal','NIFTY Pharma','NIFTY Private Bank', 'NIFTY PSU Bank' ,'NIFTY Realty']
		companies_us = ['Communication Services Select Sector SPDR Fund','Consumer Discretionary Select Sector SPDR Fund','Consumer Staples Select Sector SPDR Fund','Energy Select Sector SPDR Fund', 'Financial Select Sector SPDR Fund',
		'Health Care Select Sector SPDR Fund','Industrial Select Sector SPDR Fund','Materials Select Sector SPDR Fund', 'Technology Select Sector SPDR Fund','The Real Estate Select Sector SPDR Fund',
		'Utilities Select Sector SPDR Fund' ]
		requiredTableObjects = {'India' :[RelativeStrengthSectors,companies_india,'ticker_vs_nifty50','ticker'], 'US':[UsRelativeStrengthSectors,companies_us,'ticker_vs_s_p500','company']}
		df = pd.DataFrame(list(requiredTableObjects[country][0].objects.all().values()))
		companies = requiredTableObjects[country][1]
		dynamic_column = requiredTableObjects[country][2]
		ticker_col = requiredTableObjects[country][3]
		output,avg,pos_1sd,pos_2sd,pos_3sd,neg_1sd,neg_2sd,neg_3sd,minimum,maximum=[],[],[],[],[],[],[],[],[],[]
		for i in range (0,len(companies)):
			temp_df=df[df[ticker_col]==companies[i]].sort_values(by=['date'], ascending=True)
			temp_df['date']=temp_df['date'].astype(str)
			output.append({"name":companies[i],"data":temp_df[['date',dynamic_column]].values.tolist(),'color':'orange','lineWidth':2,'zIndex':9})
			avg.extend(list(temp_df['average'].unique()))
			pos_1sd.extend(list(temp_df['number_1sd'].unique()))
			pos_2sd.extend(list(temp_df['number_2sd'].unique()))
			pos_3sd.extend(list(temp_df['number_3sd'].unique()))
			neg_1sd.extend(list(temp_df['field_1sd'].unique()))
			neg_2sd.extend(list(temp_df['field_2sd'].unique()))
			neg_3sd.extend(list(temp_df['field_3sd'].unique()))
			if temp_df[dynamic_column].max()>temp_df['number_3sd'].max():
				maximum.append(temp_df[dynamic_column].max())
			else:
				maximum.append(temp_df['number_3sd'].max())
			if temp_df[dynamic_column].min()<temp_df['field_3sd'].min():
				minimum.append(temp_df[dynamic_column].min())
			else:
				minimum.append(temp_df['field_3sd'].min())
		context={'output':output,'average':avg,'pos_1sd':pos_1sd,'pos_2sd':pos_2sd,'pos_3sd':pos_3sd,'neg_1sd':neg_1sd,'neg_2sd':neg_2sd,'neg_3sd':neg_3sd,'title':companies,'minimum':minimum,'maximum':maximum}
		return context


class Trailing12MPEView(TemplateView):
	template_name = 'Trailing12MPE.html'
		
	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		try:
			country = self.request.session['country']
		except:
			self.request.session['country'] = 'India'
		country = self.request.session['country']
		if(self.request.session['country']=='US'):
			sp_pe_df1 = pd.DataFrame(list(Snp500TtmPriceToEarnings.objects.all().values())).sort_values(by=['date'], ascending=True)
			sp_pe_df1['date'] = sp_pe_df1['date'].astype(str)
			output_line={"name":"Trailing 12M PE of S&P 500","data":sp_pe_df1[['date','p_e']].dropna().values.tolist(),'color':'orange','lineWidth':2,'zIndex':9}
			average=sp_pe_df1['average'].unique()
			pos_1sd=sp_pe_df1['number_1sd'].unique()
			pos_2sd=sp_pe_df1['number_2sd'].unique()
			pos_3sd=sp_pe_df1['number_3sd'].unique()
			neg_1sd=sp_pe_df1['field_1sd'].unique()
			neg_2sd=sp_pe_df1['field_2sd'].unique()
			neg_3sd=sp_pe_df1['field_3sd'].unique()
			maximum=sp_pe_df1['number_3sd'].max()
			minimum=sp_pe_df1['field_3sd'].min()
			if sp_pe_df1['p_e'].max()>sp_pe_df1['number_3sd'].max():
				maximum=sp_pe_df1['p_e'].max()
			if sp_pe_df1['p_e'].min()<sp_pe_df1['field_3sd'].min():
				minimum=sp_pe_df1['p_e'].min()
			context={'output':output_line,'average':average,'pos_1sd':pos_1sd,'pos_2sd':pos_2sd,'pos_3sd':pos_3sd,'neg_1sd':neg_1sd,'neg_2sd':neg_2sd,'neg_3sd':neg_3sd,'minimum':minimum,'maximum':maximum}
		else:
			vix_df1 = pd.DataFrame(list(IndiaVix.objects.all().values())).sort_values(by=['date'], ascending=True)
			vix_df1['date'] = vix_df1['date'].astype(str)
			output_line={"name":"India Vix","data":vix_df1[['date','vix']].dropna().values.tolist(),'color':'orange','lineWidth':2,'zIndex':9}
			average=vix_df1['average'].unique()
			pos_1sd=vix_df1['number_1sd'].unique()
			pos_2sd=vix_df1['number_2sd'].unique()
			pos_3sd=vix_df1['number_3sd'].unique()
			neg_1sd=vix_df1['field_1sd'].unique()
			neg_2sd=vix_df1['field_2sd'].unique()
			neg_3sd=vix_df1['field_3sd'].unique()
			maximum=vix_df1['number_3sd'].max()
			minimum=vix_df1['field_3sd'].min()
			if vix_df1['vix'].max()>vix_df1['number_3sd'].max():
				maximum=vix_df1['vix'].max()
			if vix_df1['vix'].min()<vix_df1['field_3sd'].min():
				minimum=vix_df1['vix'].min()
			context={'output':output_line,'average':average,'pos_1sd':pos_1sd,'pos_2sd':pos_2sd,'pos_3sd':pos_3sd,'neg_1sd':neg_1sd,'neg_2sd':neg_2sd,'neg_3sd':neg_3sd,'minimum':minimum,'maximum':maximum}
		return context



class BlogsView(LoginRequiredMixin,SubscriptionValidationMixin,TemplateView):
	template_name = 'BlogsHome.html'
	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['documents'] = NewBlogs.objects.live().order_by('-posted_at')
		return context