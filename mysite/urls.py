
from django.contrib import admin
from django.urls import path, include
from django.conf.urls import handler404,handler400,handler403,handler500
from mysite.core import views
from django.contrib.auth.decorators import login_required
from wagtail.admin import urls as wagtailadmin_urls
from wagtail.core import urls as wagtail_urls
from wagtail.documents import urls as wagtaildocs_urls
from django.conf import settings
from django.conf.urls.static import static
from mysite.home import cmsviews
from mysite.core import SubscriptionAdmin as subviews
from mysite.core import Subscriptions as payment
from mysite.core import utils as utils
from django.contrib.sitemaps.views import sitemap
from mysite.core.sitemaps import StaticViewSitemap

sitemaps = {
    'static': StaticViewSitemap
}

urlpatterns = [
    path('', views.HomePageView.as_view(), name='home'),
    path('fund-management/', views.FundManagement, name='FundManagement'),
    path('fundClientAccount/', views.fundClientAccount, name='fundClientAccount'),
    path('fund-manager-portal/', views.FundManagerLogin, name='FundManagerLogin'),
    path("trade-signals/", cmsviews.TradeSignalsView.as_view(), name='tradeSignals'),
    path("market-futures-options/", cmsviews.MarketFutureOptionsView.as_view(), name='marketFutureOptions'),
    path("ai-ml-signals/", cmsviews.AIMLSignalsView.as_view(), name='AIMLSignals'),
    path("model-portfolio/", cmsviews.ListofModelPortfoliosView.as_view(), name='ModelPortfolio'),
    path("signals-performance/", cmsviews.SignalsPerformanceView.as_view(), name='SignalsPerformance'),
    path("market/view/", cmsviews.MarketView.as_view(), name='macroview'),
    path("research/", cmsviews.CompanyWiseResearchView.as_view(), name='researchview'),
    path("off-beat-street/", cmsviews.OffStreetBeatView.as_view(), name='OffStreetBeat'),
    path('market/latest/news/updates/', cmsviews.MarketNewsBlogsView.as_view(), name='marketview'),
    path('cms-admin/', include(wagtailadmin_urls)),
    path('documents/', include(wagtaildocs_urls)),
    path('news/', include(wagtail_urls)),
    path('accounts/users/portfolios/', views.MyPortfolioView.as_view(), name='indexpage'),
    path('aboutus/', views.AboutUsView.as_view(), name='aboutus'),
    path('terms-of-use/', views.TermsofUseView.as_view(), name='termsofuse'),
    path('trust-security/',views.TrustAndSecurityView.as_view(), name='trustsecurity'),
    path('faq/',views.FAQView.as_view(), name='faq'),
    path('customer/contactus/', views.CustomerContactUSView.as_view(), name='customerContactUs'),
    path('customer/feedback/', views.customerFeedback, name='customerFeedback'),
    path('notifications/', views.ListAllNotificationsView.as_view(), name='allNotifications'),
    path('privacy-policy/', views.PrivacyPolicyView.as_view(), name='privacypolicy'),
    path('accounts/portfolio/wizard/creation/',cmsviews.PortfolioCreationWizardView.as_view(), name="portfolioCreationWizard"),
    path('portfolio-selection-bywizard/fundamentalwizard/', cmsviews.FundaMentalWizardFilteringView.as_view(), name="fundaMentalWizardFiltering"),
    path('portfolio-selection-bywizard/technicalwizard/', cmsviews.TechnicalWizardFilteringView.as_view(), name="technicalWizardFiltering"),
    path('portfolio-selection-bywizard/aimlwizard/', cmsviews.AIMLWizardFilteringView.as_view(), name="aiMLWizardFiltering"),
    path('portfolio-selection-bywizard/wizardportfolio-summary/', cmsviews.WizardPortfolioSummaryView.as_view(), name="wizardPortfolioSummary"),
    path('portfolio-selection-bywizard/wizardportfolio-creation/', cmsviews.SaveWizardPortfolioView.as_view(), name="saveWizardPortfolio"),
    path('model-portfolio-returns/mtd/ytd/', cmsviews.CalculateModelPortfolioReturnsView.as_view(), name="modelPortfolioReturns"),
    path('ourmarketview/', cmsviews.OurMarketView.as_view(), name="ourMarketView"),
    path('visualportfolio/saving/', views.saveNewVisualPortfolio, name="saveNewVisualPortfolio"),
    path('point/portfolio-construction/', views.portfolioconstruction, name='portfolioconstruction'),
    path('point/portfolio-analyzer/', views.portfolioanalyzer, name='portfolioanalyzer'),
    path('point/portfolio-optimization/', views.portfoliooptimization, name='portfoliooptimization'),
    path('point/mlpredection-modelling/', views.mlprediction, name='mlprediction'),
    path('stocks/getstocksbyname/', views.getstocklist, name="getstocklist"),
    #path('stocks/india/<str:cname>/', views.SearchByStockInfo, name="SearchByStockInfo"),
    path('stocks/details/', views.StockInfoFromStockList, name="StockInfoFromStockList"),
    path('accounts/users/myprofile/', views.MyInformationFormView.as_view(), name='myprofileview'),
    path('newsletter/subscription/', views.newsLetterSubscription, name='newsLetterSubscription'),
    # path('signup/', views.UserCreateView.as_view(), name='signup'),
    # path('accounts/', include('django.contrib.auth.urls')),
    path('point-admin/', admin.site.urls),
    # path('login/', views.UserLoginView.as_view(), name='login'),
    path('accounts/', include('allauth.urls'), name='social_auth'),
    # path('activate/<uidb>/<token>', views.activate, name='activate'),
    # path('accounts/my/profile/', views.CustomerProfileView.as_view(), name='user_profile'),
    # path('ajax/load-cities/', views.load_cities, name='ajax_load_cities'),
    path('export_user_portfolio/', views.export_user_portfolio, name='export_user_portfolio'),
    path('create-portfolio/my-portfolio/filter-by/', views.countryPreference, name='countryPreference'),
    path('create-portfolio-visually/', views.create_portfolio_visually, name='createPortfolioVisually'),
    path('create-portfolio/my-portfolio/', views.CreatePortfolioView.as_view(), name='createPortfolio'),
    path('fund_dashboard/', views.fund_dashboard, name="fund_dashboard"),
    path('fundmanager-trade-notications/', views.fundTradeNotifications, name="fundTradeNotifications"),
    path('fundmanager-trade-notications/customers-displaying-based-on-stocks/', views.hedgeAccountsDisplay, name="hedgeAccountsDisplay"),
    path('customer-trade-notications/', views.CustomerTradeNotifications, name="CustomerTradeNotifications"),
    # URL's for Portfolio page
    path('save_portfolio_details/', views.SavePortfolioData, name='SavePortfolioData'),
    path('save_portfolio_details/new/', views.saveNewPortfolio, name='saveNewPortfolio'),
    path('user/portfolio/stock_delete/', views.deleteSavedStockFromPortfolio, name='deleteSavedStockFromPortfolio'),
    path('user/portfolio/stock_update/', views.updateSavedStockFromPortfolio, name='updateSavedStockFromPortfolio'),
    path('user/portfolio_export/', views.exportStocksExcel, name="exportStocksExcel"),
    path('getstock_price/', views.GetStockPriceWithSectorView.as_view(), name='Getstockprice'),
    path('getstock/alerts/', views.ListOfStockNotificationsView.as_view(), name='getStockalerts'),
    path('deleteCustomerPortfolio/', views.deleteCustomerPortfolio, name='deleteCustomerPortfolio'),
    path('signals/overview/', views.PreLoginSignalsView.as_view(), name='prelogin_signals'),
    path('analytics/overview/', views.PreLoginProducts.as_view(), name='prelogin_products'),
    # URL's for dashboard charts
    path('Portfolio_Returns/', views.portfolio_returns, name="portfolio_returns"),
    path('MonthlyPerformance/', views.monthlyPerformance, name="monthlyPerformance"),
    path('Investment_amount_sector_mktcap/', views.investment_amount_sector_mktcap, name="investment_amount_sector_mktcap"),
    path('AnalystTargetPrice/', views.analystTargetPrice, name="analystTargetPrice"),
    path('risk_overview/', views.risk_overview, name="risk_overview"),
    path('factorInfo/', views.factorInfo, name="factorInfo"),
    path('investment_amount_factors/', views.investment_amount_factors, name="investment_amount_factors"),
    path('dashboards_summary/',views.dashboards_summary,name='dashboards_summary'),
    path('historical_multiples/', views.historical_multiples, name="historical_multiples"),
    path('MacroImpact/',views.macroimpact,name='macroimpact'),
    # path('PricevsMovingAverage/',views.pricevsMovingAverage,name='pricevsMovingAverage'),
    path('Volume_Volatility/',views.volume_volatility,name='volume_volatility'),
    ## Portfolio Optimization
    path('Efficient_Frontier/', views.efficient_frontier, name="efficient_frontier"),
    path('target_return_model/', views.target_return_model, name="target_return_model"),
    path('ResultsComparision/', views.resultsComparision, name="resultsComparision"),
    ## USED FOR SAVING THE EFFICIENT FRONTIER,TARGET RETURN MODEL, MONTECARLO PORTFOLIOS
    path('opt_saved_portfolio_eff/',views.EfficientFrontierOptimizedPortfolioSaveView.as_view(), name="opt_saved_portfolio_eff"),
    path('opt_saved_portfolio_vol/',views.TargetReturnModelOptimizedPortfolioSaveView.as_view(), name="opt_saved_portfolio_vol"),
    path('home/', views.homepage_new, name='homepage_new'),
    path('welcomepage/', views.WelcomePageView.as_view(), name='welcomepage'),
    path('import_portfolio/', views.import_portfolio, name='import_portfolio'),
    ## Chat Bot
    path('alerts/save/stock/pricemovements/', views.SaveCustomerPricemovementsView.as_view(), name="saveCustomerPricemovements"),
    path('alerts/save/stock/technical-indications/', views.SaveCustomerTechnicalIndicationsView.as_view(), name="saveCustomerTechnicalIndications"),
    # path('chatbot3/', views.chatbot3, name="chatbot3"),
    # path('chatbot/', views.chatbot, name="chatbot"),
    path('portfolio-changer/', views.portfolioChanger, name="portfolioChanger"),
    path('technicalIndicators/', views.technicalIndicators, name='technicalIndicators'),
    path('getFiiDiiDaily/', views.getFiiDiiDaily, name='getFiiDiiDaily'),
    # Urls for chat bot
    # path('chatbot/portfolio-returns/', views.BotPortfolioReturns, name='BotPortfolioReturns'),
    # path('chatbot/actual-vs-expected/', views.BotActualvsExpected, name='BotActualvsExpected'),
    # path('chatbot/monthly-cumulative-returns/', views.BotMonthlyCumulative, name='BotMonthlyCumulative'),
    # path('chatbot/exposure-sector-wise/', views.BotExposureSector, name='BotExposureSector'),
    # path('chatbot/target-price/', views.BotTargetPrice, name='BotTargetPrice'),
    # path('chatbot/risk-dashboard/', views.BotRiskDashboard, name='BotRiskDashboard'),
    # path('chatbot/factor-dashboard/', views.BotFactorDashboard, name='BotFactorDashboard'),
    # path('chatbot/fama-dashboard/', views.BotFamaDashboard, name='BotFamaDashboard'),
    # path('chatbot/risk-overview/', views.BotRiskOverview, name='BotRiskOverview'),
    # path('chatbot/history-data/', views.BotHistoricalMutiples, name='BotHistoricalMutiples'),
    # path('chatbot/macro-impact/', views.BotMacroImpact, name='BotMacroImpact'),
    # path('chatbot/moving-average/', views.BotMovingAverage, name='BotMovingAverage'),
    # path('chatbot/vol-volatility/', views.BotVolumeVolatility, name='BotVolumeVolatility'),
    # path('trades-for-execution/', views.trade, name='trade'),
    path('pricing-details/', payment.PricingDetailsPageView.as_view(), name='pricingDetails'),
    path('accounts/customer/subscription/free/register/', payment.CustomerFreeSubscriptionView.as_view(), name='customerFreeSubscription'),
    path('accounts/customer/subscription/premium/INR/register/', payment.CustomerINRSubscriptionView.as_view(), name='customerINRSubscription'),
    path('accounts/customer/subscription/premium/USD/register/', payment.CustomerUSDSubscriptionView.as_view(), name='customerUSDSubscription'),
    path('accounts/customer/subscription/free/active/', payment.SubscriptionSuccessView.as_view(), name='SubscriptionSuccess'),
    path('accounts/customer/details/verify/', views.CustomerVerification, name='CustomerVerification'),
    path('accounts/customer/subscription/paymentstatus/', payment.PaymentSignatureValidationView.as_view(), name = 'payment_status'),
    path('read/all-notifications/', views.ReadALLCustomerNotificationsView.as_view(), name='readALLNotifications'),
    path('check/all-notifications/', views.CheckALLCustomerNotificationsView.as_view(), name='checkALLNotifications'),
    path('read/notification/', views.ReadCustomerNotificationView.as_view(), name='readNotification'),
    path('market-index/performance/', cmsviews.MarketIndexPerformanceView.as_view(), name='marketIndexPerformance'),
    path('accounts/users/modelportfolio/save/', cmsviews.ModelPortfolioAddingView.as_view(), name='customermodelportfolioadding'),
    path('market/latest/news/updates/relative_strength_sectors/', cmsviews.RelativeStrengthSectorsView.as_view(), name='relativestrength'),
    path('market/latest/news/updates/trailing12mpe/', cmsviews.Trailing12MPEView.as_view(), name='trailing12mpe'),
    ## Mail Alert
    path('accounts/investor/risk-profile/', views.InvestorRiskPreferences.as_view(), name='InvestorRiskPreferences'),
    path('accounts/customer/stockalerts/triggered/state-waiting/', views.ListAllWaitingAlertsView.as_view(), name='listAllWaitingAlerts'),
    path('accounts/customer/stockalerts/bystock/', views.StockWiseWaitingAlertsView.as_view(), name='listAlertsByStock'),
    path('accounts/customer/stockalerts/state-waiting/update/', views.updateMyStockAlerts, name='updateMyStockAlerts'),
    path('accounts/customer/stockalerts/state-waiting/delete/', views.deleteMyStockAlerts, name='deleteMyStockAlerts'),
    path('accounts/customer/subscriptions/', payment.GetCustomerSubscriptionsLogView.as_view(), name='getCustomerSubscriptions'),
    #Subscription Admin 
    path('accounts/admin/user/subscriptions/home/', subviews.SubscriptionAdminHomePageView.as_view(), name='homeAdmin'),
    path('accounts/admin/send-emails/', subviews.SendEmailNotificationsAdminView.as_view(), name='sendEmailsAdmin'),
    path('accounts/admin/send-signals/ideas/', subviews.SendTradeIdeasSignalsAdminView.as_view(), name='sendIdeasAdmin'),
    path('accounts/admin/manage/users/', subviews.ManageSubscriptionsAdminView.as_view(), name='manageAccountsAdmin'),
    path('accounts/admin/all/users/', subviews.AllCustomersListView.as_view(), name='viewallusers'),
    path('accounts/admin/free/users/', subviews.FreeSubscribersListView.as_view(), name='viewfreeusers'),
    path('accounts/admin/gold/users/', subviews.GoldSubscribersListView.as_view(), name='viewgoldusers'),
    path('accounts/admin/diamond/users', subviews.DiamondSubscribersListView.as_view(), name='viewdiamondusers'),
    path('daily-email/', views.SendDailyMarketUpdateEmail.as_view(), name='daily_email'),
    path('screener', views.screener, name='screener'),
    path('account_setup/', utils.daemonCreation, name='daemonCreation'),
    path('countryChange', views.countryChange, name='countryChange'),
    # path('sitemap.xml/', views.SiteMap, name='sitemap'),
    path('sitemap.xml/', sitemap, {'sitemaps': sitemaps}),
    path("Blogs/", cmsviews.BlogsView.as_view(), name='blogsview'),
    path('treemap/', views.treemap, name='treemap'),
    path('DailyMarketUpdate/', views.DailyMarketUpdate, name='DailyMarketUpdate'),
    path('robots.txt/', include('robots.urls'))
]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
handler401 = 'mysite.core.views.handler400'
handler403 = 'mysite.core.views.handler403'
handler404 = 'mysite.core.views.handler404'
handler500 = 'mysite.core.views.handler500'
